/***************Version Mai 2018********************/
/**
 * Page NewDevis.php lance l'edition d'un devis
 * @returns {boolean} [[Description]]
 */
function newDevis_start() {
    var ids = document.getElementById('nameS').value;
    if (ids === '-Choisir-') {
        alert('veuillez selectionner une société');
        return;
    } else {
        document.getElementById('btn_next').disabled = true;
        document.getElementById('nameS').disabled = true;
        document.getElementById('dateDevis').disabled = true;
        var tr = confirm('Attention, Veuillez effectuer une saisie complête du devis.');
        if (tr) {
                loadProgressBar()
                axios({
                    method: 'post',
                    url: '/index.php/New_Devis/load_operateur',
                    data: 'idContexte='+ids

                })
                    .then(function (response) {
                    document.getElementById("devisOperateur").innerHTML = response.data;
                    console.log("Lancement Devis idsociete : " + ids);
                })
                    .catch(function (error) {
                    console.log(error.response);
                });
        }
    }
}
/**
 * Lance le choix d'operateur pour les mobiles
 * @returns {boolean} [[Description]]
 */
function newDevis_startMobile()
{
    var ids=document.getElementById('nameS').value;
    axios({
        method: 'post',
        url: '/index.php/New_Devis/load_operateur_mobile',
        data: 'idContexte='+ids
    })
        .then(function (response) {
        document.getElementById("devisOperateurMobile").innerHTML = response.data;
    })
        .catch(function (error) {
        console.log(error.response);
    });
}


/**
 * Permet de lancer un devis :
 * (MOBILE OU FIXE) va chercher le details du contexte et s'occupe de la mise en page
 * 
 * @param   {boolean} mobile 
 * @returns {boolean}  HTML liste des fixes/mobiles
 */
function launch_dev(mobile){
    var ids=document.getElementById('nameS').value;
    var id;
    var operateur;
    if(mobile == 0)operateur = document.getElementById('operateur');
    else operateur = document.getElementById('operateurMobile');
    var inputs = operateur.getElementsByTagName('input');
    for(var i = 0; i < inputs.length;i++){
        input=inputs[i];
        if (input.checked) {
            id = input.value;
        }
    }
    
    if(mobile == 0)
    {
        /*
        Chargement Contexte pour Fixe
        Recuperer valeur CheckBox = idOperateur pour Fixe
        */
        axios({
            method: 'post',
            url: '/index.php/New_Devis/load_contexte_fixe',
            data: 'idContexte='+ids+'&idOperateur='+id
        })
            .then(function (response) {
            document.getElementById("devisSite").innerHTML = response.data;
            console.log("Deviser Fixe contexte " + id + " idoperateur selectionne " + id);
        })
            .catch(function (error) {
            console.log(error.response);
        });
        
        
      }else{
        
          
          /*
            Chargement Contexte pour Mobile
            Recuperer valeur CheckBox = idOperateur pour Fixe
            */
            axios({
                method: 'post',
                url: '/index.php/New_Devis/load_contexte_mobile',
                data: 'idContexte='+ids+'&idOperateur='+id
            })
                .then(function (response) {
                document.getElementById("devisMobile").innerHTML = response.data;
                console.log("Deviser Mobile contexte " + id + " idoperateur selectionne " + id);
            })
                .catch(function (error) {
                console.log(error.response);
            });
        }
}

/**
 * Mise à jour des totaux : Calcul des totaux Duree HT et TTC à partir des totaux contexte
 * et projet. Regle de Calcul ((A+B+C)-(F+H+I))*P
 * Fonction onchange sur input value
 */
function majTotalDuree(){
            console.log("Mise à jour des valeurs de conclusion TOTAL PROJET ANNEE SUIVANTE");
            var A = parseFloat(document.getElementById("totalContAbo").value);
            var B = parseFloat(document.getElementById("InvestMat").value);
            var C = parseFloat(document.getElementById("LoyerMat").value);
            if(isNaN(A))A=0;
            if(isNaN(B))B=0;
            if(isNaN(C))C=0;
            if(document.getElementById("Projet1Total")==null)return;
            var F = parseFloat(document.getElementById("Projet1Total").value);
            var H = parseFloat(document.getElementById("Projet1PrestaZen2Tarif").value);
            var I = parseFloat(document.getElementById("Projet1PrestaZen3Tarif").value);
            if(isNaN(F))F=0;
            if(isNaN(H))H=0;
            if(isNaN(I))I=0;
            var P = parseFloat(document.getElementById("selDurEng").value);
            //alert(P);
            var tmp = ((A+B+C)-(F+H+I))*P;

            document.getElementById("totalDuree").value = tmp.toFixed(2);
            tmp *= 1.20;
            document.getElementById("totalDureeTTC").value = tmp.toFixed(2);

            var total = (F+H+I)*(P-12);
            total= total.toFixed(2);
            document.getElementById("totalProjetAS").value=total;
}
 /**
  * Mise à jour Total Projet Annuel 1ere Année
  */
 function majTotalProjet(){
     console.log(" Maj du Total du Projet : TOTAL PROJET ANNUEL HT  ")
     if(!document.getElementById("Projet1Total"))total=0
     else total = parseFloat(document.getElementById("Projet1Total").value);
     var presta = parseFloat(document.getElementById("Projet1PrestaZen1Tarif").value);
    if(isNaN(presta))presta = 0;
     var loyMaint = parseFloat(document.getElementById("Projet1PrestaZen2Tarif").value);
     if(isNaN(loyMaint))loyMaint = 0;
     var loyMat = parseFloat(document.getElementById("Projet1PrestaZen3Tarif").value)
    if(isNaN(loyMat))loyMat = 0;
     var matTel = parseFloat(document.getElementById("Projet1FMA").value);
     if(isNaN(matTel))matTel = 0;
     var frais = parseFloat(document.getElementById("Projet1InvestMat").value);
    if(isNaN(frais))frais = 0;
     var remComPro = parseFloat(document.getElementById("Projet1RemCom").value);
     if(isNaN(remComPro))remComPro = 0;
    total = (((total+loyMaint+loyMat)*12)+presta+matTel+frais)-remComPro;
    document.getElementById("totalProjet").value = total;

     majTotalDuree();
}
/**
 * Appel de la focntion projet (lanceur de pop-up) sur l'ajout de pdt supplementaire
 */
function add_pdtPlus(){
    var idSite = document.getElementById('site').value;
    projet(1,'-',0,10,0,idSite,afficheModalPdt,1);
}
/**
 * Supprimer l'ajout d'un produit supplementaire
 * Met à jour la somme totale Pri et Spot
 * @param {number} id   id choixPEO
 * @param {number} Pri  Pri du produit à supprimer
 * @param {number} sPot Spot du produit à supprimer
 * @param {number} som  : id + som permet de retrouver le tuple à supprimer ds la base
 */
function supprChxIndep(id,Pri,sPot,som){
     axios({
            method: 'post',
            url: '/index.php/Projet/supprChxPlus',
            data: 'idChx='+id+'&total='+som
        })
            .then(function (response) {
            if(response.data == 1){
                      alert("Produit Supprimé");
                      document.getElementById(id).innerHTML = "";
                      var PriRec = document.getElementById('priRec').innerHTML;
                      PriRec = parseFloat(PriRec);
                      document.getElementById('priRec').innerHTML = PriRec-Pri;
                      var spotRec = document.getElementById('spotRec').innerHTML;
                      spotRec = parseFloat(spotRec);
                      document.getElementById('spotRec').innerHTML = spotRec-sPot;
            }
        })
            .catch(function (error) {
            console.log(error.response);
        });
  }
/**
 * Mise à jour des totaux des premieres conclusions : conclusions de contexte
 * Fonction Appelée sur Onchange d'un input value
 */
function majTotalCont(){

            var A = parseFloat(document.getElementById("totalContAbo").value);
            var B = parseFloat(document.getElementById("InvestMat").value);
            var C = parseFloat(document.getElementById("LoyerMat").value);
            var D = parseFloat(document.getElementById("CoutMat").value);

           if(isNaN(D))D=0;
            if(isNaN(B))B=0;
            if(isNaN(C))C=0;

            var E = (A+B+C)*12 + D;
            E= E.toFixed(2);
            document.getElementById("totalCont").value = E;
            var F = (A+B+C)*12;
            F = F.toFixed(2);
            document.getElementById("totalContAS").value = F;
            majTotalDuree();
}
$modalPdt = $('#pdtChoix');
/**
 * Fct callback : Recupere la donnée data pour l'inserer ds la pop-up de produit
 * @param {object} data donne de retour de la fonction : liste des entitees correspondant à l'id Produit
 */
function afficheModalPdt(data) {
    document.getElementById("txtInt").innerHTML = data;
    $modalPdt.modal("show");
}
var idProduit = 0;
/**
 * en 1 ere partie la fonction gère les modifications de produit (les Pri et spot deja existantes pour le site)
 * en 2 ieme partie, la fonction gère d'aller chercher la bonne liste deroulante.
 * en 3 , recupere l'id produit Appel fichier Php
 * @param {number} numProjet        =1 numero de projet
 * @param {number} idfixe           id fixe sur lequel repose le projet (=0 si projet est partie mobile)
 * @param {number} idmobile         id mobile sur lequel repose le projet
 * @param {number} devis            id Devis courant
 * @param {number} numero           numero sur lequel est effectué le projet
 * @param {number} site             si fixe alors rattaché à un site
 * @param {function} callback         Fonction d'appel une fois fct projet() terminé AfficheModalPdt(data)
 * @param {number} sanscontexte = 0 gère les produits supplementaires affectation champ specifique
 */
function projet(numProjet,idfixe,idmobile,devis,numero,site,callback,sanscontexte = 0){
//Reinitilisation du pdt si erreur
//NE GERE PAS LES NUMERO NE COMMENCANT PAS PAR 0
    console.log("Ouverture projet ");
    if(sanscontexte === 0){
        if(site !== 0){

            tmp = 'tarProj'+site;

            if(numero !== 0 && numero !== 1)tmp2 = '0'+numero;
            if(numero > 100)tmp2 = '0'+numero;
            else tmp2=numero;

            //supprimer les espaces entre les chiffres
           // tmp2 = tmp2.replace(/ /g,"");
            res = tmp +''+ tmp2;
           //alert(res);
            if(document.getElementById(res).innerText !== ''){
                var tmp = 'totalSite'+site;
                var tmp1 = document.getElementById(tmp);

                var totalSite = tmp1.innerText || tmp1.textContent;
                totalSite = parseFloat(totalSite);
                totalSite -= parseFloat(document.getElementById(res).innerText);
                document.getElementById(tmp).innerHTML = "<strong>"+totalSite+" € </strong>";


                tmp = 'priProj'+site;
                if(numero !== 0)tmp2 = '0'+numero;
                else tmp2=numero;
                //supprimer les espaces entre les chiffres
               // tmp2 = tmp2.replace(/ /g,"");
                res = tmp + tmp2;
                var Pri = document.getElementById(res).innerText;
                Pri = parseFloat(Pri);
                var PriRec = document.getElementById('priRec').innerHTML;
                PriRec = parseFloat(PriRec);
                document.getElementById('priRec').innerHTML = PriRec-Pri;

                tmp = 'spotProj'+site;
                if(numero !== 0)tmp2 = '0'+numero;
                else tmp2=numero;
                res = tmp + tmp2;
                var spot = document.getElementById(res).value;
                spot = parseFloat(spot).toFixed(2);
                var spotRec = document.getElementById('spotRec').innerHTML;
                spotRec = parseFloat(spotRec);
                document.getElementById('spotRec').innerHTML = spotRec-spot;
            }

    }else{

      var tmp4 = numero;
      if(numero > 100)tmp = 'tarProjMob'+'0'+numero;
        else tmp = 'tarProjMob'+numero;
        //supprimer les espaces entre les chiffres
       // tmp2 = tmp2.replace(/ /g,"");

      //  alert(tmp);
         if(document.getElementById(tmp).innerText !== ''){
             var tmp2 = 'totalMobile'+'0'+numero;
             var tmp3 = document.getElementById(tmp2);

             var totalSite = tmp3.innerText || tmp3.textContent;
             totalSite = parseFloat(totalSite);
             totalSite -= parseFloat(document.getElementById(tmp).innerText);
             document.getElementById(tmp2).innerHTML = "<strong>"+totalSite+" € </strong>";

             tmp = 'priProjMob0'+numero;
             var Pri = document.getElementById(tmp).innerText;
             Pri = parseFloat(Pri);
             var PriRec = document.getElementById('priRec').innerHTML;
             PriRec = parseFloat(PriRec);
             document.getElementById('priRec').innerHTML = PriRec-Pri;

             tmp = 'spotProjMob0'+numero;
//alert(tmp);
             var spot = document.getElementById(tmp).value;
             spot = parseFloat(spot).toFixed(2);
            // alert(spot);
             var spotRec = document.getElementById('spotRec').innerHTML;
             spotRec = parseFloat(spotRec);
             document.getElementById('spotRec').innerHTML = spotRec-spot;
         }
    }
   }
 //Initialisation : pour permettre la detection de l'affichage du total dans mob ou fixe.
        //idFixe = null;
          //idMob = null;
        numPro = numProjet;
        if(idfixe == 0){
            if(site != 0){
                var tmp='pdtFixe'+site;

                chxPdt = document.getElementById(tmp);
                if(chxPdt == null){
                    alert("Veuillez choisir un Produit");
                    return;
                }
            }else{

                if(idmobile == 0){
                    var tmp = 'pdtMobile'+numero;
                   // alert('site = contexte'+tmp);
                    chxPdt = document.getElementById(tmp);
                }else{

                chxPdt = document.getElementById(numero);
                }

            }
        }else{

            if(idfixe == '-'){
            //Ajout de produit supplementaire
                tmp = 'pdtFixePlus';
                //idfixe = 0;
                chxPdt = document.getElementById(tmp);
            }else{

            tmp = site+''+numero;

            //console.log(tmp);
            chxPdt = document.getElementById(tmp);
            if(chxPdt == null){
                alert("Veuillez choisir un Produit");
                return;
            }
        }
    }

        //
    idFixe = idfixe;
    idMob = idmobile;
    choice = chxPdt.selectedIndex;
    if(choice == 0){
        alert("Veuillez selectionner un produit ");
        return;
    }
    idProduit = chxPdt.value;

    var formData = new FormData();
    formData.append("id",idProduit);
    formData.append("pro",numProjet);
    formData.append("idF",idfixe);
    formData.append("idM",idmobile);
    formData.append("dev",devis);
    formData.append("site",site);
      axios({
            method: 'post',
            url: '/index.php/Projet/afficheProjet',
            data: formData
        })
            .then(function (response) {
                callback(response.data);
                console.log("Données Pop-up Projet Pdt reçu sur " + formData.get("id"));
        })
            .catch(function (error) {
                console.log(error.response);
        });
    
}

var tabIdEnt=[];
var tabIdOptEnt=[];
var remCom;
var tabInput = [];
var tabQte=[];
var tabRem=[];
var tabRemFM=[];
var tabTar=[];
var arr=[];
var idPro;
/**
 * Declenché sur bouton valider Pop-up choix de produit
 * Recupère les données selectionnées dans la pop-up de selection de produit
 * 1 - recupère les idEntit idOptEnti
 * 2 - Traitement avec le tab des Qtes -> garde que les idEnti idOptEnti avec des Qtes
 * 3 - Appel Fct JS
 */
function recupModalProjet(){

    durEngage = document.getElementById("durEng").value;
    if(durEngage == 0){
        alert("Veuillez selectionner une durée d'engagement");
        return;
    }


    arr=[];
    tabRem=[];
    tabRemFM=[];
    tabQte=[];
    tabIdEnt=[];
    tabIdOptEnt=[];
    tabTar=[];

    //Recuperation Variable caché produit package ou pas
    var pdtPkg = document.getElementById("pdtPkg");

    //Recuperation du tableau d'indice d'input :
    // Ex : "Qte" + tabInput['1']
    tabInput = document.getElementById("nbInp").getAttribute("value");
    //Decodage
    var i = 0;

    var obj = JSON.parse(tabInput);
    for (var x in obj){
        if (obj.hasOwnProperty(x)){
            arr[i]=obj[x];
            i++;
        }
    }

    //Traitement Valeur -> Qte
    //AJOUTER LES CHAMPS POUR PACKAGE REMCOM REMFM
    if(pdtPkg != null){

        for(var i = 0; i<arr.length; i++){
            var tmp='check'+arr[i].toString();
            var boole = document.getElementById(tmp).checked;
            tmp='Qte'+arr[i].toString();
            var qteIn = document.getElementById(tmp);
            if(boole || qteIn.value){
                var tmp2 = 'Qte'+arr[i].toString();
                tabQte[i] = document.getElementById(tmp2).value;
                tmp = 'RemCom'+arr[i].toString();
                tabRem[i] = document.getElementById(tmp).value;
                tmp = 'RemFM'+arr[i].toString();
                if(document.getElementById(tmp) != null)tabRemFM[i] = document.getElementById(tmp).value;
                else tabRemFM[i] = 0;
                
            }else{
                tabQte[i] = 0;
                tabRem[i] = 0;
                tabRemFM[i] = 0;
                tabTar[i] = 0;
            }

        }
    }else{
        for(var i = 0; i<arr.length; i++){
            var tmp='Qte'+arr[i].toString();
           // alert(tmp);
            var qteIn = document.getElementById(tmp);
            if(qteIn.value){
                if(idProduit == 9){
                    tabQte[i] = qteIn.value;
                    tmp = 'tar'+arr[i].toString();
                    var tmp5 = document.getElementById(tmp).value;
                    tabTar[i] = tmp5.replace(",",".");
                    var tmp3 = 'chxNPack'+arr[i].toString();
                    if (document.getElementById(tmp3))arr[i] = document.getElementById(tmp3).value;
                }else{
                    tabQte[i] = qteIn.value;
                    tmp = 'RemCom'+arr[i].toString();
                    tabRem[i] = document.getElementById(tmp).value;
                    tmp = 'RemFM'+arr[i].toString();
                    if(document.getElementById(tmp) != null)tabRemFM[i] = document.getElementById(tmp).value;
                    else tabRemFM[i] = 0;
                    var tmp3 = 'chxNPack'+arr[i].toString();
                    if (document.getElementById(tmp3))arr[i] = document.getElementById(tmp3).value;
                }
            }else{
                tabQte[i] = 0;
                tabRem[i] = 0;
                tabRemFM[i] = 0;
                tabTar[i] = 0;
            }
        }
        // alert("IdEntite$IdOptEntit : "+arr);
    }

    //Recup Duree + RemCom
  //  durEngage = document.getElementById("durEng").value;

    for(var i = 0; i < arr.length; i++){
        var t=arr[i].split('$');
        tabIdEnt[i]=t[0];
        if(tabIdOptEnt[i] == 0)tabIdOptEnt[i]=null;
        else tabIdOptEnt[i] = t[1];
    }
  /* alert("IdEnt "+tabIdEnt);
    alert("IdOptEnt "+tabIdOptEnt);
    alert(" Qte valeur associée : "+tabQte);
    alert("RemCOm Associe "+tabRem);
    alert("Tarifs Associés "+tabTar);
    alert("Rem FM associe "+tabRemFM);*/
    console.log("RecupeRation des valeurs de la Pop-up (Ident IdOptEnt ...)")
    validModalProjet(afficheTarifs);

    //validModalProjet();
}
var totalProjet=0;
function myFct(s){
    //alert("modif "+s.value);
    totalProjet +=s.value;
    totalProjet  = totalProjet.toFixed(2);
     if(document.getElementById("Projet1Total")){
         document.getElementById("Projet1Total").value += totalProjet;
         majTotalProjet();
     }
}
/**
 * Jointure avec le serveur
 * 1- regroupement des données
 * 2 - Appel Fct Serveur
 * @param {function} callback : Appel serveur reussie et traitement effectue Fct callback appelé
 */
function validModalProjet(callback){
   // alert("&idEnt="+JSON.stringify(tabIdEnt)+"&idOptEnt="+JSON.stringify(tabIdOptEnt)+"&qte="+JSON.stringify(tabQte)+"&proj="+idPro+"&pdt="+idProduit+"&dEn="+durEngage+"&remCom="+JSON.stringify(tabRem)+"&remFS="+JSON.stringify(tabRemFM)+"&tar="+JSON.stringify(tabTar));
    
    console.log("Creation Tableau d'envoi vers serveur");
    var formData = new FormData();
    formData.append("idEnt",JSON.stringify(tabIdEnt));
    formData.append("idOptEnt",JSON.stringify(tabIdOptEnt));
    formData.append("qte",JSON.stringify(tabQte));
    formData.append("pdt",idProduit);
    formData.append("dEn",durEngage);
    formData.append("remCom",JSON.stringify(tabRem));
    formData.append("remFS",JSON.stringify(tabRemFM));
    formData.append("tar",JSON.stringify(tabTar));
      axios({
            method: 'post',
            url: '/index.php/projet/projet_calcul_choice',
            data: formData
        })
            .then(function (response) {
                callback(response.data);
                console.log("Calcul & Affichage effectues");
        })
            .catch(function (error) {
          if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
          } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
          }
          console.log(error.config);
        });
}
/**
 * Recupère Bob Projet du fixe ou mobile ou pdt sup en cours
 * Mise en page des resultats du projet sur devis
 * @param {object} data Retour d'appel de fct serveur
 */
function afficheTarifs(data){
    console.log(data);
    console.log("Choix Pop-up enregistré & Affichage ...")
    if(data == 2){
        alert("Tarifs Non Présent en base avec cette duree d'engagement");
        return;
    }
    $modalPdt.modal('toggle');
    if(data == 0){
         alert("Erreur insert ChxProj");
         return;
    }
    
    //bob = JSON.parse(data);
    bob = data;
    //console.log(bob);
    
    alert("Produit Enregistré");
  
    var tmp4 ;
    if(document.getElementById('addProd').getAttribute("value") !== '1')
    {
        if(bob.site){
            if(bob.numero){
                //SI FIXE
                var tmp = 'priProj'+bob.site;
                var tmp2 = bob.numero.trim();
                //supprimer les espaces entre les chiffres
                tmp2 = tmp2.replace(/ /g,"");
                var res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.PRI.toFixed(2);

                //FMA
                var tmp = 'fmaProj'+bob.site;
                var tmp2 = bob.numero.trim();
                //supprimer les espaces entre les chiffres
                tmp2 = tmp2.replace(/ /g,"");
                var res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.MS.toFixed(2);

                tmp = 'tarProj'+bob.site;
                tmp2 = bob.numero.trim();
                //supprimer les espaces entre les chiffres
                tmp2 = tmp2.replace(/ /g,"");
                res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.tarifsHT.toFixed(2);

                tmp = 'engProj'+bob.site;
                tmp2 = bob.numero.trim();
                //supprimer les espaces entre les chiffres
                tmp2 = tmp2.replace(/ /g,"");
                res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.eng;

                tmp = 'pdtProj'+bob.site;
                tmp2 = bob.numero.trim();
                //supprimer les espaces entre les chiffres
                tmp2 = tmp2.replace(/ /g,"");
                res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.nomi;

                tmp = 'totalSite'+bob.site;
                var tmp1 = document.getElementById(tmp);

                var totalSite = tmp1.innerText || tmp1.textContent;
                totalSite = parseFloat(totalSite);
                if(isNaN(totalSite))totalSite=0;
                //console.log("Total Site present :"+totalSite+" typ : "+typeof totalSite);

                var tarifsPdt = bob.tarifsHT;
                //console.log("Tarifs Pdt : "+tarifsPdt+" type : "+typeof tarifsPdt);

                var somTotal = +totalSite+tarifsPdt;
                //console.log("Som : "+somTotal+ "type : "+typeof somTotal);
                somTotal = somTotal.toFixed(2);
                document.getElementById(tmp).innerHTML = "<strong>"+somTotal+" € </strong>";

                tmp = 'spotProj'+bob.site;
                tmp2 = bob.numero.trim();
                tmp2 = tmp2.replace(/ /g,"");
                res = tmp + tmp2;
                document.getElementById(res).value=bob.SPOT;


            }else{
                var tmp = 'priProj'+bob.site+'0';
                document.getElementById(tmp).innerHTML=bob.PRI.toFixed(2);
                tmp = 'fmaProj'+bob.site+'0';
                document.getElementById(tmp).innerHTML=bob.MS;
                tmp = 'engProj'+bob.site+'0';
                document.getElementById(tmp).innerHTML=bob.eng;
                tmp = 'pdtProj'+bob.site+'0';
                document.getElementById(tmp).innerHTML=bob.nomi;
                tmp = 'tarProj'+bob.site+'0';
                document.getElementById(tmp).innerHTML=bob.tarifsHT.toFixed(2);

                tmp = 'totalSite'+bob.site;
                var tmp1 = document.getElementById(tmp);

                var totalSite = tmp1.innerText || tmp1.textContent;
                totalSite = parseFloat(totalSite);
                if(isNaN(totalSite))totalSite=0;
                //console.log("Total Site present :"+totalSite+" typ : "+typeof totalSite);

                var tarifsPdt = bob.tarifsHT;
                //console.log("Tarifs Pdt : "+tarifsPdt+" type : "+typeof tarifsPdt);

                var somTotal = +totalSite+tarifsPdt;
                //console.log("Som : "+somTotal+ "type : "+typeof somTotal);
                somTotal = somTotal.toFixed(2);
                document.getElementById(tmp).innerHTML = "<strong>"+somTotal.toString() +" € </strong>";

                tmp = 'spotProj'+bob.site+'0';
                document.getElementById(tmp).value=bob.SPOT;

            }

        
        }else{
            //PAS de site + numero = mobile
            if(bob.numero){
                var tmp = 'priProjMob';
                var tmp2 = bob.numero.trim();
                tmp2 = tmp2.replace(/ /g,"");
                var res = tmp + tmp2;
                //res : priProjMob06 56 87 65 43
                document.getElementById(res).innerHTML=bob.PRI;

                tmp = 'tarProjMob';
                res = tmp +tmp2;
                document.getElementById(res).innerHTML=bob.tarifsHT.toFixed(2);

                tmp =  'engProjMob';
                res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.eng;
                tmp = 'pdtProjMob';
                res = tmp + tmp2;
                document.getElementById(res).innerHTML=bob.nomi;
                tmp = 'totalMobile';
                res = tmp + tmp2;
                document.getElementById(res).innerHTML = "<strong>"+bob.tarifsHT.toFixed(2) +" € </strong>";

                var tmp = 'spotProjMob';
                var res = tmp + tmp2;
                document.getElementById(res).value=bob.SPOT;
            }else{
               var  txt = "<div class='row' id='"+bob.idChx+"'><div class='col-sm-3'><label class='control-label'>";
               txt += "PDT : </label>"+bob.nomi+"</div>";
               txt += "<div class='col-sm-3 d-inline'><div class='col-sm-5'><label class='control-label'>Tarifs HT: (€) </label></div>";
               txt += "<div class='col-sm-2'>"+bob.tarifsHT.toFixed(2)+"</div></div>";
               txt += "<div class='col-sm-3 d-inline'><font color='red'><div class='col-sm-3'><label class='control-label'>Pri : </label></div>";
               txt += "<div class='col-sm-2'>"+bob.PRI.toFixed(2)+"</div></font></div>";
               txt += "<div class='col-sm-3 d-inline'><div class='col-sm-4'><label class='control-label'>ENG: (mois) </label></div>";
               txt += "<div class='col-sm-2'>"+bob.eng+"</div><button onclick='supprChxIndep("+bob.idChx+","+bob.PRI.toFixed(2)+","+bob.SPOT.toFixed(2)+","+bob.tarifsHT.toFixed(2)+")'>Supprimer</button></div>";
               document.getElementById('produitSupMobile').innerHTML += txt;
            }
        }
    }else{
        var elt = document.getElementById('site');
        txt = '<div class="row" id="'+bob.idChx+'"><article class="col-md-11 bg-info"><h2>Site '+elt.selectedIndex+' - '+elt.options[elt.selectedIndex].text+'</h2></article>';
        txt += '<div class="row"><article class="col-md-5"></article>';
        txt += '<article class="col-md-6"><div class="row"><div class="col-sm-12">';
        txt += '<label class="col-sm-3 control-label  d-inline">PDT : </label><div class="col-sm-8 control-label  d-inline">'+bob.nomi+'</div></div>';
        txt += '<div class="col-sm-12"><label class="col-sm-3 control-label  d-inline">Tarifs HT (€): </label><div class="col-sm-8 control-label  d-inline">'+bob.tarifsHT.toFixed(2)+'</div></div>';
        txt += '<div class="col-sm-12"><label class="col-sm-3 control-label  d-inline">ENG (mois): </label> <div class="col-sm-8 control-label  d-inline">'+bob.eng+'</div></div>';
        txt += '<div class="col-sm-12"><font color="red"><label class="col-sm-3 control-label  d-inline">PRI : </label> <div class="col-sm-3 control-label  d-inline">'+bob.PRI.toFixed(2)+'</div></font><label class="col-sm-3 control-label  d-inline">FMA : </label> <div class="col-sm-3 control-label  d-inline">'+bob.MS.toFixed(2)+'</div></div>';
        txt += '<div class="col-sm-12"><div class="col-sm-8 control-label  d-inline"><button onclick="supprChxIndep('+bob.idChx+','+bob.PRI.toFixed(2)+','+bob.SPOT.toFixed(2)+','+bob.tarifsHT.toFixed(2)+')">Supprimer</button></div></div>';
        txt +='</div></div></article></div></div>';
        document.getElementById('produitSup').innerHTML += txt;
    }

    var tmp = document.getElementById('spotRec');
    var totalSpot = tmp.innerText || tmp.textContent;
    //console.log("Total Spot present :"+totalSpot+" typ : "+typeof totalSpot);
    var spotBob = bob.SPOT;
    var somSpot = +totalSpot + spotBob;
    somSpot = somSpot.toFixed(2);
    document.getElementById('spotRec').innerHTML = somSpot.toString();
    tmp = document.getElementById('priRec');
    var totalRec = tmp.innerText || tmp.textContent;
    //console.log("Total Rec present :"+totalRec+" typ : "+typeof totalRec);
    var priBob = bob.PRI;
    var somPri = +totalRec + priBob;
    somPri = somPri.toFixed(2);
    document.getElementById('priRec').innerHTML = somPri.toString();
}


/*PRESTATION CHECKBOX*/
var totalPresta = 0;
/**
 * Fonction appele sur event ckeck sur Prestation Options Cap
 * Mets à jour champ de la valeur de la presta
 * @param {object} checkB Checkbox courant
 */
function majPresta(checkB){
    var qte = 0;
    if(checkB.checked){

        if(!document.getElementById("Projet1PrestaZen1Tarif")){
            qte = document.getElementById("qtePresta"+checkB.id).value;
            if( qte === '')qte = 1;
            totalPresta += parseFloat(checkB.value)*qte;
              
        }else{
            totalPresta = parseFloat( document.getElementById("Projet1PrestaZen1Tarif").value);
            qte = document.getElementById("qtePresta"+checkB.id).value;
            if( qte === '')qte = 1;
            totalPresta += parseFloat(checkB.value*qte);
            document.getElementById("Projet1PrestaZen1Tarif").value = totalPresta.toFixed(2);
            majTotalProjet();
        }
    }else{
        if(!document.getElementById("Projet1PrestaZen1Tarif")){
            qte = document.getElementById("qtePresta"+checkB.id).value;
            if( qte === '')qte = 1;
            totalPresta -= parseFloat(checkB.value*qte);
        }else{
            totalPresta = parseFloat( document.getElementById("Projet1PrestaZen1Tarif").value);
            qte = document.getElementById("qtePresta"+checkB.id).value;
            if( qte === '')qte = 1;
            totalPresta -= parseFloat(checkB.value*qte);
            document.getElementById("Projet1PrestaZen1Tarif").value = totalPresta.toFixed(2);
            majTotalProjet();
        }
    }
    console.log("Ajout de presta : " + checkB.value + "qte : " + qte);
    
    majTotalDuree();
}
/**/
function majPresta(inputB,check){
    var qte = 0;
    qte = document.getElementById(inputB.id).value;
    console.log(" Ajout de presta Qte : " + qte);
    document.getElementById(check).checked = true;
    var valeur = 0;
    valeur = document.getElementById(check).value;
    console.log(" Ajout de presta Value : " + valeur);
    if(!document.getElementById("Projet1PrestaZen1Tarif")) {
        totalPresta += valeur * qte;
    } else {
        totalPresta = parseFloat( document.getElementById("Projet1PrestaZen1Tarif").value);
        totalPresta += parseFloat(valeur * qte);
        document.getElementById("Projet1PrestaZen1Tarif").value = totalPresta.toFixed(2);
    }
    majTotalProjet();
    majTotalDuree();
}

/**/
function check_onchange(input){
    var thestring = input.id;
    var thenum = thestring.replace( /^\D+/g, '');
    document.getElementById(thenum).checked = true;
}

/**
 * Mise en page conclusion (data) retour de fct serveur
 * @param {[[Type]]} data [[Description]]
 */
function insertConclu(data){
    document.getElementById("concluSite").innerHTML = data;
    console.log("Affichage Conclu de Devis");
    majTotalDuree();
 }
/**
 * Fct declenché sur click button : Affiche Conclusion Projet
 * @param {function} callback insertConclu Mise en page conclusion
 */
function afficheConclu(callback){
   
    document.getElementById("btn_add").disabled = true;
    document.getElementById("validePart1").disabled = true;
    document.getElementById("nameS").disabled=true;
    document.getElementById("buttonValidConclu").innerHTML = '<button class="btn btn-warning" id="validePart1" onclick="afficheConclu(insertConclu)">Mettre à jour Conclusion</button>';
    axios({
            method: 'post',
            url: '/index.php/projet/affiche_conclu',
            data: 'presta='+totalPresta
        })
            .then(function (response) {
                callback(response.data);
                console.log("Demande de conclusion Devis Envoi total Presta " + totalPresta);
        })
            .catch(function (error) {
          if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
          } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
          }
          console.log(error.config);
        });
}






 function fen(){
    alert("Attention : Mail dérogatoire obligatoire dès qu'une remise est pratiquée sur les abonnements.");
}


function saveConclu(callback){
    //Conclusion Ctxte
       var maintCT = document.getElementById("InvestMat").value;
       var loyerCT = document.getElementById("LoyerMat").value;
       var achatCT = document.getElementById("CoutMat").value;
        var totalCT = document.getElementById("totalContAbo").value;
       var idDevis = document.getElementById("numDevis").getAttribute("value");
       if(maintCT=='')maintCT = null;
       if(loyerCT=='')loyerCT = null;
       if(achatCT=='')achatCT = null;

       var spotRec = document.getElementById("spotRec").getAttribute("value");
       var priRec = document.getElementById("priRec").getAttribute("value");


       //PROJET CONCLU
       var presta = document.getElementById('Projet1PrestaZen1Tarif').value;
       if(presta=='')presta = null;
       var frais = document.getElementById('Projet1InvestMat').value;
       if(frais=='')frais = null;
       var maint = document.getElementById('Projet1PrestaZen2Tarif').value;
       if(maint=='')maint = null;
       var loyer = document.getElementById('Projet1PrestaZen3Tarif').value;
       if(loyer=='')loyer = null;
       var achat = document.getElementById('Projet1FMA').value;
       if(achat=='')achat = null;
       var remise = document.getElementById('Projet1RemCom').value;
       if(remise=='')remise = null;
     //  idPro  = document.getElementById("idProj").getAttribute("value");
       var projetTotal = document.getElementById('Projet1Total').value;
        var dureePro = document.getElementById('selDurEng').value;
       //Recuperer les options cap cochées pour remplir table presta affect
    var prestAffect=[];
    var j = 0;
    var services = document.getElementById('services');
    var inputs = services.getElementsByTagName('input');
    console.log("Creation Presta Affect");  
    
    
              for(var i = 0; i < inputs.length;i++){
                  input=inputs[i];
                  if (input.checked) {
                    //Ecoute apres le 5 ieme car check...    
                      console.log(input.id.substr(5));
                      prestAffect[j]=input.id.substr(5);
                      j++
                  }
       }
        
        console.log("Tabl Presta : " + prestAffect);
        console.log("Creation Tableau de COnclusion");
        var formData = new FormData();
        formData.append("presta",presta);
        formData.append("maint",maint);
        formData.append("loyer",loyer);
        formData.append("achat",achat);
        formData.append("projetTot",projetTotal);
        formData.append("maintCT",maintCT);
        formData.append("loyerCT",loyerCT);
        formData.append("achatCT",achatCT);
        formData.append("totalCT",totalCT);
        formData.append("idDevis",idDevis);
        formData.append("frais",frais);
        formData.append("duree",dureePro);
        formData.append("rem",remise);
        formData.append("prestAffect",JSON.stringify(prestAffect));
        console.log("Envoi conclu : " +formData);
    
    
        axios({
                method: 'post',
                url: '/index.php/projet/save_conclu',
                data: formData
            })
                .then(function (response) {
                    callback(response.data);
                    console.log("Conclu Enregistrée");
            })
                .catch(function (error) {
              if (error.response) {
                  // The request was made and the server responded with a status code
                  // that falls out of the range of 2xx
                  console.log(error.response.data);
                  console.log(error.response.status);
                  console.log(error.response.headers);
              } else if (error.request) {
                  // The request was made but no response was received
                  // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                  // http.ClientRequest in node.js
                  console.log(error.request);
              } else {
                  // Something happened in setting up the request that triggered an Error
                  console.log('Error', error.message);
              }
              console.log(error.config);
            });
}
function insertPage(data){
    var idDevis = document.getElementById("numDevis").getAttribute("value");
    var sel = document.getElementById('nameS');
    var nameSoc = sel.options[sel.selectedIndex].text;
    window.scrollTo(0,0);
    document.getElementById("devisOperateur").innerHTML="";
    //document.getElementById("devisOperateur").innerHTML="<div class=\"row\"><article class=\"col-md-12\"><h2 style=\"text-align:center\">Devis N° "+idDevis+" - SOCIETE : "+nameSoc+"</article></div>";
    document.getElementById("devisSite").innerHTML=data;
    document.getElementById("concluSite").innerHTML="<div class=\"row\"><article class=\"col-md-5\"></article><article class=\"col-md-2\"><button class=\"btn btn-success\" id=\"testLiens\" onclick=\"recupComment(insertTest);\">Acceder Au Test & Liens</button></article></div>";
}
function recupComment(callback){
    var comment = document.getElementById("comment").value;
    
    axios({
                method: 'post',
                url: '/index.php/projet/save_comment',
                data: 'com='+comment
            })
                .then(function (response) {
                    callback(response.data);
                    console.log("Envoi Comment Devis sur Serveur")
            })
                .catch(function (error) {
                      if (error.response) {
                          // The request was made and the server responded with a status code
                          // that falls out of the range of 2xx
                          console.log(error.response.data);
                          console.log(error.response.status);
                          console.log(error.response.headers);
                      } else if (error.request) {
                          // The request was made but no response was received
                          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                          // http.ClientRequest in node.js
                          console.log(error.request);
                      } else {
                          // Something happened in setting up the request that triggered an Error
                          console.log('Error', error.message);
                      }
                      console.log(error.config);
            });
}
function insertTest(data){
    document.getElementById("concluSite").innerHTML=data;
    console.log("insertion Code Html pour Test");
}
