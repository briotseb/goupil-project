var site_url = 'https://www.devoapp.ovh/index.php/';
var xhr = null;


/**
 * Definit la procedure d'envoi XMLHttp
 * @returns {[[Type]]} [[Description]]
 *                     AXIOS
 */
function getXMLHttpRequest() {
        var xhr = null;

        if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
                try {
                    xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch(e) {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                xhr = new XMLHttpRequest(); 
            }
        } else {
            alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
            return null;
        }

        return xhr;
}

/**
 * Page Annuaire
 * Function qui actionne le bouton Ouvrir une Societe dans la vue Annuraire
 */
function annuaire_open()
{
   
    var $table = $('#tableClients');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    

    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    if (xhr && xhr.readyState != 0) {
        alert("Opération en cours");
        return;
    }
    try{
        xhr   = getXMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                document.getElementById('Ann_infoClient').innerHTML=xhr.responseText; 
                document.getElementById("loaderAnnuaire").style.display = "none";
                xhr.abort();
            } else if (xhr.readyState < 4) {
                document.getElementById("loaderAnnuaire").style.display = "inline";
            }
        };
       
        xhr.open("POST", './details_client', true);
       
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("id=" + ids);
    }catch(err){
        alert(err);
    }       
}

/**
 * Page Devis Supprime les devis
 * @returns {NULL} Si Devis non selectionne
 */
function devis_supprDevis()
{
   
    var $table = $('#tableDevis');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.numDev;
    });
    if(ids == 0){
        alert("Veuillez selectionner un devis");
        return; 
    }
    var r=confirm("Confirmer la suppression du devis ?");
    if(r){
        if (xhr && xhr.readyState != 0) {
            alert("Opération en cours");
            return;
        }
        try{
            xhr   = getXMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                  // location.reload();
                    document.getElementById("loaderDevis").style.display = "none";
                } else if (xhr.readyState < 4) {
                    document.getElementById("loaderDevis").style.display = "inline";
                }
            };

            xhr.open("POST", './suppr_devis', true);

            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("id=" + ids);
        }catch(err){
            alert(err);
        }     
    }

}
/**
 * Page Contexte OUvre le contexte d'une société
 * @returns {NULL} Si Societe non selectionné
 */
function contexte_open()
{
    var $table = $('#tableClients');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
            return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    var idCont = $.map(data, function (item) {
        return item.idCont;
    });  
   loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/clients/load_client',
        data: 'id='+ids+'&idCont='+idCont
       
    })
        .then(function (response) {
            document.location="/index.php/contexte/index";   
        })
        .catch(function (error) {
            console.log(error);
        });
}
/**
 * Page Contexte Modifie les informations d'une société
 * @returns {[[Type]]} [[Description]]
 */
function contexte_modifSoc()
{
    var $table = $('#tableClients');
    $modalModif = $('#contexte_modalModif');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    var idCont = $.map(data, function (item) {
        return item.idCont;
    });  

    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/clients/load_client_modif',
        data: 'id='+ids+'&idCont='+idCont
       
    })
        .then(function (response) {
            document.getElementById("details").innerHTML = response.data; 
            $modalModif.modal('show');     
        })
        .catch(function (error) {
            console.log(error);
        });
}
/**
 * Page Contexte Supprime le contexte et les informations d'une société
 * @returns {[[Type]]} [[Description]]
 */
function contexte_supprSoc()
{
    var $table = $('#tableClients');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    var r=confirm("Confirmer la suppression de la Société ?");
    if(r){
        $.ajax({
            type: "POST",
            url: site_url+"clients/suppr_client",
            // path to the controller 
                data: {id: ids},              
                success: function(data){        
                    document.location="/index.php/clients/index";
                },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
           
        });
    }
}
/**
 * Page NewDevis.php lance l'edition d'un devis
 * @returns {boolean} [[Description]]
 */
function newDevis_start()
{
    var ids=document.getElementById('nameS').value;
    if(ids === '-Choisir-'){
        alert('veuillez selectionner une société');
        return;
    }else{
        document.getElementById('btn_next').disabled = true;
        document.getElementById('nameS').disabled = true;
        document.getElementById('dateDevis').disabled = true;
        var tr = confirm('Attention, Veuillez effectuer une saisie complête du devis.');
        if(tr){
            $.ajax({
                type: 'POST',
                url: site_url+"New_Devis/load_operateur",
                data: {idContexte:ids},            
                success: function(msg){        
                    document.getElementById("devisOperateur").innerHTML = msg;
                },
                error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
                
            });
            return false;
        }
    }
}
/**
 * Lance le choix d'operateur pour les mobiles
 * @returns {boolean} [[Description]]
 */
function newDevis_startMobile()
{
    var ids=document.getElementById('nameS').value;
    $.ajax({
        type: 'POST',
        url: site_url+"New_Devis/load_operateur_mobile",
        data: {idContexte:ids},            
        success: function(msg){        
            document.getElementById("devisOperateurMobile").innerHTML = msg;
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
         }
    });
    return false;
}
/**
 *BUTTON GESTION DES SITES DANS UN CONTEXTE
**/
/**
 * Bouton Ajout Site dans Page de contexte Via Modal
 */
function contexte_AddSite()
{
    
}
/**
 * Bouton Modifier un site page contexte
 */
function contexte_ModifSite()
{
    var $tableSite = $('#site-table'); 
    var $modalSite = $('#myModal_modif_site');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner un site");
        return; 
    }
    $.ajax({
        type: 'POST',
        url: site_url+"site/load_site",
        data: {idSite:ids},            
        success: function(msg){        
            var json = JSON.parse(msg);
            $('#ModifadresseSite').val(json[0].SiteAdresse);
            $('#ModifCPSite').val(json[0].SiteCodePost);
            $('#ModifVilleSite').val(json[0].SiteVille);
            $modalSite.modal('show');
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
    });
    return false;
}
function contexte_SupprSite()
{
    var $tableSite = $('#site-table'); 
    var $modalSite = $('#myModal_modif_site');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner un site");
        return; 
    }
    var r = confirm("Attention : Supprime définitivement le site ?");
    if (r == true) {
        $.ajax({
            type: 'POST',
            url: site_url+"site/suppr_site",
            data: {idSite:ids},            
            success: function(msg){        
                alert("Site Supprimé");
                location.reload();
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
    }else{
        return false;
    }
    return false;
}
function contexte_AddLine()
{
    var $tableSite = $('#site-table'); 
    var $modalLigne = $('#MyModalAjoutLigne');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner un site");
        return; 
    }
    $('#idSite').val(ids);
    $modalLigne.modal('show');
}
function contexte_SupprLine()
{
    var $tableSite = $('#site-table');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idFixe;
    });
    if(ids == 0){
        alert("Veuillez selectionner une ligne");
        return; 
    }
    var r = confirm("Attention : Supprime définitivement la ligne ?");
    if (r == true) {
        $.ajax({
            type: 'POST',
            url: site_url+"site/suppr_line",
            data: {idFixe:ids},            
            success: function(msg){        
                alert("Ligne Supprimée");
                location.reload();
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
    }else{
        return false;
    }
        
}
function contexte_DetailsSite()
{
    var $tableSite = $('#site-table');
    var $modalDetails = $('#modalDetLigne');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    var idf = $.map(data, function (item) {
        return item.idFixe;
    });
    var idftmp = JSON.stringify(idf);
    var idstmp = JSON.stringify(ids);
    if(ids == 0){
        alert("Veuillez selectionner un site");
        $modalDetLigne.modal('toggle');
        return;
    }
    $.ajax({
        type: 'POST',
        url: site_url+"site/load_details",
        data: {site:idstmp , fixe: idftmp},            
        success: function(msg){        
            document.getElementById("txtHint1").innerHTML = msg;
            $modalDetails.modal('show');
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
    });
    return false;
}
/**
 * Fonction Ajout de Mobile Via le modal
 */
function contexte_AddMobLine()
{
}
/**
 * Bouton modifier une ligne Mobile
 * @returns {boolean} [[Description]]
 */
function contexte_ModifMobLine()
{
    var $table = $('#mobile-table');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile");
        return; 
    }
    $.ajax({
        type: 'POST',
        url: site_url+"mobile/load_mobile",
        data: {idMob:ids},            
        success: function(msg){        
            // document.getElementById("myModalBodyModif").innerHTML = msg;
            var json = JSON.parse(msg);
            $('#Mobile1NbModif').val(json[0].MobileNumero);
            $('#Mobile1DDModif').val(json[0].MobileDateDeb);
            $('#Mobile1DFModif').val(json[0].MobileDateFin);
            $('#Mobile1ForfaitModif').val(json[0].MobileForfTarifsHT);
            $('#Mobile1OptionModif').val(json[0].MobileDetailsForf);
            $modalMobile.modal('show');
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
    });
    return false;
}

function contexte_SupprMobile()
{
    var $table = $('#mobile-table');
   
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile");
        return; 
    }
    var r = confirm("Attention : Supprime définitivement le Mobile ?");
    if (r == true) {
        alert(ids);
       /* $.ajax({
            type: 'POST',
            url: site_url+"mobile/suppr_mobile",
            data: {idMob:ids},            
            success: function(msg){        
                alert("Mobile Supprimé");
               location.reload();
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });*/
        if (xhr && xhr.readyState != 0) {
            alert("Attendez que la requête ait abouti avant de faire joujou");
            return;
        }
        try{
            xhr   = getXMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                    alert("Mobile Supprimé");
                    document.getElementById("loaderMobile").style.display = "none";
                   // location.reload();
                } else if (xhr.readyState < 4) {
                    document.getElementById("loaderMobile").style.display = "inline";
                }
            };

            xhr.open("POST", '../mobile/suppr_mobile', true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("idMob=" + ids);
        }catch(err){
            alert(err);
        }
       
        
    }else{
        return;
    }
    return false;
}

function contexte_AddConsoMob()
{
    var $table = $('#mobile-table');
    var $modalMobileConso = $('#ajoutConsoMob');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile");
        return; 
    }
    var nume = $.map(data, function (item) {
        return item.mobNum;
    });
    document.getElementById('infoMob').innerHTML='Numero : '+nume;
    $modalMobileConso.modal('show');
          
}
function contexte_SupprConsoMob()
{
    var $table = $('#mobile-table');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile rattaché à la consommation");
        return; 
    }
    var idC = $.map(data, function (item) {
        return item.idConso;
    });
    var form_data = {
        idMob : ids,
        idConso: idC,
    };
    var r = confirm("Attention : Supprime définitivement la consommation ?");
    if (r == true) {
        $.ajax({
            type: 'POST',
            url: site_url+"mobile/suppr_conso",
            data: form_data,            
            success: function(msg){        
              location.reload();
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
    }else{
        return false;
    }
    return false;
}
/**
 * Ajout de produit supplementaire sur modal Ajout de mobile
 */
function open_pdt_smp_mob()
{
    var $modalAjout_smp_mob = $('#pdtAjoutSmp_mob');
    $modalAjout_smp_mob.modal('show');
}
/**
 *Bouton Modal
 *Ajout de Site
 *
**/
    /**
     * Bouton valider ajout de site
     * @param   {[[Type]]} csrf_value Id Unique de saisie de formulaire
     * @returns {boolean}  [[Description]]
     */
    function modal_ajoutSite(csrf_value)
{
    var form_data = {
        ASite: $('#adresseSite').val(),
        CPSite: $('#CPSite').val(),
        VilleSite: $('#VilleSite').val(),
        csrf_test_name: csrf_value
    };
    $.ajax({
        url: site_url+"Modal_Site/submit",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES')
                location.reload();
            else if (msg == 'NO')
                $('#alert-msg').html('<div class="alert alert-danger text-center">Erreur 01</div>');
            else
                $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
            
          
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
    });
}
/**
 * [[Description]]
 * @param {[[Type]]} csrf_value [[Description]]
 */
function modal_modifSite(csrf_value)
{
    var form_data = {
        ASite: $('#ModifadresseSite').val(),
        CPSite: $('#ModifCPSite').val(),
        VilleSite: $('#ModifVilleSite').val(),
        csrf_test_name: csrf_value
    };
    $.ajax({
        url: site_url+"Modal_Site/update",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES')
                location.reload();
            else if (msg == 'NO')
                $('#alert-msg').html('<div class="alert alert-danger text-center">Error in sending your message! Please try again later.</div>');
            else
                $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
    });
}


/**
* Modal Ajout de Ligne
**/

function modal_ligneAjout(csrf_value)
{
    var $modalConsoPkg = $('#ajoutConsoPkg');
    var $modalLigne = $('#MyModalAjoutLigne');
    var $tableSite = $('#site-table');    
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    var typeNum = 0;
    if(document.getElementById('cboxNDI').checked)typeNum=1;
    if(document.getElementById('cboxSpecial').checked)typeNum=2;
    var form_data = {
        idSite : ids,
        idConso: $('#produit').val(),
        Tarifs: $('#produit').val(),
        Qte: $('#SiteL1TZero').val(),
        Num: $('#Site1L1Nb').val(),
        Tarifs: $('#Site1L1Tarif').val(),
        Type: typeNum,
        csrf_test_name: csrf_value,
    };
    if(document.getElementById('indic').value === '0'){
        //INSERT SUR PDT NORMAL AVEC VERIF TARIFS NON VIDE
        // console.log(form_data);
        if($('#Site1L1Tarif').val() == ''){
            $('#alert-msg2').html('<div class="alert alert-danger text-center">Erreur de Saisie - veuillez rentrer un tarif!</div>');
        }else{
            $.ajax({
                url: site_url+"Site/add_ligne",
                type: 'POST',
                data: form_data,
                success: function(msg) {
                    if(msg === '0'){
                        alert('veuillez vérifier les champs');
                    }else{
                        location.reload();
                    }

                },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
            });
               
        }
    }else{
        var idPack = document.getElementById('indic').value;
        //launch modal pack choisir Options
                
        $.ajax({
            url: site_url+"Site/add_ligne",
            type: 'POST',
            data: form_data,
            success: function(msg) {
                if(msg === '0'){
                    alert('veuillez vérifier les champs');
                }else{
                    var idFixe = msg;
                    $modalLigne.modal("toggle");
                    $.ajax({
                        url: site_url+"Modal/opt_call",
                        async: false,
                        type: "POST",
                        data: {id:idPack,csrf_test_name: csrf_value},  
                        dataType: "html",
                        success: function(data) {
                            $('#opt_pkg').html(data);
                            document.getElementById('infoFixeF').innerHTML='Numero : '+$('#Site1L1Nb').val();
                            $('#idFixe2').val(msg);
                            $modalConsoPkg.modal('show');
                        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
                    })
                }
            }
        });
    }
}
/**
 * Enregistre la ligne et lance la fenetre d'ajout de consommation
 * @param   {[[Type]]} csrf_value [[Description]]
 * @returns {boolean}  [[Description]]
 */
function modal_LigneAddConso(csrf_value)
{
    var $tableSite = $('#site-table'); 
    var  $modalConsoF = $('#ajoutConso');
    var $modalLigne = $('#MyModalAjoutLigne');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    var form_data = {
        idSite : ids,
        idConso: $('#produit').val(),
        Tarifs: $('#produit').val(),
        Qte: $('#SiteL1TZero').val(),
        Num: $('#Site1L1Nb').val(),
        Tarifs: $('#Site1L1Tarif').val(),
        Type: $('#cboxNDI').val(),
        csrf_test_name: csrf_value,
    };
    if($('#Site1L1Tarif').val() == ''){
        $('#alert-msg2').html('<div class="alert alert-danger text-center">Erreur de saisie</div>');
    }else{
        $.ajax({
            url: site_url+"Site/add_ligne",
            type: 'POST',
            data: form_data,
            success: function(msg) {
                $modalLigne.modal("toggle");
                document.getElementById('infoFixe').innerHTML='Numero : '+$('#Site1L1Nb').val();
                $('#idFixe1').val(msg);
                $modalConsoF.modal("show");
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
        return false;
    }
}
function modal_LigneOpenAddPkg(){
    var $modalAjout3 = $('#pdtAjout');
    $modalAjout3.modal('show');     
}

/**
*Modal Ajout de Pdt Simple Pour Ligne Mobile
*
**/

function modal_mobileSave_pdtCtxe(csrf_value){
    var nomiOpe;
    var typePdt;
    var pdtNomi;
    if(document.getElementById('operateurlist_AddPdtMobile').value === '8'){
        nomiOpe = document.getElementById('operateurNomi_AddPdtMobile').value;
    }else{
        nomiOpe = document.getElementById('operateurlist_AddPdtMobile').value;
    }
    pdtNomi = document.getElementById('pdtNomi_AddPdtMobile').value;
    var form_data = {
        nomiOpe : nomiOpe,
        pdtNomi: pdtNomi,
        csrf_test_name: csrf_value,
    };
    $.ajax({
        url: site_url+"Mobile/add_pdtCtxe",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            //  alert(msg);
            if(msg !== '0'){
                $("#operateurMob").append($('<option>', {value: msg,text: nomiOpe}));
            }
            $('#pdtAjoutSmp_mob').modal("toggle");
            alert("Produit ajouté à la liste des choix");
        },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
    });
}
 /**
  * Modal Ajout de Pdts de type PACKAGE
  * Enregistre les nouveaux produits de type PACKAGE
  * @returns {[[Type]]} [[Description]]
  */
 function save_pdtPkgCtxe(){
     var nomiOpe;
     var typePdt;
     var pdtNomi;
     if(document.getElementById('operateurPkg').value === '8'){
         nomiOpe = document.getElementById('opeNomiPkg').value;
     }else{
         nomiOpe = document.getElementById('operateurPkg').value;
     }
     if(document.getElementById('package').value === '999'){
         ope = document.getElementById('pdtNomiPkg').value;
     }else{
         ope = document.getElementById('package').value;
     }
     var opt = document.getElementsByName('optPkg');
     var inputs = document.getElementsByClassName( 'input-sm optpkg' ),
         names  = [].map.call(inputs, function( input ) {
             return input.value;
         });
     var form_data = {
         nomiOpe : nomiOpe,
         pkg: ope,
         optpkg: names,
     };
     $.ajax({
         url: site_url+'Modal/add_pkgCtxe',
         type: 'POST',
         data: form_data,
         success: function(msg) {
             if(msg !== '0'){
                 $("#operateurLigne").append($('<option>', {value: msg,text: nomiOpe}));
             }
             alert("Produit ajouté à la liste des choix");
             $('#pdtAjoutPkg').modal("toggle");   
         },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
     });
 }
/**
 * Modal Ajout de Produits Simple pour ligne Fixe
 */
function modal_fixeSave_pdtCtxe(csrf_value){
        var nomiOpe;
        var typePdt;
        var pdtNomi;
         if(document.getElementById('operateurPdt4').value === '8'){
            nomiOpe = document.getElementById('opeNomiPdt4').value;
         }else{
             nomiOpe = document.getElementById('operateurPdt4').value;
         }
        typePdt = document.getElementById('type').value;
        pdtNomi = document.getElementById('pdtNomi').value;
       
        var form_data = {
            nomiOpe : nomiOpe,
            typePdt: typePdt,
            pdtNomi: pdtNomi,
             csrf_test_name: csrf_value,
        };
          $.ajax({
            url: site_url+"Site/add_pdtCtxe",
            type: 'POST',
            data: form_data,
            success: function(msg) {
              //  alert(msg);
               if(msg !== '0'){
                   $("#operateurLigne").append($('<option>', {value: msg,text: nomiOpe}));
               }
                document.getElementById('opeNomiPdt4').value='';
                document.getElementById('pdtNomi').value='';
                
                alert("Produit ajouté à la liste des choix");
                $('#pdtAjoutSmp').modal("toggle");
                //location.reload();
                
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
}
/**
*Fenetre Ajout de Conso pour ligne mobile
*
**/
/**
 * Ajouter des consommations à une ligne mobile
 */
function mobile_addConso(csrf_value){
    var $table = $('#mobile-table');
    var data = $table.bootstrapTable('getSelections');
        var ids = $.map(data, function (item) {
            return item.idMob;
        });
     var form_data = {
        idMob : ids,
        idConso: $('#conso').val(),
        Tarifs: $('#Mobile1TarifConso').val(),
        csrf_test_name: csrf_value,
    };
    if($('#Mobile1TarifConso').val() == ''){
        $('#alert-msgMob').html('<div class="alert alert-danger text-center">Champ vide !</div>');
       
    }else{
        $.ajax({
            url: site_url+'Mobile/add_conso',
            type: 'POST',
            data: form_data,
            success: function(msg) {
                if(msg == 'OK')
                    location.reload();
                else if (msg == 'NO')
                    $('#alert-msgMob').html('<div class="alert alert-danger text-center">Communication déjà presente.</div>');
                else
                    $('#alert-msgMob').html('<div class="alert alert-danger">' + msg + '</div>');
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
        return false;
    }
}
/**
 * Button Ajouter un type de conso
 */
function mobile_openModalTypeConso()
{
    $modalAjoutTypeConso = $('#ajoutTypeConsoMob');
    $modalAjoutTypeConso.modal('show');
}
/**
 * Ajouter un type de consommation pour Ligne MOBILE
 * @param   {[[Type]]} csrf_value [[Description]]
 * @returns {boolean}  [[Description]]
 */
function mobile_saveTypeConso(csrf_value)
{
    var form_data = {
        Conso: $('#consoNomi').val(),
        csrf_test_name: csrf_value,
    };
    if($('#consoNomi').val() == ''){
        $('#alert-msg4').html('<div class="alert alert-danger text-center">Champ vide !</div>');
       
    }else{
        $.ajax({
            url: site_url+'Mobile/add_type_conso',
            type: 'POST',
            data: form_data,
            success: function(msg) {
               $('#ajoutTypeConsoMob').modal('toggle');

               if(msg !== '0'){
                       $("#conso").append($('<option>', {value: msg,text: $('#consoNomi').val()}));
                   }
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
    return false;
   }    
}

/**
 *Modal d'ajout d'options sur ligne quant PACKAGE Selectionne
*
**/

function fixe_saveConsoPack(csrf_value,closeConso)
{
    var ids = $('#idFixe2').val();
    var form_data = {
        idFixe : ids,
        idConso: $('#packageOpt').val(),
        Tarifs: $('#Site1L1TarifConso2').val(),
        Qte: $('#Site1L1TarifQte').val(),
        csrf_test_name: csrf_value,
    };
     if($('#packageOpt').val() == 999){
        alert("Option Non valide");
    }else{
        if($('#Site1L1TarifConso2').val() == ''){
            $('#alert-msg4').html('<div class="alert alert-danger text-center">Champ vide !</div>');

        }else{
            $.ajax({
                url: site_url+'Site/add_conso',
                type: 'POST',
                data: form_data,
                success: function(msg) {
                    if(msg == 'OK'){
                        if(closeConso == 0){
                            location.reload();
                        }else{
                            $('#alert-msg4').html('<div class="alert alert-success text-center">Option Enregistrée !</div>');
                            document.getElementById('Site1L1TarifConso2').value="";
                            var tmp = document.getElementById('packageOpt');
                            if(tmp.selectedIndex == tmp.options.length - 2){
                                alert("Toutes les options ont été saisies");
                                location.reload();
                            }
                            document.getElementById('packageOpt').selectedIndex =  document.getElementById('packageOpt').selectedIndex+1; 
                            //document.getElementById('packageOpt').selectedIndex=0;
                            document.getElementById('Site1L1TarifQte').value="";
                        }
                        
                    }
                    else if (msg == 'NO')
                       $('#alert-msg4').html('<div class="alert alert-danger text-center">Option déjà presente.</div>');
                    else
                        $('#alert-msg4').html('<div class="alert alert-danger">' + msg + '</div>');
                },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
            });
            return false;
       }
    } 
}
/**
 * Ajouter des options sur Package
 * @param {[[Type]]} csrf_value [[Description]]
 */
function save_Opt(csrf_value){
        var idPack = document.getElementById('indic').value;
        var optNomi = document.getElementById('OptNomi').value;
       // alert("idPack : "+idPack+" optNomi : "+optNomi);
        var form_data = {
            pkg: idPack,
            optpkg: optNomi,
            csrf_test_name: csrf_value,
        };
        $.ajax({
            url: site_url+"Modal/add_optPkg",
            type: 'POST',
            data: form_data,
            success: function(msg) {
              if(msg !== '0'){
                   $("#packageOpt").append($('<option>', {value: msg,text: optNomi}));
               }
                alert("Option Ajoutée");
                $("#ajoutOptPkg").modal('toggle');
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
}
/**
 * Ajout la consommation sur la ligne fixe
 * @param   {[[Type]]} csrf_value 
 * @param   {[[Type]]} plus       Si = 1 ajouter une conso en plus
 * @returns {boolean}  [[Description]]
 */
function fixe_addConso(csrf_value,plus)
{
    
    var ids = $('#idFixe1').val();
    var form_data = {
        idFixe : ids,
        idConso: $('#consoF').val(),
        Tarifs: $('#Site1L1TarifConso1').val(),
        csrf_test_name: csrf_value,
    }; 
    if($('#Site1L1TarifConso1').val() == ''){
        $('#alert-msg3').html('<div class="alert alert-danger text-center">Champ vide !</div>');
       
    }else{
        $.ajax({
            url: site_url+'Site/add_conso',
            type: 'POST',
            data: form_data,
            success: function(msg) {
                if(msg == 'OK'){
                    if(plus == 0){
                        location.reload();
                    }else{
                        $('#alert-msg3').html('<div class="alert alert-success text-center">Consommation Enregistrée !</div>');
                        document.getElementById('Site1L1TarifConso1').value="";
                        document.getElementById('consoF').selectedIndex = 0; 
                    }
                }else{
                    if (msg == 'NO')
                    $('#alert-msg3').html('<div class="alert alert-danger text-center">Communication déjà presente.</div>');
                    else
                    $('#alert-msg3').html('<div class="alert alert-danger">' + msg + '</div>');
                } 
            },
         error: function( jqXHR, textStatus, errorThrown) {
                    alert( "Request failed status : "+jqXHR.status+" : textstat : " + textStatus +" errorThrown: "+errorThrown );
                }
        });
        return false;
   }
}