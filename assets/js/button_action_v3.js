/***************Version Mai 2018********************/
/**
 * Page Annuaire
 * Function qui actionne le bouton Ouvrir une Societe dans la vue Annuraire
 */
function annuaire_open()
{
   
    var $table = $('#tableClients');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    

    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/annuaire/details_client',
        data: 'id='+ids
       
    })
        .then(function (response) {
             document.getElementById('Ann_infoClient').innerHTML=response.data; 
        
        })
        .catch(function (error) {
            console.log(error.response);
        });
}

/**
 * Page Devis Supprime les devis
 * @returns {NULL} Si Devis non selectionne
 */
function devis_supprDevis()
{
   
    var $table = $('#tableDevis');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.numDev;
    });
    if(ids == 0){
        alert("Veuillez selectionner un devis");
        return; 
    }
    var r=confirm("Confirmer la suppression du devis ?");
    if(r){
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/devis/suppr_devis',
            data: 'id='+ids

        })
            .then(function (response) {
               location.reload();

            })
            .catch(function (error) {
                console.log(error.response);
            });
    }

}
/**
 * Page Contexte OUvre le contexte d'une société
 * @returns {NULL} Si Societe non selectionné
 */
function contexte_open()
{
    var $table = $('#tableClients');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
            return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    var idCont = $.map(data, function (item) {
        return item.idCont;
    });  
   loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/clients/load_client',
        data: 'id='+ids+'&idCont='+idCont
       
    })
        .then(function (response) {
            document.location="/index.php/contexte/index";   
        })
        .catch(function (error) {
            console.log(error.response);
        });
}
/**
 * Page Contexte Modifie les informations d'une société
 * @returns {[[Type]]} [[Description]]
 */
function contexte_modifSoc()
{
    var $table = $('#tableClients');
    $modalModif = $('#contexte_modalModif');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    var idCont = $.map(data, function (item) {
        return item.idCont;
    });  

    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/clients/load_client_modif',
        data: 'id='+ids+'&idCont='+idCont
       
    })
        .then(function (response) {
            document.getElementById("details").innerHTML = response.data; 
            $modalModif.modal('show');     
        })
        .catch(function (error) {
            console.log(error.respo,se);
        });
}
/**
 * Page Contexte Supprime le contexte et les informations d'une société
 * @returns {[[Type]]} [[Description]]
 */
function contexte_supprSoc()
{
    var $table = $('#tableClients');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner une Société");
        return; 
    }
    var r=confirm("Confirmer la suppression de la Société ?");
    if(r){
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/clients/suppr_client',
            data: 'id='+ids

        })
            .then(function (response) {
                document.location="/index.php/clients/index";
        })
            .catch(function (error) {
            console.log(error.response);
        });
    }
}

/**
 *BUTTON GESTION DES SITES DANS UN CONTEXTE
**/
/**
 * Bouton Ajout Site dans Page de contexte Via Modal
 */
function contexte_AddSite()
{
    
}
/**
 * Bouton Modifier un site page contexte
 */
function contexte_ModifSite()
{
    var $tableSite = $('#site-table'); 
    var $modalSite = $('#myModal_modif_site');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner un site");
        return; 
    }
    
    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/site/load_site',
        data: 'idSite='+ids
    })
        .then(function (response) {
        var json = response.data;
        $('#ModifadresseSite').val(json[0].SiteAdresse);
        $('#ModifCPSite').val(json[0].SiteCodePost);
        $('#ModifVilleSite').val(json[0].SiteVille);
        $modalSite.modal('show');
       
    })
        .catch(function (error) {
        console.log(error.response);
    });
    
    
}
function contexte_SupprSite()
{
    var $tableSite = $('#site-table'); 
    var $modalSite = $('#myModal_modif_site');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner un site");
        return; 
    }
    var r = confirm("Attention : Supprime définitivement le site ?");
    if (r == true) {
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/site/suppr_site',
            data: 'idSite='+ids
        })
            .then(function (response) {
            alert("Site Supprimé");
            location.reload();

        })
            .catch(function (error) {
            console.log(error.response);
        });
    }else{
        alert('Operation annulée');
        return false;
    }
    
}
function contexte_AddLine()
{
    var $tableSite = $('#site-table'); 
    var $modalLigne = $('#MyModalAjoutLigne');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    if(ids == 0){
        alert("Veuillez selectionner un site");
        return; 
    }
    $('#idSite').val(ids);
    $modalLigne.modal('show');
}
function contexte_SupprLine()
{
    var $tableSite = $('#site-table');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idFixe;
    });
    if(ids == 0){
        alert("Veuillez selectionner une ligne");
        return; 
    }
    var r = confirm("Attention : Supprime définitivement la ligne ?");
    if (r == true) {
       loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/site/suppr_line',
            data: 'idFixe='+ids
        })
            .then(function (response) {
            alert("Ligne Supprimé");
            location.reload();

        })
            .catch(function (error) {
            console.log(error.response);
        });
    }else{
        alert('Operation annulée');
        return false;
    }
        
}
function contexte_DetailsSite()
{
    var $tableSite = $('#site-table');
    var $modalDetails = $('#modalDetLigne');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    var idf = $.map(data, function (item) {
        return item.idFixe;
    });
    var idftmp = JSON.stringify(idf);
    var idstmp = JSON.stringify(ids);
    if(ids == 0){
        alert("Veuillez selectionner un site");
        $modalDetLigne.modal('toggle');
        return;
    }
    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/site/load_details',
        data: 'site='+idstmp+'&fixe='+idftmp
    })
        .then(function (response) {
        document.getElementById("txtHint1").innerHTML = response.data;
        $modalDetails.modal('show');
        
    })
        .catch(function (error) {
        console.log(error.response);
    });
}
/**
 * Fonction Ajout de Mobile Via le modal
 */
function contexte_AddMobLine()
{
}
/**
 * Bouton modifier une ligne Mobile
 * @returns {boolean} [[Description]]
 */
function contexte_ModifMobLine()
{
    var $table = $('#mobile-table');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile");
        return; 
    }
    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/mobile/load_mobile',
        data: 'idMob='+ids
    })
        .then(function (response) {
        var json = JSON.parse(reponse.data);
            $('#Mobile1NbModif').val(json[0].MobileNumero);
            $('#Mobile1DDModif').val(json[0].MobileDateDeb);
            $('#Mobile1DFModif').val(json[0].MobileDateFin);
            $('#Mobile1ForfaitModif').val(json[0].MobileForfTarifsHT);
            $('#Mobile1OptionModif').val(json[0].MobileDetailsForf);
            $modalMobile.modal('show');
        
    })
        .catch(function (error) {
        console.log(error.response);
    });
}

function contexte_SupprMobile()
{
    var $table = $('#mobile-table');
   
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile");
        return; 
    }
    var r = confirm("Attention : Supprime définitivement le Mobile ?");
    if (r == true) {
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/mobile/suppr_mobile',
            data: 'idMob='+ids
        })
            .then(function (response) {
            alert("Mobile Supprimé");
            location.reload();

        })
            .catch(function (error) {
            console.log(error.response);
        });
        
    }else{
        alert('Operation annulée');
        return false;
    }
    
}

function contexte_AddConsoMob()
{
    var $table = $('#mobile-table');
    var $modalMobileConso = $('#ajoutConsoMob');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile");
        return; 
    }
    var nume = $.map(data, function (item) {
        return item.mobNum;
    });
    document.getElementById('infoMob').innerHTML='Numero : '+nume;
    $modalMobileConso.modal('show');
          
}
function contexte_SupprConsoMob()
{
    var $table = $('#mobile-table');
    var data = $table.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.idMob;
    });
    if(ids == 0){
        alert("Veuillez selectionner un mobile rattaché à la consommation");
        return; 
    }
    var idC = $.map(data, function (item) {
        return item.idConso;
    });
    var r = confirm("Attention : Supprime définitivement la consommation ?");
    if (r == true) {
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/mobile/suppr_conso',
            data: 'idMob='+ids+'&idConso='+idC
        })
            .then(function (response) {
            location.reload();

        })
            .catch(function (error) {
            console.log(error.response);
        });
    }else{
        alert('Opération Annulée')
        return false;
    }
    
}
/**
 * Ajout de produit supplementaire sur modal Ajout de mobile
 */
function open_pdt_smp_mob()
{
    var $modalAjout_smp_mob = $('#pdtAjoutSmp_mob');
    $modalAjout_smp_mob.modal('show');
}
/**
 *Bouton Modal
 *Ajout de Site
 *
**/
    /**
     * Bouton valider ajout de site
     * @param   {[[Type]]} csrf_value Id Unique de saisie de formulaire
     * @returns {boolean}  [[Description]]
     */
    function modal_ajoutSite(csrf_value)
{
    
 
    var formData = new FormData();
    formData.append("ASite",$('#adresseSite').val());
    formData.append("CPSite",$('#CPSite').val());
    formData.append("VilleSite",$('#VilleSite').val());
    formData.append("csrf_test_name",csrf_value);
    loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Modal_Site/submit',
            data: formData
        })
            .then(function (response) {
            if (response.data == 'YES')location.reload();
            else $('#alert-msg').html('<div class="alert alert-danger">' + response.data + '</div>');
                
        })
            .catch(function (error) {
            console.log(error.response);
        });
}
/**
 * [[Description]]
 * @param {[[Type]]} csrf_value [[Description]]
 */
function modal_modifSite(csrf_value)
{
    var formData = new FormData();
    formData.append("ASite",$('#ModifadresseSite').val());
    formData.append("CPSite",$('#ModifCPSite').val());
    formData.append("VilleSite",$('#ModifVilleSite').val());
    formData.append("csrf_test_name",csrf_value);
    
    loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Modal_Site/update',
            data: formData
        })
            .then(function (response) {
            if (response.data == 'YES')location.reload();
            else $('#alert-msg').html('<div class="alert alert-danger">' + response.data + '</div>');
                
        })
            .catch(function (error) {
            console.log(error.response);
        });
}


/**
* Modal Ajout de Ligne
**/

function modal_ligneAjout(csrf_value)
{
    var $modalConsoPkg = $('#ajoutConsoPkg');
    var $modalLigne = $('#MyModalAjoutLigne');
    var $tableSite = $('#site-table');    
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    var typeNum = 0;
    if(document.getElementById('cboxNDI').checked)typeNum=1;
    if(document.getElementById('cboxSpecial').checked)typeNum=2;
    var formData = new FormData();
    formData.append("idSite",ids);
    formData.append("idConso",$('#produit').val());
    formData.append("Tarifs",$('#produit').val());
    formData.append("Qte",$('#SiteL1TZero').val());
    formData.append("Num",$('#Site1L1Nb').val());
    formData.append("Tarifs",$('#Site1L1Tarif').val());
    formData.append("Type",typeNum);
    formData.append("csrf_test_name",csrf_value);
    if(document.getElementById('indic').value === '0'){
        //INSERT SUR PDT NORMAL AVEC VERIF TARIFS NON VIDE
        if($('#Site1L1Tarif').val() == ''){
            $('#alert-msg2').html('<div class="alert alert-danger text-center">Erreur de Saisie - veuillez rentrer un tarif!</div>');
        }else{
            loadProgressBar()
            axios({
                method: 'post',
                url: '/index.php/Site/add_ligne',
                data: formData
            })
                .then(function (response) {
                     if(response.data == '0'){
                            alert('veuillez vérifier les champs');
                        }else{
                            location.reload();
                        }
            })
                .catch(function (error) {
                console.log(error.response);
            });

        }
    }else{
        var idPack = new FormData;
        idPack.append("id",document.getElementById('indic').value);
        axios.all([
            axios.post('/index.php/Site/add_ligne',formData),
            axios.post('/index.php/Modal/opt_call',idPack)
        ])
            .then(axios.spread((addLiRes, optRes) => {
                if(addLiRes.data == '0'){
                    alert('veuillez vérifier les champs');
                }else{
                    $modalLigne.modal("toggle");
                    $('#opt_pkg').html(optRes.data);
                    document.getElementById('infoFixeF').innerHTML='Numero : '+$('#Site1L1Nb').val();
                    $('#idFixe2').val(addLiRes.data);
                    $modalConsoPkg.modal('show');
                }
        }));
    }
}

    

/**
 * Enregistre la ligne et lance la fenetre d'ajout de consommation
 * @param   {[[Type]]} csrf_value [[Description]]
 * @returns {boolean}  [[Description]]
 */
function modal_LigneAddConso(csrf_value)
{
    var $tableSite = $('#site-table'); 
    var  $modalConsoF = $('#ajoutConso');
    var $modalLigne = $('#MyModalAjoutLigne');
    var data = $tableSite.bootstrapTable('getSelections');
    var ids = $.map(data, function (item) {
        return item.id;
    });
    var formData = new FormData();
    formData.append("idSite",ids);
    formData.append("idConso",$('#produit').val());
    formData.append("Tarifs",$('#produit').val());
    formData.append("Qte",$('#SiteL1TZero').val());
    formData.append("Num",$('#Site1L1Nb').val());
    formData.append("Tarifs",$('#Site1L1Tarif').val());
    formData.append("Type",$('#cboxNDI').val());
    formData.append("csrf_test_name",csrf_value);
    
    if($('#Site1L1Tarif').val() == ''){
        $('#alert-msg2').html('<div class="alert alert-danger text-center">Erreur de saisie</div>');
    }else{
        
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Site/add_ligne',
            data: formData
        })
            .then(function (response) {
                $modalLigne.modal("toggle");
                document.getElementById('infoFixe').innerHTML='Numero : '+$('#Site1L1Nb').val();
                $('#idFixe1').val(response.data);
                $modalConsoF.modal("show");
        })
            .catch(function (error) {
            console.log(error.response);
        });
    }
}
function modal_LigneOpenAddPkg(){
    var $modalAjout3 = $('#pdtAjout');
    $modalAjout3.modal('show');     
}

/**
*Modal Ajout de Pdt Simple Pour Ligne Mobile
*
**/

function modal_mobileSave_pdtCtxe(csrf_value){
    var nomiOpe;
    var typePdt;
    var pdtNomi;
    if(document.getElementById('operateurlist_AddPdtMobile').value === '99'){
        nomiOpe = document.getElementById('operateurNomi_AddPdtMobile').value;
    }else{
        nomiOpe = document.getElementById('operateurlist_AddPdtMobile').value;
    }
    pdtNomi = document.getElementById('pdtNomi_AddPdtMobile').value;
   
    var formData = new FormData();
    formData.append("nomiOpe",nomiOpe);
    formData.append("pdtNomi",pdtNomi);
    formData.append("csrf_test_name",csrf_value);
    
     loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/Mobile/add_pdtCtxe',
        data: formData
    })
        .then(function (response) {
        //  alert(msg);
        if(response.data !== '0'){
            $("#operateurMob").append($('<option>', {value: response.data,text: nomiOpe}));
        }
        $('#pdtAjoutSmp_mob').modal("toggle");
        alert("Produit ajouté à la liste des choix");
    })
        .catch(function (error) {
        console.log(error.response);
    });
}
 /**
  * Modal Ajout de Pdts de type PACKAGE
  * Enregistre les nouveaux produits de type PACKAGE
  * @returns {[[Type]]} [[Description]]
  */
 function save_pdtPkgCtxe(){
     var nomiOpe;
     var typePdt;
     var pdtNomi;
     if(document.getElementById('operateurPkg').value === '99'){
         nomiOpe = document.getElementById('opeNomiPkg').value;
     }else{
         nomiOpe = document.getElementById('operateurPkg').value;
     }
     if(document.getElementById('package').value === '999'){
         ope = document.getElementById('pdtNomiPkg').value;
     }else{
         ope = document.getElementById('package').value;
     }
     var opt = document.getElementsByName('optPkg');
     var inputs = document.getElementsByClassName( 'input-sm optpkg' ),
         names  = [].map.call(inputs, function( input ) {
             return input.value;
         });
    
     var formData = new FormData();
     formData.append("nomiOpe",nomiOpe);
     formData.append("pkg",ope);
     formData.append("optpkg",names);
     
     loadProgressBar()
     axios({
        method: 'post',
        url: '/index.php/Modal/add_pkgCtxe',
        data: formData
     })
        .then(function (response) {
         if(response.data !== '0'){
             console.log(response.data);
                if(document.getElementById('operateurPkg').value === '99'){
                    $("#operateurLigne").append($('<option>', {value: response.data,text: nomiOpe}));
                }
            }
        alert("Produit ajouté à la liste des choix");
        $('#pdtAjoutPkg').modal("toggle");
    })
        .catch(function (error) {
          if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
          } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
          }
          console.log(error.config);
        });
 }
/**
 * Modal Ajout de Produits Simple pour ligne Fixe
 */
function modal_fixeSave_pdtCtxe(csrf_value){
        var nomiOpe;
        var typePdt;
        var pdtNomi;
         if(document.getElementById('operateurPdt4').value === '99'){
            nomiOpe = document.getElementById('opeNomiPdt4').value;
         }else{
             nomiOpe = document.getElementById('operateurPdt4').value;
         }
        typePdt = document.getElementById('type').value;
        pdtNomi = document.getElementById('pdtNomi').value;
    
    var formData = new FormData();
    formData.append("nomiOpe",nomiOpe);
    formData.append("typePdt",typePdt);
    formData.append("pdtNomi",pdtNomi);
    formData.append("csrf_test_name",csrf_value);
    loadProgressBar()
    axios({
        method: 'post',
        url: '/index.php/Site/add_pdtCtxe',
        data: formData
    })
        .then(function (response) {
        if(response.data !== '0'){
                if(document.getElementById('operateurPdt4').value === '99')$("#operateurLigne").append($('<option>', {value: response.data,text: nomiOpe}));
        }
        document.getElementById('opeNomiPdt4').value='';
        document.getElementById('pdtNomi').value='';
                    
        alert("Produit ajouté à la liste des choix");
        $('#pdtAjoutSmp').modal("toggle");
    })
        .catch(function (error) {
        console.log(error.response);
    });
        
}
/**
*Fenetre Ajout de Conso pour ligne mobile
*
**/
/**
 * Ajouter des consommations à une ligne mobile
 */
function mobile_addConso(csrf_value){
    var $table = $('#mobile-table');
    var data = $table.bootstrapTable('getSelections');
        var ids = $.map(data, function (item) {
            return item.idMob;
        });
     var formData = new FormData();
    formData.append("idMob",ids);
    formData.append("idConso",$('#conso').val());
    formData.append("Tarifs",$('#Mobile1TarifConso').val());
    formData.append("csrf_test_name",csrf_value);
    if($('#Mobile1TarifConso').val() == ''){
        $('#alert-msgMob').html('<div class="alert alert-danger text-center">Champ vide !</div>');
       
    }else{
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Mobile/add_conso',
            data: formData
        })
        .then(function (response) {
            if(response.data == 'OK'){
                 location.reload();
            }else{
                if(response.data == 'NO'){
                    $('#alert-msgMob').html('<div class="alert alert-danger text-center">Communication déjà presente.</div>');
                }else{
                    $('#alert-msgMob').html('<div class="alert alert-danger">' + response.data + '</div>');
                }
            } 
        })
            .catch(function (error) {
            console.log(error.response);
        });
    }
}
/**
 * Button Ajouter un type de conso
 */
function mobile_openModalTypeConso()
{
    $modalAjoutTypeConso = $('#ajoutTypeConsoMob');
    $modalAjoutTypeConso.modal('show');
}
/**
 * Ajouter un type de consommation pour Ligne MOBILE
 * @param   {[[Type]]} csrf_value [[Description]]
 * @returns {boolean}  [[Description]]
 */
function mobile_saveTypeConso(csrf_value)
{
    var formData = new FormData();
    formData.append("Conso",$('#consoNomi').val());
    formData.append("csrf_test_name",csrf_value);
    if($('#consoNomi').val() == ''){
        $('#alert-msg4').html('<div class="alert alert-danger text-center">Champ vide !</div>');
       
    }else{
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Mobile/add_type_conso',
            data: formData
        })
        .then(function (response) {
            $('#ajoutTypeConsoMob').modal('toggle');
            if(response.data !== '0'){
                $("#conso").append($('<option>', {value: response.data,text: $('#consoNomi').val()}));
            }
        })
            .catch(function (error) {
            console.log(error.response);
        });
   }    
}

/**
 *Modal d'ajout d'options sur ligne quant PACKAGE Selectionne
*
**/

function fixe_saveConsoPack(csrf_value,closeConso)
{
    var formData = new FormData();
    formData.append("idFixe",$('#idFixe2').val());
    formData.append("idConso",$('#packageOpt').val());
    formData.append("Tarifs",$('#Site1L1TarifConso2').val());
    formData.append("Qte",$('#Site1L1TarifQte').val());
    formData.append("csrf_test_name",csrf_value);
    
  
     if($('#packageOpt').val() == 999){
        alert("Option Non valide");
    }else{
        if($('#Site1L1TarifConso2').val() == ''){
            $('#alert-msg4').html('<div class="alert alert-danger text-center">Champ vide !</div>');

        }else{
            
              loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Site/add_conso',
            data: formData
        })
        .then(function (response) {
           if(response.data == 'OK'){
               if(closeConso == 0){
                   location.reload();
               }else{
                   $('#alert-msg4').html('<div class="alert alert-success text-center">Option Enregistrée !</div>');
                   document.getElementById('Site1L1TarifConso2').value="";
                   var tmp = document.getElementById('packageOpt');
                   if(tmp.selectedIndex == tmp.options.length - 2){
                       alert("Toutes les options ont été saisies");
                       location.reload();
                   }
                   document.getElementById('packageOpt').selectedIndex =  document.getElementById('packageOpt').selectedIndex+1; 
                   document.getElementById('Site1L1TarifQte').value="";
               }
                        
           }
            else if (response.data == 'NO')
                $('#alert-msg4').html('<div class="alert alert-danger text-center">Option déjà presente.</div>');
            else
                $('#alert-msg4').html('<div class="alert alert-danger">' + response.data + '</div>');
        })
            .catch(function (error) {
            console.log(error.response);
        });
            
            
       }
    } 
}
/**
 * Ajouter des options sur Package
 * @param {[[Type]]} csrf_value [[Description]]
 */
function save_Opt(csrf_value){
    
    
    var formData = new FormData();
    formData.append("pkg",document.getElementById('indic').value);
    formData.append("optpkg",document.getElementById('OptNomi').value);
    formData.append("csrf_test_name",csrf_value);
      
    
     loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Modal/add_optPkg',
            data: formData
        })
        .then(function (response) {
             if(response.data !== '0'){
                   $("#packageOpt").append($('<option>', {value: response.data,text: document.getElementById('OptNomi').value}));
               }
                alert("Option Ajoutée");
                $("#ajoutOptPkg").modal('toggle');
        })
            .catch(function (error) {
          if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
          } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
          }
          console.log(error.config);
        });
        
}
/**
 * Ajout la consommation sur la ligne fixe
 * @param   {[[Type]]} csrf_value 
 * @param   {[[Type]]} plus       Si = 1 ajouter une conso en plus
 * @returns {boolean}  [[Description]]
 */
function fixe_addConso(csrf_value,plus)
{
    var formData = new FormData();
    formData.append("idFixe",$('#idFixe1').val());
    formData.append("idConso",$('#consoF').val());
    formData.append("Tarifs",$('#Site1L1TarifConso1').val());
    formData.append("csrf_test_name",csrf_value);
    if($('#Site1L1TarifConso1').val() == ''){
        $('#alert-msg3').html('<div class="alert alert-danger text-center">Champ vide !</div>');
       
    }else{
        loadProgressBar()
        axios({
            method: 'post',
            url: '/index.php/Site/add_conso',
            data: formData
        })
        .then(function (response) {
             if(response.data == 'OK'){
                    if(plus == 0){
                        location.reload();
                    }else{
                        $('#alert-msg3').html('<div class="alert alert-success text-center">Consommation Enregistrée !</div>');
                        document.getElementById('Site1L1TarifConso1').value="";
                        document.getElementById('consoF').selectedIndex = 0; 
                    }
                }else{
                    if (response.data == 'NO')
                    $('#alert-msg3').html('<div class="alert alert-danger text-center">Communication déjà presente.</div>');
                    else
                    $('#alert-msg3').html('<div class="alert alert-danger">' + response.data + '</div>');
                } 
        })
            .catch(function (error) {
            console.log(error.response);
        });
   }
}
    

    /**
     * Fonction Request : Mise à jour DropDown
     * @param {object}   oSelect Select de choix primaire
     * @param {[[Type]]} oDrop   Select à mettre à jour
     * @param {[[Type]]} url_cible url serveur
     */
function request(oSelect,oDrop,url_cible) {
    var value = oSelect.options[oSelect.selectedIndex].value;
    loadProgressBar()
    axios({
        method: 'post',
        url: url_cible,
        data: "idOpe=" + value
    })
        .then(function (response) {
        oDrop.html(response.data);
    })
        .catch(function (error) {
        console.log(error.response);
    });
}

function surligne(champ, erreur)
{
   if(erreur)
      champ.style.backgroundColor = "#fba";
   else
      champ.style.backgroundColor = "";
}