<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function list_fact($id)
    {
        
        $this->db->select('*');
        $this->db->from('Factures');
        $this->db->from('Utilisateurs');
        $this->db->where('Utilisateurs_idUtilisateurs',$id);
        $this->db->where('Utilisateurs.idUtilisateurs = Factures.Utilisateurs_idUtilisateurs');
        return $this->db->get();
    }
}
?>