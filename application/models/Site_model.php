<?php

class Site_Model extends CI_Model
{

     public function list_site($contexte=1)
     {
         /*DB QUERY
         $rqt="SELECT * FROM Site   LEFT Join Fixe On Site.idSite=Fixe.Site_idSite LEFT JOIN PdtFixe On Fixe.PdtFixe_idPdtFixe = PdtFixe.idPdtFixe LEFT JOIN Operateur On PdtFixe.Operateur_idOperateur=Operateur.idOperateur LEFT JOIN FixeType ON Fixe.FixeType_idFixeType=FixeType.idFixeType WHERE Site.Contexte_idContexte=2;
         */
         
        // $contexte = $contexte['idCont'][0];
         $this->db->select('*');
         $this->db->from('Site');
         $this->db->join('Fixe', 'Fixe.Site_idSite = Site.idSite', 'left');
         $this->db->join('PdtFixe', 'Fixe.PdtFixe_idPdtFixe = PdtFixe.idPdtFixe', 'left');
         $this->db->join('Operateur', 'PdtFixe.Operateur_idOperateur=Operateur.idOperateur', 'left');
         $this->db->where('Contexte_idContexte',$contexte);
        return $this->db->get();
     }
    public function insert_site($site)
    {
        if($this->db->insert('Site', $site))
        {
            return true;
        }
        return false;
    }
    public function get_site($idSite)
    {
        $this->db->select('*');
        $this->db->from('Site');
        $this->db->where('idSite',$idSite);
        return $this->db->get();
    }
    public function get_id($idS,$adre)
    {
        return '1';
    }
    public function update_site($site)
    {
        $this->db->set($site);
        $this->db->where('idSite', $site->idSite);
        return $this->db->update('Site');
     
    }
    public function suppr_site($idSite){
        $this->db->where('idSite', $idSite);
        return $this->db->delete('Site');
    }
    function get_produit($idOperateur)
    {
    
        
        $this->db->select('*');
        $this->db->from('PdtFixe');
        $this->db->from('Operateur');
        $this->db->from('PdtFixeType');
        $this->db->where('Operateur_idOperateur',$idOperateur);
        $wh ='PdtFixe.Operateur_idOperateur=Operateur.idOperateur';
        $this->db->where($wh);
        $wh ='PdtFixe.PdtFixeType_idPdtFixeType=PdtFixeType.idPdtFixeType';
        $this->db->where($wh);
        
        $this->db->order_by('PdtFixe.Operateur_idOperateur');
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idPdtFixe);
            array_push($stat_name, $result[$i]->PdtFixeTypeNomi." - ".$result[$i]->PdtFixeNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    function insert_ligne($ligne)
    {
        $this->db->insert('Fixe', $ligne);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    function check_ligne($idSite)
    {
        $this->db->select('FixeNumero');
        $this->db->from('Fixe');
        $this->db->where('Site_idSite',$idSite);
        $wh ='FixeNumero < 100';
        $this->db->where($wh);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
   function suppr_line($id)
   {
        $this->db->where('idFixe', $id);
        return $this->db->delete('Fixe');
   }
     function get_conso()
    {
        $this->db->select('idConsoType');
        $this->db->select('ConsoTypeNomi');
        $this->db->from('ConsoType');
        $this->db->where('ConsoTypeFixe','1');
        $this->db->where('PdtFixe_idPdtFixe',NULL);
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idConsoType);
            array_push($stat_name, $result[$i]->ConsoTypeNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
     function add_conso($dataConso)
    {
      // print_r($dataConso);
        /*
        Array
(
    [Mobile_idMobile] => 1
    [ConsoType_idConsoType] => 5
    [ConsoMobileTarifs] => 12
)
        
        
        */
        $this->db->select('*');
        $this->db->from('ConsoFixe');
        $this->db->where('Fixe_idFixe',$dataConso['Fixe_idFixe']);
        $this->db->where('ConsoType_idConsoType', $dataConso['ConsoType_idConsoType']);
        
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
            return 0;
        }else{
          $this->db->insert('ConsoFixe', $dataConso);
            return 1;
        }
    }
    public function get_tarifs($site,$fixe)
    {
        // $sql="SELECT *, ROUND(FixeTarifsHT,2) FROM Fixe WHERE idFixe ='".$fixe[$k]."' AND Site_idSite=".$site[$j];
        $this->db->select('*');
        $this->db->select('ROUND(FixeTarifsHT,2) AS T');
        $this->db->from('Fixe');
         
        $this->db->where('idFixe',$fixe);
        $this->db->where('Site_idSite',$site);
        return $this->db->get();
    }
    
    function get_typePdt()
    {
    
        
        $this->db->select('*');
        $this->db->from('PdtFixeType');
        
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idPdtFixeType);
            array_push($stat_name, $result[$i]->PdtFixeTypeNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    function insert_PdtCtxe($data){
        $str = $this->db->insert_string('PdtFixe', $data);
        $this->db->query($str);
        
    }
    function insert_Ope($nomi){
        $data = array('idOperateur' => 'NULL', 'OperateurNomi' => $nomi);
        $str = $this->db->insert_string('Operateur', $data);
        $this->db->query($str);
        return $this->db->insert_id();
    }
     function get_operateur()     
    { 
        $this->db->select('idOperateur');
        $this->db->select('OperateurNomi');
        $this->db->from('Operateur');
        $query = $this->db->get();
        $result = $query->result();

    
        $stat_id = array(0);
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idOperateur);
            array_push($stat_name, $result[$i]->OperateurNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }

}

?>