<?php

class Mobile_Model extends CI_Model
{

     public function list_mobile($contexte)
     {
         /*DB QUERY
      SELECT * FROM Mobile  LEFT JOIN ConsoMobile ON Mobile.idMobile = ConsoMobile.Mobile_idMobile LEFT JOIN ConsoType on ConsoType_idConsoType = ConsoType.idConsoType LEFT JOIN PdtMobile ON Mobile.PdtMobile_idPdtMobile = PdtMobile.idPdtMobile LEFT JOIN Operateur ON PdtMobile.Operateur_idOperateur = Operateur.idOperateur WHERE Mobile.Contexte_idContexte=".$id
         */
         
        // $contexte = $contexte['idCont'][0];
         $this->db->select('*');
         $this->db->from('Mobile');
         $this->db->join('ConsoMobile', 'Mobile.idMobile = ConsoMobile.Mobile_idMobile', 'left');
         $this->db->join('ConsoType', 'ConsoType_idConsoType = ConsoType.idConsoType', 'left');
         $this->db->join('PdtMobile', 'Mobile.PdtMobile_idPdtMobile = PdtMobile.idPdtMobile', 'left');
         $this->db->join('Operateur', 'PdtMobile.Operateur_idOperateur = Operateur.idOperateur', 'left');
         $this->db->where('Contexte_idContexte',$contexte);
         //return $this->db->get_compiled_select();
         return $query = $this->db->get();
         //return $query->result();
     }
     function get_operateur()     
    { 
        $this->db->select('idOperateur');
        $this->db->select('OperateurNomi');
        $this->db->from('Operateur');
        $wh ='Operateur.idOperateur <> 99';
        $this->db->where($wh);
        $query = $this->db->get();
        $result = $query->result();

    
        $stat_id = array(0);
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idOperateur);
            array_push($stat_name, $result[$i]->OperateurNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    function get_produit($idOperateur)
    {
        /*
        $sql1  = "SELECT idPdtMobile,PdtMobileNomi "; 
       $sql1 .= "FROM PdtMobile "; 
       $sql1 .= "WHERE Operateur_idOperateur='".$enr[0]."'"; 
       $sql1 .= "ORDER BY PdtMobileNomi";
        $sql = "SELECT idPdtMobile, PdtMobileNomi FROM PdtMobile WHERE Operateur_idOperateur = ? ORDER BY PdtMobileNomi";
        $this->db->query($sql, array($idOperateur));
        */
        /*$this->db->select('idPdtMobile');
        $this->db->select('PdtMobileNomi');
        $this->db->from('PdtMobile');
        $this->db->where('Operateur_idOperateur',$idOperateur);
        $this->db->order_by('PdtMobileNomi');
        $query = $this->db->get();*/
        $sql = "SELECT idPdtMobile, PdtMobileNomi FROM PdtMobile WHERE Operateur_idOperateur = ? ORDER BY PdtMobileNomi";
        $query = $this->db->query($sql, array($idOperateur));
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array(0);
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idPdtMobile);
            array_push($stat_name, $result[$i]->PdtMobileNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    function insert_mobile($mobile)
    {
        $this->db->insert('Mobile', $mobile);
    }
    public function get_mobile($idMob)
    {
        $this->db->select('*');
        $this->db->from('Mobile');
        $this->db->where('idMobile',$idMob);
        return $this->db->get();
    }
     public function suppr_mobile($idMob){
        $this->db->where('idMobile', $idMob);
        return $this->db->delete('Mobile');
    }
    public function check_mobile($id)
    {
        $this->db->select('MobileNumero');
        $this->db->from('Mobile');
        $this->db->where('Contexte_idContexte',$id);
        $wh ='MobileNumero < 100';
        $this->db->where($wh);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    
    }
    function get_conso()
    {
        $this->db->select('idConsoType');
        $this->db->select('ConsoTypeNomi');
        $this->db->from('ConsoType');
        $this->db->where('ConsoTypeFixe','0');
     
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idConsoType);
            array_push($stat_name, $result[$i]->ConsoTypeNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    function add_conso($dataConso)
    {
      // print_r($dataConso);
        /*
        Array
(
    [Mobile_idMobile] => 1
    [ConsoType_idConsoType] => 5
    [ConsoMobileTarifs] => 12
)
        
        
        */
        $this->db->select('*');
        $this->db->from('ConsoMobile');
        $this->db->where('Mobile_idMobile',$dataConso['Mobile_idMobile']);
        $this->db->where('ConsoType_idConsoType', $dataConso['ConsoType_idConsoType']);
        
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
            return 0;
        }else{
          $this->db->insert('ConsoMobile', $dataConso);
            return 1;
        }
    }
    
    function suppr_conso($data)
    {
        $this->db->where('Mobile_idMobile', $data['idMob']);
        $this->db->where('ConsoType_idConsoType', $data['idConso']);
        return $this->db->delete('ConsoMobile');
    }
     function insert_Ope($nomi){
        $data = array('idOperateur' => 'NULL', 'OperateurNomi' => $nomi);
        $str = $this->db->insert_string('Operateur', $data);
        $this->db->query($str);
        return $this->db->insert_id();
    }
    function insert_PdtCtxe($data){
        $str = $this->db->insert_string('PdtMobile', $data);
        $this->db->query($str);
        
    }
    function add_typeConso($data)
    {
        $this->db->insert('ConsoType', $data);
        return $this->db->insert_id();
    }
   
}

?>