<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categorie_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function get_PdtFixe($id)
    {
        /*
        DB QUERY
        SELECT * FROM Produit LEFT JOIN Operateur ON Produit.Operateur_idOperateur=Operateur.idOperateur WHERE ProduitNomi NOT IN ('Mobile / GSM')
        */
        
        $this->db->select('idProduit');
        $this->db->select('ProduitNomi');
        $this->db->from('Produit');
        $this->db->join('Operateur', 'Produit.Operateur_idOperateur=Operateur.idOperateur', 'left');
        $this->db->where('Produit.Operateur_idOperateur',$id);
        $this->db->where_not_in('ProduitNomi','Mobile / GSM');
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idProduit);
            array_push($stat_name, $result[$i]->ProduitNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    public function get_PdtMobile($id)
    {
        $this->db->select('idProduit');
        $this->db->select('ProduitNomi');
        $this->db->from('Produit');
        $this->db->where('ProduitNomi','Mobile / GSM');
        $query = $this->db->get();
        $result = $query->result();
        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idProduit);
            array_push($stat_name, $result[$i]->ProduitNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    
}