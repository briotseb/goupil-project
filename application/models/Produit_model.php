<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produit_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_Produit($id)
    {
        //$sql = "SELECT * From PdtFixe, Operateur WHERE idPdtFixe ='".$row["PdtFixe_idPdtFixe"]."' AND Operateur_idOperateur = Operateur.idOperateur";
        
        $this->db->select('*');
        $this->db->from('PdtFixe');
        $this->db->from('Operateur');
        $this->db->where('idPdtFixe',$id);
        $wh ="Operateur_idOperateur = Operateur.idOperateur";
        $this->db->where($wh);
        return $this->db->get();
    }
    function get_pkg($id)
    {
        //SELECT * FROM `PdtFixe` WHERE PdtFixeType_idPdtFixeType=3 AND Operateur_idOperateur = $id
        $this->db->select('*');
        $this->db->from('PdtFixe');
       
        $this->db->where('PdtFixeType_idPdtFixeType',3);
        $this->db->where('Operateur_idOperateur',$id);
      
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('0');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idPdtFixe);
            array_push($stat_name, $result[$i]->PdtFixeNomi);
        }
        array_push($stat_id, '999');
        array_push($stat_name, 'Saisir un nouveau package');
        
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    function get_opt_pkg($id)
    {
        //SELECT * FROM PdtFixe,ConsoType WHERE idPdtFixe = 83 AND ConsoType.PdtFixe_idPdtFixe = PdtFixe.idPdtFixe
        $this->db->select('*');
        $this->db->from('ConsoType');
        $this->db->where('ConsoType.PdtFixe_idPdtFixe',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function add_ope($id)
    {
        
       $this->db->select('idOperateur');
        $this->db->from('Operateur');
        $this->db->where_in('idOperateur',$id);
        $this->db->or_where_in('OperateurNomi',$id);
        
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $result=$query->result();
            return $result[0]->idOperateur;
        }else{
            $data = array(
                'idOperateur' => NULL,
                'OperateurNomi' => $id
            );

            $this->db->insert('Operateur', $data);
            return $this->db->insert_id();
        }
    }
    /**
     * Insertion d'un package avec l'operateur fournie. Si package deja existant renvoie son id sinon il effectue
     * l'insertion et renvoie l'id créé
     * @param  [[Type]] $idO   [[Description]]
     * @param  [[Type]] $idPkg [[Description]]
     * @return [[Type]] [[Description]]
     */
    public function add_pkg($idO,$idPkg)
    {
        $this->db->select('idPdtFixe');
        $this->db->from('PdtFixe');
        $this->db->where('Operateur_idOperateur',$idO);
        $this->db->where('PdtFixeType_idPdtFixeType',3);
        
        $this->db->where_in('idPdtFixe',$idPkg);
        $this->db->or_where_in('PdtFixeNomi',$idPkg);
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $result=$query->result();
            return $result[0]->idPdtFixe;
        }else{
            $data = array(
                'idPdtFixe' => NULL,
                'PdtFixeNomi' => $idPkg,
                'PdtFixeRq' => 'user',
                'Operateur_idOperateur' =>$idO,
                'PdtFixeType_idPdtFixeType' => 3
            );

            $this->db->insert('PdtFixe', $data);
            return $this->db->insert_id();
        }
    }
    public function add_optPkg($idO,$idP,$optPkg)
    {
        foreach($optPkg as $o)
        {
            $data = array(
                'idConsoType' => NULL,
                'ConsoTypeNomi' => $o,
                //ConsoTypeFixe : 0 => conso mobile, 1=> conso fixe
                'ConsoTypeFixe' => 1,
                'PdtFixe_idPdtFixe' =>$idP
            );
            $this->db->insert('ConsoType',$data);
        }
    }
    function get_conso($id)
    {
        $this->db->select('idConsoType');
        $this->db->select('ConsoTypeNomi');
        $this->db->from('ConsoType');
        $this->db->where('ConsoTypeFixe','1');
        $this->db->where('PdtFixe_idPdtFixe',$id);
     
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array();
        $stat_name = array();

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idConsoType);
            array_push($stat_name, $result[$i]->ConsoTypeNomi);
        }
        $statut_result = array_combine($stat_id, $stat_name);
        return $statut_result;
    }
    
      public function add_optPkgSeul($idP,$optPkg)
    {
       
            $data = array(
                'idConsoType' => NULL,
                'ConsoTypeNomi' => $optPkg,
                //ConsoTypeFixe : 0 => conso mobile, 1=> conso fixe
                'ConsoTypeFixe' => 1,
                'PdtFixe_idPdtFixe' =>$idP
            );
            $this->db->insert('ConsoType',$data);
          return $this->db->insert_id();
        }
    
    
    
}