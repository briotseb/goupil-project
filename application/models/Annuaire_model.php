<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Annuaire_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
     public function list_clients($id_user)
     {
         /*DB QUERY
       SELECT * FROM Clients LEFT JOIN Contexte ON Clients.idClients=Contexte.Clients_idClients LEFT JOIN Statut ON Clients.Statut_idStatut=Statut.idStatut WHERE Utilisateurs_idUtilisateurs=1  */
         $this->db->select('*');
         $this->db->from('Clients');
         $this->db->join('Contexte', 'Clients.idClients=Contexte.Clients_idClients', 'left');
         $this->db->join('Statut', 'Clients.Statut_idStatut=Statut.idStatut', 'left');
         $this->db->where('Utilisateurs_idUtilisateurs',$id_user);
        
         return $this->db->get();
     }
}
?>