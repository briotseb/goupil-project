<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contexte_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function add_ctxe($data)
    {
        $this->db->insert('Contexte', $data);
        return $this->db->insert_id();
    }
    //get statut table to populate the statut name dropdown
    function get_statut()     
    { 
        $this->db->select('idStatut');
        $this->db->select('StatutNomi');
        $this->db->from('Statut');
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idStatut);
            array_push($stat_name, $result[$i]->StatutNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }

    //get civilite table to populate the civilite dropdown
    function get_civilite()     
    { 
        $this->db->select('idCivilite');
        $this->db->select('CiviliteNomi');
        $this->db->from('Civilite');
        $query = $this->db->get();
        $result = $query->result();

        $civilite_id = array();
        $civilite_name = array();

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($civilite_id, $result[$i]->idCivilite);
            array_push($civilite_name, $result[$i]->CiviliteNomi);
        }
        return $civilite_result = array_combine($civilite_id, $civilite_name);
    }
    function get_accords(){
        $this->db->select('idGdsAccords');
        $this->db->select('GdsAccordsNomi');
       
        
        $this->db->from('GdsAccords');
        $query = $this->db->get();
        $result = $query->result();

        $gdsAccords_id = array('-Choisir-');
        $gdsAccords_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($gdsAccords_id, $result[$i]->idGdsAccords);
            array_push($gdsAccords_name, $result[$i]->GdsAccordsNomi);
        }
        return $gdsAccords_result = array_combine($gdsAccords_id, $gdsAccords_name);
    }
 
    public function get_obj(){
        $this->db->select('*');
        
        $this->db->from('InfoDevis');
        $query = $this->db->get();
        $result = $query->result();

        $gdsAccords_id = array('0');
        $gdsAccords_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($gdsAccords_id, $result[$i]->idInfoDevis);
            array_push($gdsAccords_name, $result[$i]->InfoDevisNomi);
        }
        return $gdsAccords_result = array_combine($gdsAccords_id, $gdsAccords_name);
    }
    public function list_ctxe_site($îdContexte)
    {
        $this->db->select('*');
         $this->db->from('Site');
        $this->db->where('Contexte_idContexte',$îdContexte);
         $query = $this->db->get();
        return $query->result();
    }
    
    
    public function list_ctxe_fixe($idSite)
    {
        // SELECT * FROM Fixe LEFT JOIN PdtFixe ON Fixe.PdtFixe_idPdtFixe = PdtFixe.idPdtFixe LEFT JOIN Operateur ON PdtFixe.Operateur_idOperateur=Operateur.idOperateur WHERE Site_idSite = 
        $this->db->select('*');
        $this->db->from('Fixe');
         $this->db->join('PdtFixe', 'Fixe.PdtFixe_idPdtFixe = PdtFixe.idPdtFixe', 'left');
         $this->db->join('Operateur', 'PdtFixe.Operateur_idOperateur=Operateur.idOperateur', 'left');
        $this->db->where('Site_idSite',$idSite);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function list_ctxe_conso_by_fixe($idFixe){
        $this->db->select('*');
        $this->db->from('ConsoFixe');
        $this->db->join('ConsoType','ConsoFixe.ConsoType_idConsoType=ConsoType.idConsoType','left');
        $this->db->where('Fixe_idFixe',$idFixe);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function list_ctxe_total_by_site($idSite)
    {
        /*SELECT SUM(ConsoTarifs) FROM 	ConsoFixe, Fixe WHERE ConsoFixe.Fixe_idFixe=Fixe.idFixe AND Site_idSite = 33*/
       
        $this->db->select('SUM(ConsoQte * ConsoTarifs) AS total',false);
        $this->db->from('ConsoFixe');
        $this->db->from('Fixe');
        $wh ='ConsoFixe.Fixe_idFixe=Fixe.idFixe';
        $this->db->where($wh);
        $this->db->where('Site_idSite',$idSite);
        $query = $this->db->get();
        $res = $query->result();
        $tmp1 = $res[0]->total;
        
         //FIXE TARIFS : SELECT SUM(FixeNbeT0 * FixeTarifsHT) AS total  FROM Fixe WHERE  Site_idSite = 2
        $this->db->select('SUM(FixeNbeT0 * FixeTarifsHT) AS total');
        $this->db->from('Fixe');
        $this->db->where('Site_idSite',$idSite);
        $query = $this->db->get();
        
        $res = $query->result();
        $tmp1 += $res[0]->total;
        return $tmp1;
    }
    public function list_ctxe_mob($îdContexte)
    {
        /*
        SELECT * FROM Mobile 
         LEFT JOIN PdtMobile ON Mobile.PdtMobile_idPdtMobile=PdtMobile.idPdtMobile 
         LEFT JOIN Operateur ON Operateur.idOperateur=PdtMobile.Operateur_idOperateur 
         
         WHERE Mobile.Contexte_idContexte=1
        
        */
        $this->db->select('*');
         $this->db->from('Mobile');
        
         $this->db->join('PdtMobile', 'Mobile.PdtMobile_idPdtMobile=PdtMobile.idPdtMobile', 'left');
         $this->db->join('Operateur', 'PdtMobile.Operateur_idOperateur=Operateur.idOperateur', 'left');
        $this->db->where('Contexte_idContexte',$îdContexte);
         $query = $this->db->get();
        return $query->result();
    }
     public function list_ctxe_conso_by_mob($id)
     {
        $this->db->select('*');
        $this->db->from('ConsoMobile');
        $this->db->join('ConsoType','ConsoMobile.ConsoType_idConsoType=ConsoType.idConsoType','left');
        $this->db->where('Mobile_idMobile',$id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function list_ctxe_total_by_mob($id)
    {
        /*
        SELECT SUM(ConsoMobileTarifs) FROM ConsoMobile WHERE ConsoMobile.Mobile_idMobile=1
        */
        $this->db->select('SUM(ConsoMobileTarifs) AS total',false);
        $this->db->from('ConsoMobile');
        $this->db->where('ConsoMobile.Mobile_idMobile',$id);
        $query = $this->db->get();
        $res = $query->result();
        $tmp1 = $res[0]->total;
        
        
        $this->db->select('MobileForfTarifsHT as total');
        $this->db->from('Mobile');
        $this->db->where('idMobile',$id);
        $query = $this->db->get();
        
        $res = $query->result();
        $tmp1 += $res[0]->total;
        return $tmp1;
    }
    public function check_cap($idContexte)
    {
        //SELECT * FROM Clients, Contexte WHERE Clients.idClients=Contexte.Clients_idClients AND Contexte.idContexte=1 AND Clients.ClientsCAP=1
         $this->db->select('*');
        $this->db->from('Clients');
        $this->db->from('Contexte');
          $wh ='Clients.idClients=Contexte.Clients_idClients';
        $this->db->where($wh);
         $wh ='Clients.ClientsCAP=1';
        $this->db->where($wh);
        
        
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
            return true;
        }else{
            return false;
        }
        
    }
    public function get_cap()
    {
        $this->db->select('idPrestation');
        $this->db->select('PrestationTarifs');
        $this->db->select('PrestationNomi');
        $this->db->from('Prestation');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
        
        $civilite_id = array();
        $civilite_name = array();

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($civilite_id, $result[$i]->PrestationTarifs);
            array_push($civilite_name, $result[$i]->PrestationNomi);
        }
        return $civilite_result = array_combine($civilite_id, $civilite_name);
    }
    
}
?>