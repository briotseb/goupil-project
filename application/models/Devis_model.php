<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Devis_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
    public function list_devis($id_user)
     {
         /*DB QUERY
        SELECT * FROM Clients, Contexte, Devis  WHERE Clients.idClients = Contexte.Clients_idClients AND Contexte.idContexte = Devis.Contexte_idContexte
         */
        $this->db->select('*');
        $this->db->from('Clients');
        $this->db->from('Devis');
        $this->db->from('Contexte');
        $wh ="Clients.idClients = Contexte.Clients_idClients";
        $this->db->where($wh);
        $wh="Contexte.idContexte = Devis.Contexte_idContexte";
        $this->db->where($wh);
        $this->db->where('Clients.Utilisateurs_idUtilisateurs',$id_user);
        return $this->db->get();
     }
    public function suppr_devis($id)
    {
        /*for($j = 0; $j<count($id['id']); $j++){
            $this->db->select('Conclusion_idConclusion AS C');
            $this->db->from('Devis');
            $this->db->where('idDevis',$id['id'][$j]);
            $query = $this->db->get();
            $res = $query->result();
             if(!empty($res[0]->C)){
                $this->db->where('idConclusion', $res[0]->C);
                $this->db->delete('Conclusion');
             }
            
            $this->db->where('idDevis', $id['id'][$j]);
            $this->db->delete('Devis');
        }*/
        
      /*N'EFFACE PAS LES CONCLUSIONS A METTRE EN PLACE*/
       return $this->db->delete('Devis', array('idDevis' => $id));
    }
    public function get_devis($id)
    {
        /*
        
       SELECT * FROM Bob, ChoixPEO, Projet WHERE Bob.ChoixPEO_idChoixPEO = ChoixPEO.idChoixPEO AND ChoixPEO.Projet_idProjet = Projet.idProjet
        */
        $this->db->select('*');
        $this->db->from('Bob');
         $this->db->from('ChoixPEO');
         $this->db->from('Projet');
        $wh ="Bob.ChoixPEO_idChoixPEO = ChoixPEO.idChoixPEO";
        $this->db->where($wh);
        $wh ="ChoixPEO.Projet_idProjet = Projet.idProjet";
        $this->db->where($wh);
          $this->db->join('Fixe', 'Fixe_idFixe=Fixe.idFixe', 'left');
        $this->db->where('Devis_idDevis',$id);
            
        $query = $this->db->get();
        return $query->result();
    }
    
 
    public function update_devis($dev,$p1)
    {
        /*
        UPDATE Devis SET DevisConcluMaint=".$maintCT." , DevisConcluLoyer=".$loyerCT." , DevisConcluAchat=".$achatCT." WHERE idDevis=".$idDev;
        */
        $this->db->set('DevisConcluMaint', $dev->DevisConcluMaint);
        $this->db->set('DevisConcluLoyer', $dev->DevisConcluLoyer);
        $this->db->set('DevisConcluAchat', $dev->DevisConcluAchat);
        $this->db->set('DevisTotal', $dev->DevisTotal);
        $this->db->set('Conclusion_idConclusion', $p1);
        $this->db->where('idDevis', $dev->idDevis);
        $this->db->update('Devis'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
    
    }
    public function update_devis_test($dev,$p1)
    {
       
        $this->db->set('TestAdsl_idTestAdsl', $p1);
        $this->db->where('idDevis', $dev);
        $this->db->update('Devis'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
    }
    public function get_sum_devis($id)
    {
        //SELECT SUM(Bob.BobTarifsHT) FROM ChoixPEO,`Projet`,Bob WHERE Projet.Devis_idDevis=175 AND Projet.idProjet=ChoixPEO.Projet_idProjet AND Bob.ChoixPEO_idChoixPEO = ChoixPEO.idChoixPEO
        $this->db->select('SUM(Bob.BobTarifsHT) AS S');
        $this->db->from('Bob');
        $this->db->from('ChoixPEO');
        $this->db->from('Projet');
        $wh ="Projet.idProjet=ChoixPEO.Projet_idProjet";
        $this->db->where($wh);
        $wh ="Bob.ChoixPEO_idChoixPEO = ChoixPEO.idChoixPEO";
        $this->db->where($wh);
        $this->db->where('Projet.Devis_idDevis',$id);
            
        $query = $this->db->get();
        return $query->result();
    }
     public function get_sum_achat_devis($id)
    {
        //SELECT SUM(Bob.BobTarifsHT) FROM ChoixPEO,`Projet`,Bob WHERE Projet.Devis_idDevis=175 AND Projet.idProjet=ChoixPEO.Projet_idProjet AND Bob.ChoixPEO_idChoixPEO = ChoixPEO.idChoixPEO
        $this->db->select('SUM(Bob.BobAchat) AS S, SUM(Bob.BobForma) AS F, SUM(Bob.BobFctVal) AS H, SUM(Bob.BobOptADSD) AS G');
        $this->db->from('Bob');
        $this->db->from('ChoixPEO');
        $this->db->from('Projet');
        $wh ="Projet.idProjet=ChoixPEO.Projet_idProjet";
        $this->db->where($wh);
        $wh ="Bob.ChoixPEO_idChoixPEO = ChoixPEO.idChoixPEO";
        $this->db->where($wh);
        $this->db->where('Projet.Devis_idDevis',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function affecte_presta($id,$idPro)
    {
       
        
            
            $data = array(
                'Devis_idDevis' => $idPro,
                'Prestation_idPrestation' => $id
            );
            
            $this->db->insert('PrestaAffecte', $data);

        
        
    }
}