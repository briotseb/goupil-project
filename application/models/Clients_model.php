<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function add_clients($cli)
    {
        $this->db->insert('Clients', $cli);
        return $this->db->insert_id();
    }
    public function suppr_client($cli)
    {
        $this->db->where('idClients', $cli);
        return $this->db->delete('Clients');
        
    }
     public function add_tech($cli)
    {
        $this->db->insert('Technique', $cli);
        
    }
    public function list_clients($id_user)
     {
         /*DB QUERY
        SELECT * FROM Clients LEFT JOIN Contexte ON Clients.idClients=Contexte.Clients_idClients WHERE Utilisateurs_idUtilisateurs=1
         */
         $this->db->select('*');
         $this->db->from('Clients');
         $this->db->join('Contexte', 'Clients.idClients=Contexte.Clients_idClients', 'left');
         $this->db->where('Utilisateurs_idUtilisateurs',$id_user);
        
         return $this->db->get();
     }

    public function get_client($id)
     {
        $query = $this->db->get_where('Clients', array('Clients.idClients' => $id));
        return $query;
     }
    public function get_tech($idClient)
    {
          $this->db->select('*');
        $this->db->from('Technique');
       
        $this->db->where('Technique.Clients_idClients',$idClient);
        //print_r($this->db->get_compiled_select());
        return $this->db->get();
    }
    public function add_info($id,$com,$obj1,$obj2,$obj3)
    {
       //SET COMMENT
        $this->db->set('Comment',$com);
        $this->db->set('ClientsObjectifs1',$obj1);
        $this->db->set('ClientsObjectifs2',$obj2);
        $this->db->set('ClientsObjectifs3',$obj3);
        $this->db->where('idClients', $id);
        return $this->db->update('Clients');
    }
    public function update_client($cli)
    {
        $this->db->where('idClients', $cli->idClients);
        return $this->db->update('Clients', $cli);
    }
    public function update_tech($cli)
    {
        
        $this->db->where('Clients_idClients', $cli->Clients_idClients);
        return $this->db->update('Technique', $cli);
    }
}
?>