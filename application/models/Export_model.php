<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_model extends CI_Model
{
    
    public function get_info($id)
    {
        //SELECT * FROM Clients, Contexte WHERE Clients.idClients = Contexte.Clients_idClients AND Contexte.idContexte =6
        $this->db->select('*');
        $this->db->from('Clients');
        $this->db->from('Contexte');
        $this->db->from('Utilisateurs');
        $wh ="Clients.idClients = Contexte.Clients_idClients";
        $this->db->where($wh);
        $wh ="Clients.Utilisateurs_idUtilisateurs = Utilisateurs.idUtilisateurs";
        $this->db->where($wh);
        $this->db->where('Contexte.idContexte',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_prest($idDevis)
    {
        $this->db->select('*');
        $this->db->from('PrestaAffecte');
        $this->db->where('Devis_idDevis',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_info_user($id)
    {
        //SELECT * FROM Utilisateurs,Clients,Contexte WHERE Utilisateurs.idUtilisateurs = Clients.Utilisateurs_idUtilisateurs AND Clients.idClients = Contexte.Clients_idClients AND Contexte.idContexte=32
        $this->db->select('*');
        $this->db->from('Utilisateurs');
        $this->db->from('Clients');
        $this->db->from('Contexte');
        $wh ="Utilisateurs.idUtilisateurs = Clients.Utilisateurs_idUtilisateurs";
        $this->db->where($wh);
        $wh ="Clients.idClients = Contexte.Clients_idClients";
        $this->db->where($wh);
        $this->db->where('Contexte.idContexte',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_objectifs($id)
    {
        $this->db->select('*');
        $this->db->from('InfoDevis');
        $this->db->where('idInfoDevis',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_devis($id)
    {
        $this->db->select('*');
        $this->db->from('Devis');
        $this->db->where('idDevis',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_site($idContext)
    {
        $this->db->select('*');
        $this->db->from('Site');
        $this->db->where('Site.Contexte_idContexte',$idContext);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_fixe($ids)
    {
        //SELECT * FROM Fixe LEFT JOIN PdtFixe ON PdtFixe.idPdtFixe = Fixe.PdtFixe_idPdtFixe LEFT JOIN FixeType ON FixeType_idFixeType = FixeType.idFixeType WHERE Site_idSite  = 8
        $this->db->select('*');
        $this->db->from('Fixe');
        $this->db->join('PdtFixe', 'PdtFixe.idPdtFixe = Fixe.PdtFixe_idPdtFixe', 'left');
        $this->db->join('FixeType', 'FixeType_idFixeType = FixeType.idFixeType', 'left');
        $this->db->where('Site_idSite',$ids);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_conso($idf)
    {
        //SELECT * FROM ConsoFixe LEFT JOIN ConsoType ON ConsoType.idConsoType = ConsoFixe.ConsoType_idConsoType WHERE ConsoFixe.Fixe_idFixe=97
         $this->db->select('*');
        $this->db->from('ConsoFixe');
        $this->db->join('ConsoType', 'ConsoType.idConsoType = ConsoFixe.ConsoType_idConsoType', 'left');
       $this->db->where('ConsoFixe.Fixe_idFixe',$idf);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_pdt($idf,$idD)
    {
        //SELECT Entite.EntiteNomi,OptionsEntite.OptionsEntiteNomi FROM `mydb_sfr`.`Projet`, ChoixPEO LEFT JOIN Entite ON ChoixPEO.Entite_idEntite = Entite.idEntite  LEFT JOIN OptionsEntite ON ChoixPEO.OptionsEntite_idOptionsEntite = OptionsEntite.idOptionsEntite WHERE `ChoixPEO`.`Projet_idProjet`=Projet.idProjet AND Fixe_idFixe = 194
        $this->db->select('*');

        $this->db->from('Projet');
        $this->db->from('ChoixPEO');
        $this->db->join('Entite', 'ChoixPEO.Entite_idEntite = Entite.idEntite', 'left');
        $this->db->join('OptionsEntite', 'ChoixPEO.OptionsEntite_idOptionsEntite = OptionsEntite.idOptionsEntite', 'left');
        $wh ="`ChoixPEO`.`Projet_idProjet`=Projet.idProjet";
        $this->db->where('Fixe_idFixe',$idf);
        $this->db->where('Devis_idDevis',$idD);
        
        
        $this->db->where($wh);
        $query = $this->db->get();
        
        return $query->result();
    }
    public function is_offer_pdt($idD){
         $this->db->select('*');
        $this->db->from('Projet');
        $this->db->from('ChoixPEO');
        $wh = "ChoixPEO.Projet_idProjet=Projet.idProjet";
        $this->db->where($wh);
        $this->db->where('Projet.Devis_idDevis',$idD);
        $wh ="(ChoixPEO.Produit_idProduit=4 OR ChoixPEO.Produit_idProduit=5)";
        $this->db->where($wh);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_pdtSup($idS,$idD)
    {
        $this->db->select('*');

        $this->db->from('Projet');
        $this->db->from('ChoixPEO');
        $this->db->join('Entite', 'ChoixPEO.Entite_idEntite = Entite.idEntite', 'left');
        $this->db->join('OptionsEntite', 'ChoixPEO.OptionsEntite_idOptionsEntite = OptionsEntite.idOptionsEntite', 'left');
        $wh ="`ChoixPEO`.`Projet_idProjet`=Projet.idProjet";
        $this->db->where('Site_idSite',$idS);
         $this->db->where('Devis_idDevis',$idD);
        $this->db->where('Fixe_idFixe',NULL);
        $this->db->where($wh);
        $query = $this->db->get();
        
        return $query->result();
    }
    function get_mobile($idc)
    {
        $this->db->select('*');
        $this->db->from('Mobile');
        $this->db->from('PdtMobile');
         $wh ="Mobile.PdtMobile_idPdtMobile = PdtMobile.idPdtMobile";
        $this->db->where($wh);
        $this->db->where('Mobile.Contexte_idContexte',$idc);
        $query = $this->db->get();
        return $query->result();
    }
    function get_conso_mob($id)
    {
        //SELECT * FROM ConsoMobile, ConsoType WHERE ConsoMobile.Mobile_idMobile=19 AND ConsoMobile.ConsoType_idConsoType= ConsoType.idConsoType
         $this->db->select('*');
        $this->db->from('ConsoMobile');
        $this->db->from('ConsoType');
         $wh ="ConsoMobile.ConsoType_idConsoType= ConsoType.idConsoType";
        $this->db->where($wh);
        $this->db->where('ConsoMobile.Mobile_idMobile',$id);
        $query = $this->db->get();
        return $query->result();
    }
    function get_pdt_mobile($idm,$idD)
    {
        //SELECT * FROM Projet,ChoixPEO LEFT JOIN OptionsEntite ON OptionsEntite.idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite LEFT JOIN Bob ON Bob.ChoixPEO_idChoixPEO=ChoixPEO.idChoixPEO WHERE Projet.Mobile_idMobile = 12 AND ChoixPEO.Projet_idProjet = Projet.idProjet
        $this->db->select('*');
        $this->db->from('Projet');
        $this->db->from('ChoixPEO');
        $this->db->join('OptionsEntite', 'OptionsEntite.idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite', 'left');
        $this->db->join('Bob', 'Bob.ChoixPEO_idChoixPEO=ChoixPEO.idChoixPEO', 'left');
        $this->db->where('Projet.Mobile_idMobile',$idm);
        $this->db->where('Projet.Devis_idDevis',$idD);
        $wh ="ChoixPEO.Projet_idProjet = Projet.idProjet";
        $this->db->where($wh);
        $query = $this->db->get();
        return $query->result();
    }
    /*
    * Produit Supplementaire rajouté dans la partie mobile du devis
    *
    */
    function get_pdtSup_mobile($idD)
    {
        $this->db->select('*');
        $this->db->from('Projet');
        $this->db->from('ChoixPEO');
        $this->db->join('OptionsEntite', 'OptionsEntite.idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite', 'left');
        $this->db->join('Bob', 'Bob.ChoixPEO_idChoixPEO=ChoixPEO.idChoixPEO', 'left');
        $this->db->where('Projet.Mobile_idMobile',NULL);
        $this->db->where('Projet.Site_idSite',NULL);
        $this->db->where('Devis_idDevis', $idD);
        $wh ="ChoixPEO.Projet_idProjet = Projet.idProjet";
        $this->db->where($wh);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_test_adsl($idD)
    {
        //SELECT * FROM Devis LEFT JOIN TestAdsl ON TestAdsl.idTestAdsl = Devis.TestAdsl_idTestAdsl LEFT JOIN SDSL ON SDSL.idSDSL = TestAdsl.SDSL_idSDSL WHERE idDevis=233
        $this->db->select('*');
        $this->db->from('Devis');
        $this->db->join('TestAdsl', 'TestAdsl.idTestAdsl = Devis.TestAdsl_idTestAdsl', 'left');
        $this->db->join('SDSL', 'SDSL.idSDSL = TestAdsl.SDSL_idSDSL', 'left');
        $this->db->where('idDevis',$idD);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_conclu($id)
    {
        $this->db->select('*');
        $this->db->from('Devis');
        $this->db->join('Conclusion', 'Devis.Conclusion_idConclusion = Conclusion.idConclusion', 'left');
        $this->db->where('idDevis',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_adr_link($id)
    {
       
        $adresse = array();
        $this->db->select('Prestation.PrestationComment AS L');
        $this->db->from('Prestation');
        $this->db->from('PrestaAffecte');
        $wh ="PrestaAffecte.Prestation_idPrestation = Prestation.idPrestation";
        $this->db->where($wh);
        $this->db->where('PrestaAffecte.Devis_idDevis',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result();
        foreach ($res1 as $a)
        {
            array_push($adresse,$a->L);
        }
        
        //SELECT DISTINCT LiensChemin FROM `ChoixPEO`, Appartient LEFT JOIN Liens ON Appartient.Liens_idLiens = Liens.idLiens WHERE ChoixPEO.Projet_idProjet IN (SELECT idProjet FROM Projet WHERE Devis_idDevis = 366) AND ChoixPEO.Entite_idEntite = Appartient.Entite_idEntite AND (Appartient.OptionsEntite_idOptionsEntite IS NULL OR Appartient.OptionsEntite_idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite)
         $this->db->distinct();
        $this->db->select('Liens.LiensChemin AS L');
            $this->db->from('ChoixPEO');
            $this->db->from('Appartient');
            $this->db->join('Liens', 'Appartient.Liens_idLiens = Liens.idLiens', 'left');
        
           
           $wh = "ChoixPEO.Projet_idProjet IN (SELECT idProjet FROM Projet WHERE Devis_idDevis =".$id.")";
            //$this->db->where('ChoixPEO.Projet_idProjet',$r->I);
            $this->db->where($wh); 
            $wh ="ChoixPEO.Entite_idEntite = Appartient.Entite_idEntite AND (Appartient.OptionsEntite_idOptionsEntite IS NULL OR Appartient.OptionsEntite_idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite)";
            $this->db->where($wh); 
            
           
            
            $query1 = $this->db->get();
            $res1 = $query1->result();
            foreach ($res1 as $a)
            {
                array_push($adresse,$a->L);
            }
         
        return $adresse;
    }
    /**
     * Retourne les logos des prestataires choisis dans le devis
     * @param [[Type]] $id idDevis en cours
     */
    public function get_prestaLogo($id){
        //SELECT Prestataire.PrestataireNomi as N,Prestataire.PrestataireLogo as L FROM PrestaAffecte,Prestation, Prestataire WHERE PrestaAffecte.Devis_idDevis=57 AND PrestaAffecte.Prestation_idPrestation = Prestation.idPrestation AND Prestation.Prestataire_idPrestataire = Prestataire.idPrestataire
        $this->db->select('Prestataire.PrestataireNomi as N');
        $this->db->select('Prestataire.PrestataireLogo as L');
          $this->db->select('Prestataire.PrestataireComment as C');
        $this->db->from('PrestaAffecte');
        $this->db->from('Prestation');
        $this->db->from('Prestataire');
        $this->db->where('PrestaAffecte.Devis_idDevis',$id);
        $wh ="PrestaAffecte.Prestation_idPrestation = Prestation.idPrestation";
        $this->db->where($wh); 
        $wh ="Prestation.Prestataire_idPrestataire = Prestataire.idPrestataire";
        $this->db->where($wh); 
        $query = $this->db->get();
        return $query->result();
    }
    
}

?>