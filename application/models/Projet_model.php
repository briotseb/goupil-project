<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projet_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function get_produit($id,$gd)
    {
    /*
    SELECT * FROM Entite LEFT JOIN OptionsEntite ON OptionsEntite.Entite_idEntite=Entite.idEntite LEFT JOIN Produit ON Produit.idProduit=   Entite.Produit_idProduit WHERE Entite.Produit_idProduit =2
    */ 
        $this->db->select('*');
        $this->db->from('Entite');
        $this->db->join('OptionsEntite', 'OptionsEntite.Entite_idEntite=Entite.idEntite', 'left');
         $this->db->join('Produit', 'Produit.idProduit=   Entite.Produit_idProduit', 'left');
        $this->db->where('Entite.Produit_idProduit',$id);
        
        
        //$this->db->where('Entite.fk_GdsAccords',NULL);
        //$this->db->where('Entite.fk_GdsAccords',$gd);
        if(empty($gd))$where = "Entite.fk_GdsAccords IS NULL";
        else $where = "(Entite.fk_GdsAccords IS NULL OR Entite.fk_GdsAccords=".$gd.")";
        $this->db->where($where);
        
        
         $query = $this->db->get();
        return $query->result();
    }
    public function get_tarifs($idEnti,$idOptEnti,$engagement)
    {
        /*
        SELECT * FROM Tarifs LEFT JOIN DureeEngagement ON Tarifs.DureeEngagement_idDureeEngagement = DureeEngagement.idDureeEngagement WHERE Entite_idEntite =".$idEnt[$i]." AND DureeEngagement.DureeEngagementNomi='".$idEnga." mois'
        
        */
     //print_r($idOptEnti);
        if(empty($idOptEnti) || $idOptEnti == ''|| $idOptEnti == 0){
            $this->db->select('*');
            $this->db->from('Tarifs');
           
            $this->db->where('Entite_idEntite',$idEnti);
            $this->db->where('DureeEngagement_idDureeEngagement',$engagement);
        }else{
             $this->db->select('*');
            $this->db->from('Tarifs');
          
            $this->db->where('OptionsEntite_idOptionsEntite',$idOptEnti);
            $this->db->where('DureeEngagement_idDureeEngagement',$engagement);
        }
        $query = $this->db->get();
        return $query->result();
        
    }
    public function get_tarifs_pack($idP,$idE){
        //SELECT TarifsHT AS T, ChoixPEO.ChoixPEOQte AS Q FROM Tarifs,ChoixPEO WHERE Tarifs.Entite_idEntite IN (SELECT Entite_idEntite FROM `ChoixPEO` WHERE Projet_idProjet=884) AND Tarifs.OptionsEntite_idOptionsEntite IN (SELECT OptionsEntite_idOptionsEntite FROM `ChoixPEO` WHERE Projet_idProjet=884 AND OptionsEntite_idOptionsEntite NOT BETWEEN 136 AND 139) AND Tarifs.DureeEngagement_idDureeEngagement=3 AND ChoixPEO.Projet_idProjet= 884 AND Tarifs.OptionsEntite_idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite
        
        $this->db->select('TarifsHT AS T, ChoixPEO.ChoixPEOQte AS Q, ChoixPEO.ChoixPEORemCom AS R');
        $this->db->from('Tarifs, ChoixPEO');
        $wh ="Tarifs.Entite_idEntite IN (SELECT Entite_idEntite FROM `ChoixPEO` WHERE Projet_idProjet=".$idP.")";
        $this->db->where($wh);
        $wh ="Tarifs.OptionsEntite_idOptionsEntite IN (SELECT OptionsEntite_idOptionsEntite FROM ChoixPEO WHERE Projet_idProjet=".$idP." AND OptionsEntite_idOptionsEntite < 136 OR OptionsEntite_idOptionsEntite > 139 AND OptionsEntite_idOptionsEntite < 158 OR OptionsEntite_idOptionsEntite > 169)";
        $this->db->where($wh);
       
        $this->db->where('Tarifs.DureeEngagement_idDureeEngagement',$idE);
        $this->db->where('ChoixPEO.Projet_idProjet',$idP);
        $wh = "Tarifs.OptionsEntite_idOptionsEntite = ChoixPEO.OptionsEntite_idOptionsEntite";
        $this->db->where($wh);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_qte_pack($idP){
        //SELECT ChoixPEOQte AS Q FROM ChoixPEO WHERE ChoixPEO.Projet_idProjet=884
        $this->db->select('ChoixPEOQte AS Q');
        $this->db->from('ChoixPEO');
         $this->db->where('ChoixPEO.Projet_idProjet',$idP);
         $query = $this->db->get();
        return $query->result();
    }
    public function get_pri($id){
        //"SELECT PriNbe FROM Pri WHERE Tarifs_idTarifs=".$row['idTarifs'];
         $this->db->select('PriNbe');
        $this->db->from('Pri');
         $this->db->where('Tarifs_idTarifs',$id);
         $query = $this->db->get();
        return $query->result();
    }
    public function get_entite($id1,$id2)
    {
        if($id2 === 0)
        {
            //$sql = "SELECT EntiteNomi AS A FROM Entite WHERE idEntite =".$idEnt[$premier];
             $this->db->select('EntiteNomi AS A');
            $this->db->from('Entite');
            $this->db->where('idEntite',$id1);
        }else{
            // $sql = "SELECT OptionsEntiteNomi AS A FROM OptionsEntite WHERE idOptionsEntite=".$idOptEnt[$premier];
            $this->db->select('OptionsEntiteNomi AS A');
            $this->db->from('OptionsEntite');
            $this->db->where('idOptionsEntite',$id2);
        }
        $query = $this->db->get();
        return $query->result();
    }
    public function add_devis($devis)
    {
        /*
        INSERT INTO `Devis` (`idDevis`, `DevisComment`, `DevisDate`, `DevisExport`, `TestAdsl_idTestAdsl`, `Contexte_idContexte`, `DevisPRI`, `DevisSPOT`) VALUES (NULL, 'Premier Devis', '".$date."', NULL, NULL, '".$cont."', NULL, NULL);"
        */
        $this->db->insert('Devis', $devis);
       
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
    public function add_project($pro)
    {
        /*
        INSERT INTO `Projet` (`idProjet`, `Mobile_idMobile`, `Fixe_idFixe`, `Devis_idDevis`, `ProjetNum`,`ProjetTotal`,`Site_idSite`) VALUES (NULL, NULL, NULL, '".$devis."','".$pdtNum."',0,'".$site."');
        */
        $this->db->select('idProjet');
        $this->db->from('Projet');
        $this->db->where('Mobile_idMobile',$pro->Mobile_idMobile);
        $this->db->where('Fixe_idFixe',$pro->Fixe_idFixe);
        $this->db->where('Devis_idDevis',$pro->Devis_idDevis);
        $this->db->where('ProjetNum',$pro->ProjetNum);
        $this->db->where('Site_idSite',$pro->Site_idSite);
      
 
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            $result = $query->result();
            $this->db->where('idProjet', $result[0]->idProjet);
            $this->db->delete('Projet');
        }
        $this->db->insert('Projet', $pro);
       
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
    public function qui_projet($id)
    {
        /*
        SELECT MobileNumero,FixeNumero,Projet.Site_idSite AS C FROM `Projet` LEFT JOIN Mobile ON Mobile.idMobile = Projet.Mobile_idMobile LEFT JOIN Fixe ON Fixe.idFixe = Projet.Fixe_idFixe WHERE idProjet =
        */
        $this->db->select('MobileNumero AS M');
        $this->db->select('FixeNumero AS F');
        $this->db->select('Projet.Site_idSite AS S');
        $this->db->from('Projet');
        $this->db->join('Mobile', 'Mobile.idMobile = Projet.Mobile_idMobile', 'left');
        $this->db->join('Fixe', 'Fixe.idFixe = Projet.Fixe_idFixe', 'left');
        $this->db->where('idProjet',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_link_per_product($id1,$id2)
    {
        //SELECT LiensChemin FROM `Appartient`  LEFT JOIN Liens ON Liens.idLiens=Appartient.Liens_idLiens WHERE Appartient.Entite_idEntite=20
        $this->db->select('LiensChemin');
        $this->db->from('Appartient');
        $this->db->join('Liens', 'Liens.idLiens=Appartient.Liens_idLiens', 'left');
        $this->db->where('Appartient.Entite_idEntite',$id1);
        $this->db->where('Appartient.OptionsEntite_idOptionsEntite',$id2);
        $query = $this->db->get();
        return $query->result();
    }
    public function insert_conclu($c)
    {
        /*
        "INSERT INTO `Conclusion` (`idConclusion`, `ConclusionPresta`, `ConclusionMaint`, `ConclusionLoyer`, `ConclusionAchat`, `ConclusionRem`) VALUES (NULL, '".$presta."', '".$maint."', '".$loyer."', '".$achat."', '".$rem."');";
*/
         
        $this->db->insert('Conclusion', $c);
       
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
    public function update_projet($p,$p1)
    {
        //UPDATE Projet SET Conclusion_idConclusion = ".$idConclu." WHERE idProjet = ".$proj;
        $this->db->set('Conclusion_idConclusion', $p1);
         $this->db->where('idProjet', $p);
         $this->db->update('Projet'); 
        
    }
    public function insert_comment($t,$t1)
    {
        //"UPDATE Devis SET DevisComment = '".$com."' WHERE idDevis=".$id;
        $this->db->set('DevisComment', $t1);
        $this->db->where('idDevis', $t);
        $this->db->update('Devis');
    }
  
        //get statut table to populate the statut name dropdown
    public function get_tech()     
    { 
        $this->db->select('idSdslTech');
        $this->db->select('SdslTechNomi');
        
        $this->db->from('SdslTech');
        $query = $this->db->get();
        $result = $query->result();
      //  print_r($result);
        //array to store department id & department name
        $stat_id = array();
        $stat_name = array();

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($stat_id, $result[$i]->idSdslTech);
            array_push($stat_name, $result[$i]->SdslTechNomi);
        }
        return $statut_result = array_combine($stat_id, $stat_name);
    }
    public function insert_sdsl($s)
    {
         $this->db->insert('SDSL', $s);
       
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
    public function insert_test($t)
    {
        $this->db->insert('TestAdsl', $t);
        return $this->db->insert_id();
       
      
    }
    public function insert_bob($b)
    {
        $this->db->insert('Bob', $b);
        
       
    }
    public function insert_chxPdt($chx)
    {
        /*
         $sql = "INSERT INTO `ChoixPEO` (`idChoixPEO`, `Produit_idProduit`, `Entite_idEntite`, `OptionsEntite_idOptionsEntite`, `Projet_idProjet`, `ChoixPEOQte`, `ChoixPEODurEng`, `ChoixPEORemCom`, `ChoixPEORemFMA` , `ChoixPEOTarHt`) VALUES (NULL, '".$pdt."', '".$idEnt[$i]."', '".$idOptEnt[$i]."', '".$proj."', '".$qte[$i]."', '".$durEng."', NULL , NULL , '".$tarifs[$i]."' );";
        */
        
      /*  $this->db->select('idChoixPEO');
        $this->db->from('ChoixPEO');
        $this->db->from('Projet');
        $wh="ChoixPEO.Projet_idProjet = Projet.idProjet";
        $this->db->where($wh);
        $this->db->where('Projet.idProjet',$chx->Projet_idProjet);
      
        
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            $result = $query->result();
            $chx->idChoixPEO = $result['idChoixPEO'];
            $this->db->replace('ChoixPEO', $chx);
            return $chx->idChoixPEO;
            
        }else
        {*/
            $this->db->insert('ChoixPEO', $chx);
            $insert_id = $this->db->insert_id();
            return  $insert_id;
       // }
        
        

        
    }
    
      public function get_accords($idc)
    {
        //SELECT GdsAccords_idGdsAccords FROM Clients, Contexte WHERE idClients=Contexte.Clients_idClients AND Contexte.idContexte=6
          //NEW RQT : SELECT GdsAccords_idGdsAccords,GdsAccords.GdsAccordsIndiceDepart FROM Clients, Contexte, GdsAccords WHERE idClients=Contexte.Clients_idClients AND Contexte.idContexte=83  AND Clients.GdsAccords_idGdsAccords = GdsAccords.idGdsAccords
        $this->db->select('GdsAccords_idGdsAccords');
        $this->db->from('Clients');
        $this->db->from('Contexte');
        $this->db->from('GdsAccords');
        $wh = 'idClients=Contexte.Clients_idClients';
        $this->db->where($wh);
        $this->db->where('Contexte.idContexte',$idc);
        $wh = 'Clients.GdsAccords_idGdsAccords = GdsAccords.idGdsAccords';
        $this->db->where($wh);
        $query = $this->db->get();
        $result = $query->result();
        if(empty($result))return $result;
        return $result[0]->GdsAccords_idGdsAccords;
        
    }
    
    public function get_indice_accords($idc)
    {
     
          //NEW RQT : SELECT GdsAccords_idGdsAccords,GdsAccords.GdsAccordsIndiceDepart FROM Clients, Contexte, GdsAccords WHERE idClients=Contexte.Clients_idClients AND Contexte.idContexte=83  AND Clients.GdsAccords_idGdsAccords = GdsAccords.idGdsAccords
        $this->db->select('GdsAccords_idGdsAccords');
        $this->db->select('GdsAccords.GdsAccordsIndiceDepart AS A');
        $this->db->from('Clients');
        $this->db->from('Contexte');
        $this->db->from('GdsAccords');
        $wh = 'idClients=Contexte.Clients_idClients';
        $this->db->where($wh);
        $this->db->where('Contexte.idContexte',$idc);
        $wh = 'Clients.GdsAccords_idGdsAccords = GdsAccords.idGdsAccords';
        $this->db->where($wh);
        $query = $this->db->get();
        $result = $query->result();
        if(empty($result))return 1;
        return $result[0]->A;
        
    }
    public function get_dureeEnga($id,$pdt)
    {
        $tmp = $this->projet_model->get_indice_accords($id);
       //PRINCIPE : CEDRE GAcc=1 => 4 * GAcc => donc 4 < = idDuree < 8 
       
        
        //SELECT Tarifs.DureeEngagement_idDureeEngagement AS A, DureeEngagementNomi AS B FROM Tarifs, Entite,DureeEngagement WHERE Tarifs.Entite_idEntite=Entite.idEntite AND DureeEngagement.idDureeEngagement = Tarifs.DureeEngagement_idDureeEngagement AND Entite.Produit_idProduit=4 GROUP BY Tarifs.DureeEngagement_idDureeEngagement
        $this->db->select('Tarifs.DureeEngagement_idDureeEngagement AS A');
        $this->db->select('DureeEngagementNomi AS B');
        $this->db->from('Tarifs');
        $this->db->from('Entite');
        $this->db->from('DureeEngagement');
        $wh = 'DureeEngagement.idDureeEngagement = Tarifs.DureeEngagement_idDureeEngagement';
        $this->db->where($wh);
        $wh1 = 'Tarifs.Entite_idEntite=Entite.idEntite';
         $this->db->where($wh1);
        $this->db->where('Entite.Produit_idProduit',$pdt);
        $this->db->group_by('Tarifs.DureeEngagement_idDureeEngagement');
        $query = $this->db->get();
        $result = $query->result();
       
        $tmp1 = $tmp;
        $tmp2 = $tmp1 + 3;
    
        $civilite_id = array();
        $civilite_name = array();

        for ($i = 0; $i < count($result); $i++)
        {
            if($pdt > 10){
                array_push($civilite_id, $result[$i]->A);
                array_push($civilite_name, $result[$i]->B);
            }else{
                if($result[$i]->A >= $tmp1 && $result[$i]->A <= $tmp2)
    //GESTION DES DUREES ENGA AVEC GDS ACCORS SUR PDT SFR
                {
                    array_push($civilite_id, $result[$i]->A);
                    array_push($civilite_name, $result[$i]->B);
                }
            }
           
        }
        return $civilite_result = array_combine($civilite_id, $civilite_name);
    
    }
    public function getMultiplDuree($idEnga){
        $this->db->select('DureeEngagementNomi');
        $this->db->from('DureeEngagement');
        $this->db->where('idDureeEngagement',$idEnga);
        $query = $this->db->get();
        $result = $query->result();
        //if(ENVIRONMENT == 'development')print_r($result);
        /*preg match => 12 mois Tel4b result attendu => 12*/
        preg_match_all('!\d+!', $result[0]->DureeEngagementNomi, $matches);
        //if(ENVIRONMENT == 'development')print_r($matches);
        return $matches[0][0];
    }
  public function suppr_chx($id)
  {
      $this->db->where('idChoixPEO',$id);
      $this->db->delete('ChoixPEO');
  }
    public function is_type($id)
    {
        $this->db->select('fk_TypeEntite As T');
        $this->db->from('Entite');
        $this->db->where('idEntite',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}
?>