<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class New_devis_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //get societe table to populate the societe name dropdown
    function get_societe($id_user)     
    {   
        /*
        SELECT Contexte.idContexte,Clients.ClientsNomEntreprise FROM Clients LEFT JOIN Contexte ON Clients.idClients=Contexte.Clients_idClients WHERE Utilisateurs_idUtilisateurs=2
        */
        $this->db->select('Contexte.idContexte');
        $this->db->select('Clients.ClientsNomEntreprise');
        $this->db->from('Clients');
        $this->db->join('Contexte', 'Clients.idClients=Contexte.Clients_idClients', 'left');
        $this->db->where('Utilisateurs_idUtilisateurs',$id_user);
        $query = $this->db->get();
        $result = $query->result();

        //array to store department id & department name
        $cont_id = array('-Choisir-');
        $soc_name = array('-Choisir-');

        for ($i = 0; $i < count($result); $i++)
        {
            array_push($cont_id, $result[$i]->idContexte);
            array_push($soc_name, $result[$i]->ClientsNomEntreprise);
        }
        return $soc_result = array_combine($cont_id, $soc_name);
    }
    
    function get_contexte($idContexte)
    {
        
    }
   function get_list_accords(){
        $this->db->select('idGdsAccords');
        $this->db->select('GdsAccordsNomi');
        $this->db->select('GdsAccordsLogo');
        
        $this->db->from('GdsAccords');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
   }
    function get_services(){
        //SELECT * FROM Prestataire
        $this->db->select('*');
        $this->db->from('Prestataire');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function get_presta_serv($id)
    {
        //SELECT * FROM `Prestation` WHERE Prestataire_idPrestataire=1
        $this->db->select('*');
        $this->db->from('Prestation');
        $this->db->where('Prestataire_idPrestataire',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
        
    
}
?>