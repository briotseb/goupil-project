<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Liens_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function get_type()
    {
        $this->db->select('*');
        $this->db->from('TypeLiens');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    public function get_liens($id)
    {
        $this->db->select('*');
        $this->db->from('Liens');
        $this->db->where('TypeLiens_idTypeLiens',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    public function get_categorie_services($id)
    {
        $this->db->select('*');
        $this->db->from('CategorieLiens');
        $this->db->where('TypeLiens_idTypeLiens',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    public function get_tuple_services($id)
    {
        $this->db->select('*');
        $this->db->from('Liens');
        $this->db->where('CategorieLiens_idCategorieLiens',$id);
         $query = $this->db->get();
        $result = $query->result();
        return $result;
        
    }
}