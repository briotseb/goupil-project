<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consommation_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_Conso($id)
    {
        // $sql = "SELECT *,ROUND(ConsoTarifs,2) FROM `ConsoFixe`, ConsoType WHERE Fixe_idFixe=".$fixe[$k]." AND ConsoType_idConsoType=idConsoType";
        
        $this->db->select('*');
        $this->db->select('ROUND(ConsoTarifs,2) AS T');
        $this->db->from('ConsoFixe');
        $this->db->from('ConsoType');
        $this->db->where('Fixe_idFixe',$id);
        $wh ="ConsoType_idConsoType=idConsoType";
        $this->db->where($wh);
        return $this->db->get();
    }
}