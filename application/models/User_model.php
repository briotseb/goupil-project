<?php
// admin
class User_model extends CI_Model {
  function __construct() { }

  public function verify_user($user_name, $password) {
      $this->db->where('UtilisateursMail', $user_name)
            ->where('password', hash('sha256',$password))
            ->limit(1)
            ->from('Utilisateurs');
      //voir la rqt
      //echo $this->db->get_compiled_select();
      $q = $this->db->get();
      if ( $q->num_rows() > 0 ) {
        // person has account with us
          //echo "Connecté";
          return $q->row();
      }
      return false;
  }
    public function getAdminInfo($id_user){
         $this->db->where('idUtilisateurs', $id_user)
            ->limit(1)
            ->from('Utilisateurs');
        //echo $this->db->get_compiled_select();
        $q = $this->db->get();
        return $q->row();
    }
}
?>