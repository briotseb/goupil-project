<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Modal_Site extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->model('site_model');
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('security');
        $this->load->library('SiteObject');
        $this->load->helper('cookie');
        
    }

    public function submit()
    {
         /**
         Test OWASP intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
        $data = $this->input->post();
        //set validation rules
        $this->form_validation->set_rules('ASite', 'Adresse', 'trim|required|xss_clean');
        $this->form_validation->set_rules('CPSite', 'Code Postal', 'trim|required');
        $this->form_validation->set_rules('VilleSite', 'Ville', 'trim|required|xss_clean');
      
        
        //run validation check
        if ($this->form_validation->run() == FALSE)
        {   //validation fails
            echo validation_errors();
        }
        else
        {
            //get the form data
            $new_site = new SiteObject();
            $new_site->SiteAdresse =  $this->input->post('ASite');
            $new_site->SiteCodePost =  $this->input->post('CPSite');
            $new_site->SiteVille = strtoupper($this->input->post('VilleSite'));
            $contexte = $this->session->userdata('idContexte');
            $new_site->Contexte_idContexte = $contexte;
            if($this->site_model->insert_site($new_site))
            {
                 echo "YES";
            }
            else
            {
                echo "NO";
            }
         
        }
    }
    function update()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
         //set validation rules
        $this->form_validation->set_rules('ASite', 'Adresse', 'trim|required|xss_clean');
        $this->form_validation->set_rules('CPSite', 'Code Postal', 'trim|required');
        $this->form_validation->set_rules('VilleSite', 'Ville', 'trim|required|xss_clean');
      
       
        //run validation check
        if ($this->form_validation->run() == FALSE)
        {   //validation fails
            echo validation_errors();
        }
        else
        {
            $new_site = new SiteObject();
            $new_site->idSite =  $this->session->userdata('id');
            $new_site->SiteAdresse =  $this->input->post('ASite');
            $new_site->SiteCodePost =  $this->input->post('CPSite');
            $new_site->SiteVille = $this->input->post('VilleSite');
            $contexte = $this->session->userdata('idContexte');
            $new_site->Contexte_idContexte = $contexte;
            $this->site_model->update_site($new_site);
            echo "YES";
            
        }
    }
    
    //custom validation function to accept alphabets and space
    function alpha_space_only($str)
    {
        if (!preg_match("/^[a-zA-Z ]+$/",$str))
        {
            $this->form_validation->set_message('alpha_space_only', 'The %s field must contain only alphabets and space');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}
?>