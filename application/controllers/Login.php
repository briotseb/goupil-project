<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->helper('security');
        $this->load->model('user_model');
    }
    public function index() {
        
    if($this->session->userdata('username')) redirect('admin/index');

    $this->form_validation->set_rules('user_name', 'Identifiant', 'required|valid_email|min_length[5]');
    $this->form_validation->set_rules('user_password', 'Mot de passe', 'required|max_length[8]');

    if ( $this->form_validation->run() !== false ) {
        // then validation passed. Get from db
        $info=$this->input->post(NULL,TRUE);
        
        
        $res = $this
                 ->user_model
                 ->verify_user(
                    $info['user_name'],
                    $info['user_password']
                 );

        if ( $res !== false ) {// @todo controle isadmin
            $this->session->set_userdata('isadmin', true);
            $this->session->set_userdata('username',$res->UtilisateursNom);
            $this->session->set_userdata('iduser',$res->idUtilisateurs);
            
            redirect('admin/index');
        }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Erreur login/password</div>');
            redirect('login/index');
        }
    }
       
   
    $this->load->view('pages/login_view');
    //$this->load->view('templates/footer');
    
    }

  public function logout() {
    $this->session->sess_destroy();
    redirect('login/index');
  }
    
}

?>