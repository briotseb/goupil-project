<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contexte extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->library('session');
            
            $this->load->helper('url_helper');
            $this->load->helper('security');
            $this->load->database();
           
            $this->load->model('contexte_model');
            $this->load->library('ClientsObject');
            $this->load->library('TechniqueObject');
            $this->load->library('SiteObject');
            $this->load->model('clients_model');
            $this->load->model('mobile_model');
            $this->load->model('site_model');
            $this->load->helper('cookie');
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
        
        }
  
        function index()
        {
            //Session Non ouverte redirection page login
            if(!$this->session->userdata('username')) redirect('login/index');
            //fetch data from statut and civilite tables
            $data['statut'] = $this->contexte_model->get_statut();
            $data['civilite'] = $this->contexte_model->get_civilite();
            $data['gdsAccords'] = $this->contexte_model->get_accords();
            $data['operateur'] = $this->mobile_model->get_operateur();
            $data['operateurModal'] = $this->site_model->get_operateur();
            $data['conso'] = $this->mobile_model->get_conso();
            $data['consoF'] = $this->site_model->get_conso();
            $data['type'] = $this->site_model->get_typePdt();
            $data['obj'] = $this->contexte_model->get_obj();
             $page = array(
            'page' => 1
             );
            if(get_cookie('client') === NULL)
            {
                //set validation rules
                $this->form_validation->set_rules('statut', 'Statut', 'required|callback_combo_check');
                $this->form_validation->set_rules('entreprise', 'Entite de l\'entreprise', 'trim|required');//alpha_numeric_spaces
                $this->form_validation->set_rules('adresse', 'Adresse de l entreprise', 'trim|required|xss_clean');
                $this->form_validation->set_rules('codeP', 'Code Postal', 'required|numeric');
                $this->form_validation->set_rules('ville', 'ville', 'required|trim|alpha_numeric_spaces');
                
                $this->form_validation->set_rules('telF', 'Téléphone Fixe', 'numeric');
                $this->form_validation->set_rules('telM', 'Téléphone Mobile', 'numeric');
                $this->form_validation->set_rules('telFax', 'Téléphone Fax', 'numeric');
                $this->form_validation->set_rules('adreWeb', 'Adresse du site web', 'trim|max_length[100]|xss_clean|prep_url|valid_url');
                //if($this->input->post('mail'))$this->form_validation->set_rules('mail', 'Courriel', 'required|valid_email');
                $this->form_validation->set_rules('mail', 'Courriel', 'required|valid_email');
                

                if($this->input->post('civiG') != 0)$this->form_validation->set_rules('civiG', 'Civilité du Gerant', 'callback_combo_check');
                if($this->input->post('nomG'))$this->form_validation->set_rules('nomG', 'Nom du Gérant', 'trim|xss_clean');
                if($this->input->post('prenomG'))$this->form_validation->set_rules('prenomG', 'Prénom du Gérant', 'trim|xss_clean');

                if($this->input->post('civiT') != 0)$this->form_validation->set_rules('civiT', 'Civilité du Technique', 'callback_combo_check');
                if($this->input->post('nomT'))$this->form_validation->set_rules('nomT', 'Nom du Technique', 'trim|xss_clean');
                if($this->input->post('prenomT'))$this->form_validation->set_rules('prenomT', 'Prénom du Technique', 'trim|xss_clean');
                $this->form_validation->set_rules('telFTech', 'Téléphone Fixe Technique', 'numeric');
                $this->form_validation->set_rules('telMTech', 'Téléphone Mobile Technique', 'numeric');
                if($this->input->post('mailTech'))$this->form_validation->set_rules('mailTech', 'Courriel', 'valid_email');
                if($this->input->post('numSiret'))$this->form_validation->set_rules('numSiret', 'Numéro de Siret', 'numeric');
                if($this->input->post('GCSocieteCheck'))$this->form_validation->set_rules('GCSociete', 'Grands Accords', 'callback_combo_check');
               // if($this->input->post('comment'))$this->form_validation->set_rules('comment', 'Commentaire sur Contexte', 'alpha_numeric_spaces|xss_clean');

                  //run validation check
                if ($this->form_validation->run() == FALSE)
                {   //validation fails
                   // echo validation_errors();
                    $this->load->view('templates/header',$page);
                    $this->load->view('pages/contexte_view', $data);
                    
                    $this->load->view('pages/site_view');
                    $this->load->view('pages/mobile_view',$data);
                    $this->load->view('pages/comment_view',$data);
                    $this->load->view('pages/new_devis_contexte_view');
                    $this->load->view('templates/footer', $data);
               
                }else{
                    $Cont = new CLientsObject();
                    $Cont->Statut_idStatut = $this->input->post('statut');
                    $Cont->ClientsNomEntreprise = $this->input->post('entreprise');
                    $Cont->ClientsNom = $this->input->post('nomG');
                    $Cont->ClientsPrenom = $this->input->post('prenomG');
                    $Cont->ClientsAdresse = $this->input->post('adresse');
                    $Cont->ClientsCodePost =  $this->input->post('codeP');
                    $Cont->ClientsVille = $this->input->post('ville');
                    $Cont->Utilisateurs_idUtilisateurs = $this->session->userdata('iduser');   
                    $Cont->ClientsSiret = $this->input->post('numSiret');
                    $Cont->ClientsTel = $this->input->post('telF');
                    $Cont->Civilite_idCivilite = $this->input->post('civiG');
                    $Cont->ClientsTelMob = $this->input->post('telM');
                    $Cont->ClientsMail = $this->input->post('mail');
                    
                    $Cont->ClientsAdresseWeb = $this->input->post('adreWeb');
                    $Cont->ClientsTelFax = $this->input->post('telFax');
                    
                   // $Cont->Comment = $this->input->post('comment');
                    if($this->input->post('GCSocieteCheck') === '1')$Cont->GdsAccords_idGdsAccords = $this->input->post('GCSociete');
                    if($this->input->post('RecSocieteCheck') === '1')$Cont->ClientsReceptel = 1;
                    if($this->input->post('CapSocieteCheck') === '1')$Cont->ClientsCAP = 1;
                    
                   // print_r($Cont);
                    //insert the form data into database
                    $last_id = $this->clients_model->add_clients($Cont);
                    $data = array(
                        'idContexte' => NULL,
                        'Clients_idClients' => $last_id
                    );
                    
                    $Tech = new TechniqueObject();
                    $Tech->Clients_idClients=$last_id;
                   
                    $Tech->TechniqueNom = $this->input->post('nomT');
                    $Tech->TechniquePrenom = $this->input->post('prenomT');
                    $Tech->TechniqueTel = $this->input->post('telFTech');
                    $Tech->TechniqueTelMob = $this->input->post('telMTech');
                    $Tech->TechniqueMail = $this->input->post('mailTech');
                    $Tech->Civilite_idCivilite = $this->input->post('civiT');

                    $this->clients_model->add_tech($Tech);

                    //$Cont= (object) $Cont;
                   // set_cookie('tech',json_encode($Tech),'0');
                       
                    //set_cookie('client',json_encode($Cont),'0');
                    
                    $last_idctxe = $this->contexte_model->add_ctxe($data);
                    $this->session->set_userdata('idContexte',$last_idctxe);


                    $site = new SiteObject();
                    $site->SiteAdresse =  $this->input->post('adresse');
                    $site->SiteCodePost = $this->input->post('codeP');
                    $site->SiteVille = $this->input->post('ville');
                    $site->Contexte_idContexte = $last_idctxe;

                    $this->site_model->insert_site($site);

                    
                     $client = $this->clients_model->get_client($last_id);
                     $client = $client->result();
                
                    $tech = $this->clients_model->get_tech($client[0]->idClients);
                    $tech = $tech->result();
      
                    set_cookie('client',json_encode($client),'0');
                    set_cookie('tech',json_encode($tech),'0');
                    
                    //display success message
                    $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center"><strong>Société Ajoutée</strong></div>');
                    redirect('Contexte/index');
                }

            }else{
                $client = json_decode(get_cookie('client'));
                $tech = json_decode(get_cookie('tech'));
                $data['client'] = $client;   
                $data['tech'] = $tech;
              
                $this->load->view('templates/header',$page);
                $this->load->view('pages/contexte_view', $data);
                
                $this->load->view('pages/site_view');
                $this->load->view('pages/mobile_view',$data);
                $this->load->view('pages/comment_view',$data);
                $this->load->view('pages/new_devis_contexte_view');
                $this->load->view('templates/footer', $data);
            } 
       
           
        }
        
    function add_supinfo(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        
            $this->form_validation->set_rules('comment', 'Commentaire sur Contexte', 'xss_clean');//alpha_numeric_spaces|
            if ($this->form_validation->run() == FALSE){
             //   echo validation_errors();
                 $this->session->set_flashdata('msgComment', '<div class="alert alert-danger text-center">Erreur</div>');
                  redirect('Contexte/index','refresh');
            }else{
                $com = $this->input->post('comment');
                $client = json_decode(get_cookie('client'));
                $obj1 = $this->input->post('objectif1');
                $obj2 = $this->input->post('objectif2');
                $obj3 = $this->input->post('objectif3');
                
                $this->clients_model->add_info($client[0]->idClients,$com,$obj1,$obj2,$obj3);
                $client = $this->clients_model->get_client($client[0]->idClients);
                $client = $client->result();
               // print_r($client);
                delete_cookie('client');
                set_cookie('client',json_encode($client),'0');
                $this->session->set_flashdata('msgComment', '<div class="alert alert-success text-center">Informations Complémentaires Ajoutées</div>');
                redirect('Contexte/index');
            }
        
    }
    
       
        //custom validation function for dropdown input
        function combo_check($str)
        {
            if ($str == '-Choisir-')
            {
                $this->form_validation->set_message('combo_check', 'Le champ Statut est requis');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }

        //custom validation function to accept only alpha and space input
        function alpha_only_space($str)
        {
            if (!preg_match("/^([-a-z ])+$/i", $str))
            {
                $this->form_validation->set_message('alpha_only_space', 'The %s field must contain only alphabets or spaces');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
    
}
?>