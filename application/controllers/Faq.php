<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Faq extends CI_Controller { 
    public function __construct() 
    {
        Parent::__construct();
        $this->load->model("annuaire_model");
        $this->load->model("clients_model");
        $this->load->model("contexte_model");
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->library('TechniqueObject');
        $this->load->helper('security');
    }
     public function index() 
     {
        //Indic au header les logos en rouge
         $page = array(
            'page' => 10
        );
        
        $this->load->view('templates/header',$page);
        $this->load->view('faq/faq_view');
        $this->load->view('templates/footer');
     }
}
?>