<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projet extends CI_Controller {
    public function __construct()
    {
        Parent::__construct();
        $this->load->model("projet_model");
        $this->load->model("site_model");
        $this->load->model("contexte_model");
        $this->load->model("categorie_model");
        $this->load->model("devis_model");


        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->library('SiteObject');
        $this->load->library('DevisObject');
        $this->load->library('ProjectObject');
        $this->load->library('ConclusionObject');
        $this->load->library('TestAdslObject');
        $this->load->library('ChoixPdtObject');

        $this->load->library('SdslObject');
        $this->load->library('exception');
        $this->load->library('BobObject');

    }
    /**
     * Affiche le contenu Pdt de la pop-up + insert un nouveau devis si non effectué
     */
    public function afficheProjet()
    {
        /**
         Test OWASP non - intrusion
         **/
        if(!$this->session->userdata('username')) redirect('login/index');
         
        $id = $this->input->post(NULL,TRUE);
       /*Array(
        [id] => 1
        [pro] => 1
        [idF] => 47
        [idM] => 0
        [dev] => 10
        [site] => 2
        )*/
      
        //Is - it GdAcc ?
        $data['GdsAcc'] = $this->projet_model->get_accords($this->session->userdata('idContexte'));
        /*Recupération Données Pdt
         Array
        (
        [0] => stdClass Object
        (
            [idEntite] => 1
            [Produit_idProduit] => 1
            [EntiteNomi] => Ligne Analogique Fax (Basse conso)
            [idOptionsEntite] =>
            [Entite_idEntite] =>
            [OptionsEntiteNomi] =>
            [idProduit] => 1
            [ProduitNomi] => Voix
            [ProduitPackage] => 0
            [Operateur_idOperateur] => 5
        )
        */
 //CAS DES PDTS EN PLUS CAR GDS ACC
        $data['pdt'] = $this->projet_model->get_produit($id['id'], $data['GdsAcc']);
      
        //1/INSERT DEVIS
        //2/INSERT PROJET

    //1/
        if(!$this->session->userdata('devis')){
            $idContexte = $this->session->userdata('idContexte');
            $devis = new DevisObject();
            $devis->DevisDate = date("Y-m-d  G:i:s");
            $devis->Contexte_idContexte = $idContexte;
            $data['devis'] = $this->projet_model->add_devis($devis);
            $this->session->set_userdata('devis',$data['devis']);
        }else{
            $data['devis'] = $this->session->userdata('devis');
        }
        
        $data['add'] = 0;
    
    //2/
        $mob=NULL;
        $fix=NULL;
        $sit=NULL;
        if($id['idM'] !== '0')$mob = $id['idM'];
        if($id['idF'] !== '0' && $id['idF'] !== '-')$fix = $id['idF'];
        if($id['site'] !== '0')$sit = $id['site'];
        
        if($id['idF'] === '-'){
            $data['add'] = 1;
        }
        
        $projet = new ProjectObject();
        $projet->Mobile_idMobile = $mob;
        $projet->Fixe_idFixe = $fix;
        $projet->Site_idSite = $sit;
        $projet->Devis_idDevis = $data['devis'];
        $projet->ProjetNum = 1;
     
    //Creation de la var session projet pour enregistrement à la fermeture de la Pop-Up
        $this->session->set_userdata('insertprojet',json_encode($projet));

        $data['Enga'] = $this->projet_model->get_dureeEnga($this->session->userdata('idContexte'),$id['id']);
        
        if(!empty($data['pdt'])){
            $p = $data['pdt'][0];
            if($p->ProduitPackage == 1)
            {
                $this->load->view('contexte/projet/projet_details_package_view',$data);
            }else{
                if($p->ProduitNomi === 'Mobile / GSM'){
                    $this->load->view('contexte/projet/projet_details_mobile_view',$data);
                }else{
                    $this->load->view('contexte/projet/projet_details_simple_view',$data);
                }
            }
            $this->load->view('contexte/projet/projet_details_fin_view',$data);
            }
        else{
            echo "<b>Etude spécifique nécessaire</b>";
        }
    }

    /**
     * Conclusion de saisie de devis
     * Données presents en memoire Php Array
(
    [__ci_last_regenerate] => 1499935937
    [isadmin] => 1
    [username] => sebastien
    [iduser] => 2
    [totalDevis] => 19.9
    [totalMS] => 45
    [idContexte] => 1
    [projet] => 81
)
     */
    public function affiche_conclu()
    {
/**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $po = $this->input->post(NULL,TRUE);
        $data['presta'] = $po['presta'];
        $tmp = $this->devis_model->get_sum_devis($this->session->userdata('devis'));

        $data['total'] = $tmp[0]->S;
        $data['ms'] = $this->session->userdata('totalMS');
        
        $tmp = $this->devis_model->get_sum_achat_devis($this->session->userdata('devis'));
        
        $data['achat'] = $tmp[0]->S;
        $data['forma'] = $tmp[0]->F;
        $data['fctVal'] = $tmp[0]->H;
        $data['optADSD'] = $tmp[0]->G;
        $this->load->view('contexte/projet/projet_conclu_view',$data);
    }


    /**
     * Fonction de Calcul : Retour Choix dans Pop-up de pdt
     *  
        Array
(
    [idEnt] => ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"]
    [idOptEnt] => ["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"]
    [qte] => ["1",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    [proj] => 10
    [pdt] => 1
    [idEn] => 12
    [remCom] => ["10",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    [remFS] => ["10",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    [tar] => [null,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
)
     */
    public function projet_calcul_choice(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        //Recupération Projet : Fait à l'ouverture de la pop-up pour insertion dans base
        // Projet en cours temp session
        $tmp = $this->projet_model->add_project(json_decode($this->session->userdata('insertprojet')));
        //$this->session->set_userdata('projet',$tmp);
        $this->session->unset_userdata('insertprojet');
    
        //Initialisation des tableaux d'ID  + Recuperation val var JS
        //Tableau des entites renseignees
        $idpourbob = 0 ;
        $t = $this->input->post(NULL,TRUE);
        $idEnt[] = array();
        $idEnt = json_decode($t['idEnt']);
        $idOptEnt[] = array();
        $idOptEnt = json_decode($t['idOptEnt']);
        $qte[] = array();
        $qte = json_decode($t['qte']);
        $remCom[] = array();
        $remCom = json_decode($t['remCom']);
        $remFS[] = array();
        $remFS = json_decode($t['remFS']);
     
        $proj = $tmp;
        $pdt = intval($t['pdt']);
        $durEng = intval($t['dEn']);
     
        //INSERT TABLE CHOIXPEO
        /* 
        *Chaque entite ou sous-entite de pdt est enregistré si une qte est renseignée
        *Attention ! : ne renseigne pas le tarifs asscoié au choix mais les id correspondants entre ligne et pdt ss pdt qte remise durEng
        */
        $tarifs[] = array();

        //Tarifs pour modal MOBILE
        if($pdt == 9){
            $tarifs = json_decode($t['tar']);
            //INSERT DANS BASE
            for($i = 0;$i < count($qte); $i++){
                if($qte[$i] != 0){
                    $chx = new ChoixPdtObject();
                    $chx->Produit_idProduit = $pdt;
                    $chx->Entite_idEntite = $idEnt[$i];
                    $chx->OptionsEntite_idOptionsEntite = $idOptEnt[$i];
                    $chx->Projet_idProjet = $proj;
                    $chx->ChoixPEOQte = $qte[$i];
                    $chx->ChoixPEODurEng = $durEng;
                    $chx->ChoixPEOTarHt = $tarifs[$i];
                    $idpourbob = $this->projet_model->insert_chxPdt($chx);
                }
            }
        }else{
            //INSERT DANS BASE
            for($i = 0;$i < count($qte); $i++){
                if($qte[$i] != 0){
                    if($idOptEnt[$i] == 0){
                        if($remFS[$i]=='')$remFS[$i]=0;
                        if($remCom[$i] == '')$remCom[$i]=0;
                         $chx = new ChoixPdtObject();
                         $chx->Produit_idProduit = $pdt;
                         $chx->Entite_idEntite = $idEnt[$i];
                         $chx->Projet_idProjet = $proj;
                         $chx->ChoixPEOQte = $qte[$i];
                         $chx->ChoixPEODurEng = $durEng;
                         $chx->ChoixPEORemCom = $remCom[$i];
                         $chx->ChoixPEORemFMA = $remFS[$i];
                    }else{
                        if($remFS[$i] =='')$remFS[$i]=0;
                        if($remCom[$i] == '')$remCom[$i]=0;
                         $chx = new ChoixPdtObject();
                         $chx->Produit_idProduit = $pdt;
                         $chx->Entite_idEntite = $idEnt[$i];
                         $chx->OptionsEntite_idOptionsEntite = $idOptEnt[$i];
                         $chx->Projet_idProjet = $proj;
                         $chx->ChoixPEOQte = $qte[$i];
                         $chx->ChoixPEODurEng = $durEng;
                         $chx->ChoixPEORemCom = $remCom[$i];
                         $chx->ChoixPEORemFMA = $remFS[$i];
                    }
                   $idpourbob = $this->projet_model->insert_chxPdt($chx);
                }
            }
        }


        //Construction de Bob
        /*
        * Bob : Choix dans sa globalite (tt les entites/ ss entites sont prises en compte) 
        * Tarifs avec Qte et remise calcule
        */
        //Porteur concerné : Site / Ligne Fixe / Ligne Mobile
        $site=0;
        $porteur = $this->projet_model->qui_projet($proj);
      
        if(!empty($porteur[0]->M))$num = $porteur[0]->M;
        else $num = $porteur[0]->F;
        if(!empty($porteur[0]->S))$site = $porteur[0]->S;



        // CREATION DE BOB : RECUP TARIFS
        //Init Variable
        $tarHT = 0;
        $pRi = 0;
        $sPot = 0;
        $cOm = 0;
        $Ms = 0;
        
        //VAR TARIFS PARTICULIERS
        $achat = 0;
        $tmpAchat=0;
        $forma = 0;
        $fctValAjou = 0;
        $optADSDFIB = 0;
        //CALCUL DU TARIF BLOQUE à 12 MOIS BDD NON RENS
        $idEnga=$durEng;
        //$idEnga = 12;
        $premier = 0;
        $essai='';
        
        //      != 'Mobile / GSM'
        if($pdt != 9){
            $name = null;
            for($i = 0;$i < count($idEnt); $i++){
                if($qte[$i] != 0){
                    if($premier == 0)$premier = $i;
                    
                    //SI ERREUR : RENVOI 2 POUR POP-UP TARIFS NON PRESENTS
                    try{
                        $data['prix'] = $this->projet_model->get_tarifs($idEnt[$i],$idOptEnt[$i],$durEng);
                        if(empty($data['prix'])){
                            throw new Exception();
                        }
                    }catch(Exception $e){
                        echo 2;
                        exit;
                    }
                    
                    //INTITULE DU PDT SELECTIONNE
                    if($idOptEnt[$i] !== '0'){
                        $tmp = $this->projet_model->get_entite(0,$idOptEnt[$i]);
                        $name .= $tmp[0]->A." &nbsp; Qte : ".$qte[$i].'<br>';
                    }else{
                        $tmp = $this->projet_model->get_entite($idEnt[$i],0);
                        $name .= $tmp[0]->A." &nbsp; Qte : ".$qte[$i].'<br>';
                    }
                    //Prix Fixé ds BDD
                    $tar = $data['prix'][0];
                    
                    //GESTION TYPE D'ACHAT
                    $materiel = $this->projet_model->is_type($idEnt[$i]);
                    
                    if(!empty($materiel[0]->T)){
               
                    
                        switch ($materiel[0]->T)
                        {
                            case 1://MATERIEL
                                $tmpAchat = ($tar->TarifsHT*$qte[$i]);
                                $achat += $tmpAchat - ($tmpAchat * $remCom[$i])/100;
                                $tar->TarifsHT = 0;
                                break;

                            case 2://FORMATION
                                $tmpAchat = ($tar->TarifsHT*$qte[$i]);
                                $forma += $tmpAchat + ($tmpAchat * $remCom[$i])/100;
                                $tar->TarifsHT = 0;
                                break;

                            case 3://FONCT VAL AJOU Tel4b
                                $tmpAchat = ($tar->TarifsHT*$qte[$i]);
                                $fctValAjou += $tmpAchat - ($tmpAchat * $remCom[$i])/100;
                                $tar->TarifsHT = 0;
                                break;
                            case 4://OPTION ADSL
                                $tmpAchat = ($tar->TarifsHT*$qte[$i]);
                                $optADSDFIB += $tmpAchat - ($tmpAchat * $remCom[$i])/100;
                                $tar->TarifsHT = 0;
                                break;
                        }
                    }
                   
    //FIN GESTION ACHAT MATERIEL

    //CALCUL REMISE OU COM SUR TARIFS BASE BDD
                    if($pdt < 11)
                    {
                        if($tar->TarifsHT !== 0){
                            if($remCom[$i] != 0 ){
                                $tar->TarifsHT = $tar->TarifsHT - ($tar->TarifsHT*$remCom[$i])/100;
                            }
                        }   
                    }else{
                        if($tar->TarifsHT !== 0){
                            if($remCom[$i] != 0 ){
                                $tar->TarifsHT = $tar->TarifsHT + ($tar->TarifsHT*$remCom[$i])/100;
                            }
                        }  
                    }
                    
                    
    //SOMME DES TARIFS DES PDTS CUMULES            
                    $tarHT += ($tar->TarifsHT*$qte[$i]);
                    
    //$COM : VOIX SFR PRE ETABLIE 
                    if($pdt == 1)
                        //VOIX SFR 
                    {
                        $cOm = $tar->Commission;
                    }else
                    {
                        if($tar->TarifsHT == 0)$cOm = ($tmpAchat*$remCom[$i])/100;
                        else $cOm = ($tar->TarifsHT*$remCom[$i])/100;
                    }
                    
                   
    //FMA => REMISE SUR MS
                    $Ms += floatval($tar->TarifsMSHT) - (floatval($tar->TarifsMSHT) * floatval($remFS[$i]))/100;
    //Ajout de mise en service * qte
                    $Ms  = $Ms * $qte[$i];
                    
    //GESTION PRI FIXE OU VARIABLE
                    $data['pri'] = $this->projet_model->get_pri($tar->idTarifs);
                    if(empty($data['pri'])){
                        //SFR + MOBILE SFR
                        if($pdt < 11){
                            $pRi += $tarHT;
                        // + FMA remisé * 0,028
                            $pRi += (($tar->TarifsMSHT - ($tar->TarifsMSHT*$remFS[$i])/100)*0.028)*$qte[$i];
                            $sPot += ($pRi*($cOm/100));
                        }else{
                            $pRi = 0;
                            $sPot += $cOm;
                        }
                        
                    }else{
                        $pRi += $data['pri'][0]->PriNbe;
                        $pRi += (($tar->TarifsMSHT - ($tar->TarifsMSHT*$remFS[$i])/100)*0.028)*$qte[$i];
                        $sPot += ($pRi*($cOm/100));
                    }
                }
            }
        }else{
            //9 : Pdt Mobile
            /**
             * PRIX PRI SPOT COMMISSION
            * Contraintes Tarifs :
            *   -> prix enregistré en base
            *   -> $com enregistré en base
            *   -> Pri enregistrée en base PriNbe : Pri Nombre
            *   -> SPot Calculé : pri *(com /100)
            */
            $tarSauv = 0;
            $name = null;
            for($i = 0;$i<count($idEnt); $i++){
                if($idOptEnt[$i] != 0){
                    if($premier == 0)$premier = $i;
                    try{
                        $data['prix'] = $this->projet_model->get_tarifs($idEnt[$i],$idOptEnt[$i],$durEng);
                        if(empty($data['prix'])){
                            throw new Exception();
                        }
                    }catch(Exception $e){
                        echo 2;
                        exit;
                    }
                    $tar = $data['prix'][0];
                    $cOm = $tar->Commission;
                    
                    if($tar->TarifsHT != '' || $tar->TarifsHT != '0' ) $tarSauv = $tar->TarifsHT;
                    $data['pri'] = $this->projet_model->get_pri($tar->idTarifs);

                    //Si PRI non renseigné en base fixé à 0
                    if(empty($data['pri']))$pRi += 0;
                    else $pRi += $data['pri'][0]->PriNbe;
                    $sPot += ($pRi*($cOm/100));

                    //Recup Nomi Produit
                    $tmp = $this->projet_model->get_entite(0,$idOptEnt[$i]);
                    $name .= $tmp[0]->A." &nbsp; Qte : ".$qte[$i].'<br>';
                }

                //Permet de gerer les parties avec Prix ou pas
                // $tarifs[$i] : liste des tarifs rentrés manuellement par l'utilisateur
                if($tarSauv != 0){
                    $tarHT += ($tarSauv*$qte[$i]);
                }else{
                    $tarHT += ($tarifs[$i]*$qte[$i]);
                }
            }
        }
        
        //PRODUIT AFFECTE
       // $name=null;
        if($name === null){
            if($idOptEnt[$premier] == 0){
                $name = $this->projet_model->get_entite($idEnt[$premier],0);
                //$sql = "SELECT EntiteNomi AS A FROM Entite WHERE idEntite =".$idEnt[$premier];
                $name = $name[0]->A." &nbsp; Qte : ".$qte[$premier];
            }else{
               // $sql = "SELECT OptionsEntiteNomi AS A FROM OptionsEntite WHERE idOptionsEntite=".$idOptEnt[$premier];
                 $name = $this->projet_model->get_entite(0,$idOptEnt[$premier]);
                $name = $name[0]->A." &nbsp; Qte : ".$qte[$premier];
            }
        }
        $durEng = $this->projet_model->getMultiplDuree($idEnga);
        $pdt = array(
            "numero" => $num,
            "nomi" => $name,
            //"nomi" => $essai,
            "eng" => $durEng,
           "ctxe" => $this->session->userdata('idContexte'),
            "site" => $site,
           "tarifsHT" => $tarHT,
            // "tarifsHT" => $tarHT,
           "achat" => $achat,
            "forma" => $forma,
            "fctval" => $fctValAjou,
            "optADSD" => $optADSDFIB,
            "PRI" => $pRi,
            "SPOT" => $sPot,
            "COM" => $cOm,
            "MS" => $Ms,
            "idChx" => $idpourbob
        );
     
        //Nouveau COntexte definie dans le devis pour un site // un numero
        $bob = new BobObject();
        $bob->BobTarifsHT = $tarHT;
        $bob->BobPri = $pRi;
        $bob->BobSpot = $sPot;
        $bob->BobEng = $idEnga;
        $bob->BobMS = $Ms;
        $bob->BobCtxe = $this->session->userdata('idContexte');
        $bob->BobNomi = $name;
        $bob->BobAchat = $achat;
        $bob->BobForma = $forma;
        $bob->BobFctVal = $fctValAjou;
        $bob->BobOptADSD = $optADSDFIB;
        
        $bob->ChoixPEO_idChoixPEO = $idpourbob;
        $this->projet_model->insert_bob($bob);
        //print_r($this->db->last_query());

    //MAJ DES VAR SESSION PHASE COMMUNICANTE POUR LES CONCLUSIONS
        $tmp = $this->session->userdata('totalDevis');
        $tmp = floatval($tmp);
        $tmp +=$tarHT;
        $this->session->set_userdata('totalDevis',$tmp);

        $tmp = $this->session->userdata('totalMS');
        $tmp += $Ms;
        $this->session->set_userdata('totalMS',$tmp);
        
        $tmp = $this->session->userdata('achat');
        $tmp += $achat;
        $this->session->set_userdata('achat',$tmp);
        
        $tmp = $this->session->userdata('forma');
        $tmp += $forma;
        $this->session->set_userdata('forma',$tmp);
        
        $tmp = $this->session->userdata('fctVal');
        $tmp += $fctValAjou;
        $this->session->set_userdata('fctVal',$tmp);
        
        $tmp = $this->session->userdata('optADSD');
        $tmp += $optADSDFIB;
        $this->session->set_userdata('optADSD',$tmp);

        
        echo json_encode($pdt, JSON_PRETTY_PRINT);
    }


    public function save_conclu()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
      //  print_r($this->input->post(NULL,TRUE));
        $donnees = $this->input->post(NULL,TRUE);
        /*
Array
(
    [id] => 1
    [pro] => 1
    [idF] => 47
    [idM] => 0
    [dev] => 10
    [site] => 2
)
*/

        $idDevis = $this->session->userdata('devis');
        $prestAffect = json_decode($donnees['prestAffect']);
       //Suivi affectation presta couple au js.
        // print_r($prestAffect);
        for($i=0;$i< count($prestAffect);$i++){

            $this->devis_model->affecte_presta($prestAffect[$i],$idDevis);

        }


        // 1 - UPDATE DEVIS CTXE CONCLU
        $idDevis = $this->session->userdata('devis');
        $devis = new DevisObject();
        $devis->idDevis = $idDevis;
        $devis->DevisTotal = $donnees['totalCT'];
        $devis->DevisConcluMaint = $donnees['maintCT'];
        $devis->DevisConcluLoyer = $donnees['loyerCT'];
        $devis->DevisConcluAchat = $donnees['achatCT'];

       // print_r($devis);
     // $this->projet_model->update_devis($devis);

        // 2 - INSERT CONCLUS


        $conclu = new ConclusionObject();
        $conclu->ConclusionPresta = $donnees['presta'];
        $conclu->ConclusionMaint = $donnees['maint'];
        $conclu->ConclusionLoyer =  $donnees['loyer'];
        $conclu->ConclusionAchat =  $donnees['achat'];
        $conclu->ConclusionRem =  $donnees['rem'];
        $conclu->ConclusionFrais = $donnees['frais'];
        $conclu->ConclusionTotal = $donnees['projetTot'];
        $conclu->ConclusionDuree = $donnees['duree'];

       // print_r($conclu);
        $idConclu = $this->projet_model->insert_conclu($conclu);
        // 3 - MAJ PROJET AVEC IDCONCLU

      //  $idDev = $this->session->userdata('devis');
        //print_r($idPro);
        $this->devis_model->update_devis($devis,$idConclu);


        //LOAD VIEW COMMENT
       $this->load->view('contexte/new_devis_comment_view');



    }
    
    
    function save_comment(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $do = $this->input->post(NULL,TRUE);
        $this->projet_model->insert_comment($this->session->userdata('devis'),$do['com']);
        $data['techno'] = $this->projet_model->get_tech();
       // print_r($data);
        $this->load->view('contexte/new_devis_test_view',$data);
        if(ENVIRONMENT == 'development'){
            print_r($this->session->all_userdata());
        }
    }
    function save_test()
    {
/**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         

        $test = new TestAdslObject();
        $test->TestAdslTete =  $this->input->post('ADSLTete');
        $test->TestAdslDateTest =  $this->input->post('ADSLDate');
        $test->TestAdslAdresse =  $this->input->post('ADSLAdresse');
        $test->TestAdslNom =  $this->input->post('ADSLNom');
        $test->TestAdslPresent =  $this->input->post('ADSPresent1');
        $test->TestAdslLongueur =  $this->input->post('ADSLLongueur');
        $test->TestAdslEtatLigne =  $this->input->post('ADSLEtatLigne');
        $test->TestAdslCentral =  $this->input->post('ADSLCentreTel');
         $test->TestAdslLongueur2 =  $this->input->post('ADSLLongueur2');
        $test->TestAdslEtatLigne =  $this->input->post('ADSLEtatLigne');


        $test->TestAdslCalibre =  $this->input->post('ADSLCalibre');
        $test->TestAdslDebitMaxEstim =  $this->input->post('ADSDebitMaxAdsl');
        $test->TestVdslDebitMaxEstim =  $this->input->post('ADSDebitMaxVdsl');
        $test->TestAdslAffaiTheo = $this->input->post('ADSLAffaib');


        $sdsl = new SdslObject();
        $sdsl->SDSL1M1P2P = $this->input->post('ADSL1m1p2p');
        $sdsl->SDSL2M1P2P = $this->input->post('ADSL2m1p2p');
        $sdsl->SDSL2MEFM1P2P = $this->input->post('ADSL2mEfm1p2p');
        $sdsl->SDSL4M1P2P =$this->input->post('ADSL4m1p2p');
        $sdsl->SDSL4MEFM1P2P = $this->input->post('ADSL4mEfm1p2p');

        $sdsl->SDSL8M1P2P = $this->input->post('ADSL8m1p2p');
        $sdsl->SDSL4P1M =$this->input->post('ADSL1m4p');
        $sdsl->SDSL4P2M = $this->input->post('ADSL2m4p');
        $sdsl->SDSL4P4M = $this->input->post('ADSL4m4p');
        $sdsl->SDSL4P8M = $this->input->post('ADSL8m4p');
        $sdsl->SDSL4P10M = $this->input->post('ADSL10m4p');
        $sdsl->FIBRE = $this->input->post('fibre');

       // print_r($sdsl);
        //INSERT SDSL RECUP ID
        $id = $this->projet_model->insert_sdsl($sdsl);

        $test->SDSL_idSDSL = $id;
    //    print_r($test);
        $id = $this->projet_model->insert_test($test);

        $this->devis_model->update_devis_test($this->session->userdata('devis'),$id);
        // $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Test Ajoutée</div>');

        redirect('Export/index');

    }

    public function supprChxPlus()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
     $id = $this->input->post(NULL,TRUE);
     //   print_r($id);
        $this->projet_model->suppr_chx($id['idChx']);
        $tmp = $this->session->userdata('totalDevis');
        $tmp -= $id['total'];
       $this->session->set_userdata('totalDevis',$tmp);
        echo 1;
    }
}
?>
