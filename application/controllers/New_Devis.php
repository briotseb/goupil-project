<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class New_Devis extends CI_Controller { 
    public function __construct() 
    {
       Parent::__construct();
        $this->load->model("new_devis_model");
        $this->load->model("site_model");
        $this->load->model("contexte_model");
        $this->load->model("categorie_model");
        $this->load->model("projet_model");
        
        
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->library('SiteObject');
    }
    
    public function index()
    {
        //Session Non ouverte redirection page login
        if(!$this->session->userdata('username')) redirect('login/index');
        //fetch data from statut and civilite tables
         $page = array(
            'page' => 3
        );
     
           if($this->input->cookie('client') !== NULL){
              delete_cookie('client');
              delete_cookie('tech');
         }
        $this->session->unset_userdata('devis');
        $this->session->unset_userdata('idContexte');
        $data['societe'] = $this->new_devis_model->get_societe($this->session->userdata('iduser'));
        $this->session->set_userdata('totalDevis',0);
        $this->session->set_userdata('totalMS',0);
        $this->session->set_userdata('achat',0);
        $this->load->view('templates/header',$page);
        $this->load->view('modals/modal_produit_view');
        $this->load->view('pages/new_devis_view',$data);
        $this->load->view('templates/footer');
    }
    public function load_operateur(){
       /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $this->session->unset_userdata('devis');
        $this->session->unset_userdata('idContexte');
        $idContexte = $this->input->post(NULL,TRUE);
        $idContexte = $idContexte['idContexte'];
        $data['GdsAcc'] = $this->projet_model->get_accords($idContexte);
        //$data['operateur'] = 
        //$list = $this->new_devis_model->get_list_accords();
        //$operateur = $this->new_devis->get_list()
        $this->load->view('contexte/new_devis_operateur',$data);
        /*foreach($list as $l){
            $data['l'] = $l;
            $this->load->view('contexte/new_devis_operateur_gdsacc',$data);
        }*/
        $this->load->view('contexte/new_devis_operateur_validation',$data);
    }
    public function load_operateur_mobile(){
        /**
         Test OWASP non - intrusion
         **/
        if(!$this->session->userdata('username')) redirect('login/index');
         
        $idContexte = $this->input->post(NULL,TRUE);
        $idContexte = $idContexte['idContexte'];
        $data['GdsAcc'] = $this->projet_model->get_accords($idContexte);
        $this->load->view('contexte/new_devis_operateur_mobile',$data);
        $this->load->view('contexte/new_devis_operateur_mobile_validation',$data);
    }
     public function load_contexte_fixe()
     {
         /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
         $var = $this->input->post(NULL,TRUE);
         $idContexte = $var['idContexte'];
         $idOperateur = $var['idOperateur'];
        //if($idCOntexte == '') creer une erreur systeme
         $this->session->set_userdata('idContexte',$idContexte);
         $data['site'] = $this->contexte_model->list_ctxe_site($idContexte);
         
         // Rajouter dans cette fonction idOperateur
         $data['PdtFixe'] = $this->categorie_model->get_PdtFixe($idOperateur);
       
         if(count($data['PdtFixe']) == 1){
             exit;
         }
         $data['idContexte'] = $idContexte;
         
         //$data['site'] => Tableau d'objet de type site
         //print_r($data['site']);
         
         $this->load->view('contexte/new_devis/new_devis_entete_view');
         $site = $data['site'];
         $numSite = 1;
         $numMobile = 1;
         $total = 0;
         foreach($site as $s)
             //$s : site object
         {
             $data['numSite'] = $numSite;
             $data['s']=$s;
             $this->load->view('contexte/new_devis/new_devis_site_view',$data);
             
             $data['fixe'] = $this->contexte_model->list_ctxe_fixe($s->idSite);
             
             foreach($data['fixe'] as $f)
             {
                 $data['f']=$f;
                 //print_r($f);
                 $this->load->view('contexte/new_devis/new_devis_fixe_view',$data);
                 
                 $data['conso'] = $this->contexte_model->list_ctxe_conso_by_fixe($f->idFixe);
                 //print_r($data['conso']);
                 foreach($data['conso'] as $c)
                 {
                     $data['c']=$c;
                     $this->load->view('contexte/new_devis/new_devis_conso_view',$data);
                 }
             }
            
             //PAS DE FIXE SUR SITE
             if(empty($data['fixe'])){
                 $this->load->view('contexte/new_devis/new_devis_fixe_view',$data);
                 $this->load->view('contexte/new_devis/new_devis_conclu_view',$data);      
             }else{
                 $data['totalSite'] = $this->contexte_model->list_ctxe_total_by_site($s->idSite);
                 $total += $data['totalSite'];
                 $this->load->view('contexte/new_devis/new_devis_conclu_view',$data);
             }
            
             $numSite += 1 ;
         }
         $this->load->view('contexte/new_devis/new_devis_site_pdt_sup_view',$data);
         $this->session->set_userdata('total',$total);
         
     }
    public function load_contexte_mobile()
    {
         /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
        
        /**
        * Total du Ctxe Fixe
        */
        $total = $this->session->userdata('total');
        $this->session->unset_userdata('total');
        $numMobile = 1;
        $var = $this->input->post(NULL,TRUE);
        $idContexte = $var['idContexte'];
        if(!is_numeric($idContexte)){
            exit('(New_Devis load_ctxte_mobile ) icContetxe : Not Numeric');
        }
        $idOperateur = $var['idOperateur'];
        if(!is_numeric($idOperateur))
        {
            exit('(New_Devis load_ctxte_mobile ) idOperateur : Not Numeric');
        }
        $data['idContexte'] = $idContexte;
        
        $this->load->view('contexte/new_devis/new_devis_mobile_entete_view');
         
         $data['mobile'] = $this->contexte_model->list_ctxe_mob($idContexte);
         $data['PdtMobile'] = $this->categorie_model->get_PdtMobile($idOperateur);
         
         if(count($data['PdtMobile']) == 1){
             exit;
         }
         foreach($data['mobile'] as $m)
         {
             //print_r($data);
             $data['numMobile'] = $numMobile;
             $data['m'] = $m;
             $this->load->view('contexte/new_devis/new_devis_mobile_view',$data);
             
             $data['conso'] = $this->contexte_model->list_ctxe_conso_by_mob($m->idMobile);
             foreach($data['conso'] as $c)
                 {
                     $data['c']=$c;
                     $this->load->view('contexte/new_devis/new_devis_mobile_conso_view',$data);
                 }
             $data['totalMob'] = $this->contexte_model->list_ctxe_total_by_mob($m->idMobile);
             $total += $data['totalMob'];
          //   $this->load->view('contexte/new_devis/new_devis_mob_pdt_sup_view',$data);
             $this->load->view('contexte/new_devis/new_devis_mob_conclu_view',$data);
             $numMobile +=1;
         }
         
         $this->load->view('contexte/new_devis/new_devis_mob_pdt_sup_view',$data);
       
         
         //LES SERVICES GOUPIL
         $this->load->view('contexte/new_devis_cap_view',$data);
         $serv = $this->new_devis_model->get_services();
         foreach($serv as $s){
             $data['serv'] = $s;
             $this->load->view('contexte/new_devis_cap_serv_view',$data);
             $presta = $this->new_devis_model->get_presta_serv($s->idPrestataire);
             foreach($presta as $p){
                 $data['presta'] = $p;
                 $this->load->view('contexte/new_devis_cap_prest_view',$data);
             }
             $this->load->view('contexte/new_devis_cap_close_view');
         }
         //fermer div services
         $this->load->view('liens/liens_conten_view');
         
         
         
         //CONCLUSION CTXE
         $data['totalCtxe'] = $total;
         $this->load->view('contexte/new_devis_ctxe_conclu',$data);
    }
    
   
}
?>