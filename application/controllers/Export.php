<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__ . '/../../vendor/autoload.php';

class Export extends CI_Controller {

    public function __construct()
    {
        Parent::__construct();
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->model('export_model');
        $this->load->model('projet_model');
        $this->load->helper('file');

    }

    public function index(){
        //Session Non ouverte redirection page login
        if(!$this->session->userdata('username')) redirect('login/index');
        $idC = $this->session->userdata('idContexte');
        $idD = $this->session->userdata('devis');
     
        
        if(empty($idC)){
            echo 'Erreur Recuperation id Contexte';
            //show_error($message, $status_code, $heading = 'An Error Was Encountered')
            exit;
        }
        if(empty($idD)){
            echo 'Erreur Recuperation id Devis';
            exit;
        }
      
        $this->session->unset_userdata('devis');
        $data['infos'] = $this->export_model->get_info($idC);
        $data['devis'] = $this->export_model->get_devis($idD);
        $data['prest'] = $this->export_model->get_prestaLogo($idD);
        /**
         *Footer Pdf
        **/
        $footer = '<table class="FooterTable"><tr>
                    <td class="Demi"> </td>
                    <td class="Demi"><img src="'.base_url("assets/img/LogoUsers/".$data['infos'][0]->UtilisateursLogo.".png").'" class="logoBP" /> &nbsp; {PAGENO}/{nbpg} </td>
                    </tr></table>';
        /**
         *Page de Couverture
        **/
        $content_p1 = $this->export_p1($data);
        $log = $content_p1;
        
        /**
         *Page d'objectifs du contexte client
        **/
        $notObj = true;
        if(!empty($data['infos'][0]->ClientsObjectifs1)){
            $content_p2 = $this->export_p2($idC,$data,$idD);
            $log .= $content_p2;
            $notObj = false;
        }
        
        
        /**
         *Page des partenaires du devis
        **/
        $notPrest = true;
        if(!empty($data['prest'][0]->N)){
            $content_p3 = $this->export_p3($idD,$data);
            $notPrest = false;
        }
        
        /**
         *Page de details des lignes Fixes
        **/
        $content_p4 = $this->export_p4($idC,$data,$idD);
        $log .= $content_p4;
        
        /**
         *page de details des lignes mobiles
        **/
        $mobile = $this->export_model->get_mobile($idC);
        $notMobile = false;
        if(!empty($mobile)) {
            $content_p5 = $this->export_p5($idC,$data,$idD);
            $log .= $content_p5;
        } else {
            $notMobile = true;
        }
        
        /**
         *Page de conclusion tarifaire
        **/
        $content_p7 = $this->export_p7($data,$idD);
        $log .= $content_p7;
        
        /**
         *Page de test de ligne
        ***/
        $data['test'] = $this->export_model->get_test_adsl($idD);
        $notTest=false;
        if(!empty($data['test'][0]->TestAdslNom))$content_p8 = $this->export_p8($idC,$data,$idD);
        else $notTest = true;
        
        

        /**
         *Details des offres de type PACKAGE
        **/
        $notPdtOffer=false;
        if(!empty($this->export_model->is_offer_pdt($idD))){
            $content_p9 = $this->export_p9($idC,$data,$idD);
            $log .= $content_p9;
        }else{
            $notPdtOffer = true;
        }
        
        

        /***
         *Derniere Page de Couv
        **/
        $data['infos'] = $this->export_model->get_info_user($idC);
        $content_p10 = $this->export_p10($data);
        $log .= $content_p10;

        /**
         * Recuperation des Liens affecté au Devis
         * assets/link/ficheCAP/CAP-PBRIntegral2.pdf
        **/

        $adressePdf = array();
        $adressePdf = $this->export_model->get_adr_link($idD);
        
      
        
       // echo $log;
        
        
       
        try {

            $pdf = new \Mpdf\Mpdf();
            $pdf->SetDisplayMode('fullpage');
           $path_css = base_url('assets/vendor/exportmpdf.css');
            //$path_css = 'https://alain:alain@www.devoapp.ovh/assets/vendor/exportmpdf.css';
            // LOAD a stylesheet
            $stylesheet = file_get_contents($path_css);
            $pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
            // Langue & caractere
            $pdf->SHYlang = 'fr';
           
            $pdf->SHYleftmin = 3; // nombre de caractere avant la césure

            $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
            $pdf->WriteHTML($content_p1);
            

            if(!$notObj)
            {
                $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
                $pdf->WriteHTML($content_p2);
                $pdf->SetHTMLFooter($footer);
            }

            
           if(!$notPrest)
           {
                $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
                $pdf->WriteHTML($content_p3);
                $pdf->SetHTMLFooter($footer);
           }
       
        
           
            $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
            $pdf->SetHTMLFooter($footer);
            $pdf->WriteHTML($content_p4);
            

            if(!$notMobile)
            {
                $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
                $pdf->WriteHTML($content_p5);
                $pdf->SetHTMLFooter($footer);
            }
            $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
            $pdf->WriteHTML($content_p7);
            $pdf->SetHTMLFooter($footer);

            if(!$notTest)
            {
                $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
                $pdf->WriteHTML($content_p8);
                $pdf->SetHTMLFooter($footer);
            }

            if(!$notPdtOffer)
            {
                $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
                $pdf->WriteHTML($content_p9);
                $pdf->SetHTMLFooter($footer);
            }
            $pdf->AddPage('L','', '', '', '',15,15,15,10,18,12);
            $pdf->WriteHTML($content_p10);
            $pdf->SetHTMLFooter($footer);

            
            $pdf->SetCreator('RedGoupil SaaS 2018 -D: '.$idD.' -C: '.$idC);
            $pdf->SetAuthor($data['infos'][0]->UtilisateursSociete.' - '.$data['infos'][0]->UtilisateursNom);
            $fichierSA = '/tmp/out'.$data['infos'][0]->idUtilisateurs.'.pdf';
            $pdf->Output($fichierSA,'F');
          
            //$pdf->Output();

        }catch(mPDF_exception $e){
            exit($e);
		}
        
         $nomDeFichier = str_replace(' ','',$data['infos'][0]->ClientsNomEntreprise);
        $nomDeFichier = $nomDeFichier.'-'.date('j-n-Y').'.pdf';
        if(!empty($adressePdf)){

            $fichier = '';
            for($i = 0; $i < count($adressePdf);$i++){
                if(!empty($adressePdf[$i]))$fichier .= ' /var/www/html/'.trim($adressePdf[$i]);
            }
            
            
            /*
            *EXTRACT METADATA
            */
            $fichierMeta = '/tmp/data'.$data['infos'][0]->idUtilisateurs.'.txt';
            $cmd = 'pdftk '.$fichierSA.' dump_data output '.$fichierMeta;
            $cmd1 =  escapeshellarg($cmd);
            $cmd2 = escapeshellcmd($cmd1);
            $cmd2 = str_replace("'",'', $cmd2);
            exec($cmd2);
            
            /*
            *JOINTURE PDF
            */
            //$fichier = str_replace(' ','\ ',$fichier);
            $fichierFinalSM = '/tmp/tmp'.$data['infos'][0]->idUtilisateurs.'-SM.pdf';
            //$fichierFinal = str_replace(' ','\ ',$fichierFinal);
            $cmd = 'pdftk '.$fichierSA.' '.$fichier.' cat output '.$fichierFinalSM;
            //$cmd = str_replace(' ','\ ',$cmd);
            /*Utiliser simple cote pour shell_exec fonctionne*/
            //shell_exec('pdftk /tmp/'.$nomDeFichier.' '.$fichier.' cat output /tmp/out10.pdf');*/
            $cmd1 =  escapeshellarg($cmd);
            $cmd2 = escapeshellcmd($cmd1);
            $cmd2 = str_replace("'",'', $cmd2);
            //echo $cmd2;
            exec($cmd2);

            /**
            *JOINTURE PDF + META
            **/
            $fichierFinal = '/tmp/tmp'.$data['infos'][0]->idUtilisateurs.'.pdf';
            $cmd = 'pdftk '.$fichierFinalSM.' update_info '.$fichierMeta.' output '.$fichierFinal;
            $cmd1 =  escapeshellarg($cmd);
            $cmd2 = escapeshellcmd($cmd1);
            $cmd2 = str_replace("'",'', $cmd2);
            exec($cmd2);
            
            //unlink('/tmp/'.$fichierSA);


            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Content-disposition: attachment; filename='.$nomDeFichier);
            header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date dans le passé
            readfile($fichierFinal);
            exit;
        }else{
            echo "pas d'adresse de fichier à joindre";
            header('Content-Type: application/pdf');
            header('Content-disposition: attachment; filename='.$nomDeFichier);
            header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date dans le passé
            readfile($fichierSA);
            exit;

       } 
    }

    public function addLogEvent($event)
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        
        $time = date("D, d M Y H:i:s");
        $time = "[".$time."] ";

        $event = $time.$event."\n";

        file_put_contents("/tmp/pdf.log", $event, FILE_APPEND);
    }
    public function export_p5($idContexte,$data,$idDevis)
    {
        $html = '';
        $mobile = $this->export_model->get_mobile($idContexte);
        $html = $this->load->view('export/export_p5_entete_view',$data,true);

        $msg = '';
        $totalMob = 0;
        $totalProj=0;
        $data['cmptSite'] = 1;

        foreach ($mobile as $m){
            $data['m'] = $m;
            $totalMob += $m->MobileForfTarifsHT;
            $html = $html . $this->load->view('export/export_p5_tabMob_view',$data,true);
            $data['cmptSite'] = $data['cmptSite'] + 1;
            $conso = $this->export_model->get_conso_mob($m->idMobile);

            foreach($conso as $c){
                $totalMob += $c->ConsoMobileTarifs;
                $data['c'] = $c;
                $html = $html . $this->load->view('export/export_p5_tabMob2_view',$data,true);
            }


        }
        $data['total'] = $totalMob;
        $html = $html . $this->load->view('export/export_p5_tabMob_conclu_view',$data,true);

        $html = $html . $this->load->view('export/export_p5_projet_entete_view',$data,true);
        foreach ($mobile as $m)
        {
            $data['proj'] = $this->export_model->get_pdt_mobile($m->idMobile,$idDevis);
            foreach($data['proj'] as $p){
                $data['p'] = $p;
                $html = $html . $this->load->view('export/export_p5_projetDet_view',$data,true);
                if($p->BobTarifsHT != NULL)$totalProj = $totalProj + $p->BobTarifsHT;
            }

        }
        $data['proj'] = $this->export_model->get_pdtSup_mobile($idDevis);
        foreach($data['proj'] as $p){
                $data['p'] = $p;
                $html = $html . $this->load->view('export/export_p5_projetDet_view',$data,true);
                if($p->BobTarifsHT != NULL)$totalProj = $totalProj + $p->BobTarifsHT;
            }
        $data['total'] = $totalProj;
        $html = $html . $this->load->view('export/export_p5_pied_view',$data,true);

        return $html;
    }
    public function export_p4($idC,$data,$idDevis)
    {

        $html = '';
        $site = $this->export_model->get_site($idC);
        $html = $html . $this->load->view('export/export_p4_entete_view',$data,true);
        $msg = '';
        $totalSite = 0;
        $totalProj = 0;
        $data['cmptSite'] = 1;

      foreach ($site as $s){
            $totalSite = 0;
            $data['site'] = $s;
            $html = $html . $this->load->view('export/export_p4_tabCtxe_view',$data,true);
            $data['cmptSite'] = $data['cmptSite'] + 1;
            $fixe = $this->export_model->get_fixe($s->idSite);

           foreach($fixe as $f){
                $cmpt = 0;
                $totalSite += $f->FixeTarifsHT;
                $data['fixe'] = $f;
                $html = $html . $this->load->view('export/export_p4_tabCtxe1_view',$data,true);
                $cmpt +=1;
                $conso = $this->export_model->get_conso($f->idFixe);
                foreach($conso as $c){
                    $totalSite += $c->ConsoTarifs;
                    $data['conso'] = $c;
                    $html = $html . $this->load->view('export/export_p4_tabCtxe2_view',$data,true);
                    $cmpt +=1;

                }
            }
          $data['total'] = $totalSite;
             $html = $html . $this->load->view('export/export_p4_tabCtxeConclu_view',$data,true);
      }
        //$html = $html . $this->load->view('export/export_p4_projet_entete_view',$data,true);
        $html = $html . '</div>';
        $data['cmptSite'] = 1;
        $sauvOptEntiPack = '';
        $somTarPack = 0;
        $QtePack = 0;
        $html = $html . '<div id="projet1">';
        foreach ($site as $s){
            $totalProj = 0;
            $fixe = $this->export_model->get_fixe($s->idSite);
            $data['site'] = $s;
            $html = $html . $this->load->view('export/export_p4_projet_entete_view',$data,true);
             $data['cmptSite'] = $data['cmptSite'] + 1;
            foreach($fixe as $f){
                $pack = false;
                $data['proj'] = $this->export_model->get_pdt($f->idFixe,$idDevis);

                foreach($data['proj'] as $p){
                    $data['p'] = $p;
                    if($p->Produit_idProduit === '4' || $p->Produit_idProduit === '5'){
                        if(!$pack){
                            $som = 0;
                            $data['prix'] = $this->projet_model->get_tarifs_pack($p->Projet_idProjet,$p->ChoixPEODurEng);
                           // $qte = $this->projet_model->get_qte_pack($p->Projet_idProjet);
                            
                            
                            for($i=0;$i < count($data['prix']);$i++){
                                //$data['prix'][$i] = $data['prix'][$i]->T * (int)$data['prix'][$i]->Q;
                                $tmpPrix = $data['prix'][$i]->T * $data['prix'][$i]->Q;
                                $tmpPrix = $tmpPrix - ($tmpPrix * ($data['prix'][$i]->R/100));
                                //$data['prix'][$i] = number_format($data['prix'][$i]->T,2) - (number_format($data['prix'][$i]->T,2) * (number_format($data['prix'][$i]->R,2) / 100));
                                
                                //$som +=$data['prix'][$i];
                                $som += number_format($tmpPrix,2);
                
                            }
                          
                            $data['prix'] = $this->projet_model->get_tarifs($p->idEntite,$p->idOptionsEntite,$p->ChoixPEODurEng);
                            $data['link'] = $this->projet_model->get_link_per_product($p->idEntite,$p->idOptionsEntite);
                            $data['prix'][0]->T = $som;
                            $totalProj += $som;
                            $data['p']->ChoixPEOQte = 1;
                            $html = $html . $this->load->view('export/export_p4_projetDet_view',$data,true);
                            $pack = true;
                        }
                    }else{
                        $pack = false;
                        $data['prix'] = $this->projet_model->get_tarifs($p->idEntite,$p->idOptionsEntite,$p->ChoixPEODurEng);
                        $data['link'] = $this->projet_model->get_link_per_product($p->idEntite,$p->idOptionsEntite);
                    // print_r($p);
                        $html = $html . $this->load->view('export/export_p4_projetDet_view',$data,true);
                        //   print_r($p);
                       // $totalProj += number_format($data['prix'][0]->TarifsHT,2) * (int)$p->ChoixPEOQte;
                        $tmpTotal = number_format($data['prix'][0]->TarifsHT,2) * (int)$p->ChoixPEOQte;
                        $tmpTotal = $tmpTotal - ( $tmpTotal * (number_format($p->ChoixPEORemCom,2) / 100 ));
                        $totalProj += $tmpTotal;

                    }

                }
             }
            $data['projSup'] = $this->export_model->get_pdtSup($s->idSite,$idDevis);
            foreach($data['projSup'] as $p){
                $data['p'] = $p;
                $data['prix'] = $this->projet_model->get_tarifs($p->idEntite,$p->idOptionsEntite,$p->ChoixPEODurEng);
                // print_r($p);
                $html = $html . $this->load->view('export/export_p4_projetDet_view',$data,true);
                //   print_r($p);
               // $totalProj += number_format($data['prix'][0]->TarifsHT,2) * (int)$p->ChoixPEOQte;
                $tmpTotal = number_format($data['prix'][0]->TarifsHT,2) * (int)$p->ChoixPEOQte;
               $tmpTotal = number_format($tmpTotal,2) - ( number_format($tmpTotal,2) * (number_format($p->ChoixPEORemCom,2) / 100 ));
                $totalProj += $tmpTotal;

            }
            $data['total'] = $totalProj;
            $html = $html . $this->load->view('export/export_p4_pied_view',$data,true);

         }



        return $html;

    }


    public function export_p2($idC,$data,$idD)
    {
        $data['infos'] = $this->export_model->get_info($idC);
        $data['devis'] = $this->export_model->get_devis($idD);

        $data['obj1'] = $this->export_model->get_objectifs($data['infos'][0]->ClientsObjectifs1);


        $data['obj2'] = $this->export_model->get_objectifs($data['infos'][0]->ClientsObjectifs2);
        $data['obj3'] = $this->export_model->get_objectifs($data['infos'][0]->ClientsObjectifs3);
        $html = $this->load->view('export/export_p2_view',$data,true);
        return $html;
    }
    public function export_p3($idD,$data)
    {
        $html = '';
        $prestaLogo = $this->export_model->get_prestaLogo($idD);

        $html = $this->load->view('export/export_p3_view',$data,true);
        foreach($prestaLogo as $p){
            $data['prestAdr'] = json_decode(json_encode($p), True);
            $html = $html . $this->load->view('export/export_p3_presta_view',$data,true);
        }

        return $html;
    }
    public function export_p7($data,$idD)
    {
        $data['conclu'] = $this->export_model->get_conclu($idD);
        $html = $this->load->view('export/export_p7_view',$data,true);
        return $html;
    }
    public function export_p1($data)
    {
        $html = $this->load->view('export/export_p1_view',$data,TRUE);
      //    $html = $this->load->view('export/export_p4_test_view',$data,TRUE);
        return $html;
    }
    public function export_p8($idC,$data,$idD)
    {


        $html = $this->load->view('export/export_p8_view',$data,true);
        return $html;
    }
    public function export_p9($idC,$data,$idD)
    {

        $Package = 0;
        $html = $this->load->view('export/export_p9_entete_view',$data,true);
        $site = $this->export_model->get_site($idC);
        foreach ($site as $s){

            $data['site'] = $s;
            $fixe = $this->export_model->get_fixe($s->idSite);
            $html1 = $this->load->view('export/export_p9_site_view',$data,true);
            foreach($fixe as $f){
                $proj = $this->export_model->get_pdt($f->idFixe,$idD);
               // print_r($data['proj']);
                foreach($proj as $p){

                    if($p->Produit_idProduit === '4' || $p->Produit_idProduit === '5'){
                        $data['proj'] = $p;
                        $data['prix'] = $this->projet_model->get_tarifs($p->idEntite,$p->idOptionsEntite,$p->ChoixPEODurEng);
                        $html1 = $html1 . $this->load->view('export/export_p9_pdt_view',$data,true);
                        $Package = 1;
                    }
                }

             }
             if($Package === 1){
                 $html = $html . $html1;
                 $Package = false;
                 $html = $html . '</tbody></table></div>';
             }



         }

        return $html;
    }

    public function export_p10($data)
    {
        $html = $this->load->view('export/export_p10_view',$data,true);
        return $html;
    }

}
