<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Site extends CI_Controller { 
    public function __construct() {
        Parent::__construct();
        $this->load->model("site_model");
         $this->load->model("produit_model");
         $this->load->model("consommation_model");
        $this->load->library('session');
        $this->load->helper('url_helper');
         $this->load->helper('cookie');
         $this->load->library('LigneObject');
        $this->load->library('ProduitFixeObject');
    }
 public function index()
     {
     /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $this->load->view("pages/site_view");
        $this->load->view('modals/modal_site_view');
     }
    public function list_site()
     {
/**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
          // Datatables Variables
         /* $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));*/

    //print_r($this->session->userdata('idContexte'));
          $site = $this->site_model->list_site($this->session->userdata('idContexte'));
        
          $data = array();

          foreach($site->result() as $r) {
              $tmp = str_replace(' ','',$r->FixeNumero);
              if($tmp < 100)$r->FixeNumero=NULL;  
               $data[] = array(
                 /*   $r->idSite,
                    $r->SiteAdresse,
                    $r->SiteCodePost,
                    $r->SiteVille,
                    $r->Contexte_idContexte*/
                   'id' => $r->idSite,
                   'idCont' => $r->Contexte_idContexte,
                   'adresse' =>  $r->SiteAdresse,
                   'code' =>  $r->SiteCodePost,
                   'ville' => $r->SiteVille,
                   'idFixe' => $r->idFixe,
                   'fixeNum' => $r->FixeNumero,
                   'opeNomi' => $r->OperateurNomi, 
                   'prodNomi' => $r->PdtFixeNomi,
                   'idType' => $r->FixeType_idFixeType
               );
          }
    

         /* $output = array(
               "draw" => $draw,
                 "recordsTotal" => $site->num_rows(),
                 "recordsFiltered" => $site->num_rows(),
                 "data" => $data
            );*/
          echo json_encode($data);
          exit();
     }
    public function load_site()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idSite=$this->input->post(NULL,TRUE);
        
        $idSite = $idSite['idSite'];
        $site = $this->site_model->get_site($idSite);
        $site = $site->result();
        $this->session->set_userdata('id',$site['0']->idSite);
        $this->session->set_userdata('idContexte',$site['0']->Contexte_idContexte);
     
        echo json_encode($site);
    }
    public function suppr_site()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idSite=$this->input->post(NULL,TRUE);
        $idSite = $idSite['idSite'];
        $site = $this->site_model->suppr_site($idSite);
    }
    function ajax_call()
{
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
    //check to see people wont go directly
        $idope=$this->input->post(NULL,TRUE);
        $pdt = $this->site_model->get_produit($idope['idOpe']);

        //dropdown
        $attributes = 'class = "form-control input-sm" id = "produit"';
        echo form_dropdown('produit', $pdt, set_value('produit') ,$attributes);
    
}
    
    
     function add_ligne()
    {
         /**
         Test OWASP intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
        
         $data = $this->input->post(NULL,TRUE);
         //!is_numeric($data['Tarifs']) || -> Chiffre à virgule
         if( !is_numeric($data['Qte']) ){
            // echo "Veuillez vérifier les champs";
             echo 0;
            // exit('Site / add_ligne Qte : Not Numeric');
             exit();
         }
         if(!empty($data['Num'])){
             $tmp = str_replace(' ','',$data['Num']);
             if(!is_numeric($tmp)){
                 echo 0;
                 exit();
                 //exit('Erreur Number not Numeric');
             }
         }
         $type=$data['Type'];
         $ligne = new LigneObject();
         $ligne->FixeNbeT0 = $data['Qte'];
         
         
         $ligne->FixeTarifsHT = $data['Tarifs'];
         
         if(empty($data['Num'])){
             $nb = $this->site_model->check_ligne($data['idSite']);
             if($nb === NULL){
                 $data['Num']=1;
             }else{
                 $data['Num']=(int)$nb+1;
             }
         }
         if($data['Type'] == NULL)$data['Type']=3;
         $ligne->FixeNumero = $data['Num'];
         $ligne->Site_idSite = $data['idSite'];
         $ligne->PdtFixe_idPdtFixe = $data['idConso'];
         $ligne->FixeType_idFixeType = $data['Type'];
      
         $tmp = $this->site_model->insert_ligne($ligne);
         echo $tmp;
         
        }
             
    
  
    public function suppr_line()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idFixe=$this->input->post(NULL,TRUE);
        $idFixe = $idFixe['idFixe'];
        $site = $this->site_model->suppr_line($idFixe);
    }
    public function load_details(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        /*
        Array
(
    [idSite] => Array
        (
            [0] => 2
            [1] => 32
        )

) */
        $id=$this->input->post(NULL,TRUE);
       // print_r($id);
       
        
        $site = json_decode($id['site']);
        $fixe = json_decode($id['fixe']);
        
       //  print_r($site);
         //   print_r('------');
        //  print_r($fixe);
        
        $txt = '';
        $k=0;
        $sitePrec=0;
    for($j = 0; $j<count($site); $j++){
        if($sitePrec != $site[$j]){
            if($sitePrec != 0)$txt .= '<hr>';
            $tmp = $this->site_model->get_site($site[$j]);
            $tmp = $tmp->result();
      
            $txt .= '<div class="row">
            <div class="col-md-8"><h1>Details Site</h1></div>
            </div>
            <div class=row>
                <div class="col-md-4"><label for="AdresseSite1">Adresse</label><div id="AdresseSite1">'.$tmp[0]->SiteAdresse.'</div></div>
                <div class="col-md-4"><label for="CPSite1">Code Postal</label><div id="CPSite1">'.$tmp[0]->SiteCodePost.'</div></div>
                <div class="col-md-4"><label for="VilleSite1">Ville</label><div id="VilleSite1">'.$tmp[0]->SiteVille.'</div></div>
            </div>';
           // print_r($txt);
        
        for($k = 0; $k<count($fixe); $k++){
            
            $tmp = $this->site_model->get_tarifs($site[$j],$fixe[$k]);
         //  print_r($site);
            if($tmp->num_rows() !=0){
                $tmp = $tmp->result();
//print_r($tmp);
                 $tmp1 = str_replace(' ','',$tmp[0]->FixeNumero);
                if((int)$tmp1 < 100)$tmp[0]->FixeNumero='';
          
                $txt .= '<div class="row"><div class="col-md-8"><h1>Details Numero : '.$tmp[0]->FixeNumero.'</h1></div></div>';
                $txt .= '<div class="row">';
                $txt .= '<div class="col-xs-6 col-sm-3">';
                $txt .=  '<label>Quantité de produits</label><div>'.$tmp[0]->FixeNbeT0.'</div></div>';


                $tmp1 = $this->produit_model->get_produit($tmp[0]->PdtFixe_idPdtFixe);
                $tmp1 = $tmp1->result();
               // print_r($tmp1);
                
                $txt .= '<div class="col-xs-6 col-sm-3">';
                $txt .=  '<label>Operateur</label><div>'.$tmp1[0]->OperateurNomi.'</div></div>';
                $txt .= '<div class="col-xs-6 col-sm-3">';
                $txt .=  '<label>Produits</label><div>'.$tmp1[0]->PdtFixeNomi.'</div></div>';
                $txt .= '<div class="col-xs-6 col-sm-3">';
                $txt .=  '<label>Tarifs fixe HT à l\'unitée</label><div>'.$tmp[0]->T.' €</div></div></div>';

               

                $tmp2 = $this->consommation_model->get_conso($fixe[$k]);
                $tmp2 = $tmp2->result();
                if(!empty($tmp2)){
                     $txt .= '<div class = "row"><div class="col-md-4"></div><div class="col-md-4"><h1>Options/Consommations</h1></div></div>';
                }
               foreach($tmp2 as $tmp2){
                   
                    $txt .= '<div class = "row"><div class="col-xs-6 col-sm-2">';
                    $txt .= '<label>Quantité</label><div>'.$tmp2->ConsoQte.'</div></div>';
                    $txt .= '<div class="col-xs-6 col-sm-6"><label>Option/Consommation</label><div>'.$tmp2->ConsoTypeNomi.'</div></div>';
                      
                    $txt .= '<div class="col-md-3"><label>Tarifs</label><div>'.$tmp2->T.' €</div>';
                    $txt .= '</div></div>';
                }
                $sitePrec = $site[$j];
            }
        }
    }
    }
        echo $txt;
    }
   public function add_conso()
   {
       /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idFixe=$this->input->post(NULL,TRUE);
      // print_r($idFixe);
        if(empty($idFixe['Qte']))$idFixe['Qte']=1;
        $data = array(
                'Fixe_idFixe' =>  $idFixe['idFixe'],
                'ConsoType_idConsoType'  => $idFixe['idConso'],
                'ConsoTarifs'  => $idFixe['Tarifs'],
                'ConsoQte' => $idFixe['Qte']
            );
           // print_r($data);
            $fixe = $this->site_model->add_conso($data);
        
            if(!$fixe){
                echo "NO";
            }else{
                echo "OK";
            }
        
    }
    public function add_pdtCtxe()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $test = false;
        $pdt = new ProduitFixeObject();
        $data = $this->input->post(NULL,TRUE);
        //print_r($data);
        if(!is_numeric($data['nomiOpe'])){
            $data['nomiOpe'] = addslashes($data['nomiOpe']);
            $data['nomiOpe'] = $this->site_model->insert_Ope($data['nomiOpe']);
            
            $test = true;
        }
        $pdt->PdtFixeNomi = addslashes($data['pdtNomi']);
        $pdt->Operateur_idOperateur = $data['nomiOpe'];
        $pdt->PdtFixeType_idPdtFixeType = $data['typePdt'];
        $pdt->PdtFixeRq = 'user';
        $this->site_model->insert_PdtCtxe($pdt);
        if($test){
            echo $data['nomiOpe'];
        }else{
            echo 0;
        }
        
    }
}
?>