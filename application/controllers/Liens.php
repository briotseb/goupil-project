<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Liens extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('security');
         $this->load->model('user_model');
         $this->load->model('liens_model');
        $this->load->model('new_devis_model');
        $this->load->helper('cookie');
    }
    public function index() {
         
        //Session Non ouverte redirection page login
        if(!$this->session->userdata('username')) redirect('login/index');
         if($this->input->cookie('client') !== NULL){
              delete_cookie('client');
              delete_cookie('tech');
         }
        
        $page = array(
            'page' => 9
        );
        $this->load->view('templates/header',$page);
        
        /**
        Affichage Liens de RedServices
        */
        $this->load->view('liens/liens_services_view');
        $liens = $this->new_devis_model->get_services();
        foreach($liens as $l){
            $data['l'] = $l;
            $this->load->view('liens/liens_intitule_service_view',$data);
            //$tuple = $this->liens_model->get_tuple_services($l->idCategorieLiens);
             $tuple = $this->new_devis_model->get_presta_serv($l->idPrestataire);
            foreach($tuple as $t){
                $data['t'] = $t;
                $this->load->view('liens/liens_tuple_service_view',$data);
            }
            $this->load->view('liens/liens_pied_service_view');
        }
        $this->load->view('liens/liens_conten_view');
        
        /**
        Affichage Liens Operateur SFR,...
        */
        $this->load->view('liens/liens_operateur_view');
        $liens = $this->liens_model->get_categorie_services(2);
        foreach($liens as $l){
            $data['l'] = $l;
            $this->load->view('liens/liens_intitule_operateur_view',$data);
            $tuple = $this->liens_model->get_tuple_services($l->idCategorieLiens);
            foreach($tuple as $t){
                $data['t'] = $t;
                $this->load->view('liens/liens_tuple_operateur_view',$data);
            }
            $this->load->view('liens/liens_pied_service_view');
        }
        $this->load->view('liens/liens_conten_view');
        
        /**
        Affichage Liens Terminaux
        */
        $this->load->view('liens/liens_terminaux_view');
        $liens = $this->liens_model->get_categorie_services(3);
        foreach($liens as $l){
            $data['l'] = $l;
            $this->load->view('liens/liens_intitule_operateur_view',$data);
            $tuple = $this->liens_model->get_tuple_services($l->idCategorieLiens);
            foreach($tuple as $t){
                $data['t'] = $t;
                $this->load->view('liens/liens_tuple_operateur_view',$data);
            }
            $this->load->view('liens/liens_pied_service_view');
        }
        $this->load->view('liens/liens_conten_view');
       $this->load->view('liens/liens_conten_view');
        
        $this->load->view('templates/footer');
     }
    
}
?>