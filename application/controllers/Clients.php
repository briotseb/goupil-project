<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Clients extends CI_Controller { 
    public function __construct() 
    {
        Parent::__construct();
        $this->load->model("clients_model");
        $this->load->model("site_model");
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->model('contexte_model');
        $this->load->library('ClientsObject');
        $this->load->library('TechniqueObject');
        $this->load->helper('security');
    }
    public function clients_list()
     {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $clients = $this->clients_model->list_clients($this->session->userdata('iduser'));

        $data = array();

        foreach($clients->result() as $r) {

               $data[] = array(
                   'id' => $r->idClients,
                   'Nom' => $r->ClientsNom,
                   'Entr' =>  $r->ClientsNomEntreprise,
                   'Adr' =>  $r->ClientsAdresse,
                   'code' => $r->ClientsCodePost,
                   'ville' => $r->ClientsVille,
                   'siret' => $r->ClientsSiret,
                   'tel' => $r->ClientsTel, 
                   'mob' => $r->ClientsTelMob,
                   'mail' => $r->ClientsMail,
                   'idCont' => $r->idContexte
               );
          }
    

         /*  $array[$i] = array('id' => $row["idClients"],'Nom' => $row["ClientsNom"],'Entr' => $row["ClientsNomEntreprise"],'Adr' => $row["ClientsAdresse"],'code' => $row["ClientsCodePost"],'ville' => $row["ClientsVille"],'siret' => $row["ClientsSiret"], 'tel' => $row["ClientsTel"], 'mob' => $row["ClientsTelMob"],'mail' => $row["ClientsMail"],'idCont'=> $row["idContexte"]);
            $i++;
            );*/
        //print_r($this->session->userdata('iduser'));
          echo json_encode($data);
          exit();
     }
    
     public function index() 
     {
         
        //Session Non ouverte redirection page login
         if(!$this->session->userdata('username')) redirect('login/index');
         $page = array(
            'page' => 2
        );
        
         if($this->input->cookie('client') !== NULL){
              delete_cookie('client');
              delete_cookie('tech');
         }
         $this->session->unset_userdata('devis');
        
        $this->session->unset_userdata('idContexte');
       //  print_r($this->session->all_userdata());
        $this->load->view('templates/header',$page);
        $this->load->view('pages/clients_view');
        $this->load->view('templates/footer');
     }
     public function load_client(){
         /**
          *Test OWASP non intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
        $id=$this->input->post(NULL,TRUE);
         //   print_r($idCont);                           
        $client = $this->clients_model->get_client($id['id']);
        $client = $client->result();
        $tech = $this->clients_model->get_tech($client[0]->idClients);
        $tech = $tech->result();
    //  print_r($client);
        
        $this->session->set_userdata('idContexte',$id['idCont']);
        set_cookie('client',json_encode($client),'0');
        set_cookie('tech',json_encode($tech),'0');
        return true;
      }
    public function load_client_modif(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $id=$this->input->post(NULL,TRUE);   
        $client = $this->clients_model->get_client($id['id']);
        $client = $client->result();
        $tech = $this->clients_model->get_tech($client[0]->idClients);
        $tech = $tech->result();
        $this->session->set_userdata('idContexte',$id['idCont']);
        //  $idSite = $this->site_model->get_id($id['idCont']['0'],$client->ClientsAdresse);
        //print_r($idSite);
        $data['client'] = $client;
        $data['tech'] = $tech;
        $data['statut'] = $this->contexte_model->get_statut();
         $data['civilite'] = $this->contexte_model->get_civilite();
         $data['gdsAccords'] = $this->contexte_model->get_accords();
           
       
        $this->load->view('modals/modal_modif_societe_view',$data);
            
       
      }
    
    public function suppr_client()
        
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         $id=$this->input->post(NULL,TRUE);
        $this->clients_model->suppr_client($id['id']);
        
    }

    public function update_client()
        
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        
        $this->form_validation->set_rules('statut', 'Statut', 'required|callback_combo_check');
        $this->form_validation->set_rules('entreprise', 'Entite de l\'entreprise', 'trim|required');
        $this->form_validation->set_rules('adresse', 'Adresse de l entreprise', 'trim|required|xss_clean');
        $this->form_validation->set_rules('codeP', 'Code Postal', 'required|numeric');
        $this->form_validation->set_rules('ville', 'ville', 'required|trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('telF', 'Téléphone Fixe', 'numeric');
        $this->form_validation->set_rules('telM', 'Téléphone Mobile', 'numeric');
        $this->form_validation->set_rules('telFax', 'Téléphone Fax', 'numeric');
        $this->form_validation->set_rules('adreWeb', 'Adresse du site web', 'trim|max_length[100]|xss_clean|prep_url|valid_url');
       // if($this->input->post('mail'))$this->form_validation->set_rules('mail', 'Courriel', 'required|valid_email');
       $this->form_validation->set_rules('mail', 'Courriel', 'required|valid_email');
        if($this->input->post('civiG') != 0)$this->form_validation->set_rules('civiG', 'Civilité du Gerant', 'callback_combo_check');
        if($this->input->post('nomG'))$this->form_validation->set_rules('nomG', 'Nom du Gérant', 'trim|xss_clean');
        if($this->input->post('prenomG'))$this->form_validation->set_rules('prenomG', 'Prénom du Gérant', 'trim|xss_clean');
        if($this->input->post('civiT') != 0)$this->form_validation->set_rules('civiT', 'Civilité du Technique', 'callback_combo_check');
        if($this->input->post('nomT'))$this->form_validation->set_rules('nomT', 'Nom du Technique', 'trim|xss_clean');
        if($this->input->post('prenomT'))$this->form_validation->set_rules('prenomT', 'Prénom du Technique', 'trim|xss_clean');
        $this->form_validation->set_rules('telFTech', 'Téléphone Fixe Technique', 'numeric');
        $this->form_validation->set_rules('telMTech', 'Téléphone Mobile Technique', 'numeric');
        if($this->input->post('mailTech'))$this->form_validation->set_rules('mailTech', 'Courriel', 'valid_email');
        if($this->input->post('numSiret'))$this->form_validation->set_rules('numSiret', 'Numéro de Siret', 'numeric');
        if($this->input->post('GCSocieteCheck'))$this->form_validation->set_rules('GCSociete', 'Grands Accords', 'callback_combo_check');
        if ($this->form_validation->run() == FALSE)
        {   //validation fails
            echo validation_errors();
        }else{
            $Cont = new ClientsObject();
            $Cont->idClients = $this->input->post('idc');
            $Cont->Statut_idStatut = $this->input->post('statut');
            $Cont->ClientsNomEntreprise = $this->input->post('entreprise');
            $Cont->ClientsNom = $this->input->post('nomG');
            $Cont->ClientsPrenom = $this->input->post('prenomG');
            $Cont->ClientsAdresse = $this->input->post('adresse');
            $Cont->ClientsCodePost =  $this->input->post('codeP');
            $Cont->ClientsVille = $this->input->post('ville');
            $Cont->Civilite_idCivilite = $this->input->post('civiG');
            $Cont->Utilisateurs_idUtilisateurs = $this->session->userdata('iduser');   
            $Cont->ClientsSiret = $this->input->post('numSiret');
            $Cont->ClientsTel = $this->input->post('telF');
            $Cont->ClientsTelMob = $this->input->post('telM');
            $Cont->ClientsMail = $this->input->post('mail');
            $Cont->ClientsAdresseWeb = $this->input->post('adreWeb');
            $Cont->ClientsTelFax = $this->input->post('telFax');
                    
            // $Cont->Comment = $this->input->post('comment');
            if($this->input->post('GCSocieteCheck') === '1')$Cont->GdsAccords_idGdsAccords = $this->input->post('GCSociete');
            if($this->input->post('RecSocieteCheck') === '1')$Cont->ClientsReceptel = 1;
            if($this->input->post('CapSocieteCheck') === '1')$Cont->ClientsCAP = 1;
            $this->clients_model->update_client($Cont);
            
            
            $Tech = new TechniqueObject();
            $Tech->Clients_idClients=$this->input->post('idc');
            $Tech->TechniqueNom = $this->input->post('nomT');
            $Tech->TechniquePrenom = $this->input->post('prenomT');
            $Tech->TechniqueTel = $this->input->post('telFTech');
            $Tech->TechniqueTelMob = $this->input->post('telMTech');
            $Tech->TechniqueMail = $this->input->post('mailTech');
            $Tech->Civilite_idCivilite = $this->input->post('civiT'); 
            $this->clients_model->update_tech($Tech);
            
            redirect('Clients/index');
        }   
    }
     //custom validation function for dropdown input
        function combo_check($str)
        {
            if ($str == '-Choisir-')
            {
                $this->form_validation->set_message('combo_check', 'Le champ Statut est requis');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }

        //custom validation function to accept only alpha and space input
        function alpha_only_space($str)
        {
            if (!preg_match("/^.*([-a-z ])+.*$/i", $str))
            {
                $this->form_validation->set_message('alpha_only_space', 'The %s field must contain only alphabets or spaces');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
    
}
?>