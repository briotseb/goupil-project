<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Devis extends CI_Controller { 
    public function __construct() 
    {
       Parent::__construct();
        $this->load->model("devis_model");
        $this->load->model("new_devis_model");
        $this->load->model("contexte_model");
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->model("site_model");
        $this->load->model("categorie_model");
        
        
       
    }
   public function list_devis()
     {
       /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
       
        $devis = $this->devis_model->list_devis($this->session->userdata('iduser'));

        $data = array();

        foreach($devis->result() as $r) {

               $data[] = array(
                   'idCont' => $r->idContexte,
                   'numDev' => $r->idDevis,
                   'NomEntr' =>  $r->ClientsNomEntreprise,
                   'ville' =>  $r->ClientsVille,
                   'dateDev' => $r->DevisDate,
                   'comDev' => $r->DevisComment
               );
          }
    /*
     $array[$i] = array('idCont' => $row["idContexte"],'numDev' => $row["idDevis"],'NomEntr' => $row["ClientsNomEntreprise"],'ville' => $row["ClientsVille"],'dateDev' => $row["DevisDate"],'comDev' => $row["DevisComment"]);
    
    */

        
          echo json_encode($data);
          exit();
     }
    public function index() 
     {
         
        //Session Non ouverte redirection page login
         if(!$this->session->userdata('username')) redirect('login/index');
         $page = array(
            'page' => 4
        );
        if(get_cookie('client') !== NULL){
            delete_cookie('client');
            delete_cookie('tech');
              
        }
      
       $this->session->unset_userdata('devis');
        $this->load->view('templates/header',$page);
        $this->load->view('pages/devis_view');
        $this->load->view('templates/footer');
     }
     public function suppr_devis()
    {
         /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $id=$this->input->post(NULL,TRUE);
        
        $this->devis_model->suppr_devis($id['id']);
        
    }
    public function load_devis()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $page = array(
            'page' => 4
        );
        $id = $this->input->post(NULL,TRUE);
      /*
      Array
(
    [id] => Array
        (
            [0] => 184
        )

)
      
      */
        
       //Construction des variables
        $donnees = $this->devis_model->get_devis($id['id'][0]);
        /*  
        Array
(
    [0] => stdClass Object
        (
            [idBob] => 6
            [BobNomi] => Ligne Analogique Fax (Basse conso)
            [BobTarifsHT] => 15.12
            [BobPri] => 15.12
            [BobSpot] => 16.3339
            [BobEng] => 12
            [BobMS] => 45
            [ChoixPEO_idChoixPEO] => 14
            [idChoixPEO] => 14
            [Produit_idProduit] => 1
            [Entite_idEntite] => 1
            [OptionsEntite_idOptionsEntite] => 
            [Projet_idProjet] => 184
            [ChoixPEOQte] => 1
            [ChoixPEODurEng] => 12
            [ChoixPEORemCom] => 24
            [ChoixPEORemFMA] => 0
            [ChoixPEOTarHt] => 
            [idProjet] => 184
            [Mobile_idMobile] => 
            [Fixe_idFixe] => 47
            [Devis_idDevis] => 191
            [ProjetNum] => 1
            [ProjetTotal] => 
            [Site_idSite] => 2
            [Conclusion_idConclusion] => 
        )
*/
        
        foreach ($donnees as $d){
            //Tarrifs
            $tmp = 'tarProj'.$d->Site_idSite.''.$d->FixeNumero;
            $data[$tmp] = $d->BobTarifsHT;
            $tmp = 'priProj'.$d->Site_idSite.''.$d->FixeNumero;
            $data[$tmp] = $d->BobPri;
            $tmp = 'engProj'.$d->Site_idSite.''.$d->FixeNumero;
            $data[$tmp] = $d->BobEng;
            $tmp = 'pdtProj'.$d->Site_idSite.''.$d->FixeNumero;
            $data[$tmp] = $d->BobNomi;
            $tmp = 'totalSite'.$d->Site_idSite;
            $data[$tmp] = $d->BobNomi;
        }
      
        
        //Chargement Page
        $this->session->set_userdata('idContexte',$id['idc'][0]);
        $idContexte = $id['idc'][0];
         $data['site'] = $this->contexte_model->list_ctxe_site($id['idc'][0]);
         $data['PdtFixe'] = $this->categorie_model->get_PdtFixe();
         $data['idContexte'] = $id['idc'][0];
         //$data['site'] => Tableau d'objet de type site
         //print_r($data['site']);
         
         $this->load->view('contexte/new_devis/new_devis_entete_view');
         $site = $data['site'];
         $numSite = 1;
         $numMobile = 1;
         $total = 0;
         foreach($site as $s)
             //$s : site object
         {
             $data['numSite'] = $numSite;
             $data['s']=$s;
             $this->load->view('contexte/new_devis/new_devis_site_view',$data);
             
             $data['fixe'] = $this->contexte_model->list_ctxe_fixe($s->idSite);
             
             foreach($data['fixe'] as $f)
             {
                 $data['f']=$f;
                 //print_r($f);
                 $this->load->view('contexte/new_devis/new_devis_fixe_view',$data);
                 
                 $data['conso'] = $this->contexte_model->list_ctxe_conso_by_fixe($f->idFixe);
                 //print_r($data['conso']);
                 foreach($data['conso'] as $c)
                 {
                     $data['c']=$c;
                     $this->load->view('contexte/new_devis/new_devis_conso_view',$data);
                 }
             }
             
             //PAS DE FIXE SUR SITE
             if(empty($data['fixe'])){
                 $this->load->view('contexte/new_devis/new_devis_fixe_view',$data);
                 $this->load->view('contexte/new_devis/new_devis_conclu_view',$data);
                 
             }else{
                 $data['totalSite'] = $this->contexte_model->list_ctxe_total_by_site($s->idSite);
                 $total += $data['totalSite'];
                 $this->load->view('contexte/new_devis/new_devis_conclu_view',$data);
             }
             $numSite += 1 ;
         }
         
         $this->load->view('contexte/new_devis/new_devis_mobile_entete_view');
         
         $data['mobile'] = $this->contexte_model->list_ctxe_mob($idContexte);
         $data['PdtMobile'] = $this->categorie_model->get_PdtMobile();
         
         foreach($data['mobile'] as $m)
         {
             $data['numMobile'] = $numMobile;
             $data['m'] = $m;
             $this->load->view('contexte/new_devis/new_devis_mobile_view',$data);
             
             $data['conso'] = $this->contexte_model->list_ctxe_conso_by_mob($m->idMobile);
             foreach($data['conso'] as $c)
                 {
                     $data['c']=$c;
                     $this->load->view('contexte/new_devis/new_devis_mobile_conso_view',$data);
                 }
             $data['totalMob'] = $this->contexte_model->list_ctxe_total_by_mob($m->idMobile);
             $total += $data['totalMob'];
             $this->load->view('contexte/new_devis/new_devis_mob_conclu_view',$data);
         }
         if(empty($data['mobile'])){
             $this->load->view('contexte/new_devis/new_devis_mobile_view',$data);
             $this->load->view('contexte/new_devis/new_devis_mob_conclu_view',$data);
         }
         
         //LES OPTIONS CAP
         if($this->contexte_model->check_cap($idContexte))
         {
            //load view Cap
             $data['cap'] = $this->contexte_model->get_cap();
             $this->load->view('contexte/new_devis_cap_view',$data);
           
         }
         //CONCLUSION CTXE
         $data['totalCtxe'] = $total;
        $this->load->view('contexte/new_devis_ctxe_conclu',$data);
         
         
         
        
    }
}
?>