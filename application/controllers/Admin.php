<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('security');
         $this->load->model('user_model');
         $this->load->model('admin_model');
    }
     public function index() {
         
        //Session Non ouverte redirection page login
         if(!$this->session->userdata('username')) redirect('login/index');
      
         $data = $this->user_model->getAdminInfo($this->session->userdata('iduser'));
        //print_r($data);

        $page = array(
            'page' => 10
        );
        $this->load->view('templates/header',$page);
        $this->load->view('pages/admin_view',$data);
        $this->load->view('templates/footer');
     }
    public function resi_sent(){
       
            $to      = 'sbbriot@gmail.com';
            $subject = 'Demande de resiliation';
            $message = $this->input->post('textEmail');
         
            $headers = 'From: ' . $this->input->post('ctemail') . "\r\n" .
            'Reply-To: ' . $this->input->post('ctemail') . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
       
        $this->session->set_flashdata('msg', 'Email Envoye');
    }
    public function list_facture(){
        
        $clients = $this->admin_model->list_fact($this->session->userdata('iduser'));

        $data = array();

        foreach($clients->result() as $r) {

               $data[] = array(
                   'id' => $r->idFactures,
                   'Nom' => $r->FacturesNomi,
                   'Act' =>  '<a target="_blank" href="'. base_url("assets/user/".$r->FacturesAdresse) . '">Ouvrir</a>'
               );
          }
          echo json_encode($data);
          exit();
    }
}
?>
