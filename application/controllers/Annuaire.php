<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Annuaire extends CI_Controller { 
    public function __construct() 
    {
        Parent::__construct();
        $this->load->model("annuaire_model");
        $this->load->model("clients_model");
        $this->load->model("contexte_model");
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->library('TechniqueObject');
        $this->load->helper('security');
    }
     public function index() 
     {
         
        //Session Non ouverte redirection page login
         if(!$this->session->userdata('username')) redirect('login/index');
        
         //Indic au header les logos en rouge
         $page = array(
            'page' => 8
        );
        
         if($this->input->cookie('client') !== NULL){
              delete_cookie('client');
              delete_cookie('tech');
         }
         $this->session->unset_userdata('devis');
        
        $this->session->unset_userdata('idContexte');
     
        $this->load->view('templates/header',$page);
        $this->load->view('pages/annuaire_view');
        $this->load->view('templates/footer');
     } 
    public function details_client(){
        $id = $this->input->post(NULL,TRUE);
        $client = $this->clients_model->get_client($id['id']);
        $client = $client->result();
        $data['client'] = $client;
        $tech = $this->clients_model->get_tech($client[0]->idClients);
        $data['tech'] = $tech->result();
        $data['statut'] = $this->contexte_model->get_statut();
        $data['civilite'] = $this->contexte_model->get_civilite();
        $data['gdsAccords'] = $this->contexte_model->get_accords();
        $tmp = $this->load->view('annuaire/ficheclient_view',$data,true);
        echo $tmp;
    }
    public function clients_list()
     {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $clients = $this->annuaire_model->list_clients($this->session->userdata('iduser'));

        $data = array();

        foreach($clients->result() as $r) {

               $data[] = array(
                   'id' => $r->idClients,
                   'Nom' => $r->ClientsNom,
                   'Entr' =>  $r->ClientsNomEntreprise,
                   'Adr' =>  $r->ClientsAdresse,
                   'code' => $r->ClientsCodePost,
                   'ville' => $r->ClientsVille,
                   'siret' => $r->ClientsSiret,
                   'tel' => "<a href=tel:".$r->ClientsTel.">".$r->ClientsTel."</a>", 
                   'stat' => $r->StatutNomi,
                   'mail' => $r->ClientsMail,
                   'idCont' => $r->idContexte
               );
          }
        echo json_encode($data);
        exit();
     }
}
?>