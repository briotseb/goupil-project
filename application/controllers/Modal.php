<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Modal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->model('produit_model');
         $this->load->model('site_model');
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('security');
        
        $this->load->helper('cookie');
        
    } 
    function ajax_call()
    {
    /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idope=$this->input->post(NULL,TRUE);
        if(!is_numeric($idope['idOpe'])){
            exit('(ajax_call Modal) idOperateur : Not Numeric');
        }
        $pkg = $this->produit_model->get_pkg($idope['idOpe']);

    //    print_r($pdt);
        //dropdown
        $attributes = 'class = "form-control input-sm" id = "package"';
        echo form_dropdown('package', $pkg, set_value('package') ,$attributes);
    
    }
    function opt_Pkg()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idope=$this->input->post(NULL,TRUE);
        if(!is_numeric($idope['idOpe'])){
           exit('(opt_pkg Modal)idOperateur : Not Numeric');
        }
        $pkg = $this->produit_model->get_opt_pkg($idope['idOpe']);
       // print_r($pkg);
        $res = '';
        foreach($pkg as $p)
        {
             $res .= '<div>'.$p->ConsoTypeNomi.'</div>';
        }
       
        echo $res;
    }
    
    
    public function opt_call()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $id = $this->input->post(NULL,TRUE);
        $opt = $this->produit_model->get_conso($id['id']);
        $opt[999] = "Ajouter une option";
        
        $attributes = 'class = "form-control input-sm" id = "packageOpt" onchange = "open_addOpt()"';
        echo form_dropdown('packageOpt', $opt, set_value('packageOpt') ,$attributes);
        
    }
    
    public function add_pkgCtxe()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $id = $this->input->post(NULL,TRUE);
        $optpkg_list = explode(',',$id['optpkg']);
        // print_r($optpkg_list);
          /*
     //SITUATION AJOUT D4OPTION SUR PCKAGE EXISTANT
     Array
(
    [nomiOpe] => 1
    [ope] => 83
    [optpkg] => Array
        (
            [0] => option3
            [1] => option4
        )

)
// SITUATION AJOUT OPE + PACK + OPTION
Array
(
    [nomiOpe] => jose
    [ope] => joseph
    [optpkg] => Array
        (
            [0] => option1
            [1] => option2
        )

)

Array
(
    [nomiOpe] => 1
    [pkg] => 301
    [optpkg] => Essai
)
        
        */
        
       // $idOpe = $this->produit_model->add_ope($id['nomiOpe']);
        
        //
        $test = false;
        /**
        * Si selectionner -> id de l'operateur sinon texte
        * Nouvel Operateur ou non ?
        */
        if(!is_numeric($id['nomiOpe'])){
            $id['nomiOpe'] = addslashes($id['nomiOpe']);
            $id['nomiOpe'] = $this->site_model->insert_Ope($id['nomiOpe']);
            $test = true;
        }
        $id_pkg = $this->produit_model->add_pkg($id['nomiOpe'],$id['pkg']);
        if(!is_numeric($id_pkg)){
            exit('Modal add_pkgCtxe $id_pkg : not Numeric');
        }
        $this->produit_model->add_optPkg($id['nomiOpe'],$id_pkg,$optpkg_list);
        
        if($test){
            echo $id['nomiOpe'];
        }else{
            echo 0;
        }
        
       
      
    }
    public function add_optPkg()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $donnees = $this->input->post(NULL,TRUE);
        $id = $this->produit_model->add_optPkgSeul($donnees['pkg'],$donnees['optpkg']);
        echo $id;
    }
  
}
?>