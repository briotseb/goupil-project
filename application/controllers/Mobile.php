<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Mobile extends CI_Controller { 
    public function __construct() {
        Parent::__construct();
        $this->load->helper('form');
        $this->load->model("mobile_model");
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
        $this->load->library('MobileObject');
       
    }
    public function index()
     {
     
 }
    public function list_mobile()
     {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $site = $this->mobile_model->list_mobile($this->session->userdata('idContexte'));
        $data = array();
        foreach($site->result() as $r) {
            $tmp = str_replace(' ','',$r->MobileNumero);
            if($tmp < 100)$r->MobileNumero=NULL;   
            $data[] = array(
                'idMob' => $r->idMobile,
                'idCont' => $r->Contexte_idContexte,
                'mobNum' =>  $r->MobileNumero,
                'info' => $r->MobileInfos,
                'DateD' => $r->MobileDateDeb,
                'DateF' => $r->MobileDateFin,
                'TarForf' => $r->MobileForfTarifsHT,
                'DetForf' => $r->MobileDetailsForf, 
                'idOpe' => $r->Operateur_idOperateur,
                'OpeNomi' => $r->OperateurNomi,
                'ConsoTypeNomi' => $r->ConsoTypeNomi,
                'ConsoTypeTar' => $r->ConsoMobileTarifs,
                'idConso' => $r->ConsoType_idConsoType
            );
        }
        echo json_encode($data);
        exit();   
    }
    
    function old_ajax_call(){
         /*      
    //check to see people wont go directly
        $idope=$this->input->post(NULL,TRUE);
       // print_r($idope);
        $pdt = $this->mobile_model->get_produit($idope['idOpe']);

       
        //dropdown
        $attributes = 'class = "form-control input-sm" id = "produit"';
        echo form_dropdown('produit', $pdt, set_value('produit') ,$attributes);*/
        
       /* header("Content-Type: text/xml");
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
        echo "<list>";*/
    }
    function ajax_call()
    {
        /**
         Test OWASP non - intrusion
         **/
        if(!$this->session->userdata('username')) redirect('login/index');
        $id = $this->input->post(NULL,TRUE);

        if ($id) {
            $pdt = $this->mobile_model->get_produit($id);
            //dropdown
            $attributes = 'class = "form-control input-sm" id = "produit"';
            echo form_dropdown('produit', $pdt, set_value('produit') ,$attributes);
        }    
    }
    function add_mobile()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        //set validation rules
       // $this->form_validation->set_rules('Mobile1Nb', 'Numéro de Mobile', 'required|numeric');
        $this->form_validation->set_rules('Mobile1DD', 'Date de debut', 'trim');
        $this->form_validation->set_rules('Mobile1DF', 'Date de fin', 'trim');
        $this->form_validation->set_rules('operateur', 'Operateur', 'required|numeric');
        $this->form_validation->set_rules('produit', 'Produit', 'required|numeric');
       // $this->form_validation->set_rules('Mobile1Forfait', 'Tarifs Forfait', 'required|numeric');
        $this->form_validation->set_rules('Mobile1Forfait', 'Tarifs Forfait', 'required|numeric');
        $this->form_validation->set_rules('Mobile1Option', 'Détails du forfait', 'trim|alpha_numeric');
        //$this->form_validation->set_rules('Mobile1Inf', 'Informations Complementaires', 'trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('Mobile1Inf', 'Informations Complementaires', 'trim');
        
        if ($this->form_validation->run() == FALSE)
        {   //validation fails
            echo validation_errors();
        }else{
            $mobile = new MobileObject();
            if(empty($this->input->post('Mobile1Nb'))){
                $nb = $this->mobile_model->check_mobile($this->session->userdata('idContexte'));
                if($nb === NULL){
                    $mobile->MobileNumero=1;
                }else{
                    $mobile->MobileNumero=(int)$nb+1;
                }
            }else{
                $mobile->MobileNumero = $this->input->post('Mobile1Nb',TRUE);
            }
            
            $mobile->MobileDateDeb = $this->input->post('Mobile1DD',TRUE);
            $mobile->MobileDateFin = $this->input->post('Mobile1DF',TRUE);
            $mobile->Contexte_idContexte = $this->session->userdata('idContexte',TRUE);
            
            if($this->input->post('operateur',TRUE) == 0)$mobile->Operateur_idOperateur = null;
            else $mobile->Operateur_idOperateur = $this->input->post('operateur',TRUE);
            
            
            if($this->input->post('produit',TRUE) == 0)$mobile->PdtMobile_idPdtMobile = null;
            else $mobile->PdtMobile_idPdtMobile = $this->input->post('produit',TRUE);
            
            $mobile->MobileForfTarifsHT = $this->input->post('Mobile1Forfait',TRUE);  
            $mobile->MobileDetailsForf = $this->input->post('Mobile1Option',TRUE);  
            $mobile->MobileInfos = $this->input->post('Mobile1Inf',TRUE);  
          
            $this->mobile_model->insert_mobile($mobile);
            redirect('contexte/index');
        }
             
    }
    function checkDateFormat($date) {
        if (preg_match("/[0-31]{2}\/[0-12]{2}\/[0-9]{4}/", $date)) {
            if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
                return true;
            else
                return false;
        } else {
            return false;
        }
    } 
    public function load_mobile()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idMob=$this->input->post(NULL,TRUE);
        //print_r($idMob);
        
        $idMob = $idMob['idMob'];
        $mob = $this->mobile_model->get_mobile($idMob);
        $mob = $mob->result();
        $this->session->set_userdata('id',$mob['0']->idMobile);
        $this->session->set_userdata('idContexte',$mob['0']->Contexte_idContexte);
     
        echo json_encode($mob);
    }
     public function suppr_mobile()
    {
         /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
         $idMob = $this->input->post(NULL,TRUE);
         $mob = $this->mobile_model->suppr_mobile($idMob['idMob']);
    }
    
    
    public function add_conso(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $idMob=$this->input->post(NULL,TRUE);
       //print_r($idMob['idConso']);
  
        $data = array(
                'Mobile_idMobile' =>  $idMob['idMob'],
                'ConsoType_idConsoType'  => $idMob['idConso'],
                'ConsoMobileTarifs'  => $idMob['Tarifs']
            );
            
            $mob = $this->mobile_model->add_conso($data);
        
            if(!$mob){
                echo "NO";
            }else{
                echo "OK";
            }
        
    }
    public function load_mobile_conso(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        
         $idMob=$this->input->post(NULL,TRUE);
        //print_r($idMob);
        
        $idMob = $idMob['idMob'];
        $mob = $this->mobile_model->get_mobile($idMob);
        $mob = $mob->result();
        
    }
    public function suppr_conso(){
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        
        $data=$this->input->post(NULL,TRUE);
        /* [idMob] => 69
    [idConso] => 5*/
        
       
      $this->mobile_model->suppr_conso($data);
    }
public function add_pdtCtxe()
        
    {
    /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $test = false;
       
        $data = $this->input->post(NULL,TRUE);
        
        if(!is_numeric($data['nomiOpe'])){
            $data['nomiOpe'] = $this->mobile_model->insert_Ope($data['nomiOpe']);
            $test = true;
        }
        
        $data1 = array('idPdtMobile' => 'NULL', 'PdtMobileNomi' => $data['pdtNomi'],'Operateur_idOperateur' => $data['nomiOpe'], 'PdtMobRq' => 'user');
        
       
        $this->mobile_model->insert_PdtCtxe($data1);
        if($test){
            echo $data['nomiOpe'];
        }else{
            echo 0;
        }
        
    }
    public function add_type_conso()
    {
        /**
         Test OWASP non - intrusion
         **/
         if(!$this->session->userdata('username')) redirect('login/index');
         
        $data = $this->input->post(NULL,TRUE);
        //print_r($data);
        $data = array(
            'idConsoType' =>  NULL,
            'ConsoTypeNomi'  => $data['Conso'],
            'ConsoTypeFixe'  => 0,
            'PdtFixe_idPdtFixe' => NULL
        );
           // print_r($data);
            $type = $this->mobile_model->add_typeConso($data);
        
            if(!$type){
                echo "NO";
            }else{
                echo $type;
            }
    }
}
   

?>