<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChoixPdtObject{
    
    public $idChoixPEO = NULL;
    public $Produit_idProduit = NULL;
    public $Entite_idEntite = NULL;
    public $OptionsEntite_idOptionsEntite = NULL;
    public $Projet_idProjet = NULL;
    public $ChoixPEOQte = NULL;
    
    public $ChoixPEODurEng = NULL;
    public $ChoixPEORemCom = NULL;
    public $ChoixPEORemFMA = NULL;
    public $ChoixPEOTarHt = NULL;
  
   
}
?>