<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ClientsObject{
    public $idClients = NULL;
    public $Utilisateurs_idUtilisateurs = NULL;
    public $ClientsNom = NULL;
    public $ClientsPrenom = NULL;
    public $ClientsNomEntreprise = NULL;
    public $ClientsAdresse = NULL;
    public $ClientsCodePost = NULL;
    public $ClientsVille = NULL;
    public $ClientsSiret = NULL;
    public $ClientsTel = NULL;
    public $ClientsTelMob = NULL;
    public $ClientsMail = NULL;
    public $GdsAccords_idGdsAccords = NULL;
    public $Civilite_idCivilite = 0;
    public $Statut_idStatut = NULL;
    public $ClientsCAP = NULL;
    public $ClientsReceptel = NULL;
    public $Comment = NULL;
    public $ClientsObjectifs1 = NULL;
    public $ClientsObjectifs2 = NULL;
    public $ClientsObjectifs3 = NULL;
    public $ClientsTelFax = NULL;
    public $ClientsAdresseWeb = NULL;
}
?>