<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pdf {


	 public function __construct()

	{

		$CI = & get_instance();

		log_message('Debug', 'mPDF class is loaded.');

	}


	function load($param=NULL)

	{

		//require_once __DIR__ .'/../../assets/vendor/src/Mpdf.php';
        require_once __DIR__ . '/../../vendor/autoload.php';



		if ($param == NULL)

		{

			$param = '"en-GB-x","A4","","",10,10,10,10,6,3';

		}

       
		//return new mPDF($param);
      //  return  new mPDF('utf-8', 'A4-L');
        return new \Mpdf\Mpdf();
	}

}
?>