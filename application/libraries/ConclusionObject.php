<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ConclusionObject{
    public $idConclusion = NULL;
    public $ConclusionPresta = NULL;
    public $ConclusionMaint = NULL;
    public $ConclusionLoyer = NULL;
    public $ConclusionAchat = NULL;
    public $ConclusionRem = NULL;
    public $ConclusionFrais = NULL;
   public $ConclusionTotal = NULL;
   public $ConclusionDuree = NULL;
}
?>