<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DevisObject{
    public $idDevis = NULL;
    public $DevisComment = NULL;
    public $DevisDate = NULL;
    public $DevisExport = NULL;
    public $TestAdsl_idTestAdsl = NULL;
    public $Contexte_idContexte = NULL;
    public $DevisPRI = NULL;
    public $DevisSPOT = NULL;
    public $DevisConcluMaint = NULL;
    public $DevisConcluLoyer = NULL;
    public $DevisConcluAchat = NULL;
    public $DevisTotal = NULL;
    public $Conclusion_idConclusion = NULL;
    
   
}
?>