<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SdslObject{
    public $idSDSL = NULL;
    public $SDSL1M1P2P = NULL;
    public $SDSL2M1P2P = NULL;
    public $SDSL2MEFM1P2P = NULL;
    public $SDSL4M1P2P = NULL;
    public $SDSL4MEFM1P2P = NULL;
    
    public $SDSL8M1P2P = NULL;
    public $SDSL4P1M = NULL;
    public $SDSL4P2M = NULL;
    public $SDSL4P4M = NULL;
    public $SDSL4P8M = NULL;
    public $SDSL4P10M = NULL;
    public $FIBRE = NULL;
    
   
}
?>