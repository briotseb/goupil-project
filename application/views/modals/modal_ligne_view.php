<!--Modal Ajout Ligne-->
<div class="modal fade" id="MyModalAjoutLigne" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Ajouter une Ligne Fixe</h4>
            </div>
            <div class="modal-body">
               <?php
                $csrf = array(
                    'name' => $this->security->get_csrf_token_name(),
                    'hash' => $this->security->get_csrf_hash()
                    );
                ?>
                <div class="form-group">
                    <input name="idSite" id="idSite" type="hidden">
                   
                    <label for="site1Operateur" class="col-sm-4 control-label">Opérateur</label>
                    <div class="col-xs-7">
                        <div class="ope-drop">
                            <?php
                            $attributes = 'class = "form-control input-sm" id = "operateurLigne" onchange = "request(this,$(\'#pdt-dropdown\'),\'/index.php/Site/ajax_call\')"';
                            echo form_dropdown('operateurLigne',$operateur,set_value('operateur'),$attributes);
                            ?>
                        </div>
                    </div><br><br>
                    <label for="Mobile1Operateur" class="col-sm-4 control-label">Produits</label>
                    <div class="col-xs-7">
                        <div id="pdt-dropdown">
                            <select class="form-control input-sm">
                                <option value=''>--------</option>
                            </select>
                        </div>
                    </div>
                    <br><br>
                </div>   
                
                 <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input id='indic' hidden="hidden">
                <label for="Site1L1TZero" class="col-sm-4 control-label">Quantité de produits</label> 
                <div class="col-xs-2">
                    <input type="text" name="Site1L1TZero"  id="SiteL1TZero"  class="form-control input-sm">
                </div>
                <br><br>
                <label for="Site1L1Nb" class="col-sm-4 control-label">Numéro</label>
                <div class="col-xs-8">
                    <input type="tel" name="Site1L1Nb" id="Site1L1Nb" class="form-control input-sm">
                </div>
                <br><br>
                <label for="Site1L1Type" class="col-sm-4 control-label">Type</label>
                <div class="col-xs-8">
                    <input type="checkbox" id="cboxNDI" value="1">&nbsp;&nbsp;NDI
                    <input type="checkbox" id="cboxSpecial" value="2">&nbsp;&nbsp;Spécial
                </div>
                <br><br>
                <label for="Site1L1Tarif" class="col-sm-4 control-label">Tarif HT (à l'unité)</label>
                <div class="col-xs-7 input-group" style="padding-left:15px">
                    <input type="number" name="Site1L1Tarif" id="Site1L1Tarif" class="form-control input-sm">
                    <div class="input-group-addon">€</div>
                </div>
                <div id="getPack"></div>
                <br><br>
                <div id="alert-msg2"></div>
                <div id="infosPack"></div>
            </div>
            <div class="modal-footer">
                  <button type="button" class="btn btn-danger" id="goAddPdt" onclick="modal_LigneOpenAddPkg()">Ajouter un Opérateur / Package</button>
                <button type="button" class="btn btn-info" onclick="modal_LigneAddConso('<?php echo $this->security->get_csrf_hash()?>')" id="goConsoFixe">Ajouter Une Consommation</button>
                <button type="button" id="SaveLigneClose" onclick="modal_ligneAjout('<?php echo $this->security->get_csrf_hash()?>')" class="btn btn-success">Suivant</button>
            </div>
              
        </div>
    </div>
    <div id="getCode"></div>
</div>  
<?php $this->load->view('modals/modal_produit_ctxe_ajout') ?>
<?php $this->load->view('modals/modal_site_conso_pkg_view') ?>
<script type="text/javascript">
    $( "#pdt-dropdown" ).change(function () {
        var elt = document.getElementById('produit');
        if (elt.selectedIndex == -1)return null;
        var texte = elt.options[elt.selectedIndex].text;
        if( texte.indexOf("PACKAGE - ")!== -1){
            alert("Veuillez saisir le numéro et la quantité puis cliquer sur suivant pour saisir les tarifs des options du package");
            $('#infosPack').html('<div class="alert alert-success text-center">Veuillez cliquer sur Suivant pour ajouter des options/consommations au package selectionné.</div>');
            document.getElementById('Site1L1Tarif').disabled=true;
            document.getElementById('goConsoFixe').disabled=true;
            document.getElementById('indic').value = elt.value;
       }else{
            document.getElementById('Site1L1Tarif').disabled=false;
            document.getElementById('goConsoFixe').disabled=false;
            document.getElementById('indic').value = 0;
            $('#getPack').html('');
            $('#infosPack').html('');
        }
    });
</script>