<!-- modal form -->
<div id="myModal_modif_site" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php 
            $attributes = array("name" => "update_site_form", "id" => "update_site_form");
            echo form_open("Modal_Site/update", $attributes);
            ?>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modifier un Site</h4>
            </div>
            <div class="modal-body" id="myModalBodyModif">
            <div class="form-group">
                    <label for="adresseSite">Adresse</label>
                    <input type="text" class="form-control" placeholder="Adresse du site" id="ModifadresseSite">
               </div>
               <div class="form-group">
               <label for="CPSite">Code Postal</label>
                <input class="form-control" id="ModifCPSite" name="CPSite" placeholder="Code Postal du site" type="number" />
                </div>
            <div class="form-group">
                    <label for="VilleSite">Ville</label>
                    <input class="form-control" style="text-transform: uppercase;" id="ModifVilleSite" name="VilleSite" placeholder="Ville du site" type="text"/>
                </div>
                <div id="alert-msg1"></div>
            </div>
             
            <div class="modal-footer">
                <input  type="button" data-dismiss="modal"  class="btn btn-danger" value="Annuler" />
                <input  onclick="modal_modifSite('<?php echo $this->security->get_csrf_hash()?>')" name="submit" type="button"  class="btn btn-primary" value="Modifier" />
            </div>
            <?php echo form_close(); ?>            
        </div>
    </div>
</div>
