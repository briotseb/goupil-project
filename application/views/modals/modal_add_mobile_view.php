
<div id="myModalMobile" class="modal fade" aria-labelledby="myModalLabelMobile" tabindex="-1" role="dialog" aria-   labelledby="myModalLabel" >
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <?php $attributes = array("name" => "mobile_form", "id" => "mobile_form");
            echo form_open("Mobile/add_mobile", $attributes);?>
        
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Ajouter un Mobile</h4>
      </div>
      <div class="modal-body">
           <div class="form-group">
               <label for="Mobile1Nb" class="col-sm-4 control-label">Numéro</label>
               <div class="col-xs-7">
               <input type="tel" name="Mobile1Nb" id="Mobile1Nb" class="form-control input-sm">
               </div><br><br>
          </div>
          <div class="form-group">
               <label for="Mobile1Inf" class="col-sm-4 control-label">Infos rattachées (Nom du porteur/service)</label>
               <div class="col-xs-7">
               <input type="text" name="Mobile1Inf" id="Mobile1Inf" class="form-control input-sm">
               </div><br><br>
          </div>
          <div class="form-group">
              <label for="Mobile1DD" class="col-sm-4 control-label">Date début</label>
              <div class="col-xs-7">
                <input type="date" name="Mobile1DD" id="Mobile1DD" class="form-control input-sm" placeholder="yyyy-mm-dd">
              </div><br><br>
          </div>
          <div class="form-group">     
              <label for="Mobile1DF" class="col-sm-4 control-label">Date fin</label>
              <div class="col-xs-7">
                  <input type="date" name="Mobile1DF" id="Mobile1DF" class="form-control input-sm" placeholder="yyyy-mm-dd">
              </div>
              <br><br>
          </div>
            <div class="form-group">
              <label for="Mobile1Operateur" class="col-sm-4 control-label">Opérateur</label>
                 <div class="col-xs-7">
                     <div id="ope-dropdown">
                      
                         <?php
                         $attributes = 'class = "form-control input-sm" id = "operateurMob" onchange = "request(this,$(\'#pdt-dropdown-mobile\'),\'/index.php/Mobile/ajax_call\')" ';
                         echo form_dropdown('operateur',$operateur,set_value('operateur'),$attributes);
                         ?>
                     </div>
                     
                </div><br><br>
              <label for="Mobile1Operateur" class="col-sm-4 control-label">Produits</label>
                <div class="col-xs-7">
              <div id="pdt-dropdown-mobile">
                  <select class="form-control input-sm">
                      <option value=''>--------</option>
                  </select>
                 
                    </div></div><br><br>
    
          </div>
          <div class="form-group">
              <label for="Mobile1Forfait" class="col-sm-4 control-label">Forfait HT</label>
                <div class="col-xs-7 input-group" style="padding-left:15px">
                    <input type="text" name="Mobile1Forfait" id="Mobile1Forfait" class="form-control input-sm">
                    <div class="input-group-addon">€</div>
              </div><br><br>
          </div>
            
          <div class="form-group">
              <label for="Mobile1Option" class="col-sm-4 control-label">Détails du forfait</label>
              <div class="col-xs-7">
                  <textarea name="Mobile1Option" id="Mobile1Option" class="form-control input-sm" rows="4" ></textarea>  
              </div><br><br><br><br>
             </div>
           <div id="alert-msg1"></div>
        </div>

      <div class="modal-footer">
          <button type="button" id="" class="btn btn-danger" onclick="open_pdt_smp_mob()">Ajouter Un Opérateur / Produit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">Fermer</button>
          <button type="submit" class="btn btn-success">Enregistrer</button>
      </div>
        <?php echo form_close(); ?>    
    </div>
  </div>
</div>
<script type="text/javascript">
    //$('#pdt-dropdown-mobile')
  /*  function request(oSelect,oDrop) {
        var value = oSelect.options[oSelect.selectedIndex].value;
        var xhr   = getXMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                //readData(xhr.responseXML);
                oDrop.html(xhr.responseText);
                document.getElementById("loader").style.display = "none";
            } else if (xhr.readyState < 4) {
                document.getElementById("loader").style.display = "inline";
            }
        };

        xhr.open("POST", "<?php echo site_url('Mobile/ajax_call'); ?>", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("idOpe=" + value);
    }
*/
</script>