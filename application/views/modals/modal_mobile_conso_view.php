<div class="modal fade" id="ajoutConsoMob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Ajouter une Consommation Mobile</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="infoMob" class="col-sm-4 control-label">Informations Ligne</label>
                <div id="infoMob"></div>
                <br><br>
            </div>
            <!--Conso Mobile1-->
            <div class="form-group">
                <label for="Mobile1Conso" class="col-sm-4 control-label">Consommation</label>
                <div class="col-xs-8">
                 <?php
                        $attributes = 'class = "form-control input-sm" name = "conso" id = "conso"';
                         echo form_dropdown('conso',$conso,set_value('conso'),$attributes);
                   
                    ?>
                </div>
            </div>
            <br><br>
            <div class="form-group">
                <label for="Mobile1TarifConso" class="col-sm-4 control-label">Tarif Conso</label>
                <div class="col-xs-7 input-group" style="padding-left:15px">
                
                <input type="number" name="Mobile1TarifConso" id="Mobile1TarifConso" class="form-control input-sm">
                <div class="input-group-addon">€</div></div>
            </div>
                
            <br><br>
          <div id="alert-msgMob"></div>
        </div>
      <div class="modal-footer">
           <button type="button" class="btn btn-danger" onclick="mobile_openModalTypeConso()">Ajouter un type de consommation</button>
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">Annuler</button>
          
            <button type="submit" onclick="mobile_addConso('<?php echo $this->security->get_csrf_hash()?>')" class="btn btn-success" >Enregistrer la Consommation</button>
      </div>
       
    </div>
  </div>
</div>
<?php $this->load->view('modals/modal_ctxe_ajout_conso_mob_view') ?>
