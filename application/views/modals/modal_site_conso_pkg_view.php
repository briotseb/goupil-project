<div class="modal fade" id="ajoutConsoPkg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ajouter les options de package</h4>
            </div>
             <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                    ?>
            <div class="modal-body" id="modalPkg">
                <label for="infoLigne" class="col-sm-4 control-label">Informations Ligne</label>
                <div id="infoFixeF"></div>
                <br><br>
                 <input name="idFixe1" id="idFixe2" type="hidden">
                 <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <label for="Site1L1Conso1" class="col-sm-4 control-label">Option</label> 
                <div class="col-xs-8" id="opt_pkg">
                    
                </div>
                <br><br>
                <label for="Site1L1TarifQte" class="col-sm-4 control-label">Quantité</label>
                <div class="col-xs-5 input-group" style="padding-left:15px">
                    <input type="number" name="Site1L1TarifQte" id="Site1L1TarifQte" placeholder="1" class="form-control input-sm">
                </div>
                 <br><br>
                <label for="Site1L1TarifConso1" class="col-sm-4 control-label">Tarif Option</label>
                <div class="col-xs-7 input-group" style="padding-left:15px">
                    <input type="number" name="Site1L1TarifConso2" id="Site1L1TarifConso2" class="form-control input-sm">
                    <div class="input-group-addon">€</div></div>
                <br><br>
            </div>
             <div id="alert-msg4"></div>
            <br><br>
            <div class="modal-footer">
             
                <button type="button" class="btn btn-default" onclick="location.reload()" style="float:left;">Fermer</button>
                <button type="button" onclick="fixe_saveConsoPack('<?php echo $this->security->get_csrf_hash()?>',1)" class="btn btn-info">Suivant</button>
                <button type="button" onclick="fixe_saveConsoPack('<?php echo $this->security->get_csrf_hash()?>',0)" class="btn btn-success">Enregistrer & Fermer</button>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="ajoutOptPkg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                  <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                    ?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ajouter une option</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <label>Ajouter Option : </label>
                <input type="text" id="OptNomi" class="input-sm">
            </div>
            <div class="modal-footer">
                <button type="button" onclick="save_Opt('<?php echo $this->security->get_csrf_hash()?>')" class="btn btn-success">Ajouter</button>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">
    function open_addOpt(){
        var idPack = document.getElementById('indic').value;
        if(document.getElementById('packageOpt').value == 999)$("#ajoutOptPkg").modal('show');
    }
</script>