<div class="modal fade" id="ajoutTypeConsoMob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        ?>
            
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Ajouter un type de consommation</h4>
        </div>
        <div class="modal-body">
          
            <!--Conso Mobile1-->
            <div class="form-group">
                <label for="Mobile1Conso" class="col-sm-4 control-label">Type de consommation</label>
                <div class="col-xs-8">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                   <input type="text" class="input-sm" id="consoNomi">
                </div>
            </div>
            <br><br>
            
                
            <br><br>
          <div id="alert-msg4"></div>
        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">Annuler</button>
          <button type="submit" onclick="mobile_saveTypeConso('<?php echo $this->security->get_csrf_hash()?>')" class="btn btn-success">Ajouter</button>
      </div>
       
    </div>
  </div>
</div>
