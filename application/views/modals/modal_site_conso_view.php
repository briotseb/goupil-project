<div class="modal fade" id="ajoutConso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ajouter une Consommation Mensuelle</h4>
            </div>
            <div class="modal-body">
                   <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                    ?>
                <label for="infoLigne" class="col-sm-4 control-label">Informations Ligne</label>
                <div id="infoFixe"></div>
                <br><br>
                 <input name="idFixe1" id="idFixe1" type="hidden">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <label for="Site1L1Conso1" class="col-sm-4 control-label">Consommation</label> 
                <div class="col-xs-8">
                     <?php
                        $attributes = 'class = "form-control input-sm" id = "consoF"';
                         echo form_dropdown('consoF',$consoF,set_value('consoF'),$attributes);
                   
                    ?>
                </div>
                <br><br>
                <label for="Site1L1TarifConso1" class="col-sm-4 control-label">Tarif Conso</label>
                <div class="col-xs-7 input-group" style="padding-left:15px">
                    <input type="number" name="Site1L1TarifConso1" id="Site1L1TarifConso1" class="form-control input-sm">
                    <div class="input-group-addon">€</div></div>
                <br><br>
            </div>
             <div id="alert-msg3"></div>
            <br><br>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="location.reload()">Annuler</button>
                <button type="button" onclick="fixe_addConso('<?php echo $this->security->get_csrf_hash()?>',1)" class="btn btn-info">Ajouter Une Consommation</button>
                <button type="button" onclick="fixe_addConso('<?php echo $this->security->get_csrf_hash()?>',0)" class="btn btn-success">Enregistrer & Fermer</button>
            </div>
        </div>
    </div>
</div>