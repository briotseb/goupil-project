<!--MODAL CHX PDT-->
<div class="modal fade " id="pdtAjoutSmp_mob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                <h4 class="modal-title" id="myModalLabel" style="color:red;"> Veuillez respecter S.V.P la nomenclature de la facture de l'opérateur... Merci </h4>
            </div>
                <div class="modal-body">
                    <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                    ?>
                    <div class="row">
                        <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    <div class="col-md-3">
                        <label for="operateur" class="control-label">Selectionner un opérateur</label>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $selected = 0;
                        $attributes = 'class = "form-control" id = "operateurlist_AddPdtMobile" name= "operateur" onchange="autreOpe2()"';
                        echo form_dropdown('operateur',$operateur,$selected,$attributes);?>
                    </div>
                        </div>
                    <br>
                     <div class="row">
                     <div class="col-md-3">
                        <label for="type" class="control-label">OU saisir un Opérateur</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="input-sm" id="operateurNomi_AddPdtMobile" disabled="true" style="text-transform: capitalize;" placeholder="Nomenclature : Xxxxxxxx">
                    </div>
                         </div><br>
                     <div class="row">
                     <div class="col-md-3">
                        <label for="type" class="control-label">Saisir le nouveau produit</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="input-sm" id="pdtNomi_AddPdtMobile">
                    </div>
                         </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="validProj" class="btn btn-success" onclick="modal_mobileSave_pdtCtxe('<?php echo $this->security->get_csrf_hash()?>')">Valider</button>
                </div>
            </div>
        </div>
    </div> 

<script>
    function autreOpe2(){
        if(document.getElementById('operateurlist_AddPdtMobile').value === '8'){
            document.getElementById('operateurNomi_AddPdtMobile').disabled = false;
        }
    }
</script>