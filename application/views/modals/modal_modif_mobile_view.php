<div id="myModalMobileModif" class="modal fade" aria-labelledby="myModalLabelMobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <?php $attributes = array("name" => "mobile_form", "id" => "mobile_form");
            echo form_open("Mobile/update_mobile", $attributes);?>
        
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Ajouter un Mobile</h4>
      </div>
      <div class="modal-body">
           <div class="form-group">
               <label for="Mobile1Nb" class="col-sm-4 control-label">Numéro</label>
               <div class="col-xs-7">
               <input type="tel" name="Mobile1Nb" id="Mobile1NbModif" class="form-control input-sm">
               </div><br><br>
          </div>
           <div class="form-group">
               <label for="Mobile1Inf" class="col-sm-4 control-label">Infos rattachées (Nom du porteur/service)</label>
               <div class="col-xs-7">
               <input type="text" name="Mobile1Inf" id="Mobile1Inf" class="form-control input-sm">
               </div><br><br>
          </div>
          <div class="form-group">
              <label for="Mobile1DD" class="col-sm-4 control-label">Date début</label>
              <div class="col-xs-7">
                <input type="date" name="Mobile1DD" id="Mobile1DDModif" class="form-control input-sm" placeholder="yyyy-mm-dd">
              </div><br><br>
          </div>
          <div class="form-group">     
              <label for="Mobile1DF" class="col-sm-4 control-label">Date fin</label>
              <div class="col-xs-7">
                  <input type="date" name="Mobile1DF" id="Mobile1DFModif" class="form-control input-sm" placeholder="yyyy-mm-dd">
              </div>
              <br><br>
          </div>
            <div class="form-group">
              <label for="Mobile1Operateur" class="col-sm-4 control-label">Opérateur</label>
                 <div class="col-xs-7">
                     <div id="ope-dropdown">
                      
                         <?php
                         $attributes = 'class = "form-control input-sm" id = "operateur"';
                         echo form_dropdown('operateur',$operateur,set_value('operateur'),$attributes);
                         ?>
                     </div>
                </div><br><br>
              <label for="Mobile1Operateur" class="col-sm-4 control-label">Produits</label>
                <div class="col-xs-7">
              <div id="pdt-dropdown">
                  <select class="form-control input-sm" id="produit">
                      <option value=''>--------</option>
                  </select>
                    </div></div><br><br>
    
          </div>
          <div class="form-group">
              <label for="Mobile1Forfait" class="col-sm-4 control-label">Forfait HT</label>
                <div class="col-xs-7 input-group" style="padding-left:15px">
                    <input type="number" name="Mobile1Forfait" id="Mobile1ForfaitModif" class="form-control input-sm">
                    <div class="input-group-addon">€</div>
              </div><br><br>
          </div>
            
          <div class="form-group">
              <label for="Mobile1Option" class="col-sm-4 control-label">Détails du forfait</label>
              <div class="col-xs-7">
                  <textarea name="Mobile1Option" id="Mobile1OptionModif" class="form-control input-sm" rows="4" ></textarea>  
              </div><br><br><br><br>
          </div>
          <div id="alert-msg1"></div>
        </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          
        <button type="submit" id="submit_modif" class="btn btn-success">Modifier</button>
      </div>
        
         <?php echo form_close(); ?> 
    </div>
  </div>
</div>

<script type="text/javascript">
$('#submit_modif').click(function() {
    var form_data = {
        Num: $('#Mobile1Nb').val(),
        Inf: $('#Mobile1Inf').val(),
        DateDeb: $('#Mobile1DD').val(),
        DateFin: $('#Mobile1DF').val(), 
         DateFin: $('#produit').val(), 
         DateFin: $('#Mobile1Forfait').val(), 
         DateFin: $('#Mobile1Option').val()
    };
    $.ajax({
        url: "<?php echo site_url('Modal_Site/update'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES')
                location.reload();
            else if (msg == 'NO')
                $('#alert-msg').html('<div class="alert alert-danger text-center">Error in sending your message! Please try again later.</div>');
            else
                $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
            
          
        }
    });
    return false;
});
</script>