
      <div class="modal-body">
          <?php 
          $attributes = array("class" => "form-horizontal", "id" => "contexteform", "name" => "contexteform");
          echo form_open("Clients/update_client", $attributes);
        ?>
         
        <fieldset>
        <div class="col-md-12">
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="statut" class="control-label">Statut</label>
                    </div>
                    <div class="col-md-6">
                <?php 
                        $attributes = 'class = "form-control" id = "statut" name= "statut"';
                        $selected = $client[0]->Statut_idStatut;
                        echo form_dropdown('statut',$statut,$selected,$attributes);
                        ?>
                        <span class="text-danger"><?php echo form_error('statut'); ?></span>
                    </div>
                </div>
            </div>
            
            <input name="idc" type="hidden" value="<?php echo $client[0]->idClients?>">
            

            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="entreprise" class="control-label">Entreprise</label>
                    </div>
                    <div class="col-md-6">
                        <?php 
                        $value = $client[0]->ClientsNomEntreprise;
                        $input_attr = array("class" => "form-control", "type" => "text", "placeholder" => "Entité de l'entreprise", "id" => "entreprise", "name" => "entreprise", "value" => set_value('entreprise', $value));
                        echo form_input($input_attr);?>
                        <span class="text-danger"><?php echo form_error('entreprise'); ?></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="adresse" class="control-label">Adresse</label>
                    </div>
                    <div class="col-md-6">
                        <?php 
                        $value = $client[0]->ClientsAdresse;
                        $input_attr = array("class" => "form-control", "type" => "text", "placeholder" => "Adresse de l'entreprise", "id" => "adresse", "name" => "adresse", "value" => set_value('adresse', $value));
                        echo form_input($input_attr);
                        ?>
                        <span class="text-danger"><?php echo form_error('adresse'); ?></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label for="codeP" class="control-label">Code Postal</label>
                    </div>
                    <div class="col-sm-3">
                        <?php 
                        $value = $client[0]->ClientsCodePost;
                        $input_attr = array("class" => "form-control", "type" => "number", "placeholder" => "Code Postal", "id" => "codeP", "name" => "codeP", "value" => set_value('codeP', $value));
                        echo form_input($input_attr);
                        ?>
                        <span class="text-danger"><?php echo form_error('codeP'); ?></span>
                    </div>
                <div class="col-md-3">
                    <label for="ville" class="control-label">Ville</label>
                </div>
                    <div class="col-sm-3">
                        <?php 
                        $value = $client[0]->ClientsVille;
                        $input_attr = array("class" => "form-control", "type" => "text", "placeholder" => "Ville", "id" => "ville", "name" => "ville", "value" => set_value('ville', $value), "style" => "text-transform: uppercase;");
                        echo form_input($input_attr);
                        ?>
                    <span class="text-danger"><?php echo form_error('ville'); ?></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="telF" class="control-label">Tel. Fixe</label>
                    </div>
                    <div class="col-sm-3">
                        <?php 
                        $value = $client[0]->ClientsTel;
                        $input_attr = array("class" => "form-control", "type" => "number", "placeholder" => "Tel.fixe", "id" => "telF", "name" => "telF", "value" => set_value('telF', $value));
                        echo form_input($input_attr);
                        ?>
                        <span class="text-danger"><?php echo form_error('telF'); ?></span>
                    </div>
                    <div class="col-md-3">
                        <label for="telM" class="control-label">Tel. Mobile</label>
                    </div>
                    <div class="col-sm-3">
                        <?php 
                        $value = $client[0]->ClientsTelMob;
                        $input_attr = array("class" => "form-control", "type" => "number", "placeholder" => "Tel. Mobile", "id" => "telM", "name" => "telM", "value" => set_value('telM', $value));
                        echo form_input($input_attr);
                        ?>
                        <span class="text-danger"><?php echo form_error('telM'); ?></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
            <div class="row colbox">
            <div class="col-md-3">
                <label for="telF" class="control-label">Tel. Fax</label>
            </div>
            <div class="col-sm-3">
                
                <?php 
               
                    $value = $client[0]->ClientsTelFax;
                   $input_attr = array("class" => "form-control", "type" => "number", "placeholder" => "Tel. Fax", "id" => "telFax", "name" => "telFax", "value" => set_value('telFax', $value));
                   
                     
                
                 echo form_input($input_attr);
                ?>
               
                <span class="text-danger"><?php echo form_error('telF'); ?></span>
            </div>
               
            <div class="col-md-2">
                <label for="telM" class="control-label">Site web</label>
            </div>
            <div class="col-sm-4">
                
                <?php 
               
                    $value = $client[0]->ClientsAdresseWeb;
                   $input_attr = array("class" => "form-control", "type" => "url", "placeholder" => "http://", "id" => "adreWeb", "name" => "adreWeb", "value" => set_value('adreWeb', $value));
                   
                     
                
                 echo form_input($input_attr);
                ?>
               
                <span class="text-danger"><?php echo form_error('telM'); ?></span>
            </div>
            </div>
            </div>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="mail" class="control-label">Adresse Mail</label>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo '<input id="mail" name="mail" placeholder="Courriel Société" type="text" class="form-control" value="' .set_value('mail',$client[0]->ClientsMail);
                        echo '" />';
                        ?>
                        <span class="text-danger"><?php echo form_error('mail'); ?></span>
                    </div>
                </div>
            </div>
                <hr>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="civiG" class="control-label">Gérant</label>
                    </div>
                    <div class="col-md-6">
                        <?php 
                        $attributes = 'class = "form-control" id = "civiG" name= "civiG"';
                        $selected=0;
                        if(isset($client[0]->Civilite_idCivilite))
                        {
                            $selected = $client[0]->Civilite_idCivilite;
                        }
                        echo form_dropdown('civiG',$civilite,$selected,$attributes);
                        ?>
                        <span class="text-danger"><?php echo form_error('civiG'); ?></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="nomG" class="control-label">Nom</label>
                    </div>
                    <div class="col-sm-3">
                        <?php
                        echo '<input id="nomG" name="nomG" placeholder="Nom du Gérant" type="text" class="form-control" value="' .set_value('nomG',$client[0]->ClientsNom);
                        echo '" />';
                        ?>
                        <span class="text-danger"><?php echo form_error('nomG'); ?></span>
                    </div>
                    <div class="col-md-3">
                        <label for="prenomG" class="control-label">Prénom</label>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo '<input id="prenomG" name="prenomG" placeholder="Prénom du Gérant" type="text" class="form-control" value="' .set_value('prenomG',$client[0]->ClientsPrenom);
                            echo '" />';
                        ?>
                        <span class="text-danger"><?php echo form_error('prenomG'); ?></span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="civiT" class="control-label">Contact Technique</label>
                    </div>
                    <div class="col-md-6">
                        <?php 
                            $attributes = 'class = "form-control" id = "civiT" name= "civiT"';
                            $selected=0;
                            
                                if(isset($tech[0]->Civilite_idCivilite))
                                {
                                    $selected = $tech[0]->Civilite_idCivilite;
                                }
                            
                            echo form_dropdown('civiT',$civilite,$selected,$attributes);
                        ?>
                        <span class="text-danger"><?php echo form_error('civiT'); ?></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row colbox">
                    <div class="col-md-3">
                        <label for="nomT" class="control-label">Nom</label>
                    </div>
                    <div class="col-sm-3">
                <?php
              
                    if(isset($tech[0])){
                        echo '<input id="nomT" name="nomT" placeholder="Nom du Contact Tech." type="text" class="form-control" value="' .set_value('nomT',$tech[0]->TechniqueNom);
                        
                    }else{
                         echo '<input id="nomT" name="nomT" placeholder="Nom du Contact Tech." type="text" class="form-control" value="' .set_value('nomT');
                    }
                    echo '" />';
               
               ?>
                
               
                <span class="text-danger"><?php echo form_error('nomT'); ?></span>
            </div>
            
            <div class="col-md-3">
                <label for="prenomT" class="control-label">Prénom</label>
            </div>
            <div class="col-sm-3">
                
                 <?php
              
                     if(isset($tech[0])){
                    echo '<input id="prenomT" name="prenomT" placeholder="Prénom du Contact Tech." type="text" class="form-control" value="' .set_value('prenomT',$tech[0]->TechniquePrenom);
                     }else{
                          echo '<input id="prenomT" name="prenomT" placeholder="Prénom du Contact Tech." type="text" class="form-control"  value="' .set_value('prenomT');
                     }
                    echo '" />';
               
                
               ?> 
              
                <span class="text-danger"><?php echo form_error('prenomT'); ?></span>
            </div>
            </div>
            </div>
             <div class="form-group">
            <div class="row colbox">
            <div class="col-md-3">
                <label for="telFTech" class="control-label">Tel. Fixe</label>
            </div>
            <div class="col-sm-3">
                 <?php
               
                     if(isset($tech[0])){
                    echo '<input id="telFTech" name="telFTech" placeholder="Tel.fixe" type="number" class="form-control"   value="' .set_value('telFTech',$tech[0]->TechniqueTel);
                     }else{
                          echo '<input id="telFTech" name="telFTech" placeholder="Tel.fixe" type="number" class="form-control"  value="' .set_value('telFTech');
                     }
                    echo '" />';
                
               ?> 
               
                <span class="text-danger"><?php echo form_error('telFTech'); ?></span>
            </div>
               
            <div class="col-md-3">
                <label for="telMTech" class="control-label">Tel. Mobile</label>
            </div>
            <div class="col-sm-3">
                 <?php
               
                    if(isset($tech[0])){
                    echo '<input id="telMTech" name="telMTech" placeholder="Tel. Mobile" type="number" class="form-control" value="' .set_value('telMTech',$tech[0]->TechniqueTelMob);
                    }else{
                         echo '<input id="telMTech" name="telMTech" placeholder="Tel. Mobile" type="number" class="form-control" value="' .set_value('telMTech');
                    }
                    echo '" />';
                
               ?> 
               
                <span class="text-danger"><?php echo form_error('telMTech'); ?></span>
            </div>
            </div>
            </div>
             <div class="form-group">
            <div class="row colbox">
            <div class="col-md-3">
                <label for="mailTech" class="control-label">Adresse Mail</label>
            </div>
            <div class="col-md-6">
                <?php
                
                     if(isset($tech[0])){
                    echo '<input id="mailTech" name="mailTech" placeholder="Courriel du Contact Technique" type="text" class="form-control"  value="' .set_value('mailTech',$tech[0]->TechniqueMail);
                     }else{
                          echo '<input id="mailTech" name="mailTech" placeholder="Courriel du Contact Technique" type="text" class="form-control"  value="' .set_value('mailTech');
                     }
                    echo '" />';
               
               ?> 
               
                
                <span class="text-danger"><?php echo form_error('mailTech'); ?></span>
            </div>
            </div>
            </div>
            <hr>
            
            <div class="form-group">
            <div class="row colbox">
            <div class="col-md-3">
                <label for="numSiret" class="control-label">Numéro Siret</label>
            </div>
            <div class="col-sm-3">
                <?php
               
                    echo '<input id="numSiret" name="numSiret" placeholder="Num. Siret" type="number" class="form-control"  value="' .set_value('numSiret',$client[0]->ClientsSiret);
                    echo '" />';
                
              ?>
                <span class="text-danger"><?php echo form_error('numSiret'); ?></span>
            </div>
               
            <div class="col-md-3">
                <label for="eligi" class="control-label">Eligibilité Financière</label>
            </div>
            <div class="col-sm-3">
                <a href="http://india-op.neufcegetel.fr/indiaweb/elig.aspx" target="_blank" class="form-control ">Vérifier</a>
            </div>
            
            </div>
            </div>
            
            <div class="form-group">
            <div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
                 <input id="btn_cancel" name="btn_cancel" type="button" data-dismiss="modal" class="btn btn-danger" value="Annuler" />
                <?php
             
                     echo '<input id="btn_add" name="btn_add" type="submit"  class="btn btn-primary" value="Modifier" />';
                
                ?>
                
            </div>
            </div>
     
        </div>
        
        <div class="col-md-5 Trait">
        
        <div>
        <!--Partenaire Grands Accords-->
        <img src="<?php echo base_url("assets/img/logo-grandsaccords.png");?>" width="100" height="96" alt="Futur Grands Accords"/>
            
              <?php
              $data = array(
                     'name'          => 'GCSocieteCheck',
                     'id'            => 'GCSocieteCheck',
                    'value'         => '1'
                     );
            
                if($client[0]->GdsAccords_idGdsAccords !== NULL){
                    $data = array(
                     'name'          => 'GCSocieteCheck',
                     'id'            => 'GCSocieteCheck',
                     'value'         => '1',
                     'checked'       => TRUE
                    
                 );
                
                } 
                 else {
                     $data = array(
                     'name'          => 'GCSocieteCheck',
                     'id'            => 'GCSocieteCheck',
                     'value'         => '1',
                     'checked'       => FALSE
                    
                 );
                  
                 }
                   echo  form_checkbox($data);
            
               
                ?>
       &nbsp;&nbsp;<label for="GCSociete" >Partenaire Grands Accords</label><br>
        <label for="GroupeSociete" class="col-sm-4 control-label">Grands Accords</label>
         <div class="col-xs-7">
             
               <?php 
                
                $attributes = 'class = "form-control" id = "GCSociete" name = "GCSociete"';
                $selected=0;
                  if(isset($client[0]->GdsAccords_idGdsAccords))
                {
                      $selected = $client[0]->GdsAccords_idGdsAccords;
                }
                echo form_dropdown('GCSociete',$gdsAccords, $selected, $attributes);
                ?>
             
                
                <span class="text-danger"><?php echo form_error('GCSociete'); ?></span>
            </div><br><br><br><br>
        </div>
            <div class="col-md-5">
        <img src="<?php echo base_url("assets/img/logo-receptel.png");?>" width="100" height="96" alt="Receptel"/>
                <?php
              $data = array(
                     'name'          => 'RecSocieteCheck',
                     'id'            => 'RecSocieteCheck',
                    'value'         => '1'
                     );
             
                if($client[0]->ClientsReceptel === '1'){
                    $data = array(
                     'name'          => 'RecSocieteCheck',
                     'id'            => 'RecSocieteCheck',
                     'value'         => '1',
                     'checked'       => TRUE
                    
                 );
                
                } 
                 else {
                     $data = array(
                     'name'          => 'RecSocieteCheck',
                     'id'            => 'RecSocieteCheck',
                     'value'         => '1',
                     'checked'       => FALSE
                     
                 );
                  
                 }
                   echo  form_checkbox($data);
             
               
                ?>
       <!-- <input type="checkbox" name="GCSociete" id="RecSocieteCheck">&nbsp;&nbsp;<label for="GCSociete" >Partenaire Receptel</label><br><br><br>-->
            </div>
    
        <div class="col-md-5">
        <img src="<?php echo base_url("assets/img/logo-CAP.png");?>" width="100" height="96" alt="CAP"/>
            
            <?php
              $data = array(
                     'name'          => 'CapSocieteCheck',
                     'id'            => 'CapSocieteCheck',
                    'value'         => '1'
                     );
          
                if($client[0]->ClientsCAP === '1'){
                    $data = array(
                     'name'          => 'CapSocieteCheck',
                     'id'            => 'CapSocieteCheck',
                     'value'         => '1',
                     'checked'       => TRUE
                    
                 );
                 
                } 
                 else {
                     $data = array(
                     'name'          => 'CapSocieteCheck',
                     'id'            => 'CapSocieteCheck',
                     'value'         => '1',
                     'checked'       => FALSE
                    
                 );
                   
                 }
                 echo  form_checkbox($data);
            
                ?>
            <br>
        </div>
     
            
        </div>
        </fieldset>
        <?php echo form_close(); ?>
        <?php echo $this->session->flashdata('msg'); ?>  	
							
								
       
      
    </div>
  