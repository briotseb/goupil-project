<!--MODAL CHX PDT-->
    <div class="modal fade " id="pdtChoix" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
              
                </div>
                <div class="modal-body">
                    <div id="txtInt"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="validProj" class="btn btn-success" onclick="recupModalProjet()">Valider</button>
                </div>
            </div>
        </div>
    </div> 