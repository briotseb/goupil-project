<!-- modal form -->
<div id="myModal" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $attributes = array("name" => "site_form", "id" => "site_form");
            echo form_open("Modal_Site/submit", $attributes);?>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ajouter un Site</h4>
            </div>
            <div class="modal-body" id="myModalBody">
              
                <div class="form-group">
                    <label for="adresseSite">Adresse</label>
               <?php 
                $value='';
                $input_attr = array("class" => "form-control", "type" => "text", "placeholder" => "Adresse du site", "id" => "adresseSite", "value" => set_value('adresseSite', $value));
                echo form_input($input_attr);
                    
                    ?>
                   <span class="text-danger"><?php echo form_error('adresseSite'); ?></span>
                </div>
                
                <div class="form-group">
                    <label for="CPSite">Code Postal</label>
                    <input class="form-control" id="CPSite" name="CPSite" placeholder="Code Postal du site" type="number" value="<?php echo set_value('CPSite'); ?>" />
                     <span class="text-danger"><?php echo form_error('CPSite'); ?></span>
                </div>

                <div class="form-group">
                    <label for="VilleSite">Ville</label>
                    <input class="form-control" style="text-transform: uppercase;" id="VilleSite" name="VilleSite" placeholder="Ville du site" type="text" value="<?php echo set_value('VilleSite'); ?>" />
                    <span class="text-danger"><?php echo form_error('VilleSite'); ?></span>
                </div>
                <div id="alert-msg"></div>
            </div>
            <div class="modal-footer">
                <input  type="button" data-dismiss="modal"  class="btn btn-danger" value="Annuler" />
               <input   type="button"  class="btn btn-primary" onclick="modal_ajoutSite('<?php echo $this->security->get_csrf_hash()?>')" value="Ajouter Un Site" />
                
            </div>
            
            <?php echo form_close(); ?>   
              
        </div>
    </div>
</div>