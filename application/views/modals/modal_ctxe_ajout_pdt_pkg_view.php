<!--MODAL CHX PDT-->
<div class="modal fade " id="pdtAjoutPkg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                <h4 class="modal-title" id="myModalLabel" style="color:red;"> Veuillez respecter S.V.P la nomenclature de la facture de l'opérateur... Merci </h4>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="operateur" class="control-label" >Sélectionner un opérateur</label>
                        </div>
                        <div class="col-md-3">
                            <?php
                            $selected = 0;
                            $attributes = 'class = "form-control" id = "operateurPkg" name= "operateurPkg" onchange = "request(this,$(\'#pdt-dropdown-pkg\'),\'/index.php/Modal/ajax_call\')"';
                            echo form_dropdown('operateurPkg',$operateurModal,$selected,$attributes);?>
                        </div>

                        <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                    ?>

                         <div class="col-md-3">
                             <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <label for="type" class="control-label">OU Ajouter un Opérateur</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="input-sm" id="opeNomiPkg" disabled="true" style="text-transform: capitalize;" placeholder="Nomenclature : Xxxxxxxx">
                        </div>
                    </div><hr>
                     <div class="row">
                         <div class="col-md-3">
                            <label for="type" class="control-label">Séléctionner un package</label>
                        </div>
                        <div class="col-md-3">
                           <div id="pdt-dropdown-pkg">
                                <select class="form-control input-sm">
                                    <option value=''>--------</option>
                                </select>
                            </div>
                        </div>

                         <div class="col-md-3">
                            <label for="type" class="control-label">OU Ajouter un Package</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="input-sm" id="pdtNomiPkg" disabled="true">
                        </div>
                    </div><hr>
                
                    <div class="row">
                        
                        <div class="col-md-3">
                            <div id="optPre"></div>
                            <button class="add_field_button btn btn-info">Ajouter un champ d'option</button></div>
                         <div class="col-md-6">
                            <div class="input_fields_wrap">
                                
                                <div>
                                    <label for="type" class="control-label">Option Supplémentaire: </label>
                                    <input class="input-sm optpkg" type="text" name="optPkg[]"><br>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="validProj" class="btn btn-success" onclick="save_pdtPkgCtxe('<?php echo $this->security->get_csrf_hash()?>')">Enregistrer</button>
                </div>
            </div>
        </div>
    </div> 
<script>
    
     $(document).ready(function () {
        $( "#operateurPkg" ).change(function () {
                 if(document.getElementById('operateurPkg').value == 99){
                     document.getElementById('opeNomiPkg').disabled = false;
                 }
        });
         $( "#pdt-dropdown-pkg" ).change(function () {
             if(document.getElementById('package').value == 999)document.getElementById('pdtNomiPkg').disabled = false;
             else request(document.getElementById('package'),$('#optPre'),'/index.php/Modal/opt_Pkg');
         });
          
    
            /*  $( "#pdt-dropdown-pkg" ).change(function () {
            //  alert(document.getElementById('package').value);
                  if(document.getElementById('package').value == 999)document.getElementById('pdtNomiPkg').disabled = false;
                 
                  var tmp = document.getElementById('package').value;
                  $.ajax({
                    url: "<?php echo site_url('Modal/opt_Pkg'); ?>",
                    async: false,
                    type: "POST",
                    data: "idOpe="+tmp,
                    dataType: "html",

                    success: function(data) {
                        $('#optPre').html(data);
                    }
                })
              
              });
         */
         var max_fields      = 10; //maximum input boxes allowed
         var wrapper         = $(".input_fields_wrap"); //Fields wrapper
         var add_button      = $(".add_field_button"); //Add button ID
         
         var x = 1; //initlal text box count
         $(add_button).click(function(e){ //on add input button click
             e.preventDefault();
             if(x < max_fields){ //max input box allowed
                 x++; //text box increment
                 $(wrapper).append('<div> <label for="type" class="control-label">Option Supplémentaire : </label><input type="text" class="input-sm optpkg"  name="optPkg[]"/><a href="#" class="remove_field">Supprimer Champ</a></div>'); //add input box
            }
        });
         $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
             e.preventDefault(); $(this).parent('div').remove(); x--;
        })    
     });
</script>