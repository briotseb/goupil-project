<!--MODAL CHX PDT-->
<div class="modal fade " id="pdtAjout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
                <div class="modal-body">
                   <button type="button" id="add_pdt_smp" class="btn btn-primary btn-lg btn-block" onclick="open_pdt_smp()">Ajouter un produit simple</button>
                        <br>
                    <button type="button" id="add_pdt_pkg" class="btn btn-info btn-lg btn-block" onclick="open_pdt_pkg()">Ajouter un package</button>
                </div>
                <div class="modal-footer">
                   
                </div>
            </div>
        </div>
    </div> 
<?php $this->load->view('modals/modal_ctxe_ajout_pdt_simple_view');?>
<?php $this->load->view('modals/modal_ctxe_ajout_pdt_pkg_view');?>
<script>
     $modalChoice = $('#pdtAjout');
    $modalAjout_smp = $('#pdtAjoutSmp');
    $modalAjout_pkg = $('#pdtAjoutPkg');
   function open_pdt_smp()
    {
        $modalChoice.modal('toggle');
        $modalAjout_smp.modal('show');
    }
    
    function open_pdt_pkg()
    {
        $modalChoice.modal('toggle');
        $modalAjout_pkg.modal('show');
    }
</script>