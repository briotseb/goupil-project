<link href="<?php echo base_url("assets/vendor/exportmpdf.css");?>" rel="stylesheet" type="text/css">

<div id="Titre"><h2 class="titre colorB" style="font-size:24pt">&Eacute;tude et analyse technique de votre ligne ADSL / Fibre</h2></div>
<div id="SousTitre"></div> <!-- permet de passer en dessous de la div titre -->
<div id="Test"><p>ADSL / Fibre Optique mutualisée ou dédiée / SDSL1, 2, 3, 4 méga 2 paies ou 4 paires ? <br><!-- texte d'introduction-->
	«Les connexions haut débit» utilisent la technologie ADSL. Ainsi , les possibilités qui vous sont offertes en terme de débits et de services sont
	dépendantes des équipements des opérateurs présents dans le central téléphonique dont vous dépendez ainsi que de la longueur et la qualité de votre 
	ligne téléphonique.</p>
</div>

<div id="Test" class="L100" style="float:left"><!-- données récoltées sur le test -->
<p>Central téléphonique : <?php echo $test[0]->TestAdslCentral ?> <br>
longueur de ligne : <?php echo $test[0]->TestAdslLongueur2 ?> mètres <br>
affaiblissement théorique : <?php echo $test[0]->TestAdslAffaiTheo ?>dB <br>
état de la ligne : <?php echo $test[0]->TestAdslEtatLigne ?> <br>
débit maximum estimé ADSL : <?php echo $test[0]->TestAdslDebitMaxEstim ?> Mbps <br>
débit maximum estimé VDSL : <?php echo $test[0]->TestVdslDebitMaxEstim ?></p>
</div>
<div id="Test" style="float:left"><!-- données récoltées sur le test -->
<p>Résultats pour le numéro de tête de ligne <?php echo $test[0]->TestAdslTete ?> (<?php echo $test[0]->TestAdslDateTest ?>)<br>
Adresse retournée :  <?php echo $test[0]->TestAdslAdresse ?><br>
Nom précécesseur : <?php echo $test[0]->TestAdslPresent ?><br>
Longueur(s) et calibre(s) <?php echo $test[0]->TestAdslLongueur ?>m , <?php echo $test[0]->TestAdslCalibre ?></p>
</div>

<hr>
<table id="Test" class="margeT30"><!-- donées récomtées sur le test -->
<tr>
    <td class="L35">&nbsp;</td>
	<td>SDSL 1M 1p/2p</td>
    <td>SDSL 2M 1p/2p</td>
    <td>SDSL 2M EFM 1p/2p (2p)</td>
    <td>SDSL 4M 1p/2p (2p)</td>
    <td>SDSL 4M EFM 1p/2p (2p)</td>
    <td>SDSL 8M 1p/2P (2p)</td>
  </tr>
<tr>
    <td><strong>SDSL 1P/2P</strong></td>
	<td><img src="<?php 
        switch ($test[0]->SDSL1M1P2P) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL2M1P2P) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL2MEFM1P2P) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL4M1P2P) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL4MEFM1P2P) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL8M1P2P) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
  </tr>
<tr>
    <td>&nbsp;</td>
	<td>SDSL 1M 4p</td>
    <td>SDSL 2M 4p (4p)</td>
    <td>SDSL 4M 4p (4p)</td>
    <td>SDSL 8M 4p (4p)</td>
    <td>SDSL 10M 4p (4p)</td>
    
  </tr>
<tr>
    <td><strong>SDSL 4P</strong></td>
	<td><img src="<?php 
        switch ($test[0]->SDSL4P1M) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL4P2M) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL4P4M) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL4P8M) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td><img src="<?php 
        switch ($test[0]->SDSL4P10M) {
    case 0:
        echo base_url("assets/img/test-8.png");
        break;
    case 1:
        echo base_url("assets/img/test-1.png");
        break;
    case 2:
        echo base_url("assets/img/test-2.png");
        break;
    case 3:
        echo base_url("assets/img/test-3.png");
        break;
    case 4:
        echo base_url("assets/img/test-4.png");
        break;
    case 5:
        echo base_url("assets/img/test-5.png");
        break;
    case 6:
        echo base_url("assets/img/test-6.png");
        break;
    case 7:
        echo base_url("assets/img/test-7.png");
        break;
            
}
        
        ?>" width="80" height="10" alt=""/></td>
    <td>&nbsp;</td>
  </tr>
</table>
<hr>
<table id="TestLegende" class="margeT30"><!-- légende des couleurs -->
<tr>
    <td><img src="<?php echo base_url("assets/img/test-1.png");?>" width="10" height="10"/></td>
	<td>Éligible</td>
    <td><img src="<?php echo base_url("assets/img/test-2.png");?>" width="10" height="10"/></td>
    <td>DSLAM full-Désat entre 2 et 6 mois</td>
    <td><img src="<?php echo base_url("assets/img/test-3.png");?>" width="10" height="10"/></td>
    <td>Étude nécessaire</td>
    <td><img src="<?php echo base_url("assets/img/test-4.png");?>" width="10" height="10"/></td>
	<td>Inéligible</td>
    <td><img src="<?php echo base_url("assets/img/test-5.png");?>" width="10" height="10"/></td>
    <td>NDI Inconnu</td>
    <td><img src="<?php echo base_url("assets/img/test-6.png");?>" width="10" height="10"/></td>
    <td>Non disponible</td>
    <td><img src="<?php echo base_url("assets/img/test-7.png");?>" width="10" height="10"/></td>
    <td>Éligibilité interrompue</td>
  </tr>
</table>
 