<div id="Titre"><h2 class="titre colorB">Synthèse financière</h2></div>
<div id="SousTitre"></div> <!-- permet de passer en dessous de la div titre -->



<div id="contexteConclusion">
<table>
<thead>
  <tr>
    <th colspan="2" class="text-left">SITUATION ACTUELLE</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="L140">Total abonnement général HT / mois</td>
    <td class="L50 text-right"><?php echo number_format($devis[0]->DevisTotal,2) ?> €</td>
  </tr>
      <?php 
      if($devis[0]->DevisConcluMaint !== NULL && $devis[0]->DevisConcluMaint !== '0'){
    
     ?>
  <tr>
    <td>Coût maintenance HT / an</td>
    <td class="text-right"><?php echo number_format($devis[0]->DevisConcluMaint,2)?> €</td>
  </tr>
      <?php 
      }
        if($devis[0]->DevisConcluLoyer !== NULL && $devis[0]->DevisConcluLoyer !== '0'){
      ?>
      <tr>
          <td>Coût Loyer HT / an</td>
          <td class="text-right"><?php echo number_format($devis[0]->DevisConcluLoyer,2)?> €</td>
      </tr>
      <?php
        }
         if($devis[0]->DevisConcluAchat !== NULL && $devis[0]->DevisConcluAchat !== '0'){
      ?>
      <tr> 
          <td>Coût Achat HT / an</td>
          <td class="text-right"><?php echo number_format($devis[0]->DevisConcluAchat,2)?> €</td>
      </tr>
          <?php
         }
      ?>
  </tbody>
  <tfoot>
  <tr>
    <th class="text-left">Total prévision 1<sup>ere</sup> année HT/an</th>
    <th class="text-right">
        <?php
        $a = $devis[0]->DevisTotal;
        $b = $devis[0]->DevisConcluMaint;
        $c = $devis[0]->DevisConcluLoyer;
        $d = $devis[0]->DevisConcluAchat;
        $res = ($a+$b+$c)*12 + $d;
        $res2 = ($a+$b+$c)*12;
        echo number_format($res,2,","," ");
        ?>
        €</th>
  </tr>
  <tr>
    <th class="text-left">Total prévision année suivante HT / an</th>
    <th class="text-right"><?php echo number_format($res2,2,","," "); ?> €</th>
  </tr>
  </tfoot> 
</table>
</div>


<div id="projet1Conclusion" class="margeT30">
<table>
  <thead>
  <tr>
    <th colspan="2" class="text-left">PROJET</th>
  </tr>
  </thead>
  <tbody>
 <tr>
    <td class="L140">Total abonnement général HT / mois</td>
    <td class="L50 text-right"><?php echo number_format($conclu[0]->ConclusionTotal,2) ?> €</td>
  </tr> 
      <?php 
    if($conclu[0]->ConclusionPresta !== NULL && $conclu[0]->ConclusionPresta !== '0'){
      ?>
      <tr>
          <td>Les services supplémentaires</td>
          <td class="text-right"><?php echo number_format($conclu[0]->ConclusionPresta,2) ?>  €</td>
      </tr>
      <?php
    }
      if($conclu[0]->ConclusionMaint !== NULL && $conclu[0]->ConclusionMaint !== '0'){
      ?>
      <tr>
          <td>Coût maintenance HT / mois</td>
          <td class="text-right"><?php echo number_format($conclu[0]->ConclusionMaint,2)?> €</td>
    </tr>
      <?php
      }
        if($conclu[0]->ConclusionLoyer !== NULL && $conclu[0]->ConclusionLoyer !== '0'){
      ?>
      <tr>
          <td>Coût Loyer HT / mois</td>
          <td class="text-right"><?php echo number_format($conclu[0]->ConclusionLoyer,2)?> €</td>
      </tr>
      <?php
      }
        if($conclu[0]->ConclusionAchat !== NULL && $conclu[0]->ConclusionAchat !== '0'){
      ?>
  <tr>
    <td>Terminaux téléphonique / Adresses IPv4</td>
    <td class="text-right"><?php echo number_format($conclu[0]->ConclusionAchat,2) ?> €</td>
  </tr>
      <?php
        }
        
      if($conclu[0]->ConclusionFrais !== NULL && $conclu[0]->ConclusionFrais !== '0'){
      ?>
  <tr>
    <td>Frais d'activation de service</td>
    <td class="text-right"><?php echo number_format($conclu[0]->ConclusionFrais,2) ?> €</td>
  </tr>
      <?php
        }
      ?>
  
  </tbody>
  <tfoot>
       <?php
      //Calcul totalProjet
       if($conclu[0]->ConclusionTotal !== NULL && $conclu[0]->ConclusionTotal !== '0'){
           $somConclu = (($conclu[0]->ConclusionTotal + $conclu[0]->ConclusionMaint + $conclu[0]->ConclusionLoyer)*12);
           $somConclu += $conclu[0]->ConclusionPresta + $conclu[0]->ConclusionAchat + $conclu[0]->ConclusionFrais;
      ?>
      
  <tr>
    <th class="text-left">Total général</th>
    <th class="text-right"><?php echo number_format($somConclu,2) ?> €</th>
  </tr>
      <?php
       }
      $somConclu = ($conclu[0]->ConclusionTotal + $conclu[0]->ConclusionMaint + $conclu[0]->ConclusionLoyer)*(($conclu[0]->ConclusionDuree)-12);
      if($somConclu !==0){
          ?>
<tr>
    <th class="text-left">Total Projet Année Suivante</th>
    <th class="text-right"><?php echo number_format($somConclu,2) ?> €</th>
  </tr>
      
      
      <?php
      }
      if($conclu[0]->ConclusionRem !== NULL && $conclu[0]->ConclusionRem !== '0'){
      ?>
  <tr>
    <td>Remise commerciale</td>
    <td class="text-right"><?php  echo number_format($conclu[0]->ConclusionRem,2) ?> €</td>
  </tr>
      <?php
      }
      $somConclu = (($a+$b+$c) - ($conclu[0]->ConclusionTotal + $conclu[0]->ConclusionMaint + $conclu[0]->ConclusionLoyer))*$conclu[0]->ConclusionDuree;
      if($somConclu !== 0){
      ?>
      <tr>
          <th class="text-left">Différence sur la durée du contrat (<?php echo $conclu[0]->ConclusionDuree?> mois) HT</th>
          <th class="text-right"><?php echo number_format($somConclu,2) ?> €</th>
      </tr>
      <?php
          
      }
      $somConclu = $somConclu * 1.20;
      if($somConclu !== 0){
          
          ?>
      <tr>
          <th class="text-left">Différence sur la durée du contrat (<?php echo $conclu[0]->ConclusionDuree?> mois) TTC</th>
          <th class="text-right"><?php echo number_format($somConclu,2) ?> €</th>
      </tr>
      <?php
      }
      ?>
  
  </tfoot>  
</table>    
</div>
