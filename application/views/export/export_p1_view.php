<link href="<?php echo base_url("assets/vendor/exportmpdf.css");?>" rel="stylesheet" type="text/css">
<div class="couv1  text-center">
<img src="<?php echo base_url("assets/img/LogoUsers/".$infos[0]->UtilisateursLogo.".png")?>" alt="1133.png" class="logoCouv"/>
</div>


<div class="CouvTitre">
	<h1>Audit télécom</h1>
    <h2 class="couvh2">Optimisons vos solutions télécom !</h2>
</div>    
 

<div class="CouvInfo text-left">
	<p><img src="<?php echo base_url("assets/vendor/images/ZeroPapier.png");?>" alt="1133.png" width="64" height="110" /> <strong>Fait le <?php
        $date = date_create($devis[0]->DevisDate);
        echo date_format($date, 'd-m-Y');
        ?> </strong></p>
</div>
<div class="CouvClient text-right">
	<h2> <?php echo $infos[0]->ClientsNomEntreprise ?></h2>
    <p> <?php echo $infos[0]->ClientsAdresse ?> <br>
    <?php echo $infos[0]->ClientsCodePost ?> <?php echo $infos[0]->ClientsVille ?></p>
</div>
