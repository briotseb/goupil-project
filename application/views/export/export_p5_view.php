<link href="<?php echo base_url("assets/vendor/exportmpdf.css");?>" rel="stylesheet" type="text/css">

<div id="contexte">
<table> <!-- Table à répéter en haut de chaque page -->
  <tr class="H30">
    <td class="L70">SITUATION ACTUELLE</td>
    <td class="L40">N° de ligne</td>
    <td>Coût</td>
  </tr>
</table>

<table> <!-- Table de contexte 20 lignes Max-->
  <thead><!-- numero et nom du site -->
  <tr>
    <th colspan="3" class="text-left">1- 71000 MÂCON</th> 
  </tr>
  </thead>
  <tbody> <!-- liste des produits contexte du site -->
  <tr>
    <td class="L70">OOOOOOOOOO OOOOOOOOO O...</td><!-- 22 caractères Max + les 3 points -->
    <td class="L40">&nbsp;</td>
    <td class="text-right">461.65 €</td>
  </tr>
  <tr>
    <td>2 TO Local National illimité</td>
    <td><span class="NDI">03 85 22 92 79</span></td>
    <td class="text-right">163.90 €</td>
  </tr>
  <tr>
    <td>Consommations vers mobile mé...</td>
    <td>&nbsp;</td>
    <td class="text-right">14.87 €</td>
  </tr>
  <tr>
    <td>Consommation vers N° specia...</td>
    <td>&nbsp;</td>
    <td class="text-right">3.73 €</td>
  </tr>
  <tr>
    <td>20 SDA</td>
    <td>&nbsp;</td>
    <td class="text-right">18.20 €</td>
  </tr>
  <tr>
    <td>Ligne Analogique Fax</td>
    <td>03 85 22 92 80</td>
    <td class="text-right">13.00 €</td>
  </tr>
  <tr>
    <td>ADSL</td>
    <td>&nbsp;</td>
    <td class="text-right">23,23 €</td>
  </tr>
  </tbody>
  <tfoot><!-- total du site -->
  <tr>
    <th colspan="2" class="text-left">Total mensuel HT</th>
    <th class="text-right">688.58 €</th>
  </tr>
  </tfoot> 
</table>

<table> <!-- Table du site 2-->
  <thead><!-- numero et nom du site -->
  <tr>
    <th colspan="3" class="text-left">2- 71210 COMMÂ</th> 
  </tr>
  </thead>
  <tbody> <!-- liste des produits contexte du site -->
  <tr>
    <td class="L70">OOOOOOOOOO OOOOOOOOO O...</td><!-- 22 caractères Max + les 3 points -->
    <td class="L40">&nbsp;</td>
    <td class="text-right">461.65 €</td>
  </tr>
  <tr>
    <td>2 TO Local National illimité</td>
    <td><span class="NDI">03 85 22 92 79</span></td>
    <td class="text-right">163.90 €</td>
  </tr>
  <tr>
    <td>Consommations vers mobile mé...</td>
    <td>&nbsp;</td>
    <td class="text-right">14.87 €</td>
  </tr>
  <tr>
    <td>Consommation vers N° specia...</td>
    <td>&nbsp;</td>
    <td class="text-right">3.73 €</td>
  </tr>
  <tr>
    <td>20 SDA</td>
    <td>&nbsp;</td>
    <td class="text-right">18.20 €</td>
  </tr>
  <tr>
    <td>Ligne Analogique Fax</td>
    <td>03 85 22 92 80</td>
    <td class="text-right">13.00 €</td>
  </tr>
  <tr>
    <td>ADSL</td>
    <td>&nbsp;</td>
    <td class="text-right">23,23 €</td>
  </tr>
  </tbody>
  <tfoot><!-- total du site -->
  <tr>
    <th colspan="2" class="text-left">Total mensuel HT</th>
    <th class="text-right">688.58 €</th>
  </tr>
  </tfoot> 
</table>
</div>

<div id="projet1">
<table> <!-- Table à répéter en haut de chaque page -->
  <tr class="H30">
    <td>PROJET 1</td>
    <td class="text-right"><img src="images/clic.gif"  class="clic" height="35px" alt="Cliquez sur l'image pour en savoir plus"/></td>
  </tr>
</table>

<table><!-- Table de Projet 20 lignes Max -->
  <thead>
  <tr>
    <th colspan="3">&nbsp;</th>
  </tr>
  </thead>
  <tbody><!-- liste des produit du site -->
  <tr >
    <td class="L75">OOOOOOOOOO OOOOOOOOO OOO...</td><!-- 24 caractères Max + les 3 points -->
    <td class="text-right">461.65 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">2 TO Local National illimité</td>
    <td class="text-right">163.90 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">Consommations vers mobile mé...</td>
    <td class="text-right">14.87 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">Consommation vers N° specia...</td>
    <td class="text-right">3.73 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">20 SDA</td>
    <td class="text-right">18.20 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">Ligne Analogique Fax</td>
    <td class="text-right">13.00 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">ADSL</td>
    <td class="text-right">23,23 €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">8</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">9</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">10</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">11</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
   <tr>
    <td class="L75">12</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
   <tr>
    <td class="L75">13</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
   <tr>
    <td class="L75">14</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
   <tr>
    <td class="L75">15</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
   <tr>
    <td class="L75">16</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">17</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">18</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">19</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  <tr>
    <td class="L75">20</td>
    <td class="text-right"> €</td>
    <td width="05mm"><a href="#"><img src="images/plus.png" height="20" alt="En savoir plus"/></a></td>
  </tr>
  </tbody>
  <tfoot>
  <tr>
    <th>Total mensuel HT</th>
    <th class="text-right">688.58 €</th>
    <th width="05mm">&nbsp;</th>
  </tr>
  </tfoot>  
</table>    
</div>