<!--Milieu-->
<div class="container BgBlanc">
<div class="row">
<article class="col-md-12">
<h1>INFORMATIONS</h1>
</article>
<article class="col-md-4">
<table class="table table-striped Trait">
<tr>
<td ><h5><?php echo $UtilisateursNom; ?></h5></td>
<td><img src="<?php echo base_url("assets/img/Modifier.png");?>" width="24" height="20" alt="Modifier"/></td>
</tr>
<tr>
<td><h5> Tel : <?php echo $UtilisateursTel;?></h5></td>
<td><img src="<?php echo base_url("assets/img/Modifier.png");?>" width="24" height="20" alt="Modifier"/></td>
</tr>
<tr>
<td><h5> E-mail : <?php if(empty($UtilisateursMailContact)){
                            echo $UtilisateursMail;
                        }else{
                            echo $UtilisateursMailContact;
                        } 
                    ?>
    </h5></td>
<td><img src="<?php echo base_url("assets/img/Modifier.png");?>" width="24" height="20" alt="Modifier"/></td>
</tr>
    <tr>
<td><h5>Société : <?php echo $UtilisateursSociete;?></h5></td>
<td><img src="<?php echo base_url("assets/img/Modifier.png");?>" width="24" height="20" alt="Modifier"/></td>
</tr>
</table>
    
</article>
    <article class="col-md-4">
    <img src="<?php echo base_url("assets/img/logo-receptel.png");?>" width="100" height="96" alt="Receptel"/>
    <input type="checkbox" name="GCSociete" id="GCSocieteCheck">&nbsp;&nbsp;<label for="GCSociete" >Partenaire Receptel</label>
    </article>
    
    <article class="col-md-4">
        <p> Logo Utilisateur <small>(fichier JPG)</small></p>
        <p><img src="<?php echo base_url("assets/img/LogoUsers/".strtolower($UtilisateursLogo).".png");?>" width="120" height="30" alt=""/></p>
        <article class="col-md-8">
        <label for="ImageFond" class="col-sm-4 control-label">Changer l'image</label>
            
        </article>
         <!--Ajouter Groupement -->
        <article class="col-md-4">
        <button type="button" id="ImageFond" disabled="disabled" class="btn btn-danger">Changer</button>
        </article>
    </article>
    
    <article class="col-md-6">
        <input type="checkbox" name="GCSociete" id="GCSocieteCheck">&nbsp;&nbsp;<label for="GCSociete" >Votre slogan</label>
         <input type="text" disabled="disabled" class="form-control input-sm" value="<?php echo $UtilisateursSlogan ?>">
    </article>
     <article class="col-md-6">
        <input type="checkbox" name="GCSociete" id="GCSocieteCheck">&nbsp;&nbsp;<label for="GCSociete" >Horaire d'ouverture</label>
         <input type="text" disabled="disabled" class="form-control input-sm" value="<?php echo $UtilisateursHoraires ?>">
    </article>
</div>


<div class="col-md-12 PadTop"><hr></div>

    
<div class="row" style="text-align:center">
   <div class="col-md-6">
    <div class="col-md-6" >
        <img src="<?php echo base_url("assets/img/picto_contrat.png");?>"  alt=""/>
        <br>
        <a href="<?php echo base_url("assets/".strtolower($UtilisateursLogo)."/contrat.pdf");?>" target="_blank">
            <h5>Consulter mon contrat</h5>
        </a>
            <h5>Consulter mes factures</h5>
        <a href="#" data-toggle="modal" data-target="#myModal1">
            <h5>Demander la résiliation de mon contrat</h5>
        </a>
    </div>
   
    </div>
  <div class="col-md-6">
      
      <div class="col-md-6" >
       <img src="<?php echo base_url("assets/img/picto_banque.png");?>"  alt=""/>
          <br>
        <h5>Modifier mes coordonnées bancaires</h5>
    </div>
</div>
<div class="col-md-12 PadTop"><hr></div>
</div>
</div>
<div id="myModal" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Factures</h4>
            </div>
            <div class="modal-body" id="myModalBody">
                <div class="col-md-12" style="text-align:center">
                    <button class="btn btn-info" type="button">Ouvrir</button>
                </div>
                 <div class="col-md-12">
                    <table id="site-table" data-toggle="table" data-url="<?php echo site_url("Site/list_site") ?>"  data-show-refresh="true" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="true">
                        <thead>
                            <tr>
                                <th data-field="state" data-checkbox="true"></th>
                                <th data-field="nomFichier" data-sortable="true">Fichier</th>
                            </tr>
                        </thead>
                    </table>
                </div>
              
            </div>
            <div class="modal-footer">
                <input  type="button" data-dismiss="modal"  class="btn btn-danger" value="Fermer" />
            </div>
        </div>
    </div>
</div>
<div id="myModal1" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Demande de résiliation</h4>
            </div>
            <div class="modal-body" id="myModalBody">
        <?php
                $attributes = array("class" => "form-horizontal", "id" => "resiform", "name" => "resiform");
                echo form_open("Admin/resi_sent", $attributes);
        ?>
                    <div class="col-md-12" style="text-align:center">
                         <input  type="button" data-dismiss="modal"  class="btn btn-danger" value="Annuler" />
                    </div>

                     <div class="col-md-12">
                         <label>Sujet</label>
                         <input type="text" class="form-control" value="Demande de résiliation"><br><br>
                         <label>Objet</label> 
                         <input hidden="hidden" name="ctemail" value="<?php echo $UtilisateursMail; ?>">
                         <textarea rows="6" cols="50" class="form-control" name="textEmail">
                             Bonjour, je souhaiterais résilier mon contrat RedGoupil.
                             <?php 
                            echo 'Informations Contrat';
                           echo "\r\n";
                           echo 'Nom : '.$UtilisateursNom.'';
                           echo "\r\n";
                           echo 'Mail : ';  
                           if(empty($UtilisateursMailContact)){
                               echo $UtilisateursMail;
                                }else{
                               echo $UtilisateursMailContact;
                           }
                           echo "\r\n";
                           echo 'Societe : '.$UtilisateursSociete;
                             ?>
                         </textarea>
                    </div>
                <button class="btn btn-info" name="button_pressed" type="submit" value="1">Envoyer</button>  
                <?php echo form_close(); ?>
                <?php echo $this->session->flashdata('msg'); ?>    
        
               
            </div>
            
        </div>
    </div>
</div>