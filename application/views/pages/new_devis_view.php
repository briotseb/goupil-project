<!--Milieu-->
<div class="container BgBlanc">
    <div class="row">
    <article class="col-md-12"><h1>PAGE DE GARDE</h1></article>
        <article class="col-md-7 Trait ">
            <fieldset>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-md-3">
                            <label for="nameS" class="control-label">Nom Société</label>
                        </div>
                        <div class="col-md-6">
                            <?php 
                            $attributes = 'class = "form-control" id = "nameS"';
                            $selected=0;
                            echo form_dropdown('nameS',$societe,$selected,$attributes);
                            ?>
                            <span class="text-danger"><?php echo form_error('nameS'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-md-3">
                            <label for="dateDevis" class="control-label">Date du devis :</label>
                        </div>
                        <div class="col-md-6">
                            <input type="date" name="dateDevis" class="form-control input-sm" id="dateDevis" value="<?php echo date('Y-m-d')?>">
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
                    <input id="btn_cancel" type="reset" class="btn btn-danger" onclick="location.reload()" value="Annuler" />
                    <input id="btn_next" type="submit" class="btn btn-primary" onclick="newDevis_start()" value="Suivant" />
                </div>
            </div>
        </article>

    <!--Valider societe -->
        <article class="col-md-4 d-inline">
            <h6>Ces informations sont purement indicatives</h6><br>
            <div class="col-md-5">
                <img src="<?php echo base_url("assets/img/PRI.gif");?>">
                <label for="priRec" >PRI :</label>
            </div>
            <div class="col-md-2">
                <b><div id="priRec"></div></b>
                <br><br>
            </div>
        </article>
        <article class="col-md-4 d-inline">
            <div class="col-md-9">
                <img src="<?php echo base_url("assets/img/SPOT.gif");?>">
                <label for="spotRec" >SPOT en conquête : €</label>
            </div>
            <div class="col-md-2">
                <b> 
                    <div id="spotRec" style="color:red"></div> 
                </b>
            </div>
        </article>
    </div>
    <div class="col-md-12 PadTop"><hr></div>
    <div id="devisOperateur"></div>
    <div id="devisSite"></div>
    <div id="concluSite"></div>
</div>

