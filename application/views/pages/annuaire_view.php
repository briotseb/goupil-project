<!--Milieu-->
<div class="container BgBlanc">
    <article class="col-md-3"><h1>ANNUAIRE</h1></article>
    <article class="col-sm-9 control-label">
        <button type="button" id="Ann_OuvrirSoc" class="btn btn-default borderRouge" onclick="annuaire_open()">Afficher les informations de la Société</button>
        <a href="<?php echo site_url('Clients/index') ?>"><button type="button" id="Ann_ModifSoc" class="btn btn-default borderRouge" >Modifier les informations de la Société</button></a>
         
   
    </article>
     <div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table id="tableClients" data-toggle="table" data-url="<?php echo site_url("Annuaire/clients_list") ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="nom" data-sort-order="asc">
						    <thead>
						    <tr>
                                <th data-field="state" data-checkbox="true"></th>
						        <th data-field="Entr" data-sortable="true">Entreprise</th>
                                <th data-field="Nom" data-sortable="true">Nom Gerant</th>
						        <th data-field="ville"  data-sortable="true">Ville</th>
                                <th data-field="tel"  data-sortable="true">Tel.</th>
                                <th data-field="mail"  data-sortable="true">Mail</th>
                                <th data-field="stat"  data-sortable="true">Statut</th>
                            
						       
						    </tr>
						    </thead>
						</table>
					</div>
				</div>  
			</div>
    <div id="Ann_infoClient"></div>
</div>
<!--Fin Milieu-->
<script>
   
    </script>
  