 <?php 
        $disabled=false;
        if(isset($client)){
            $disabled=true;
          // print_r($client);
        }
?>
<div class="col-md-12 PadTop" ><hr></div>
<div class="col" style="text-align:center"><h1>MOBILE</h1></div>

<article class="col-sm-6 control-label">
    <article class="col-md-3"><h1>LIGNE</h1></article>
    <article class="col-md-7">
        <button type="button" class="btn btn-default borderRouge" data-toggle="modal" data-target="#myModalMobile"
                <?php
                if(!$disabled)echo 'disabled="disabled"';
                ?>
                >Ajouter</button>&nbsp;&nbsp;&nbsp;
        <!--  <button type="button" onclick="contexte_ModifMobLine()" disabled="disabled" class="btn btn-default borderRouge" data-toggle="modal" data-      target="#modalModifMob">Modifier</button>&nbsp;&nbsp;&nbsp;-->
        <button type="button" onclick="contexte_SupprMobile()" class="btn btn-default borderRouge"
                 <?php
                if(!$disabled)echo 'disabled="disabled"';
                ?>
                >Supprimer</button>
          
    </article>
</article> 
<article class="col-sm-6 control-label">
    <article class="col-md-6"><h1>OPTIONS / CONSOMMATIONS</h1></article>
    <article class="col-md-6">
        <button type="button"  class="btn btn-default borderRouge" onclick="contexte_AddConsoMob()">Ajouter</button>&nbsp;&nbsp;&nbsp;
        <button type="button"  class="btn btn-default borderRouge" onclick="contexte_SupprConsoMob()">Supprimer</button>
      
    </article>
</article>
    <!--TABLEAU MOBILE-->
<div class="col-md-12">
   <table id="mobile-table" data-toggle="table" data-url="<?php echo site_url("Mobile/list_mobile") ?>"  data-show-refresh="true" data-show-toggle="false"   data-show-columns="false" data-search="true" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="nom" data-sort-order="asc">
                <thead>
                    <tr>
                        <th data-field="state" data-checkbox="true"></th>
                        <th data-field="mobNum" data-sortable="true">Numéro</th>
                        <th data-field="info" data-sortable="true">Infos ligne</th>
                        
                        <th data-field="DateD" data-sortable="true">Date Debut</th>
                        <th data-field="DateF" data-sortable="true">Date Fin</th>
                        <th data-field="TarForf" data-sortable="true">Tarif Forfait</th>
                        <th data-field="OpeNomi"  data-sortable="true">Operateur</th>
                        <th data-field="ConsoTypeNomi"  data-sortable="true">Consommation</th>
                        <th data-field="ConsoTypeTar"  data-sortable="true">Tarif Consommation</th>
                    </tr>
                </thead>
            </table>
        </div>



<?php $this->view('modals/modal_add_mobile_view');?>
<?php $this->view('modals/modal_modif_mobile_view');?>
<?php $this->view('modals/modal_mobile_conso_view', $conso);?>
<?php $this->load->view('modals/modal_ctxe_ajout_pdt_mob_view');?>
