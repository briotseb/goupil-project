<?php 
$disabled=false;
if(isset($client)){
    $disabled=true;
    // print_r($client);
}
?>
<div class="col-md-12 PadTop" ><hr></div>
<div class="col" style="text-align:center"><h1>FIXE & DATA</h1></div>
<div class="row  BgBlanc">
    <article class="col-sm-6 control-label">
        <article class="col-md-2"><h1>SITE</h1></article>
        <article class="col-md-7">
            <button type="button" class="btn btn-default borderRouge" data-toggle="modal" data-target="#myModal"
                    <?php
                if(!$disabled)echo 'disabled="disabled"';
                    ?>
                    >Ajouter</button>&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-default borderRouge" onclick="contexte_ModifSite()"
                     <?php
                if(!$disabled)echo 'disabled="disabled"';
                ?>
                    >Modifier</button>&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-default borderRouge" onclick="contexte_SupprSite()"
                     <?php
                if(!$disabled)echo 'disabled="disabled"';
                ?>
                    >Supprimer</button>&nbsp;&nbsp;&nbsp;
        </article>
    </article>
    <article class="col-sm-6 control-label">
        <article class="col-md-2"><h1>LIGNE</h1></article>
        <article class="col-md-10">
            <button type="button" class="btn btn-default borderRouge" onclick="contexte_AddLine()"
                     <?php
                if(!$disabled)echo 'disabled="disabled"';
                ?>
                    >Ajouter</button>&nbsp;&nbsp;&nbsp;
            <!--<button type="button" id="modifLigne" class="btn btn-default borderRouge" onclick="AffichModifLigne();" data-toggle="modal" data-target="#modalModifLigne" disabled="disabled">Modifier</button>&nbsp;&nbsp;&nbsp;-->
            <button type="button" onclick="contexte_SupprLine()" class="btn btn-default borderRouge">Supprimer</button>&nbsp;&nbsp;&nbsp;
            <button type="button" onclick="contexte_DetailsSite()" class="btn btn-default borderRouge" 
                     <?php
                if(!$disabled)echo 'disabled="disabled"';
                ?>
                    >Afficher Details</button>
        </article>
    </article>
    
    <!--TABLEAU LISTING SITE - LIGNE FIXE-->    
    <div class="container" >
        <div class="row">
            <div class="col-md-12">
                <table id="site-table" data-toggle="table" data-url="<?php echo site_url("Site/list_site") ?>"  data-show-refresh="true" data-show-toggle="false" data-show-columns="false" data-search="true" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="nom" data-sort-order="asc">
                    <thead>
                        <tr>
                            <th data-field="state" data-checkbox="true"></th>
                            <th data-field="adresse" data-sortable="true">Site</th>
                            <th data-field="code" data-sortable="true">Code Postal</th>
                            <th data-field="ville" data-sortable="true">Ville</th>
                            <th data-field="opeNomi"  data-sortable="true">Operateur</th>
                            <th data-field="fixeNum"  data-sortable="true" >Numero Fixe</th>
                            <th data-field="prodNomi"  data-sortable="true">Produit Ligne Fixe</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->view('modals/modal_site_conso_view',$consoF);?>
<?php $this->view('modals/modal_site_view');?>
<?php $this->view('modals/modal_modif_site_view');?>
<?php $this->view('modals/modal_ligne_view');?>
<?php $this->view('modals/modal_site_details');?>


