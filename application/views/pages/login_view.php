<html>
<head>
<meta charset="utf-8">
<title>Red Goupil SaaS</title>
 <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.2.1.min.js");?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js");?>"></script>
    
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css");?>" />
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap-table.js");?>"></script>
    <link rel="stylesheet" href="<?php echo base_url("assets/css/style2.css");?>" />
 </head>
<body>
<div class="container-fluid header">
<div class="container BgBlanc">
</div></div>
<!--Fin Header-->

<!--Milieu-->
<div class="container BgBlanc">
<div class="row">
<article class="col-md-12"><p>&nbsp;</p></article>
<article class="col-md-3 col-md-offset-1">
<p><img src="<?php echo base_url("assets/img/Logo-RedGoupil.png");?>" width="336" height="159" alt="Redgoupil"/></p>
<p>Bienvenue sur votre Espace de gestion client et devis.</p>
<p>Pensez à vous déconnecter avant de quitter, afin de garder les informations contenues confidentielles.</p>

</article>

<article class="col-md-5 col-md-offset-1">   
    <div style="padding: 20px;" id="form-olvidado">
     <?php 
    $attributes = array("class" => "form-horizontal", "id" => "loginform", "name" => "loginform");
    echo form_open("Login/index", $attributes);?>
    <h1>SE CONNECTER</h1>
    <div class="form-group input-group">
        <span class="input-group-addon">
            @
        </span>
          <input class="form-control" placeholder="Votre Email" name="user_name" type="email" required autofocus="" value="<?php echo set_value('user_name'); ?>">
         <span class="text-danger"><?php echo form_error('user_name'); ?></span>
        </div>
        <div class="form-group input-group">
          <span class="input-group-addon">
            <i class="glyphicon glyphicon-lock">
            </i>
          </span>
          <input class="form-control" placeholder="Votre mot de passe" name="user_password" type="password" value="<?php echo set_value('user_password'); ?>" required>
           <span class="text-danger"><?php echo form_error('user_password'); ?></span>
        </div>
        <div class="form-group">
          <input type="submit" name="login" value="Se Connecter" class="btn btn-primary btn-block" />
             <p class="help-block">
            <a class="pull-right text-muted" href="mailto:redservices@redgoupil.fr?subject=Probleme de connexion" id="olvidado"><small>Mot de passe oublié?</small></a>
          </p>
        </div>
        <?php echo form_close(); ?>
        <?php echo $this->session->flashdata('msg'); ?>
        <strong>
            <span class="text-danger" style="color:black;"></span>
        </strong>
    </div>
<div style="display: none;" id="form-olvidado">
    <h4 class="">
      Mot de passe perdu?
    </h4>
    <form accept-charset="UTF-8" role="form" id="login-recordar" method="post">
      <fieldset>
        <span class="help-block">
          Email address you use to log in to your account
          <br>
          We'll send you an email with instructions to choose a new password.
        </span>
        <div class="form-group input-group">
          <span class="input-group-addon">
            @
          </span>
          <input class="form-control" placeholder="Email" name="email" type="email" required="">
        </div>
        <button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">
          Continue
        </button>
        <p class="help-block">
          <a class="text-muted" href="#" id="acceso"><small>Se Connecter</small></a>
        </p>
      </fieldset>
    </form>
  </div>
</article>
</div>
    
</div>
    <!--Footer-->
<div class="container-fluid footer">
    <p class="faq" style="float:left">
        <a href="<?php echo site_url('faq/index') ?>"><img style="max-height:60px;"src="<?php echo base_url("assets/img/GoupilFAQ.png");?>"></a>
    </p>
    <p class="mentions"> SaaS v3.1.8 RedGoupil <a href="http://www.mikaeldaval.com/" target="_blank">MDCOM</a> 2016</p>
   
</div>   
</body>
</html>
