<div class="container BgBlanc" >
    <article class="col-md-3"><h1>DEVIS CLIENTS</h1></article>
     <article class="col-sm-9 control-label">
         <!-- <button type="button" id="Devis" disabled="disabled" class="btn btn-default borderRouge">Ouvrir le devis</button>-->
         <button type="button" id="dev_SupprDevis"  onclick="devis_supprDevis()" class="btn btn-default borderRouge">Supprimer le devis</button>
         
   
    </article>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <table id="tableDevis" data-toggle="table" data-url="<?php echo site_url("Devis/list_devis") ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="nom" data-sort-order="asc">
                    <thead>
                        <tr>
                            <th data-field="state" data-checkbox="true"></th>
                            <th data-field="numDev" data-sortable="true">Num. Devis</th>
                            <th data-field="NomEntr" data-sortable="true">Entreprise</th>
                            <th data-field="ville" data-sortable="true">Ville</th>
                            <th data-field="dateDev" data-sortable="true">Date Devis</th>
                            <th data-field="comDev" data-sortable="true">Commentaires</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
