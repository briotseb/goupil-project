<!--Milieu-->
<div class="container BgBlanc">
    <article class="col-md-3"><h1>FICHE CLIENTS</h1></article>
    <article class="col-sm-9 control-label">
        <button type="button" onclick="contexte_open()" class="btn btn-default borderRouge">Modifier le Contexte</button>
        <button type="button" onclick="contexte_modifSoc()" class="btn btn-default borderRouge">Modifier la Société</button>
        <button type="button" onclick="contexte_supprSoc()" class="btn btn-default borderRouge">Supprimer la Société</button>       
    </article>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <table id="tableClients" data-toggle="table" data-url="<?php echo site_url("Clients/clients_list") ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="nom" data-sort-order="asc">
                    <thead>
                        <tr>
                            <th data-field="state" data-checkbox="true"></th>
                            <th data-field="Entr" data-sortable="true">Entreprise</th>
                            <th data-field="Nom" data-sortable="true">Nom Gerant</th>
                            <th data-field="Adr" data-sortable="true">Adresse</th>
                            <th data-field="code" data-sortable="true">Code Postal</th>
                            <th data-field="ville"  data-sortable="true">Ville</th>
                            <th data-field="tel"  data-sortable="true">Tel.</th>
                            <th data-field="mob"  data-sortable="true">Mobile</th>
                            <th data-field="mail"  data-sortable="true">Mail</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!--MODAL Modif Site-->
<div class="modal fade bs-example-modal-lg" id="contexte_modalModif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modifier la société</h4>
      </div>
        <div id="details"></div>
      </div>
    </div>
</div>