<html>
<head>
<meta charset="utf-8">
<title>Red Goupil SaaS</title>
 <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.2.1.min.js");?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js");?>"></script>
    
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css");?>" />
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap-table.js");?>"></script>
    <link rel="stylesheet" href="<?php echo base_url("assets/css/style1.css");?>" />
 </head>
<body>
<div class="container-fluid header">
<div class="container BgBlanc">
</div></div>
<!--Fin Header-->

<!--Milieu-->
<div class="container BgBlanc">
<div class="row">
<article class="col-md-12"><p>&nbsp;</p></article>
<article class="col-md-3 col-md-offset-1">
<p><img src="<?php echo base_url("assets/img/Logo-RedGoupil.png");?>" width="336" height="159" alt="Redgoupil"/></p>
<p>Bienvenue sur votre Espace de gestion client et devis.</p>
<p>Pensez à vous déconnecter avant de quitter, afin de garder les informations contenues confidentielles.</p>

</article>

<article class="col-md-5 col-md-offset-1">     
     <?php 
        $attributes = array("class" => "form-horizontal", "id" => "loginform", "name" => "loginform");
        echo form_open("Login/index", $attributes);?>
        <h1>SE CONNECTER</h1>
        <div class="form-group">
            <label for="name">Email</label>
            <input type="text" name="user_name" placeholder="Votre Email" required class="form-control" value="<?php echo set_value('user_name'); ?>" />
            <span class="text-danger"><?php echo form_error('user_name'); ?></span>
        </div>
        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="user_password" placeholder="Votre mot de passe" required class="form-control" value="<?php echo set_value('user_password'); ?>" />
            <span class="text-danger"><?php echo form_error('user_password'); ?></span>
        </div>
        <div class="form-group">
          <input type="submit" name="login" value="Se Connecter" class="AjoutRouge" />
        </div>
        <?php echo form_close(); ?>
        <?php echo $this->session->flashdata('msg'); ?>
        <strong>
            <span class="text-danger" style="color:black;"></span>
        </strong> 
</article>
</div>
    <p><br><br><br><br></p>
</div>
