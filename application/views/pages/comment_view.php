<?php 
$disabled=false;
if(isset($client)){
    $disabled=true;
    //  print_r($client);
}     
?>
<div class="col-md-12 PadTop"><hr></div>
<?php
$attributes = array("class" => "form-horizontal", "id" => "commentform", "name" => "commentform");
echo form_open("Contexte/add_supinfo", $attributes);
?>
<div class="col-md-7">
    <fieldset>
    <legend>Informations Complémentaires</legend>
       
        <label for="objContexte" class="control-label">AXES D'OPTIMISATION DE VOS OBJECTIFS</label>
        <div class="form-group">
          
             <div class="col-sm-4">
                <div class="circle circle2">
                  <h2>1<small>er</small>
                      <?php 
                
                $attributes = 'class = "form-control" id = "obj1" name= "obj1"';
                $selected=0;
                  if($disabled)
                {
                     
                      $selected = $client[0]->ClientsObjectifs1;
                }else{
                      $attributes .= 'disabled="disabled"';
                  }
                echo form_dropdown('objectif1',$obj,$selected,$attributes);
                      ?>
                    
                    </h2>
                <h2><br></h2>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="circle circle3">
                    <h2>2<small>ième</small>
                    <?php 
                
                $attributes = 'class = "form-control" id = "obj2" name= "obj2"';
                $selected=0;
                  if($disabled)
                {
                     
                      $selected = $client[0]->ClientsObjectifs2;
                }else{
                      $attributes .= 'disabled="disabled"';
                  }
                echo form_dropdown('objectif2',$obj,$selected,$attributes);
                      ?>
                    </h2>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="circle circle1">
                    <h2>3<small>ième</small>
                    <?php 
                
                $attributes = 'class = "form-control" id = "obj3" name= "obj3"';
                $selected=0;
                  if($disabled)
                {
                     
                      $selected = $client[0]->ClientsObjectifs3;
                }else{
                      $attributes .= 'disabled="disabled"';
                  }
                echo form_dropdown('objectif3',$obj,$selected,$attributes);
                      ?>
                    </h2>
                </div>
            </div>
            
            
               
        </div>
       <div class="form-group">
            <label for="comContexte" class="control-label">Commentaire sur votre contexte et vos demandes lors de notre contact</label>
            <?php
            if($disabled){
                echo '<textarea class="form-control" rows="6"  id="comment" name="comment">' .set_value('comment',$client[0]->Comment);
                echo '</textarea>';
            }else{
                echo '<textarea class="form-control" rows="6" disabled="disabled" id="comment" name="comment">' .set_value('comment');
                echo '</textarea>';
            }
           
            ?>
            <span class="text-danger"><?php echo form_error('comment'); ?></span>
        </div>
    
    </fieldset>
        <?php echo $this->session->flashdata('msgComment'); ?>     
</div>
<div class="col-sm-offset-4 col-lg-8 col-sm-8">
 
    <?php
                if($disabled){
                    echo '<input id="btn_add" name="btn_add" type="submit"  class="btn btn-primary" value="Valider" />';
                }else{
                     echo '<input id="btn_add" name="btn_add" type="submit" disabled="disabled" class="btn btn-primary" value="Valider" />';
                }
                ?>
                
            </div>
<?php echo form_close(); ?>
