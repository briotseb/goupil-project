<html>
    <head>  
        <meta charset="utf-8">
        <title>Red Goupil SaaS</title>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.2.1.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js");?>"></script>
        <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css");?>" />
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap-table.js");?>"></script>
        <link rel="stylesheet" href="<?php echo base_url("assets/css/style2.css");?>" />
        <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/rikmms/progress-bar-4-axios/0a3acf92/dist/nprogress.css" />
         <style type="text/css">
            #nprogress .bar {
                background: red !important;
                height: 4px;
            }
 
            #nprogress .peg {
                box-shadow: 0 0 10px red, 0 0 5px red !important;
            }
 
            #nprogress .spinner-icon {
                border-top-color: red !important;
                border-left-color: red !important;
            }
        </style> 
        <link rel="shortcut icon" href="<?php echo base_url("assets/img/favicon.ico");?>" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url("assets/img/favicon.ico");?>" type="image/x-icon">
  <script type="text/javascript">
    $(document).ready(function (){
        $("#loading-div-background").css({ opacity: 1.0 });
    });

    function ShowProgressAnimation(){
        $("#loading-div-background").show();
    }
</script>
        <script>
function my_func(){
    var myWindow = window.open("", "Red Goupil", "toolbar=no,scrollbars=no,titlebar=yes,resizable=yes,top=200,left=500,width=400,height=400");
    myWindow.document.write("<div id='loading-div' class='ui-corner-all'><img style='margin-left:70px;' src='https://redgoupil.com/assets/img/goupil-chrono.gif');'/><br>EN COURS DE CHARGEMENT.<br> MERCI DE PATIENTER...</div>");
    console.log("Generation Devis");
    setTimeout(function() {
        myWindow.close();
        window.scrollTo(0,0);
    }, 8000);
}
</script>
       
    </head>
<body>
   
    <?php
            if(ENVIRONMENT == 'development')
            {
                echo " <h3 style='text-align:center'>VERSION EN DEVELOPPEMENT</h3>";
                print_r(ENVIRONMENT);
                print_r('\n');
                print_r($_SERVER);

            }
    ?>
    <div class="container-fluid header">
        <div class="row BgBlanc">
            <article class="col-md-2">
                <a href="<?php echo site_url('admin/index') ?>">
                    <img src="<?php echo base_url("assets/img/Logo-RedGoupil-80.png");?>" width="169" height="80" alt="RedGoupil"/>
                </a>
            </article>
            <article class="col-md-6">
                <nav class="header-menu">
                    <ul>
                        <li>
                            <a href="<?php echo site_url('contexte/index') ?>">
                                <img src="
                                          <?php 
                                if($page===1)echo base_url("assets/img/Nouveau.png");
                               else echo base_url("assets/img/Nouveau-2.png")    
                                          ?>
                                          " width="45" height="47" alt="nouveau"/>
                                <br>nouveau<br>contexte</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('clients/index')?>" class="header-menu">
                                <img src="<?php 
                            if($page !==2)echo base_url("assets/img/Fiche-2.png");
                               else echo base_url("assets/img/Fiche.png");
                                          ?>
                                          " width="45" height="47" alt="contexte"/>
                                <br>contexte<br>client</a>
                        </li> 
                        <li>
                            <a href="<?php echo site_url('New_Devis/index') ?>">
                                <img src="<?php 
    if($page!==3)echo base_url("assets/img/Nouveau-2.png");
                                          else echo base_url("assets/img/Nouveau.png");
                                          ?>" width="45" height="47" alt="nouveau"/>
                                <br>nouveau<br>devis</a>
                        </li> 
                        <li><a href="<?php echo site_url('Devis/index') ?>">
                            <img src="<?php 
                                      if($page !== 4 )echo base_url("assets/img/Fiche-2.png");
                                      else echo base_url("assets/img/Fiche.png");
                                      ?>" width="45" height="47" alt="devis"/>
                            <br>devis<br>client</a>
                        </li>
                    
                        <li><a href="https://sync.suite-stockage-cloud.sfrbusinessteam.fr/" target="_blank">
                            <img src="<?php 
                                      if($page !== 6)echo base_url("assets/img/Cloud-2.png");
                                      else echo base_url("assets/img/Cloud.png");?>" width="45" height="47" alt="sync"/>
                            <br>sync<br>&nbsp;</a>
                        </li> 
                        <li><a href="https://app.tilkee.com/#/login" target="_blank">
                            <img src="<?php echo base_url("assets/img/envoi.png");?>" width="45" height="47" alt="Tilkee"/>
                            <br>Via Tilkee<br>&nbsp;</a>
                        </li> 
                    </ul>
                </nav> 
            </article>
            <article class="col-md-4">
                <nav class="header-menu">
                    <ul>
                        <li><a href="<?php echo site_url('Annuaire/index') ?>"><img src="<?php 
                            if($page !== 8)echo base_url("assets/img/Box-2.png");
                            else echo base_url("assets/img/Box.png");?>" width="45" height="47" alt="annuaire"/>
                            <br>annuaire<br>&nbsp;</a>
                        </li>
                        <li><a href="<?php echo site_url('Liens/index') ?>">
                            <img src="<?php if($page!==9)echo base_url("assets/img/Liens-2.png");
                                 else echo base_url("assets/img/Liens.png");?>" width="45" height="47" alt=""/>
                            <br>Liens<br> ext/int</a>
                        </li> 
                        <li style="width:70px;height:70px;"><a href="<?php echo site_url('admin/index') ?>">
                            <img src="
                                      <?php 
    if($page!==10)echo base_url("assets/img/Log.png");
                               else echo base_url("assets/img/Log.png"); ?>" width="45" height="47" alt=""/><br><?php echo $this->session->userdata('username'); 
                            ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('login/logout') ?>">
                                <img src="<?php echo base_url("assets/img/onoff.png");?>" width="45" height="47" alt=""/><br>Quitter<br>
                            </a>
                        </li>
                    </ul>
                </nav> 
            </article>
        </div>
</div>
