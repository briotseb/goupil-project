<?php
$attributes = array("class" => "form-horizontal", "id" => "testform", "name" => "testform");
        echo form_open("Projet/save_test", $attributes);
?>
<!--Resultat ADSL-->
<div class="row">
    <article class="col-md-12"><h1>RÉSULTATS DU TEST DE VOTRE LIGNE ADSL</h1>
<!--
<div class="col-md-6">
          <div class="bg-success text-white">Eligible</div>
          <div class="bg-warning text-white">DSLAM full-Désat entre 2 et 6 mois</div>
          <div class="bg-primary text-white">Etude nécessaire</div>
          <div class="bg-danger text-white">Inéligible</div>
          <div class="bg-fu text-white">NDI inconnu</div>
          <div class="bg-faded">Non disponible</div>
          <div class="bg-jo text-white">Eligibilité Interrompue</div>
        
</div><br>
-->
       
        <h2>Réaliser les tests avant de remplir cette partie.<br>
            <a href="http://india-op.neufcegetel.fr/IndiaWeb/Login.aspx?ReturnUrl=/indiaweb-efault.aspx" target="_blank"><font color="#0C72BC">INDIA</font></a><br>
            <a href="http://www.degrouptest.com" target="_blank"><font color="#0C72BC">Degroup Test</font></a><br><br></h2></article>
    
</div>
<!--Resultat ADSL tete de ligne-->
<div class="row">
    <label for="ADSLNb" class="col-md-2 control-label">tête de ligne</label>
    <div class="col-xs-2">
        <input name="ADSLTete" class="form-control input-sm" type="tel">
    </div>
    <label for="ADSLDate" class="col-md-2 control-label">Date de test</label>
    <div class="col-xs-2">
        <input name="ADSLDate" class="form-control input-sm" type="date">
    </div>
    <br><br>
    <label for="ADSLAdresse" class="col-md-2 control-label">Adresse</label>
    <div class="col-xs-4">
        <input name="ADSLAdresse" class="form-control input-sm" type="text">
    </div>
    <label for="ADSLNom" class="col-md-2 control-label">Nom </label>
    <div class="col-xs-2">
        <input name="ADSLNom" class="form-control input-sm" type="text">
    </div>
    <br><br>
    <label for="ADSLPresent1" class="col-md-2 control-label">Titulaire précédent</label>
    <div class="col-xs-8">
        <input name="ADSPresent1" class="form-control input-sm" type="text"></div>
   <!-- <label for="ADSLPresent2" class="col-md-1 control-label">Present </label>
    <div class="col-xs-2">
        <input name="ADSLPresent2" class="form-control input-sm" type="text"></div>
    <label for="ADSLPresent3" class="col-md-1 control-label">Present </label>
    <div class="col-xs-2">
        <input name="ADSLPresent3" class="form-control input-sm" type="text"></div>
    -->
    <br><br>
    <label for="ADSLLongueur" class="col-md-2 control-label">Longueur (mêtres)</label>
    <div class="col-xs-2">
        <input name="ADSLLongueur" class="form-control input-sm" type="text"></div>
    <label for="ADSLCalibre" class="col-md-1 control-label">Calibre </label>
    <div class="col-xs-2">
        <input name="ADSLCalibre" class="form-control input-sm" type="text"></div>
    <br><br>
    <label for="ADSLCentreTel" class="col-md-2 control-label">Central Téléphonique</label>
    <div class="col-xs-2">
        <input name="ADSLCentreTel" class="form-control input-sm" type="text">
    </div>
    <label for="ADSLLongueur2" class="col-md-1 control-label">Longueur </label>
    <div class="col-xs-2">
        <input name="ADSLLongueur2" class="form-control input-sm" type="text"></div>
    <label for="ADSLAffaib" class="col-md-3 control-label"><span class="SFR">Affaiblissement théorique</span></label>
    <div class="col-xs-2">
        <input name="ADSLAffaib" class="form-control input-sm" type="text"></div>
    <br><br>
    <label for="ADSLEtatLigne" class="col-md-2 control-label">Etat de la ligne</label>
    <div class="col-xs-2">
        <input name="ADSLEtatLigne" class="form-control input-sm" type="text"></div>
    <label for="ADSLDebitMaxAdsl" class="col-md-2 control-label">Débit max estim ADSL</label>
    <div class="col-xs-2">
        <input name="ADSDebitMaxAdsl" class="form-control input-sm" type="text"></div>
    <label for="ADSLDebitMaxVdsl" class="col-md-2 control-label">Débit max estim VDSL</label>
    <div class="col-xs-2">
        <input name="ADSDebitMaxVdsl" class="form-control input-sm" type="text"></div>
</div>
<div class="row">
    <br><br>
<div class="col-md-3"></div>
<div class="col-md-10">
    <span class="label label-success">Eligible</span>
    <span class="label label-warning">DSLAM full-desat</span>
    <span class="label label-primary">Etude nécessaire</span>
    <span class="label label-danger">Inéligible</span>
    <span class="label label-ndi">NDI Inconnu</span>
    <span class="label label-default">Non disponible</span>
    <span class="label label-jaune">Eligibilité interrompue</span>
    
    
</div>
    </div>
<!-- Resultat ADSL SDSL 1P/2P-->
<div class="row">
    <article class="col-md-12"><h2>SDSL 1P/2P</h2> </article>
    <div class="col-md-12">
        <label for="ADSL1m1p2p" class="col-md-2 control-label">SDSL 1M 1p/2p</label>
        <div class="col-md-2">
        <?php
            $attributes = 'class = "form-control input-sm" id = "statut" name= "ADSL1m1p2p"';
            echo form_dropdown('ADSL1m1p2p',$techno,$attributes);
            ?>
        
        </div>
    
        <label for="ADSL2m1p2p" class="col-md-2 control-label">SDSL 2M 1p/2p</label>
        <div class="col-md-2">
        <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL2m1p2p" name= "ADSL2m1p2p"';
            echo form_dropdown('ADSL2m1p2p',$techno,$attributes);
            ?>
        
        </div>
    
        <label for="ADSL2mEfm1p2p" class="col-md-2 control-label">SDSL 2M EFM 1p/2p (2p)</label>
        <div class="col-md-2"> 
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL2mEfm1p2p" name= "ADSL2mEfm1p2p"';
            echo form_dropdown('ADSL2mEfm1p2p',$techno,$attributes);
            ?>
        
        </div>
    </div>
    <article class="col-md-12">
        <label for="ADSL4m1p2p" class="col-md-2 control-label">SDSL 4M 1p/2p (2p)</label>
        <div class="col-md-2">
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL4m1p2p" name= "ADSL4m1p2p"';
            echo form_dropdown('ADSL4m1p2p',$techno,$attributes);
            ?>
        </div>   
    
        <label for="ADSL4mEfm1p2p" class="col-md-2 control-label">SDSL 4M EFM 1p/2p (2p)</label>
        <div class="col-md-2">
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL4mEmf1p2p" name= "ADSL4mEfm1p2p"';
            echo form_dropdown('ADSL4mEfm1p2p',$techno,$attributes);
            ?>
        </div> 
     
        <label for="ADSL8m1p2p" class="col-md-2 control-label">SDSL 8M 1p/2P (2p)</label>
        <div class="col-md-2">
        
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL8m1p2p" name= "ADSL8m1p2p"';
            echo form_dropdown('ADSL8m1p2p',$techno,$attributes);
            ?>
        </div>
    </article>
</div>
<!-- Resultat ADSL SDSL 4P-->
<div class="row">
    <article class="col-md-12"><h2>SDSL 4P</h2></article>
    <article class="col-md-12">
        <label for="ADSL1m4p" class="col-md-2 control-label">SDSL 1M 4p</label>
        <div class="col-md-2">
        
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL1m4p" name= "ADSL1m4p"';
            echo form_dropdown('ADSL1m4p',$techno,$attributes);
            ?>
        </div>
    
        <label for="ADSL2m4p" class="col-md-2 control-label">SDSL 2M 4p (4p)</label>
        <div class="col-md-2">
        
        
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL2m4p" name= "ADSL2m4p"';
            echo form_dropdown('ADSL2m4p',$techno,$attributes);
            ?>
        </div>
    
        <label for="ADSL4m4p" class="col-md-2 control-label">SDSL 4M 4p (4p)</label>
        <div class="col-md-2">
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL4m4p" name= "ADSL4m4p"';
            echo form_dropdown('ADSL4m4p',$techno,$attributes);
            ?>
        
        </div>
    </article>
    <article class="col-md-12">   
        <label for="ADSL8m4p" class="col-md-2 control-label">SDSL 8M 4p (4p)</label>
        <div class="col-md-2">
        
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL8m4p" name= "ADSL8m4p"';
            echo form_dropdown('ADSL8m4p',$techno,$attributes);
            ?>
        
        </div>   
    
        <label for="ADSL10m4p" class="col-md-2 control-label">SDSL 10M 4p (4p)</label>
        <div class="col-md-2">
        
         <?php
            $attributes = 'class = "form-control input-sm" id = "ADSL10m4p" name= "ADSL10m4p"';
            echo form_dropdown('ADSL10m4p',$techno,$attributes);
            ?>
        </div> 
    </article>
</div>
<!-- Resultat ADSL SDSL 4P-->
<div class="row">
    <article class="col-md-12"><h2>ELIGIBILITE</h2></article>
    <article class="col-md-12">
        <label for="fibre" class="col-md-2 control-label">Fibre</label>
        <div class="col-xs-1">
        
         <?php
            $attributes = 'class = "form-control input-sm" id = "fibre" name= "fibre"';
            echo form_dropdown('fibre',$techno,$attributes);
            ?>
        </div>
    </article>
</div>
<?php echo $this->session->flashdata('msg'); ?>
<div class="col-md-12 PadTop"><hr></div>
<div class="col-md-12 PadTop"><hr></div>
<div class="row"><article class="col-md-4"></article>
    <article class="col-md-2">   
        <input id="btn_add"  name="btn_add" type="submit"  class="btn btn-primary btn-lg"  onclick="my_func();" value="Valider pour terminer votre devis" />
       <i class="fa fa-spinner" aria-hidden="true"></i>
    </article>
</div>

<div class="col-md-12 PadTop"><hr></div>
 <?php echo form_close(); ?> 

