<?php
//RETRAITEMENT DE LA DONNEE SITE   
   //array to store department id & department name
        $stat_id = array('-Choisir-');
        $stat_name = array('-Choisir-');

        for ($i = 0; $i < count($site); $i++)
        {
            array_push($stat_id, $site[$i]->idSite);
            array_push($stat_name, $site[$i]->SiteAdresse.' - '. $site[$i]->SiteCodePost.' - '.$site[$i]->SiteVille);
        }
    $statut_result = array_combine($stat_id, $stat_name);
?>
<div class="col-md-12 PadTop"><hr></div>
<div class="row">
    <article class="col-md-5">
        <table class="table table-contexte table-condensed" style="background-color: #c1c1c1"><tr>
        <td class="col-sm-12 text-center" style="color:black;">Ajouter des produits Fixes supplémentaires aux sites</td>
        </tr></table>
    </article>

     <article class="col-md-6">
        <div class="row">
            <label>Site</label>
            <?php
                        $attributes = 'class = "form-control" id = "site"';
                        echo form_dropdown('site',$statut_result, set_value('site'), $attributes);
            ?>
            
            <label>Produit</label>
             <?php
                        $attributes = 'class = "form-control" id = "pdtFixePlus"';
                        echo form_dropdown('PdtFixe',$PdtFixe, set_value('PdtFixe'), $attributes);
            ?>
            <br>
            <button class="btn btn-info" style="float:right;" id="valideProjet" onclick="add_pdtPlus()">Ajouter</button>
                 
        </div>
    </article>
    
</div>
 <div id="produitSup"></div>

<div class="col-md-12 PadTop"><hr></div>
<div class="form-group">
    <div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
        <input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Etape Suivante (Mobile)" onclick="newDevis_startMobile()" />
    </div>
</div>
<div class="col-md-12 PadTop"><hr></div>
<div id="devisOperateurMobile"></div>