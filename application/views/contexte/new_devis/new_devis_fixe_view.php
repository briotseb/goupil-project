<?php
if(!empty($fixe)){
  //  print_r($f);
?>
<div class="row">
    <article class="col-md-5"><table class="table table-contexte table-condensed"><tr>
       <!-- <td class="col-sm-3">Ligne analogique</td>-->
         <td class="col-sm-1"><?php echo $f->FixeNbeT0 ?></td>
        <td class="col-sm-3"><?php echo $f->PdtFixeNomi ?></td>
        <td class="col-sm-2"><?php echo $f->OperateurNomi ?></td>
        <td class="col-sm-4">
          <?php  
        $affich = '<span>';
    if($f->FixeType_idFixeType === '1')$affich='<span class="NDI">';
    if($f->FixeType_idFixeType === '2')$affich='<span class="SPE">';
    echo $affich;
    //GERER LES NUMERO A ESPACES
    $tmp = str_replace(' ','',$f->FixeNumero);
    if((int)$tmp > 100){
        echo $f->FixeNumero; 
        
    }
    echo '</span>';
            ?></td>
        <td class="col-sm-6 text-right"><?php echo number_format($f->FixeNbeT0*$f->FixeTarifsHT,2,","," ")?> €</td>
        </tr></table>
    </article>

    <article class="col-md-6">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-6 col-sm-6 d-inline">
                        <div class="col-sm-6">
                        <?php
                        $attributes = 'class = "form-control" id = "'.$f->Site_idSite.''.(int)$tmp.'"';
                        echo form_dropdown('PdtFixe',$PdtFixe, set_value('PdtFixe'), $attributes);
                        ?>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-info" id="valideProjet" onclick="projet(1,<?php echo $f->idFixe; ?>,0,10,<?php echo (int)$tmp ?>,'<?php echo $f->Site_idSite; ?>',afficheModalPdt)">Valider</button><br><br>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 d-inline">
                        <div class="col-sm-6">
                            <label class="d-inline">Tarifs HT (€): </label>
                        </div>
                        <div class="col-sm-5">
                            <div class="d-inline" id="tarProj<?php echo $f->Site_idSite.''.$tmp?>">
                            <?php if(isset(${'tarProj'.$f->Site_idSite.''.$tmp}))echo ${'tarProj'.$f->Site_idSite.''.$tmp};?>
                            </div> 
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 d-inline">
                        <div class="col-sm-6">
                            <label class="control-label">ENG (mois): </label>
                        </div>
                        <div class="col-sm-5">
                            <div class="d-inline" id="engProj<?php echo $f->Site_idSite.''.$tmp?>">
                                <?php if(isset(${'engProj'.$f->Site_idSite.''.$tmp}))echo ${'engProj'.$f->Site_idSite.''.$tmp};?>
                            </div> 
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 d-inline">
                        <font color="red">
                            <div class="col-sm-3">
                                <input hidden="hidden" id="spotProj<?php echo $f->Site_idSite.''.$tmp?>" value="">
                                <label class="control-label d-inline">PRI : </label>
                            </div>
                             <div class="col-sm-3">
                                 <div class="d-inline" id="priProj<?php echo $f->Site_idSite.''.$tmp?>">
                                     <?php if(isset(${'priProj'.$f->Site_idSite.''.$tmp}))echo ${'priProj'.$f->Site_idSite.''.$tmp};?>
                                </div>
                            </div>
                        </font>
                        
                        <div class="col-sm-3">
                                
                                <label class="control-label d-inline">FMA : </label>
                            </div>
                             <div class="col-sm-3">
                                 <div class="d-inline" id="fmaProj<?php echo $f->Site_idSite.''.$tmp?>">
                                     <?php if(isset(${'fmaProj'.$f->Site_idSite.''.$tmp}))echo ${'fmaProj'.$f->Site_idSite.''.$tmp};?>
                                </div>
                        </div>
                    </div>
            
                   <div class="d-inline" id="fmaProj<?php echo $f->Site_idSite.''.$tmp?>">
                                     
                                </div>
                </div>
                
                <label class="col-sm-3 control-label  d-inline">PDT : </label>
                <div class="col-sm-8 control-label  d-inline" id="pdtProj<?php echo $f->Site_idSite.''.$tmp?>">
                  <?php if(isset(${'pdtProj'.$f->Site_idSite.''.$tmp}))echo ${'pdtProj'.$f->Site_idSite.''.$tmp};?>
                </div> 
                
                
            </div>
        </div>
    </article>
</div>
    
    
    
    
    <?php
    
}else{
   
        ?>
    
    
    
    <div class="row">
         <article class="col-md-5"></article>
        
        <article class="col-md-6">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-6 col-sm-6 d-inline">
                        <div class="col-sm-6">
                        <?php
                        $attributes = 'class = "form-control" id = "pdtFixe'.$s->idSite.'"';
                        echo form_dropdown('PdtFixe',$PdtFixe, set_value('PdtFixe'), $attributes);
                        ?>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-info" id="valideProjet" onclick="projet(1,0,0,10,0,'<?php echo $s->idSite; ?>',afficheModalPdt)">Valider</button><br><br>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 d-inline">
                        <div class="col-sm-6">
                            <label class="control-label">Tarifs HT (€): </label>
                        </div>
                        <div class="col-sm-2">
                            <div class="" id="tarProj<?php echo $s->idSite?>0"></div> 
                        </div>
                    </div> 
                    <div class="col-6 col-sm-6 d-inline">
                        <div class="col-sm-6">
                            <label class="control-label">ENG (mois): </label>
                        </div>
                        <div class="col-sm-2">
                            <div class="" id="engProj<?php echo $s->idSite?>0"></div> 
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 d-inline">
                        <font color="red">
                            <div class="col-sm-3">
                                  <input hidden="hidden" id="spotProj<?php echo $s->idSite?>0" value="">
                                <label class="control-label">PRI : </label>
                            </div>
                            <div class="col-sm-3">
                                <div class="" id="priProj<?php echo $s->idSite?>0"></div>
                            </div>
                        </font>
                         <div class="col-sm-3">
                                 
                                <label class="control-label">PRI : </label>
                            </div>
                            <div class="col-sm-3">
                                <div class="" id="fmaProj<?php echo $s->idSite?>0"></div>
                            </div>
                    </div>
                </div>
                <label class="col-sm-3 control-label">PDT : </label>
                <div id="pdtProj<?php echo $s->idSite?>0"></div> 
            </div>
        </div>
    </article>
</div>
        
<?php
}
?>
    
    