<?php
/*$presta = $_POST['presta'];
$idProjet = $_POST['idProjet'];

if($presta == 0)$presta='ht';
$tot = $_POST['total'];
$ms = 0;
//echo "Presta : ".$presta;
//echo "total : ".$tot;
session_start();




for($i=0;$i < count($_SESSION['bob']);$i++){
    $tot +=$_SESSION['bob'][$i]["tarifsHT"];
    $ms += $_SESSION['bob'][$i]["MS"];
}*/
//print_r($this->session->all_userdata());
/*
Array
(
    [__ci_last_regenerate] => 1499935937
    [isadmin] => 1
    [username] => sebastien
    [iduser] => 2
    [totalDevis] => 19.9
    [totalMS] => 45
    [idContexte] => 1
    [projet] => 81
)

*/

if(ENVIRONMENT == 'development'){
    print_r($this->session->all_userdata());
}
?>
<div class="row">
    <article class="col-md-7">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10"><h2 style="text-align:center;">PROJET</h2></td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Total général abonnement mensuel HT</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1Total" id="Projet1Total" class="form-control input-sm" disabled="disabled" type="number" value="<?php echo number_format($total,2,".","")?>">
                    <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Les services supplémentaires</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1PrestaZen1Tarif" id="Projet1PrestaZen1Tarif" class="form-control input-sm" type="number" value="<?php echo number_format($presta,2,".","") ?>" onchange="majTotalProjet();">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Coût  maintenance mensuel HT</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1PrestaZen2Tarif" id="Projet1PrestaZen2Tarif" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalProjet();">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Loyer mensuel HT matériel téléphonique</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1PrestaZen3Tarif" id="Projet1PrestaZen3Tarif" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalProjet();"> <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Achat matériel téléphonique à prévoir HT</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input id="Projet1FMA" name="Projet1FMA" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalProjet();" value="<?php echo number_format($achat,2,".","") ?>">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
     <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                    <td class="col-sm-10">Location matériel téléphonique à prévoir HT ( € / Mois)</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1InvestMat" id="Projet1InvestMat" class="form-control input-sm"  disabled="disabled" type="number" placeholder="">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
       <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                    <td class="col-sm-10">Gestion de projet / Déploiement / Formation </td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1InvestMat" id="Projet1InvestMat" class="form-control input-sm"  disabled="disabled" type="number" placeholder="<?php echo number_format($forma,2,".","") ?>">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Options ADSL / SDSL / Fibre </td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1InvestMat" id="Projet1InvestMat" class="form-control input-sm"  disabled="disabled" type="number" placeholder="<?php echo number_format($optADSD,2,".","") ?>">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Frais d'activation de service   / Création de ligne……..</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1InvestMat" id="Projet1InvestMat" class="form-control input-sm"  disabled="disabled" type="number" placeholder="<?php echo number_format($ms,2,".","") ?>">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
     <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Fonctions à valeur ajoutée (€ / Mois)</td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input name="Projet1InvestMat" id="Projet1InvestMat" class="form-control input-sm"  disabled="disabled" type="number" placeholder="<?php echo number_format($fctVal,2,".","") ?>">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    
    <article class="col-md-9">
        <table class="table bg-projet1 table-condensed">
            <tr>
                <td class="col-sm-10">Remise commerciale HT</td>
                <td class="col-sm-2">
<div class="input-group">
<input name="Projet1InvestMat" id="Projet1RemCom" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalProjet();">
<div class="input-group-addon">€</div>
</div>
</td>
            </tr></table>
    </article>';
    <?php
    $total *= 12;
                               $total += $ms + $presta;
    ?>

    <article class="col-md-9">
        <table class="table bg-info table-condensed">
            <tr>
                <td class="col-sm-10"><h2>TOTAL  PROJET  ANNUEL HT 1ere ANNEE</h2></td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input type="number" id="totalProjet"  class="form-control input-sm" disabled="disabled" value="<?php echo number_format($total,2,".","") ?>"><div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="col-sm-10"><h2>TOTAL  PROJET  ANNEE SUIVANTE</h2></td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input type="number" id="totalProjetAS"  class="form-control input-sm" disabled="disabled" value="<?php number_format($total,2,".","") ?>"><div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>';

    <article class="col-md-9">
        <table class="table bg-info table-condensed">
            <tr>
                <td class="col-sm-6">Différence sur la durée totale du contrat</td>
                <td class="col-sm-2">
                    <select id="selDurEng" onchange="majTotalDuree();">
                        <option value="12">12 mois</option>
                        <option value="24">24 mois</option>
                        <option value="36">36 mois</option>
                        <option value="48">48 mois</option>
                        <option value="60">60 mois</option>
                    </select>
                </td>
                <td class="col-sm-1"><b>HT</b></td>
                <td class="col-sm-2">
                    <div class="input-group">
                        <input type="number" id="totalDuree"   class="form-control input-sm" disabled="disabled">
                        <div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr></table>
    </article>
    <article class="col-md-9">
        <table class="table table-condensed">
            <tr>
                <td class="col-sm-6"></td>
                <td class="col-sm-2"></td>
                <td class="col-sm-1 bg-info"><b>TTC</b></td>
                <td class="col-sm-2 bg-info">
                    <div class="input-group">
                        <input type="number" id="totalDureeTTC" class="form-control input-sm" disabled="disabled"><div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
</div>

<div class="col-md-12 PadTop"><hr></div>
<div class="row">
    <article class="col-md-4"></article>
    <article class="col-md-2">   
   <!-- <article class="col-md-5"><button class="btn btn-info" disabled="disabled" onclick="nextProject();">Ajouter un projet</button></article>-->
        <button id="page2" class="btn btn-success"  onclick="saveConclu(insertPage);">Finaliser le devis (Commentaire & Test)</button></article>
</div>
<div class="col-md-12 PadTop"><hr></div>