<article class="col-md-12"><h1>OPERATEUR MOBILE</h1></article>
<div class="row" style="margin-left:20px;">
    <div id="operateurMobile">
    <div class="col-md-2" style="margin:10px">
        <?php
        $data = array(
            'name'          => 'CBSFR',
            'id'            => 'CBSFR',
            'value'         => '5',
             'checked'       => TRUE,
            'disabled'      => 'disabled'
        );
        echo  form_radio($data);
        ?>
        <img src="<?php echo base_url("assets/img/operateur/sfr.jpg");?>" width="80" height="80" alt="Operateur SFR"/>
    </div>
    <div class="col-md-2" style="margin:10px">
        <?php
        $data = array(
            'name'          => 'CBSFR',
            'id'            => 'CBSFR',
            'value'         => '7',
           'disabled'      => 'disabled'
        );
        echo  form_radio($data);
        ?>
        <img src="<?php echo base_url("assets/img/operateur/coriolis.jpg");?>" width="80" height="80" alt="Operateur SFR"/>
    </div>
    <div class="col-md-2" style="margin:10px">
        <?php
        $data = array(
            'name'          => 'CBSFR',
            'id'            => 'CBSFR',
            'value'         => '1',
            'disabled'      => 'disabled'
        );
        echo  form_radio($data);
        ?>
        <img src="<?php echo base_url("assets/img/operateur/mb.jpg");?>" width="80" height="80" alt="Operateur SFR"/>
    </div>
</div>
</div>