
<div class="col-md-12 PadTop"><hr></div>
<!--CONCLUSION-->
<div class="row">
    <article class="col-md-11 bg-info"><h2 style="text-align:center;">CONCLUSION</h2></article>
</div>
<!--Conclusion contexte -->
<div class="row">
    <article class="col-md-7">
        <table class="table table-contexte table-condensed">
            <tr>
                <td class="col-sm-10"><h5 style="text-align:center;">CONTEXTE</h5></td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table table-contexte table-condensed">
            <tr>
                <td class="col-sm-10">TOTAL ABONNEMENT GÉNÉRAL MENSUEL HT CONTEXTE</td>
                <td class="col-sm-3">
                    <div class="input-group"><input type="number" step="0.01" id="totalContAbo" disabled="disabled" class="form-control input-sm" value="<?php echo number_format($totalCtxe,2,".","");//'.number_format($totalFixeMob,2,".","").'?>"><div class="input-group-addon">€</div>
                    </div>
                </td>
        </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table table-contexte table-condensed">
            <tr>
                <td class="col-sm-10">COUT MAINTENANCE MENSUEL HT</td>
                <td class="col-sm-2"><div class="input-group">
                    <input id="InvestMat" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalCont();"><div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr>
        </table>
    </article>
    <article class="col-md-9">
        <table class="table table-contexte table-condensed">
            <tr>
                <td class="col-sm-10">LOYER MENSUEL HT matériel téléphonique</td>
                <td class="col-sm-2"><div class="input-group"><input name="" id="LoyerMat" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalCont();"><div class="input-group-addon">€</div>
                    </div></td>
            </tr></table>
    </article>
    <article class="col-md-9">
        <table class="table table-contexte table-condensed">
            <tr>
                <td class="col-sm-10">ACHAT MATERIEL téléphonique à prévoir HT</td>
                <td class="col-sm-2"><div class="input-group"><input id="CoutMat" class="form-control input-sm" type="number" placeholder="ht" onchange="majTotalCont();"><div class="input-group-addon">€</div>
                    </div></td>
            </tr></table>
    </article>
</div>
    
   
        <!--Conclusion Total contexte -->
<div class="row">
    <article class="col-md-9">
        <table class="table bg-info table-condensed">
            <tr>
                <td class="col-sm-10"><h2>TOTAL CONTEXTE 1ere ANNEE  HT</h2></td>
  
                <td class="col-sm-3"><div class="input-group">
                    <?php
                    
                    $total = $totalCtxe * 12;
                   // echo $total;
                    //Probleme d'affichage des milliers
                   $total = number_format($total,2,".","");
                 //  echo $total;
                    ?>
                    <input type="number" id="totalCont" class="form-control input-sm" disabled="disabled" value="<?php echo $total; ?>"><div class="input-group-addon">€</div>
                    </div></td>
            </tr></table></article>
</div>
      <!--Conclusion Total contexte -->
<div class="row">
    <article class="col-md-9">
        <table class="table bg-info table-condensed">
            <tr>
                <td class="col-sm-10"><h2>TOTAL CONTEXTE ANNEE SUIVANTE  HT</h2></td>
  
                <td class="col-sm-2">
                    <div class="input-group">
                        <input type="number" id="totalContAS" class="form-control input-sm" disabled="disabled" value="<?php echo $total; ?>"><div class="input-group-addon">€</div>
                    </div>
                </td>
            </tr></table></article>
</div>
<div class="col-md-12 PadTop"><hr></div>
<div class="row"><article class="col-md-5"></article><article class="col-md-2" id="buttonValidConclu">
    <button class="btn btn-info" id="validePart1" onclick="afficheConclu(insertConclu)">Valider Projet</button></article></div>
<div class="col-md-12 PadTop"><hr></div>