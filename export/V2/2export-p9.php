<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div id="Titre"><h2 class="titre colorB" style="font-size:24pt">D&eacute;tail de offres multiples</h2></div>
<div id="SousTitre"></div> <!-- permet de passer en dessous de la div titre -->

<div id="Package"><!-- le détail de chaque package -->
<table>
<thead><!-- si le tableau fait plus de 19 lignes le thead  est répété dans la page suivante avant la suite du tbody -->
  <tr>
    <th>1 - 71000 MÂCON</th> <!-- le numéro et nom du site  -->
   </tr>
</thead>
<tbody>
   <tr> 
    <td>PBE</td><!-- le nom du package -->
   </tr>
      <tr> 
    <td>PBE DSL/ THD : pour vos sites principaux</td><!-- les différentes options du package -->
   </tr>
   <tr>
    <td>Ligne bureau Adsolu</td>
</tr>
<tr>
    <td>Microsoft Lync</td>
</tr>   
<tr>
    <td>Ligne Fax</td>
</tr>   
<tr>
    <td>Poste Polycom vvx310</td>
</tr>   
<tr>
    <td>OOOOOOOOOO OOOOOOOOO OOOOOOOOO...</td><!-- 30 caractères Max + les 3 points -->
</tr>
<tr>
    <td>8</td>
</tr>
<tr>
    <td>9</td>
</tr>
<tr>
    <td>10</td>
</tr>
<tr>
    <td>11</td>
</tr>
<tr>
    <td>12</td>
</tr>
<tr>
    <td>13</td>
</tr>
<tr>
    <td>14</td>
</tr>
<tr>
    <td>15</td>
</tr>
<tr>
    <td>16</td>
</tr>
<tr>
    <td>17</td>
</tr>
<tr>
    <td>18</td>
</tr>

</tbody>
</table>
</div>


<div id="Package">
<table>
<thead>
  <tr>
    <th>2 - 71000 MÂCON</th>
   </tr> 
</thead>
<tbody>
<tr> 
    <td>PBE</td><!-- le nom du package -->
   </tr>
      <tr> 
    <td>PBE DSL/ THD : pour vos sites principaux</td><!-- les différentes options du package -->
   </tr>
</tbody>  
</table>
</div>


<div id="Package">
<table>
<thead>
  <tr>
    <th>3 - 71000 MÂCON</th>
   </tr> 
</thead>
<tbody>
<tr> 
    <td>PBE</td><!-- le nom du package -->
   </tr>
      <tr> 
    <td>PBE DSL/ THD : pour vos sites principaux</td><!-- les différentes options du package -->
   </tr>
</tbody>  
</table>
</div>


<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	// Header & Footer
	$pdf->SetHTMLFooter('
<table class="FooterTable"><tr>
<td class="Demi"> </td>
<td class="Demi"><img src="images/LogoSociete.png" class="logoBP" /> &nbsp; {PAGENO}/{nbpg} </td>
</tr></table>');
	
	// Export pdf
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>