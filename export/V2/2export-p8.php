<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div id="Titre"><h2 class="titre colorB" style="font-size:24pt">&Eacute;tude et analyse technique de votre ligne ADSL / Fibre</h2></div>
<div id="SousTitre"></div> <!-- permet de passer en dessous de la div titre -->

<div id="Test"><p>ADSL / Fibre Optique mutualisée ou dédiée / SDSL1, 2, 3, 4 méga 2 paies ou 4 paires ? <br><!-- texte d'introduction-->
	«Les connexions haut débit» utilisent la technologie ADSL. Ainsi , les possibilités qui vous sont offertes en terme de débits et de services sont
	dépendantes des équipements des opérateurs présents dans le central téléphonique dont vous dépendez ainsi que de la longueur et la qualité de votre 
	ligne téléphonique.</p>
</div>

<div id="Test" class="L100" style="float:left"><!-- donées récoltées sur le test -->
<p>Central téléphonique : TRECLUN-UCBI8 <br>
longueur de ligne : 2618 mètres <br>
affaiblissement théorique : 30.3024dB <br>
état de la ligne : ACTIVE <br>
débit maximum estimé ADSL : 11.68 Mbps <br>
débit maximum estimé VDSL : incompatible</p>
</div>

<div id="Test" style="float:left"><!-- donées récoltées sur le test -->
<p>Résultats pour le numéro de tête de ligne 0380390558 (jeudi 11 août 2016)<br>
Adresse retournée : RUE SALLE SAINT MARTIN - CHAMPDOTRE (21)<br>
Nom précécesseur : KUTTER PATRICIA<br>
présent en : CEGETEL opt(3), NEUF TELECOM opt(1)<br>
Longueur(s) et calibre(s) 1900m (0,6), 710</p>
</div>

<hr>
<table id="Test" class="margeT30"><!-- donées récomtées sur le test -->
<tr>
    <td class="L35">&nbsp;</td>
	<td>SDSL 1M 1p/2p</td>
    <td>SDSL 2M 1p/2p</td>
    <td>SDSL 2M EFM 1p/2p (2p)</td>
    <td>SDSL 4M 1p/2p (2p)</td>
    <td>SDSL 4M EFM 1p/2p (2p)</td>
    <td>SDSL 8M 1p/2P (2p)</td>
  </tr>
<tr>
    <td><strong>SDSL 1P/2P</strong></td>
	<td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-4.png" width="80" height="10" alt=""/></td>
  </tr>
<tr>
    <td>&nbsp;</td>
	<td>SDSL 1M 1p/2p</td>
    <td>SDSL 2M 1p/2p</td>
    <td>SDSL 2M EFM 1p/2p (2p)</td>
    <td>SDSL 4M 1p/2p (2p)</td>
    <td>SDSL 4M EFM 1p/2p (2p)</td>
    <td>SDSL 8M 1p/2P (2p)</td>
  </tr>
<tr>
    <td><strong>SDSL 4P</strong></td>
	<td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-1.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-4.png" width="80" height="10" alt=""/></td>
    <td><img src="images/test-4.png" width="80" height="10" alt=""/></td>
    <td>&nbsp;</td>
  </tr>
</table>
<hr>
<table id="TestLegende" class="margeT30"><!-- légende des couleurs -->
<tr>
    <td><img src="images/test-1.png" width="10" height="10"/></td>
	<td>Éligible</td>
    <td><img src="images/test-2.png" width="10" height="10"/></td>
    <td>DSLAM full-Désat entre 2 et 6 mois</td>
    <td><img src="images/test-3.png" width="10" height="10"/></td>
    <td>Étude nécessaire</td>
    <td><img src="images/test-4.png" width="10" height="10"/></td>
	<td>Inéligible</td>
    <td><img src="images/test-5.png" width="10" height="10"/></td>
    <td>NDI Inconnu</td>
    <td><img src="images/test-6.png" width="10" height="10"/></td>
    <td>Non disponible</td>
    <td><img src="images/test-7.png" width="10" height="10"/></td>
    <td>Éligibilité interrompue</td>
  </tr>
</table>
  </td></tr>
</table>

<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	// Header & Footer
	$pdf->SetHTMLFooter('
<table class="FooterTable"><tr>
<td class="Demi"> </td>
<td class="Demi"><img src="images/LogoSociete.png" class="logoBP" /> &nbsp; {PAGENO}/{nbpg} </td>
</tr></table>');
	
	// Export pdf
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>