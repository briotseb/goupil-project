<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div id="Titre"><h2 class="titre colorB">Synthèse financière</h2></div>
<div id="SousTitre"></div> <!-- permet de passer en dessous de la div titre -->


<div id="CAP">
<table>
  <tr>
  <th><img src="images/CAP-bateau.jpg"/></th>
  </tr>
  <tr>
  <th><p>Prestation CAP<br>
  		FORFAIT<br><br>
        499,00 €</p></th>
  </tr>
  <tr>
  <td><img src="images/CAP-slogan.png" /></td>
  </tr>
</table>
</div>


<div id="contexteConclusion">
<table>
<thead>
  <tr>
    <th colspan="2" class="text-left">SITUATION ACTUELLE</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="L140">Total abonnement général HT/an</td>
    <td class="L50 text-right">1456,62 €</td>
  </tr>
  <tr>
    <td>Coût maintenance HT/an</td>
    <td class="text-right">1200,00 €</td>
  </tr>
  </tbody>
  <tfoot>
  <tr>
    <th class="text-left">Total prévision 1<sup>ere</sup> année HT/an</th>
    <th class="text-right">18679,44 €</th>
  </tr>
  <tr>
    <th class="text-left">Total prévision année suivantes HT/an</th>
    <th class="text-right">17730,12 €</th>
  </tr>
  </tfoot> 
</table>
</div>


<div id="projet1Conclusion" class="margeT30">
<table>
  <thead>
  <tr>
    <th colspan="2" class="text-left">PROJET 1</th>
  </tr>
  </thead>
  <tbody>
 <tr>
    <td class="L140">Total abonnement général HT/an</td>
    <td class="L50 text-right">1456,62 €</td>
  </tr>
  <tr>
    <td>Prestation CAP</td>
    <td class="text-right">499,00 €</td>
  </tr>
  <tr>
    <td>Terminaux téléphonique</td>
    <td class="text-right">250,00 €</td>
  </tr>
  </tbody>
  <tfoot>
  <tr>
    <th class="text-left">Total général</th>
    <th class="text-right">239.40 €</th>
  </tr>
  <tr>
    <td>Remise commerciale</td>
    <td class="text-right">150,00 €</td>
  </tr>
  <tr>
    <th class="text-left">Economie sur le durée du contrat (36 mois)</th>
    <th class="text-right">250,00 €</th>
  </tr>
  </tfoot>  
</table>    
</div>


<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	// Header & Footer
	$pdf->SetHTMLFooter('
<table class="FooterTable"><tr>
<td class="Demi"> </td>
<td class="Demi"><img src="images/LogoSociete.png" class="logoBP" /> &nbsp; {PAGENO}/{nbpg} </td>
</tr></table>');
	
	// Export pdf
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>