<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div id="Titre"><h2 class="titre colorB">Axes d'optimisation de vos objectifs</h2></div>
<div id="SousTitre"></div>

<table>
<tr >
    <td>
    <h3 class="titre">Lors de notre entretien, vous nous avez demandé :</h3></td>
</tr>
</table>

<table class="marge margeT30">
<tr >
    <td class="BgCercleR Tier text-center" style="height:54mm"><h5 class="colorB">Simplicité</h5></td>
    <td class="BgCercleV Tier text-center "><h5 class="colorB">&Eacute;conomies</h5></td>
    <td class="BgCercleJ Tier text-center"><h5 class="colorB">Interlocuteur unique</h5></td>
  </tr>
</table>
<table class="marge margeT30">
  <tr>
    <td>
    <p>Le principal moustique vecteur du paludisme en Afrique subsaharienne est l’anophèle Anopheles gambiae. 
    Ses femelles repèrent les personnes qu’elles piquent la nuit grâce à leur odorat, même si des indices visuels et thermiques interviennent aussi. 
    L’équipe de John Carlson, à l’Université Yale, et celle de Laurence Zwiebel, à l'Université Vanderbilt de Nashville, ont analysé le fonctionnement des récepteurs olfactifs en jeu. 
    Elle montre que certains d'entre eux sont particulièrement sensibles à des composants de l’odeur humaine.</p></td>
  </tr>
</table>

<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	// Header & Footer
	$pdf->SetHTMLFooter('
<table class="FooterTable"><tr>
<td class="Demi"> </td>
<td class="Demi"><img src="images/LogoSociete.png" class="logoBP" /> &nbsp; {PAGENO}/{nbpg} </td>
</tr></table>');
	
	// Export pdf
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>