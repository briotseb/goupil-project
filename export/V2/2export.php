<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div class="couv1  text-center">
<img src="images/1133.png" alt="1133.png" class="logoCouv"/>
</div>


<div class="CouvTitre">
	<h1>Audit télécom</h1>
    <h2 class="titre">Optimisons vos solutions télécom !</h2>
</div>    
 

<div class="CouvInfo text-left">
	<p><img src="images/ZeroPapier.png" alt="1133.png" width="64" height="110" /> <strong>Fait le 29/06/2017</strong></p>
</div>
<div class="CouvClient text-right">
	<h2> HANDISERTION</h2>
    <p> 1000 avenue Maréchal de Lattre de Tassigny <br>
    71000 Mâcon</p>
</div>





<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>