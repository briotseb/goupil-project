<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div class="couv4  text-center">

</div>
<!-- Table à répéter en haut de chaque page -->

<div id="Couv4">
	<h1>N'attendez pas demain <br>
    pour adopter la technologie du futur</h1>
    <h2>Bénéficiez d'une palette complète d'offres internet Haut débit et Très Haut débit !</h2>
</div>    
 

<div class="Commercial">
   <p class="Commercialtexte"><img src="images/LogoSociete.png"/ class="Commercial"><br>
   Votre interlocuteur commercial</p>
   <p class="CommercialInfo">M. Charet Frédéric<br>
   <a href="mailto:frederic.charet@futur.fr">frederic.charet@futur.fr</a></p>
</div>


<div class="Slogan">
   <h3 class="Sloganh3">L'équipe client <br>de Globacom <br>à Besançon</h3>
   <h1 class="Sloganh1">03 81 41 79 89</h1>
   <p class="Sloganp">du lundi au vendredi de 7h30 à 19h<br>
   et le samedi de 8h à 12h</p>
</div>


<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>