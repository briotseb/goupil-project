<?php
ob_start();
?>
<link href="exportmpdf.css" rel="stylesheet" type="text/css">

<div id="Titre"><h2 class="titre colorB">Axes d'optimisation des prestataires</h2></div>
<div id="SousTitre"></div>

<table>
<tr >
    <td>
    <h3 class="titre">Nous avons sélectionné pour vous les prestataires suivants :</h3></td>
</tr>
</table>

<!--A reproduire pour chaque prestataire <div id="Prestataire"> -->
<div id="Prestataire">
<table class="margeT30">
<tr >
    <td width="60mm"><img src="images/receptel.png" class="Prestataire"/></td>
    <td class="70mm"><p class="Prestataire">Le principal moustique vecteur du paludisme en Afrique subsaharienne est l’anophèle Anopheles gambiae. 
    Ses femelles repèrent les personnes qu’elles piquent la nuit grâce à leur odorat.</p></td>
  </tr>
</table>
</div>



<?php
$content = ob_get_clean();
include("mpdf60/mpdf.php");
try {
	$pdf=new mPDF('utf-8', 'A4-L');
	$pdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('exportmpdf.css');
	$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	// Langue & caractere
	$pdf->SHYlang = 'fr';
	$pdf->SHYleftmin = 3; // nombre de caractere avant la césure
	
	
	// Header & Footer
	$pdf->SetHTMLFooter('
<table class="FooterTable"><tr>
<td class="Demi"> </td>
<td class="Demi"><img src="images/LogoSociete.png" class="logoBP" /> &nbsp; {PAGENO}/{nbpg} </td>
</tr></table>');
	
	// Export pdf
	$pdf->WriteHTML($content);
	$pdf->Output();
/*exit;*/
}
catch(mPDF_exception $e){
		die($e);
		}
?>